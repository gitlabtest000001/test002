import React,  { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {Redirect} from "react-router-dom";
import axios from 'axios';
import {AppConfig} from "../appConfig";
import Loading from "../components/Loading";
import {useDispatch, useSelector} from "react-redux";
import { GetMe, GetMeManager, LoadDataAction, userLogout } from '../containers/Member/Auth/actions';
import { adminLogout } from '../containers/Admin/Auth/actions';
//import {LoadDataAction, Logout} from "../../containers/Auth/actions";

userGuard.propTypes = {
    children: PropTypes.any,
};

export function userGuard({children}) {
    const dispatch = useDispatch();
    const [isLogin, setIsLogin] = useState(false);
    const [isLoading, setLoading] = useState(true);
    const Token = localStorage.getItem('dvg_user_token');

    if (!Token || Token === '') {
        return <Redirect to='/dang-nhap' />;
    }

    useEffect(() => {
        if (Token) {
            async function checkLogin() {
                if (Token) {
                    const configManager = {
                        method: 'get',
                        url: `${AppConfig.apiUrl}/auth/customer/me`,
                        headers: {Authorization: `Bearer ${Token}`}
                    };

                    const user = await axios(configManager);

                    if (user.status === 200) {
                        dispatch(GetMe(Token));
                        setIsLogin(true);
                        setLoading(false);
                    } else {
                        dispatch(userLogout());
                    }

                }
            }
            checkLogin();
        }

    },[dispatch])

    if (isLoading) {
        return <Loading />;
    }

    if (isLogin) {
        return children;
    }

    return <Redirect to='/dang-nhap' />;
}

export function adminGuard({ children }) {
    const dispatch = useDispatch();
    const [isLogin, setIsLogin] = useState(false);
    const [isLoading, setLoading] = useState(true);
    const Token = localStorage.getItem('dvg_manager_token');

    if (!Token || Token === '') {
        return <Redirect to='/admin/login' />;
    }

    useEffect(() => {
        if (Token) {
            async function checkLogin() {
                if (Token) {
                    const configManager = {
                        method: 'get',
                        url: `${AppConfig.apiUrl}/auth/manager/me`,
                        headers: {Authorization: `Bearer ${Token}`}
                    };

                    const user = await axios(configManager);

                    if (user.status === 200) {
                        dispatch(GetMeManager(Token));
                        setIsLogin(true);
                        setLoading(false);
                    } else {
                        dispatch(adminLogout());
                    }

                }
            }
            checkLogin();
        }

    },[dispatch])

    if (isLoading) {
        return <Loading />;
    }

    if (isLogin) {
        return children;
    }

    return <Redirect to='/admin/login' />;
}
