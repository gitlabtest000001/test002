import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Redirect } from 'react-router-dom';
import axios from 'axios';
import {useDispatch, useSelector} from "react-redux";
import {AppConfig} from "../appConfig";
import Loading from "../components/Loading";
import { userLogout } from '../containers/Member/Auth/actions';
import { adminLogout } from '../containers/Admin/Auth/actions';

userGuest.propTypes = {
    children: PropTypes.any,
}

export function userGuest({children}) {
    const dispatch = useDispatch();
    const [isLogin, setIsLogin] = useState(false);
    const [isLoading, setLoading] = useState(true);
    const userToken = localStorage.getItem('dvg_user_token');
    const user = useSelector(state => state.root.currentUser.user);

    if (user) {
        if (typeof user.status !== 'undefined' && user.status === 200) {
            return <Redirect to="/taikhoan"/>;
        }
    }

    if (!userToken || userToken === '') {
        return children;
    }

    useEffect(() => {
        if (userToken) {
            async function getUser() {
                await axios({
                    method: 'get',
                    url: `${AppConfig.apiUrl}/auth/customer/me`,
                    headers: {Authorization: `Bearer ${userToken}`}
                }).then(function (response) {
                    if (response.status === 200) {
                        setIsLogin(true);
                        setLoading(false);
                    }
                }).catch(function (error) {
                    dispatch(userLogout())
                })
            }

            getUser();
        }

    }, [dispatch]);

    if (isLoading) {
        return <Loading/>;
    }

    if (isLogin) {
        return <Redirect to="/taikhoan"/>;
    }

    return children;
}

adminGuest.propTypes = {
    children: PropTypes.any,
}

export function adminGuest({children}) {
    const dispatch = useDispatch();
    const [isLogin, setIsLogin] = useState(false);
    const [isLoading, setLoading] = useState(true);
    const userToken = localStorage.getItem('dvg_manager_token');
    const user = useSelector(state => state.root.AdminAuth.user);

    if (user) {
        if (typeof user.status !== 'undefined' && user.status === 200) {
            return <Redirect to="/admin/dashboard"/>;
        }
    }

    if (!userToken || userToken === '') {
        return children;
    }

    useEffect(() => {
        if (userToken) {
            async function getUser() {
                await axios({
                    method: 'get',
                    url: `${AppConfig.apiUrl}/auth/manager/me`,
                    headers: {Authorization: `Bearer ${userToken}`}
                }).then(function (response) {
                    if (response.status === 200) {
                        setIsLogin(true);
                        setLoading(false);
                    }
                }).catch(function (error) {
                    dispatch(adminLogout())
                })
            }

            getUser();
        }

    }, [dispatch]);

    if (isLoading) {
        return <Loading/>;
    }

    if (isLogin) {
        return <Redirect to="/admin/dashboard"/>;
    }

    return children;
}
