import React from 'react';
import PropTypes from 'prop-types';
import {Card} from 'antd';
import Chart from 'react-apexcharts';
import {formatDate, formatVND} from "../utils/helpers";

LineChart.propTypes = {
    dateList: PropTypes.array,
    series: PropTypes.array,
    title: PropTypes.string,
};

function LineChart(props) {
    const {dateList, series, title} = props;

    const chart = {
        options: {
            chart: {
                background: "#fff",
                stacked: false,
                toolbar: {
                    show: false
                },
                zoom: false
            },
            colors: ['#1f87e6', '#ff5c7c', '#077857'],
            dataLabels: {
                enabled: false
            },
            grid: {
                borderColor: "#dedede",
                yaxis: {
                    lines: {
                        show: false
                    }
                }
            },
            legend: {
                show: true,
                position: 'top',
                horizontalAlign: 'right',
                labels: {
                    colors: "#5e5e5e"
                }
            },
            markers: {
                size: 4,
                strokeColors: ['#1f87e6', '#27c6db'],
                strokeWidth: 0,
                shape: 'circle',
                radius: 2,
                hover: {
                    size: undefined,
                    sizeOffset: 2
                }
            },
            stroke: {
                width: 3,
                curve: 'smooth',
                lineCap: 'butt',
                dashArray: [0, 3]
            },
            theme: {
                mode: 'light'
            },
            tooltip: {
                theme: 'dark'
            },
            xaxis: {
                axisBorder: {
                    color: "#f9f9f9"
                },
                axisTicks: {
                    show: true,
                    color: "#f9f9f9"
                },
                categories: dateList,
                labels: {
                    style: {
                        colors: "#383838"
                    },
                    formatter: function (value) {
                        return formatDate(value);
                    }
                }
            },
            yaxis: [
                {
                    axisBorder: {
                        show: true,
                        color: "#f9f9f9"
                    },
                    axisTicks: {
                        show: true,
                        color: "#f9f9f9"
                    },
                    labels: {
                        style: {
                            colors: "#43a1d0"
                        },
                        formatter: function (value) {
                            return formatVND(value);
                        }
                    }
                },
                {
                    axisTicks: {
                        show: true,
                        color: "#f9f9f9"
                    },
                    axisBorder: {
                        show: true,
                        color: "#f9f9f9"
                    },
                    labels: {
                        style: {
                            colors: "#ff5c7c"
                        },
                        formatter: function (value) {
                            return formatVND(value);
                        }
                    },
                    opposite: true
                },
                {
                    axisTicks: {
                        show: true,
                        color: "#f9f9f9"
                    },
                    axisBorder: {
                        show: true,
                        color: "#f9f9f9"
                    },
                    labels: {
                        style: {
                            colors: "#077857"
                        },
                        formatter: function (value) {
                            return formatVND(value);
                        }
                    },
                    opposite: true
                },
            ]
        },
        series: series
    };
    return (
        <Card title={title} size="small">
            <Chart
                type="line"
                height="400"
                {...chart}
            />
        </Card>

    );
}

export default LineChart;
