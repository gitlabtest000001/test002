import React from 'react';
import history from '../utils/history';
import { AppConfig } from '../appConfig';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';

function ProductWidgetItem(props) {
    return (
        <Row
            className={"mb-3 border-b border-gray-200 pb-3"}
            onClick={() => history.push(`/san-pham/${props.item.slug}`)}
            gutter={[16, 16]}
        >
            <Col xs={24} lg={8} md={8}>
                <img
                    src={AppConfig.apiUrl+props.item.featureImage} alt={props.item.title}
                    className={"w-full h-18 object-cover rounded-md"}
                />
            </Col>
            <Col xs={24} lg={16} md={16}>
                <h3 className={"font-medium"}>{props.item.name}</h3>
            </Col>
        </Row>
    );
}

export default ProductWidgetItem;
