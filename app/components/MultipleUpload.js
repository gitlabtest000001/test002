import React, {useState} from 'react';
import LoadingOutlined from "@ant-design/icons/lib/icons/LoadingOutlined";
import PlusOutlined from "@ant-design/icons/lib/icons/PlusOutlined";
import Upload from "antd/es/upload";
import {AppConfig} from "../appConfig";
import {message, Tooltip} from "antd";
import axios from "axios";
import {useDispatch} from "react-redux";
import {hideLoader, showLoader} from "../containers/App/actions";

MultipleUpload.propTypes = {

};

MultipleUpload.defaultProps = {
    uploadLabel: 'Chọn ảnh',
}

function MultipleUpload({uploadLabel, currentList, onChooseMedia}) {
    const dispatch = useDispatch()
    const adminToken = localStorage.getItem('dvg_manager_token');
    const [loading, setLoading] = useState(false);
    const [fileList, setFileList] = useState(currentList ? currentList : []);

    const onChange = info => {
        dispatch(showLoader())
        if (info.file.status === 'uploading') {
            setLoading(true);
            info.file.status = 'done'
        }
        if (info.file.status === 'done') {
            //message.success(`${info.file.name} tải lên thành công`);
            console.log(info.fileList);
            let newFileList = info.fileList.map(file => {
                if (file.response) {
                    file = {
                        id: file.response[0].id,
                        name: file.response[0].name,
                        url: `${AppConfig.apiUrl}${file.response[0].url}`,
                        uid: file.uid
                    }
                }
                return file;
            });
            setFileList(newFileList)
            onChooseMedia(newFileList)
            setLoading(false);
            dispatch(hideLoader())
        } else if (info.file.status === 'error') {
            message.error(info.file.response.message);
            setLoading(false);
            dispatch(hideLoader())
        }
    }

    function handleRemove(info){
        dispatch(showLoader())
        const newList = fileList.filter((file) => file.id !== info.id);
        if (newList) {
            setFileList(newList);
            onChooseMedia(newList);
        }
        axios.delete(
            `${AppConfig.apiUrl}/media/${info.id}`,
            {headers: {Authorization: `Bearer ${adminToken}`},}
        ).then(function (response) {
            dispatch(hideLoader())
            message.success(`Đã xoá ${info.name}`)
        }).catch(function (error) {
            dispatch(hideLoader())
        })
    }

    const uploadButton = (
        <div>
            {loading ? <LoadingOutlined /> : <PlusOutlined />}
            <div style={{ marginTop: 8 }}>{uploadLabel}</div>
        </div>
    );

    return (
        <Upload
            name="files"
            listType="picture-card"
            className="avatar-uploader"
            action={`${AppConfig.apiUrl}/media/upload`}
            headers={{Authorization: `Bearer ${adminToken}`}}
            onChange={onChange}
            onRemove={handleRemove}
            multiple
            fileList={fileList}
        >
            {uploadButton}
        </Upload>
    );
}

export default MultipleUpload;
