import React from 'react';
import { AppConfig } from '../appConfig';

function FeedbackItem(props) {
    return (
        <div className={"border border-gray-300 rounded-lg p-5 text-center"}>
            <img src={AppConfig.apiUrl+props.item.avatar} className={"mb-5 rounded-md w-20 h-20 object-cover"} alt={props.item.name}/>
            <h4 className={"font-medium text-lg mb-3"}>{props.item.name}</h4>
            <p className={"text-gray-500 text-xs mb-3"}>{props.item.job}</p>
            <p>{props.item.content}</p>
        </div>
    );
}

export default FeedbackItem;
