import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Slider from '@ant-design/react-slick';
import { IoArrowBack, IoArrowForward } from 'react-icons/io5';
import { AppConfig } from '../appConfig';

SliderGallery.propTypes = {

};

function SliderGallery(props) {

    function PrevArrow(props) {
        const { onClick } = props;
        return (
            <div
                onClick={onClick}
                className={"rounded-full w-10 h-10 bg-white shadow hover:shadow-lg items-center justify-center flex absolute top-1/3 md:top-1/2 z-10 left-1 cursor-pointer"}
            >
                <IoArrowBack size={"20"}/>
            </div>
        );
    }

    function NextArrow(props) {
        const { onClick } = props;
        return (
            <div
                onClick={onClick}
                className={"rounded-full w-10 h-10 bg-white shadow hover:shadow-lg items-center justify-center flex absolute top-1/3 md:top-1/2 z-5 right-1 cursor-pointer"}
            >
                <IoArrowForward size={"20"}/>
            </div>
        );
    }
    const settings = {
        autoPlay: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: <PrevArrow />,
        nextArrow: <NextArrow />,
        swipeToSlide: true,
        infinite: true,
    };

    return (
        <div>
            <Slider
                {...settings}
            >
                {props.gallery.map((item, index) => (
                    <a href={item.link} target={'_blank'}>
                        <img src={AppConfig.apiUrl+item.image} alt={item.title} className={"w-full object-cover rounded-xl"}/>
                    </a>
                ))}
            </Slider>
        </div>
    );
}

export default SliderGallery;
