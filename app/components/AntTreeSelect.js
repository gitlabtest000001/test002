import React from 'react'
import { Tree } from 'antd'
import TreeSelect from "antd/es/tree-select";
const { TreeNode } = Tree

const AntTreeSelect = ({ hierarchy, onChange, parent, onClear, ...custom }) => {
    const renderTreeNodes = data =>
        data.map(item => {
            if (item.children) {
                return (
                    <TreeNode title={item.name} value={item.id} key={item.id} dataRef={item}>
                        {renderTreeNodes(item.children)}
                    </TreeNode>
                )
            }
            return <TreeNode key={item.id} {...item} />
        })

    return (
        <TreeSelect
            showSearch
            value={parent}
            style={{ width: '100%' }}
            dropdownStyle={{ maxHeight: 400, overflow: 'auto' }}
            placeholder="Không có"
            allowClear
            onClear={onClear}
            treeDefaultExpandAll
            onChange={onChange}
            filterTreeNode={(search, item) => {
                return item.title.toLowerCase().indexOf(search.toLowerCase()) >= 0;
            }}
            {...custom}
        >
            {renderTreeNodes(hierarchy)}
        </TreeSelect>
    )
}

export default AntTreeSelect
