import React from 'react';
import PropTypes from 'prop-types';
import Button from 'antd/es/button';
import { IoArrowBack, IoArrowForward } from 'react-icons/io5';
import Slider from '@ant-design/react-slick';
import history from '../utils/history';
import FeedbackItem from './FeedbackItem';

FeedbackHorizontalList.propTypes = {

};

function FeedbackHorizontalList(props) {

    function PrevArrow(props) {
        const { onClick } = props;
        return (
            <div
                onClick={onClick}
                className={`rounded-full w-8 h-8 md:w-10 md:h-10 bg-white shadow hover:shadow-lg items-center justify-center flex absolute top-1/3 -left-3 md:-left-5 cursor-pointer`}
                style={{zIndex: 2}}
            >
                <IoArrowBack size={"20"}/>
            </div>
        );
    }

    function NextArrow(props) {
        const { onClick } = props;
        return (
            <div
                onClick={onClick}
                className={`rounded-full w-8 h-8 md:w-10 md:h-10 bg-white shadow hover:shadow-lg items-center justify-center flex absolute top-1/3 -right-3 md:-right-5  cursor-pointer`}
                style={{zIndex: 2}}
            >
                <IoArrowForward size={"20"}/>
            </div>
        );
    }

    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        rows: 1,
        autoplay: true,
        prevArrow: <PrevArrow />,
        nextArrow: <NextArrow />,
        adaptiveHeight: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    initialSlide: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    initialSlide: 1
                }
            }
        ]
    };

    return (
        <div className={"max-w-6xl mx-auto z-20"}>
            <div>
                <Slider className={"slider-margin"} {...settings}>
                    {props.items && props.items.map((item, index) => (
                        <FeedbackItem item={item} />
                    ))}
                </Slider>
            </div>
        </div>
    );
}

export default FeedbackHorizontalList;
