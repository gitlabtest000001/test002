import React, {useState} from 'react';
import LoadingOutlined from "@ant-design/icons/lib/icons/LoadingOutlined";
import PlusOutlined from "@ant-design/icons/lib/icons/PlusOutlined";
import Upload from "antd/es/upload";
import {AppConfig} from "../appConfig";
import {message, Tooltip} from "antd";
import axios from "axios";
import {useDispatch} from "react-redux";
import {hideLoader, showLoader} from "../containers/App/actions";

ThumbnailUpload.propTypes = {

};

ThumbnailUpload.defaultProps = {
    uploadLabel: 'Chọn ảnh',
    currentImage: null
}

function ThumbnailUpload({uploadLabel, currentImage, onChooseMedia}) {
    const dispatch = useDispatch();
    const adminToken = localStorage.getItem('dvg_manager_token');
    const [loading, setLoading] = useState(false);
    const [imageUrl, setImageUrl] = useState(currentImage && currentImage);

    function handleRemove(info){
        dispatch(showLoader())
        onChooseMedia(null);
        axios.delete(
            `${AppConfig.apiUrl}/media/${info.response[0].id}`,
            {headers: {Authorization: `Bearer ${adminToken}`},}
        ).then(function (response) {
            dispatch(hideLoader())
            message.success(`Đã xoá ảnh`)
        }).catch(function (error) {
            dispatch(hideLoader())
        })
    }

    const handleChange = info => {
        console.log(info);
        dispatch(showLoader())
        if (info.file.status === 'error') {
            message.error(info.file.response.message);
            setLoading(false);
            dispatch(hideLoader())
            return;
        }
        if (info.file.status === 'done') {
            message.success(`${info.file.name} tải lên thành công`);
            let file = info.file;
            if (file.response) {
                file = `${AppConfig.apiUrl}${file.response[0].thumb_url}`
                onChooseMedia(file)
                setTimeout(() => {
                    setImageUrl(file)
                }, 500)
                dispatch(hideLoader())
            }
        }
    };

    const uploadButton = (
        <div>
            <PlusOutlined />
            <div style={{ marginTop: 8 }}>{uploadLabel}</div>
        </div>
    );

    return (
        <Upload
            name="files"
            listType="picture-card"
            showUploadList={false}
            className="avatar-uploader"
            action={`${AppConfig.apiUrl}/media/upload`}
            headers={{Authorization: `Bearer ${adminToken}`}}
            onChange={handleChange}
            onRemove={handleRemove}
        >
            {imageUrl ? <img src={imageUrl} style={{ width: '100%', height: '100px', objectFit: "cover" }} />
                : uploadButton}
        </Upload>
    );
}

export default ThumbnailUpload;
