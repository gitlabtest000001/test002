import React from 'react';
import PropTypes from 'prop-types';
import Button from 'antd/es/button';
import { IoAddOutline, IoRemoveOutline } from 'react-icons/io5';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/Col';

InputCustomerQuantityHorizontal.propTypes = {

};

function InputCustomerQuantityHorizontal({
    elder,
    adult,
    child,
    baby,
    onSetAdult,
    onSetElder,
    onSetChild,
    onSetBaby,
}) {
    return (
        <div>
            <Row gutter={[20, 10]}>
                <Col xs={24} lg={elder >=0 ? 12 : 8} md={elder >=0 ? 12 : 8}>
                    <div className={"flex flex-row items-center justify-between bg-gray-50 border border-gray-200 rounded-full"}>
                        <Button
                            shape={"circle"}
                            size={"small"}
                            icon={<IoRemoveOutline />}
                            disabled={adult === 1}
                            onClick={() => {
                                onSetAdult(adult - 1);
                            }}
                        />
                        <div className={"mx-1"}>
                            <span className={"font-medium"}>{adult}</span> <span className={"text-gray-500"}>người lớn</span>
                        </div>
                        <Button
                            shape={"circle"}
                            icon={<IoAddOutline />}
                            size={"small"}
                            onClick={() => {
                                onSetAdult(adult + 1);
                            }}
                        />
                    </div>
                </Col>
                <Col xs={24} lg={elder >=0 ? 12 : 8} md={elder >=0 ? 12 : 8}>
                    <div className={"flex flex-row items-center justify-between bg-gray-50 border border-gray-200 rounded-full"}>
                        <Button
                            shape={"circle"}
                            size={"small"}
                            icon={<IoRemoveOutline />}
                            disabled={child === 0}
                            onClick={() => {
                                onSetChild(child - 1);
                            }}
                        />
                        <div className={"mx-1"}>
                            <span className={"font-medium"}>{child}</span> <span className={"text-gray-500"}>trẻ em</span>
                        </div>
                        <Button
                            shape={"circle"}
                            icon={<IoAddOutline />}
                            size={"small"}
                            onClick={() => {
                                onSetChild(child + 1);
                            }}
                        />
                    </div>
                </Col>
                <Col xs={24} lg={elder >=0 ? 12 : 8} md={elder >=0 ? 12 : 8}>
                    <div className={"flex flex-row items-center justify-between bg-gray-50 border border-gray-200 rounded-full"}>
                        <Button
                            shape={"circle"}
                            size={"small"}
                            icon={<IoRemoveOutline />}
                            disabled={baby === 0}
                            onClick={() => {
                                onSetBaby(baby - 1);
                            }}
                        />
                        <div className={"mx-1"}>
                            <span className={"font-medium"}>{baby}</span> <span className={"text-gray-500"}>em bé</span>
                        </div>
                        <Button
                            shape={"circle"}
                            icon={<IoAddOutline />}
                            size={"small"}
                            onClick={() => {
                                onSetBaby(baby + 1);
                            }}
                        />
                    </div>
                </Col>
                {elder >=0 &&
                <Col xs={24} lg={elder >=0 ? 12 : 8} md={elder >=0 ? 12 : 8}>
                    <div className={"flex flex-row items-center justify-between bg-gray-50 border border-gray-200 rounded-full"}>
                        <Button
                            shape={"circle"}
                            size={"small"}
                            icon={<IoRemoveOutline/>}
                            disabled={elder === 0}
                            onClick={() => {
                                onSetElder(elder - 1);
                            }}
                        />
                        <div className={"mx-1"}>
                            <span className={"font-medium"}>{elder}</span> <span className={"text-gray-500"}>người cao tuổi</span>
                        </div>
                        <Button
                            shape={"circle"}
                            icon={<IoAddOutline/>}
                            size={"small"}
                            onClick={() => {
                                onSetElder(elder + 1);
                            }}
                        />
                    </div>
                </Col>
                }
            </Row>
        </div>
    );
}

export default InputCustomerQuantityHorizontal;
