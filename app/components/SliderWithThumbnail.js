import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Slider from '@ant-design/react-slick';
import { IoArrowBack, IoArrowForward } from 'react-icons/io5';
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";
import { AppConfig } from '../appConfig';

SliderWithThumbnail.propTypes = {

};

function SliderWithThumbnail(props) {
    const [state, setState] = useState({})
    const [photoIndex, setPhotoIndex] = useState(0)

    const handleClickImage = (e, image) => {
        e && e.preventDefault();

        setState({
            current: image
        })
    }

    const handleCloseModal = (e) => {
        e && e.preventDefault();

        setState({
            current: ''
        })
    }
    const [nav1, setNav1] = useState(null);
    const [nav2, setNav2] = useState(null);
    const [slider1, setSlider1] = useState(null);
    const [slider2, setSlider2] = useState(null);

    useEffect(() => {
        setNav1(slider1);
        setNav2(slider2);
    });


    console.log(state);

    function PrevArrow(props) {
        const { onClick } = props;
        return (
            <div
                onClick={onClick}
                className={"rounded-full w-10 h-10 bg-white shadow hover:shadow-lg items-center justify-center flex absolute top-1/2 z-10 -left-5 cursor-pointer"}
            >
                <IoArrowBack size={"20"}/>
            </div>
        );
    }

    function NextArrow(props) {
        const { onClick } = props;
        return (
            <div
                onClick={onClick}
                className={"rounded-full w-10 h-10 bg-white shadow hover:shadow-lg items-center justify-center flex absolute top-1/2 z-5 -right-5 cursor-pointer"}
            >
                <IoArrowForward size={"20"}/>
            </div>
        );
    }
    const settings = {
        dots: false,
        autoPlay: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        prevArrow: <PrevArrow />,
        nextArrow: <NextArrow />,
        swipeToSlide: true,
        infinite: false,
        asNavFor: '.slider-nav'
    };

    const settingsThumbs = {
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        dots: false,
        arrows: false,
        centerMode: false,
        swipeToSlide: true,
        focusOnSelect: true,
        centerPadding: '10px',
        infinite: false
    };

    return (
        <div>
            <Slider
                {...settings}
                asNavFor={nav2}
                ref={slider => (setSlider1(slider))}
            >
                {props.gallery.map((item, index) => (
                    <img src={AppConfig.apiUrl+item.url} alt={item.name} className={"w-full h-80 md:h-96 object-cover rounded-md"} onClick={ e => handleClickImage(e, AppConfig.apiUrl+item.url)}/>
                ))}
            </Slider>
            <Slider
                {...settingsThumbs}
                asNavFor={nav1}
                ref={slider => (setSlider2(slider))}
                className={"mt-3 gallery-slider-thumb slider-margin"}
            >
                {props.gallery.map((item, index) => (
                    <img src={AppConfig.apiUrl+item.url} alt={item.name} className={"w-full h-10 md:h-20 object-cover rounded-md"}/>
                ))}
            </Slider>
            {state && state.current &&
            <Lightbox
                mainSrc={AppConfig.apiUrl+props.gallery[photoIndex].url}
                nextSrc={AppConfig.apiUrl+props.gallery[(photoIndex + 1) % props.gallery.length].url}
                prevSrc={AppConfig.apiUrl+props.gallery[(photoIndex + props.gallery.length - 1) % props.gallery.length].url}
                onCloseRequest={handleCloseModal}
                onMovePrevRequest={() =>
                    setPhotoIndex((photoIndex + props.gallery.length - 1) % props.gallery.length)
                }
                onMoveNextRequest={() =>
                    setPhotoIndex((photoIndex + 1) % props.gallery.length)
                }
            />
            }
        </div>
    );
}

export default SliderWithThumbnail;
