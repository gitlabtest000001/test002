import React from 'react';
import PropTypes from 'prop-types';
import Skeleton from 'react-loading-skeleton';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';

ListPageLoading.propTypes = {

};

function ListPageLoading(props) {
    return (
        <div className={"max-w-6xl mx-auto my-5"}>
            <Skeleton height={20} className={"mb-2"}/>
            <div className={"flex justify-between mb-5"}>
                <Skeleton height={50} width={300} />
                <Skeleton height={50} width={200} />
            </div>
            <div className={"mb-5"}>
                <Row gutter={[16, 16]}>
                    <Col xs={24} lg={8} md={8}>
                        <div>
                            <Skeleton height={20} width={300} count={10} />
                        </div>
                    </Col>
                    <Col xs={24} lg={16} md={16}>
                        <div className={"mb-5"}>
                            <Row gutter={[20, 20]}>
                                <Col xs={24} lg={8} md={8}>
                                    <Skeleton height={200} />
                                </Col>
                                <Col xs={24} lg={16} md={16}>
                                    <Skeleton height={40} className={"mb-3"} />
                                    <Skeleton height={15} count={3}/>
                                    <Skeleton height={40} width={150} className={"mt-10"}/>
                                </Col>
                            </Row>
                        </div>
                        <div className={"mb-5"}>
                            <Row gutter={[20, 20]}>
                                <Col xs={24} lg={8} md={8}>
                                    <Skeleton height={200} />
                                </Col>
                                <Col xs={24} lg={16} md={16}>
                                    <Skeleton height={40} className={"mb-3"} />
                                    <Skeleton height={15} count={3}/>
                                    <Skeleton height={40} width={150} className={"mt-10"}/>
                                </Col>
                            </Row>
                        </div>
                        <div className={"mb-5"}>
                            <Row gutter={[20, 20]}>
                                <Col xs={24} lg={8} md={8}>
                                    <Skeleton height={200} />
                                </Col>
                                <Col xs={24} lg={16} md={16}>
                                    <Skeleton height={40} className={"mb-3"} />
                                    <Skeleton height={15} count={3}/>
                                    <Skeleton height={40} width={150} className={"mt-10"}/>
                                </Col>
                            </Row>
                        </div>

                    </Col>
                </Row>
            </div>

        </div>
    );
}

export default ListPageLoading;
