import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {Modal, Button, Drawer} from 'antd';
import {useDispatch, useSelector} from "react-redux";
import {changeModalContent, hideModal} from "../containers/App/actions";

AntModal.propTypes = {
    centered: PropTypes.bool
};

function AntModal(props) {
    const { centered, ...custom } = props;
    const dispatch = useDispatch();

    const isVisible = useSelector(state => state.root.utility.showModal);
    const title = useSelector(state => state.root.utility.title);
    const modalWidth = useSelector(state => state.root.utility.modalWidth);
    const component = useSelector(state => state.root.utility.component);

    const handleCancel = () => {
        dispatch(hideModal());
        dispatch(changeModalContent());
    }

    return (
        <Modal
            title={title !== null ? title : false}
            visible={isVisible}
            onCancel={handleCancel}
            footer={false}
            destroyOnClose
            maskClosable={true}
            width={modalWidth}
            {...custom}
        >
            {component}
        </Modal>
    );
}

export default AntModal;
