import React from 'react';
import { Link } from 'react-router-dom';
import { formatVND } from '../utils/helpers';
import history from '../utils/history';
import { AppConfig } from '../appConfig';

function ProductItem(props) {
    const prices = props.item.prices.sort((a, b) => {
        return a.price - b.price
    })
    return (
        <div
            className={`${!props.home && 'border border-gray-300 rounded-lg'} p-2 md:p-5 text-center relative group cursor-pointer`}
            onClick={() => history.push(`/san-pham/${props.item.slug}`)}
        >
            <div className={"mb-3"}>
                <img
                    src={AppConfig.apiUrl+props.item.featureImage} alt={props.item.title}
                    className={"w-full h-64 md:h-40 md:h-64 object-cover"}
                />
            </div>
            <div>
                <h3 className={"font-medium text-base hover:text-green-600"}>{props.item.name}</h3>
                {prices && prices.length > 1 ?
                    <div>
                        <span className={"font-bold text-red-600 text-base"}>{formatVND(prices[0].price)} - {formatVND(prices[prices.length - 1].price)}</span>
                    </div>
                    :
                    <div>
                        {
                            prices && prices.length === 1 &&
                                prices[0].instock <= 0 ?
                                <div>
                                    <span className={"font-bold text-red-600 text-base"}>Liên hệ</span>
                                </div>
                                :
                                <div>
                                    <span className={"font-bold text-red-600 text-base"}>{prices[0] && prices[0].price && formatVND(prices[0].price)}</span>
                                </div>
                        }
                    </div>
                }
            </div>
        </div>
    );
}

export default ProductItem;
