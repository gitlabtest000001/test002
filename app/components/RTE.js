import React, { useMemo, useRef } from 'react';
import { AppConfig } from '../appConfig';
//import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import JoditEditor from 'jodit-react';

RichTextEditor.propTypes = {

};

RichTextEditor.defaultProps = {
    stickyToolbar: '70px'
}

function RichTextEditor({defaultValue, onDataChange, ...custom}) {

    const editor = useRef(null)

    /*function uploadImageCallBack(file) {
        return new Promise(
            (resolve, reject) => {
                const Token = localStorage.getItem('admin_token');
                let formData = new FormData();
                formData.append("files", file);
                axios.post(
                    `${AppConfig.apiUrl}/media/upload`,
                    formData,
                    {
                        headers: {Authorization: `Bearer ${Token}`},
                        'Content-Type': 'multipart/form-data',
                    }
                ).then(res => {
                    const response = `${AppConfig.apiUrl}${res.data[0].url}`;
                    resolve({ data: { link: response } })
                }).catch(error => ({error}));
            }
        )
    }

    let initialEditorState;

    if (typeof defaultValue !== 'undefined') {
        let contentBlock = htmlToDraft(defaultValue);
        let contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
        initialEditorState = EditorState.createWithContent(contentState);
    } else {
        initialEditorState = EditorState.createEmpty();
    }

    const [editorState, setEditorState] = useState(initialEditorState);

    const onEditorStateChange = (editorStateData) => {
        setEditorState(editorStateData);
        let data = draftToHtml(convertToRaw(editorStateData.getCurrentContent()));
        onDataChange(data);
    };*/

    const Token = localStorage.getItem('dvg_manager_token');
    const config = {
        enableDragAndDropFileToEditor: true,
        placeholder: '',
        //toolbarStickyOffset: 68,
        buttons: [
            'source', '|',
            'bold',
            'strikethrough',
            'underline',
            'italic', '|',
            'ul',
            'ol', '|',
            'align',
            //'outdent', 'indent',  '|',
            'font',
            'fontsize',
            //'brush',
            'paragraph', '|',
            'image',
            'video',
            'table',
            'link', '|', //'undo', 'redo', '|',
            'hr',
            'eraser',
            'copyformat', '|',
            'symbol',
            'fullsize',
            //'print',
            //'about'
        ],
        colors: {
            greyscale:  ['#000000', '#434343', '#666666', '#999999', '#B7B7B7', '#CCCCCC', '#D9D9D9', '#EFEFEF', '#F3F3F3', '#FFFFFF'],
            palette:    ['#980000', '#FF0000', '#FF9900', '#FFFF00', '#00F0F0', '#00FFFF', '#4A86E8', '#0000FF', '#9900FF', '#FF00FF'],
            full: [
                '#E6B8AF', '#F4CCCC', '#FCE5CD', '#FFF2CC', '#D9EAD3', '#D0E0E3', '#C9DAF8', '#CFE2F3', '#D9D2E9', '#EAD1DC',
                '#DD7E6B', '#EA9999', '#F9CB9C', '#FFE599', '#B6D7A8', '#A2C4C9', '#A4C2F4', '#9FC5E8', '#B4A7D6', '#D5A6BD',
                '#CC4125', '#E06666', '#F6B26B', '#FFD966', '#93C47D', '#76A5AF', '#6D9EEB', '#6FA8DC', '#8E7CC3', '#C27BA0',
                '#A61C00', '#CC0000', '#E69138', '#F1C232', '#6AA84F', '#45818E', '#3C78D8', '#3D85C6', '#674EA7', '#A64D79',
                '#85200C', '#990000', '#B45F06', '#BF9000', '#38761D', '#134F5C', '#1155CC', '#0B5394', '#351C75', '#733554',
                '#5B0F00', '#660000', '#783F04', '#7F6000', '#274E13', '#0C343D', '#1C4587', '#073763', '#20124D', '#4C1130'
            ]
        },
        buttonsXS: [
            'bold',
            'image', '|',
            'brush',
            'paragraph', '|',
            'align', '|',
            'undo', 'redo', '|',
            'eraser',
            'dots'
        ],
        colorPickerDefaultTab: 'background',
        uploader: {
            url: `${AppConfig.apiUrl}/media/upload`,  //your upload api url
            insertImageAsBase64URI: false,
            imagesExtensions: ['jpg', 'png', 'jpeg', 'gif'],
            headers: {Authorization: `Bearer ${Token}`},
            // insertImageAsBase64URI: false,
            // withCredentials: true,
            filesVariableName: function (e) {
                return "files";
            },
            prepareData: function (data) {
                data.append("id", 16);
                return data;
            },

            isSuccess: function (resp) {
                return resp;
            },
            process: function (resp) {
                return {
                    files: resp,
                    path: resp[0].url,
                    baseurl: AppConfig.apiUrl,
                    error: resp.error,
                    message: resp.message,
                };
            },

            defaultHandlerSuccess: function (data) {
                var field = "files";
                if (data[field] && data[field].length) {
                    this.selection.insertImage(data.baseurl + data[field][0].url);
                }
            },
        },
        readonly: false,
        style: {
            fontFamily: 'Open Sans'
        },

    }

    return useMemo( () => (
        /*<Editor
            editorState={editorState}
            onEditorStateChange={onEditorStateChange}
            toolbarClassName="rdw-toolbar"
            wrapperClassName="rdw-wrapper"
            editorClassName="rdw-editor"
            toolbar={{
                inline: { inDropdown: true },
                list: { inDropdown: true },
                textAlign: { inDropdown: true },
                link: { inDropdown: false },
                history: { inDropdown: true },
                image: {
                    uploadCallback: uploadImageCallBack,
                    inputAccept: 'image/gif,image/jpeg,image/jpg,image/png,image/svg',
                    previewImage: true,
                    alt: { present: true, mandatory: false }
                },
            }}
        />*/

        <JoditEditor
            ref={editor}
            value={defaultValue}
            config={config}
            tabIndex={1} // tabIndex of textarea
            //onBlur={newContent => setContent(newContent)} // preferred to use only this option to update the content for performance reasons
            onBlur={content => onDataChange(content)}
        />
    ), []);
}

export default RichTextEditor;
