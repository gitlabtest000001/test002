import React, {useState} from 'react';
import PropTypes from 'prop-types';
import {IoAddOutline, IoRemoveOutline} from 'react-icons/io5';

InputSpinner.propTypes = {

};

function InputSpinner({initialNumber, minimum, maximum, setNumber}) {
    const [value, setValue] = useState(initialNumber)

    return (
        <div className={"flex flex-row justify-between items-center"}>
            <div className={"rounded bg-blue-300 w-8 h-8 justify-center items-center flex mr-3"}>
                <IoRemoveOutline className={"text-white"}/>
            </div>
            <div>
                <span className={"font-medium text-base"}>{value}</span>
            </div>
            <div className={"rounded bg-blue-300 w-8 h-8 justify-center items-center flex ml-3"}>
                <IoAddOutline className={"text-white"}/>
            </div>
        </div>
    );
}

export default InputSpinner;
