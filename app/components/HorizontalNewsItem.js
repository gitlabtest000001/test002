import React from 'react';
import { AppConfig } from '../appConfig';
import { formatDateTime } from '../utils/helpers';
import { ClockCircleOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import { IoArrowForward } from 'react-icons/io5';
import Logo from '../images/logo.png'

function HorizontalNewsItem(props) {
    return (
        <Row className={"mb-5 border-b border-gray-200 pb-5"} gutter={[16, 16]}>
            <Col xs={24} lg={8} md={8}>
                {props.item.featureImage ?
                    <img src={AppConfig.apiUrl + props.item.featureImage} className={"w-full h-52 object-cover rounded"}
                         alt={props.item.title}/>
                    :
                    <img src={Logo} className={"w-full h-52 object-contain rounded"}
                         alt={props.item.title}/>
                }
            </Col>
            <Col xs={24} lg={16} md={16}>
                <Link to={`/tin-tuc/${props.item.slug}`} className={"cursor-pointer"}>
                    <h2 className={"font-medium text-lg mb-3 hover:text-blue-500"}>{props.item.title}</h2>
                </Link>
                <p className={"mb-3 text-gray-500"}>
                    <ClockCircleOutlined /> {formatDateTime(props.item.createdAt)}
                </p>
                <p className={"mb-3"}>{props.item.excerpt}...</p>
                <Link className={"text-green-600 hover:underline"} to={`/tin-tuc/${props.item.slug}`}>
                    Đọc tiếp <IoArrowForward className={"-mt-1"}/>
                </Link>
            </Col>
        </Row>
    );
}

export default HorizontalNewsItem;
