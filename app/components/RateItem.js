import React from 'react';
import PropTypes from 'prop-types';

RateItem.propTypes = {

};

function RateItem(props) {
    return (
        <div className={"flex flex-row items-center my-1"}>
            <div className={"bg-blue-400 rounded px-1 mr-1"}>
                <span className={"text-white text-xs md:text-normal"}>{props.rate}</span>
            </div>
            <div>
                <span className={"text-gray-600 text-xs md:text-normal"}>{props.review}</span>
            </div>
            <div className={"ml-1 hidden md:inline-block"}>
                <span className={"text-gray-500 text-xs"}>({props.total} đánh giá)</span>
            </div>
        </div>
    );
}

export default RateItem;
