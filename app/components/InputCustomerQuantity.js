import React from 'react';
import PropTypes from 'prop-types';
import Button from 'antd/es/button';
import { IoAddOutline, IoRemoveOutline } from 'react-icons/io5';

InputCustomerQuantity.propTypes = {

};

function InputCustomerQuantity({
    elder,
    adult,
    child,
    baby,
    onSetAdult,
    onSetElder,
    onSetChild,
    onSetBaby,
}) {
    return (
        <div>
            <div className={"flex flex-row justify-between items-center mb-3 pb-3 border-b border-gray-100"}>
                <Button
                    icon={<IoRemoveOutline />}
                    shape={"circle"} type="primary"
                    disabled={adult === 1}
                    onClick={() => {
                        onSetAdult(adult - 1);
                    }}
                />
                <span className={"text-base"}><span className={"font-medium text-lg mr-1 text-blue-500"}>{adult}</span> <span className={"text-gray-700"}>Người lớn</span></span>
                <Button
                    icon={<IoAddOutline />}
                    shape={"circle"}
                    type="primary"
                    onClick={() => {
                        onSetAdult(adult + 1);
                    }}
                />
            </div>
            <div className={"flex flex-row justify-between items-center mb-3 pb-3 border-b border-gray-100"}>
                <Button
                    icon={<IoRemoveOutline />}
                    shape={"circle"} type="primary"
                    disabled={child === 0}
                    onClick={() => {
                        onSetChild(child - 1);
                    }}
                />
                <span className={"text-base"}><span className={"font-medium text-lg mr-1 text-blue-500"}>{child}</span> <span className={"text-gray-700"}>Trẻ em</span></span>
                <Button
                    icon={<IoAddOutline />}
                    shape={"circle"}
                    type="primary"
                    onClick={() => {
                        onSetChild(child + 1);
                    }}
                />
            </div>
            <div className={"flex flex-row justify-between items-center"}>
                <Button
                    icon={<IoRemoveOutline />}
                    shape={"circle"} type="primary"
                    disabled={baby === 0}
                    onClick={() => {
                        onSetBaby(baby - 1);
                    }}
                />
                <span className={"text-base"}><span className={"font-medium text-lg mr-1 text-blue-500"}>{baby}</span> <span className={"text-gray-700"}>Em bé</span></span>
                <Button
                    icon={<IoAddOutline />}
                    shape={"circle"}
                    type="primary"
                    onClick={() => {
                        onSetBaby(baby + 1);
                    }}
                />
            </div>
        </div>
    );
}

export default InputCustomerQuantity;
