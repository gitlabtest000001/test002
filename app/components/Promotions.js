import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { IoArrowBack, IoArrowForward } from 'react-icons/io5';
import { Link } from 'react-router-dom';
import Slider from '@ant-design/react-slick';
import Loading from './Loading';
import axios from 'axios';
import { AppConfig } from '../appConfig';

Promotions.propTypes = {

};

function Promotions(props) {

    const [loadingSlider, setLoadingSlider] = useState(true);
    const [slider, setSlider] = useState([]);

    useEffect(() => {
        async function getSlider() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/slider-item?sliderId=${props.sliderId && props.sliderId}`,
            }).then(function(response) {
                if (response.status === 200) {
                    setSlider(response.data);
                    setLoadingSlider(false);
                }
            }).catch(function(error) {
                console.log(error);
            })
        }

        getSlider();
    }, [props.sliderId])


    function PrevArrow(props) {
        const { onClick } = props;
        return (
            <div
                onClick={onClick}
                className={"rounded-full w-8 h-8 md:w-10 md:h-10 bg-white shadow hover:shadow-lg items-center justify-center flex absolute top-16 z-10 -left-3 md:-left-5 cursor-pointer"}
            >
                <IoArrowBack size={"20"}/>
            </div>
        );
    }

    function NextArrow(props) {
        const { onClick } = props;
        return (
            <div
                onClick={onClick}
                className={"rounded-full w-8 h-8 md:w-10 md:h-10 bg-white shadow hover:shadow-lg items-center justify-center flex absolute top-16 z-10 -right-3 md:-right-5 cursor-pointer"}
            >
                <IoArrowForward size={"20"}/>
            </div>
        );
    }

    const settings = {
        dots: false,
        autoPlay: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 2,
        prevArrow: <PrevArrow />,
        nextArrow: <NextArrow />,
        swipeToSlide: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 2,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 2,
                    initialSlide: 3
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    initialSlide: 1
                }
            }
        ]
    };
    return (
        loadingSlider ? <Loading /> :
        <div className={"px-3 md:px-0 max-w-6xl mx-auto z-20 my-10"}>
            <Slider className={"slider-margin"} {...settings}>
                {slider && slider.map((item, index) => (
                    <Link to={"#"}>
                        <div className={"text-center"}>
                            <img src={AppConfig.apiUrl+item.image} className={"rounded-md object-cover w-full h-40"} alt={item.name} />
                        </div>
                    </Link>
                ))}
            </Slider>
        </div>
    );
}

export default Promotions;
