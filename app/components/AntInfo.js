import React, {useState} from 'react';
import PropTypes from 'prop-types';
import { Drawer, List, Avatar, Divider, Col, Row } from 'antd';
import {useDispatch, useSelector} from "react-redux";
import { useMediaQuery } from 'react-responsive'
import {changeInfoContent, hideInfo} from "../containers/App/actions";

AntInfo.propTypes = {

};

function AntInfo(props) {
    const isMobile = useMediaQuery({ maxWidth: 767 });
    const { centered, ...custom } = props;
    const dispatch = useDispatch();

    const isVisible = useSelector(state => state.root.utility.showInfo);
    const title = useSelector(state => state.root.utility.title);
    const component = useSelector(state => state.root.utility.component);

    const handleCancel = () => {
        dispatch(hideInfo());
        dispatch(changeInfoContent())
    }

    return (
        <Drawer
            title={title}
            width={isMobile ? '100%' : '30%'}
            placement="right"
            closable={true}
            onClose={handleCancel}
            visible={isVisible}
            destroyOnClose
            maskClosable={false}
            {...custom}
        >
            {component}
        </Drawer>
    );
}

export default AntInfo;
