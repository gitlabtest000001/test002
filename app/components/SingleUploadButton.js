import React, {useState} from 'react';
import LoadingOutlined from "@ant-design/icons/lib/icons/LoadingOutlined";
import PlusOutlined from "@ant-design/icons/lib/icons/PlusOutlined";
import Upload from "antd/es/upload";
import {message, Tooltip} from "antd";
import axios from "axios";
import { AppConfig } from '../appConfig';
import UploadOutlined from '@ant-design/icons/lib/icons/UploadOutlined';
import Button from 'antd/es/button';

SingleUploadButton.propTypes = {

};

SingleUploadButton.defaultProps = {
    uploadLabel: 'Chọn ảnh',
    currentImage: null
}

function SingleUploadButton({uploadLabel, currentImage, onChooseMedia}) {
    const token = localStorage.getItem('dvg_user_token');
    const [loading, setLoading] = useState(false);
    const [imageUrl, setImageUrl] = useState(currentImage && currentImage);

    function beforeUpload(file) {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            message.error('Vui lòng chọn file ảnh!');
        }
        const isLt5M = file.size / 1024 / 1024 < 5;
        if (!isLt5M) {
            message.error('Kích thước không được lớn hơn 5MB!');
        }
        return isJpgOrPng && isLt5M;
    }

    function handleRemove(info){
        onChooseMedia(null);
        axios.delete(
            `${AppConfig.apiUrl}/media/${info.response[0].id}`,
            {headers: {Authorization: `Bearer ${token}`},}
        ).then(response => (
            message.success(`Đã xoá ảnh`)
        )).catch(error => message.error(error))
    }

    const handleChange = info => {
        if (info.file.status === 'error') {
            message.error(info.file.response.message);
            setLoading(false);
            return;
        }
        if (info.file.status === 'done') {
            message.success(`${info.file.name} tải lên thành công`);
            let file = info.file;
            if (file.response) {
                file = `${AppConfig.apiUrl}${file.response[0].url}`
                onChooseMedia(file)
                setImageUrl(file)
            }
        }
    };

    return (
        <Upload
            name="files"
            showUploadList={false}
            className="avatar-uploader"
            action={`${AppConfig.apiUrl}/media/upload`}
            headers={{Authorization: `Bearer ${token}`}}
            beforeUpload={beforeUpload}
            onChange={handleChange}
            onRemove={handleRemove}
        >
            <Button type={"primary"} icon={<UploadOutlined />}>{uploadLabel}</Button>
        </Upload>
    );
}

export default SingleUploadButton;
