import {message} from "antd";
import React from "react";

export const SuccessMessage = (data) => {
    message.loading('Đang tải...', 0.1)
        .then(() => message.success(data.message, 2.5))
}

export const ErrorMessage = (error) => {

    let imessage = error.message;

    if (Array.isArray(imessage)) {
        imessage = imessage.map(mes => (
            <>
                {mes}<br/>
            </>
        ))
    } else {
        imessage = error.message;
    }

    message.loading('Đang tải...', 0.1)
        .then(() => message.error(imessage, 2.5))
}
