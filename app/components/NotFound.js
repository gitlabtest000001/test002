import React from 'react';
import PropTypes from 'prop-types';
import NotFoundImg from '../images/Error404.png'
import Button from 'antd/es/button';
import { Link } from 'react-router-dom'

NotFound.propTypes = {

};

NotFound.defaultProps = {
    title: 'Không tìm thấy theo các tiêu chí tìm kiếm của bạn',
    message: 'Vui lòng thay đổi tiêu chí tìm kiếm',
    showButton: false,
    buttonText: 'Khám phá',
    buttonLink: '/'
}

function NotFound(props) {
    return (
        <div className={"my-10 text-center"}>
            <img src={NotFoundImg} alt={"Notfound"} className={"mb-5 w-64"} />
            <p className={"text-gray-700 font-medium"}>{props.title}</p>
            <p className={"text-gray-400"}>{props.message}</p>
            {props.showButton &&
                <p className={"mt-10"}>
                    <Link to={props.buttonLink} class={"bg-red-600 text-white px-5 py-2 hover:text-white rounded hover:bg-red-500"}>
                        {props.buttonText}
                    </Link>
                </p>

            }
        </div>
    );
}

export default NotFound;
