import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { hideLoader, hideModal, showLoader } from '../../containers/App/actions';
import axios from 'axios';
import { AppConfig } from '../../appConfig';
import Table from 'antd/es/table';
import { Link } from 'react-router-dom';
import { checkRole, formatDateTime } from '../../utils/helpers';
import Tag from 'antd/es/tag';
import Space from 'antd/es/space';
import Button from 'antd/es/button';
import { EditTwoTone } from '@ant-design/icons';
import Image from 'antd/es/image';
import Row from 'antd/es/grid/Row';
import { IoAttach, IoCloudUpload, IoDownloadOutline, IoRemoveCircleOutline, IoTrashBin } from 'react-icons/all';
import Upload from 'antd/es/upload';
import { message } from 'antd';

function FileManager(props) {
    // File Upload
    let token = null;
    if (props.manager) {
        token = localStorage.getItem('dvg_manager_token');
    } else {
        token = localStorage.getItem('dvg_user_token');
    }

    const dispatch = useDispatch();
    const [files, setFiles] = useState({
        media: [],
        count: 0
    });

    const [page, setPage] = useState( 1);
    const [flag, setFlag] = useState( true);
    const [pageSize, setPageSize] = useState( 5);
    const [selected, setSelected] = useState([])

    useEffect(() => {
        dispatch(showLoader());
        async function getFiles() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/media`,
                params: {
                    page: page,
                    limit: pageSize,
                },
                headers: {Authorization: `Bearer ${token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setFiles(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getFiles();
    },[page, pageSize, flag])

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Preview',
            dataIndex: 'url',
            key: 'url',
            render: (text, row) =>
                <Image src={AppConfig.apiUrl+row.url} className={"w-10 h-10 object-cover border border-gray-300 rounded-md"} />
        },
        {
            title: 'Ngày đăng',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: (key, row) => (
                formatDateTime(row.createdAt)
            )
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: (key, row) => (
                <Space size="middle">
                    <Button type="danger" size="small" ghost icon={<IoTrashBin className={"mr-1"} />} onClick={() => handleRemove(row.id)} >
                        Xoá
                    </Button>
                </Space>
            )
        },
    ]

    const rowSelection = {
        onChange: (selectedRowKeys, selectedRows) => {
            setSelected(selectedRows)
        },
    };

    function handleInsert() {
        props.onChooseFile(selected)
        dispatch(hideModal())
    }

    console.log(selected);

    function beforeUpload(file) {
        const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png';
        if (!isJpgOrPng) {
            message.error('Vui lòng chọn file ảnh!');
        }
        const isLt5M = file.size / 1024 / 1024 < 5;
        if (!isLt5M) {
            message.error('Kích thước không được lớn hơn 5MB!');
        }
        return isJpgOrPng && isLt5M;
    }

    function handleRemove(id){
        dispatch(showLoader())
        axios.delete(
            `${AppConfig.apiUrl}/media/${id}`,
            {headers: {Authorization: `Bearer ${token}`},}
        ).then((response) => {
            dispatch(hideLoader())
            message.success(`Đã xoá ảnh`)
            setFiles({count: files.count, media: files.media.filter((item) => item.id !== id)})
            setFlag(!flag)
        }).catch((error) => {
            message.error(`Lỗi`)
            dispatch(hideLoader())
        })
    }

    const handleChange = info => {
        if (info.file.status === 'error') {
            message.error(info.file.response.message);
            return;
        }
        if (info.file.status === 'done') {
            message.success(`${info.file.name} tải lên thành công`);
            let file = info.file;
            if (file.response) {
                setFiles({media: [file.response[0], ...files.media], count: files.count + file.response.length})
                setFlag(!flag)
            }
        }
    };
    return (
        <div>
            <div className={"flex items-center justify-between mb-5"}>
                <div>
                    <Upload
                        name="files"
                        showUploadList={false}
                        action={`${AppConfig.apiUrl}/media/upload`}
                        headers={{Authorization: `Bearer ${token}`}}
                        beforeUpload={beforeUpload}
                        onChange={handleChange}
                        multiple={props.multiple ? 'true' : 'false'}
                    >
                        <Button
                            icon={<IoCloudUpload className={"mr-1"}/>}
                        >
                            Tải lên
                        </Button>
                    </Upload>
                </div>
                <div>
                    <Button
                        type={"primary"}
                        icon={<IoAttach className={"mr-1"}/>}
                        onClick={handleInsert}
                    >
                        Sử dụng
                    </Button>
                </div>
            </div>
            <Table
                size={"small"}
                rowKey={"id"}
                rowSelection={{
                    type: props.multiple ? 'checkbox' : 'radio',
                    ...rowSelection,
                }}
                scroll={{x: '100%'}}
                columns={columns}
                dataSource={files && files.media}
                pagination={{
                    current: page,
                    pageSize: pageSize,
                    total: files && files.count,
                    onChange: ((page, pageSize) => {
                        setPage(page)
                        setPageSize(pageSize)
                    }),
                    position: ['bottomLeft']
                }}
            />
        </div>
    );
}

export default FileManager;
