import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import Slider from '@ant-design/react-slick';
import {IoArrowBack, IoArrowForward} from 'react-icons/io5';
import Skeleton from 'react-loading-skeleton';
import defaultImage from '../images/default-tour.jpg'
import { AppConfig } from '../appConfig';

LocationSlider.propTypes = {

};

function LocationSlider(props) {

    function PrevArrow(props) {
        const { onClick } = props;
        return (
            <div
                onClick={onClick}
                className={"rounded-full w-8 h-8 md:w-10 md:h-10 bg-white shadow hover:shadow-lg items-center justify-center flex absolute top-1/4 z-10 -left-3 cursor-pointer"}
            >
                <IoArrowBack size={"20"}/>
            </div>
        );
    }

    function NextArrow(props) {
        const { onClick } = props;
        return (
            <div
                onClick={onClick}
                className={"rounded-full w-8 h-8 md:w-10 md:h-10 bg-white shadow hover:shadow-lg items-center justify-center flex absolute top-1/4 z-10 -right-3 cursor-pointer"}
            >
                <IoArrowForward size={"20"}/>
            </div>
        );
    }

    const settings = {
        dots: false,
        infinite: true,
        speed: 500,
        slidesToShow: 7,
        slidesToScroll: 6,
        prevArrow: <PrevArrow />,
        nextArrow: <NextArrow />,
        swipeToSlide: true,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 4,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    initialSlide: 4
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    initialSlide: 4
                }
            }
        ]
    };

    return (
        <div className={"my-3 md:my-10 px-3 md:px-0 bg-white py-3 border border-gray-200 md:border-0"}>
            <div className={"mb-3 md:mb-10"}>
                <h2 className={"text-xl md:text-2xl font-medium text-gray-700"}>Khách sạn theo địa điểm</h2>
            </div>

            <Slider className={"slider-margin"} {...settings}>
                {props.locations && props.locations.map((item, index) => (
                    //<Link to={`/khach-san/${item.id}/${item.slug}`}>
                    <Link to={{
                        pathname: `/khach-san/tim-kiem/${item.id}/${item.slug}`
                    }}>
                        <div className={"text-center hover:scale-50"}>
                            <img src={item.featureImage ? AppConfig.apiUrl+item.featureImage : defaultImage} className={"w-16 h-16 md:h-32 md:w-32 rounded-full object-cover"} alt={item.name} />
                            <p className={"text-gray-800 md:text-base font-medium mt-2"}>
                                {item.name}
                            </p>
                            <p className={"text-gray-500 font-regular hidden md:block"}>
                                {item.posts} khách sạn
                            </p>
                        </div>
                    </Link>
                ))}
            </Slider>
        </div>
    );
}

export default LocationSlider;
