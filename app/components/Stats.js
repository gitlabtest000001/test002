import React from 'react';
import PropTypes from 'prop-types';
import Avatar from "antd/es/avatar";
import UserOutlined from "@ant-design/icons/lib/icons/UserOutlined";
import {
    IoWallet,
    IoToday,
    IoLogoUsd,
    IoReceipt
} from 'react-icons/io5';
import {formatVND} from "../utils/helpers";

Stats.propTypes = {

};

function Stats(props) {
    return (
        <div>
            <div className="grid gap-6 mb-5 md:grid-cols-2 xl:grid-cols-2">
                <div className="min-w-0 rounded-sm border border-gray-100 overflow-hidden bg-white dark:bg-gray-800" >
                    <div className="p-4 flex items-center">
                        <div className="p-3 rounded-full text-red-500 dark:text-red-100 bg-red-100 dark:bg-red-500 mr-4">
                            <IoToday size={24} />
                        </div>
                        <div>
                            <p className="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                                Đặt chỗ
                            </p>
                            <p className="text-lg font-semibold text-gray-700 dark:text-gray-200">
                                {props.stats && props.stats.bookingCount}
                            </p>
                        </div>
                    </div>
                </div>
                <div className="min-w-0 rounded-sm  border border-gray-100 overflow-hidden bg-white dark:bg-gray-800" >
                    <div className="p-4 flex items-center">
                        <div className="p-3 rounded-full text-blue-500 dark:text-blue-100 bg-blue-100 dark:bg-blue-500 mr-4">
                            <IoReceipt size={24} />
                        </div>
                        <div>
                            <p className="mb-2 text-sm font-medium text-gray-600 dark:text-gray-400">
                                Doanh thu
                            </p>
                            <p className="text-lg font-semibold text-gray-700 dark:text-gray-200">
                                {props.stats && formatVND(props.stats.bookingRevenue)}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Stats;
