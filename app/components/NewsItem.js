import React from 'react';
import { AppConfig } from '../appConfig';
import history from '../utils/history';
import Logo from '../images/logo.png';

function NewsItem(props) {
    return (
        <div
            className={"border border-gray-300 rounded-lg text-center cursor-pointer"}
            onClick={() => history.push(`/tin-tuc/${props.item.slug}`)}
        >
            {props.item.featureImage ?
                <img src={AppConfig.apiUrl + props.item.featureImage} className={"w-full h-52 object-cover rounded"}
                     alt={props.item.title}/>
                :
                <img src={Logo} className={"w-full h-52 object-contain rounded"}
                     alt={props.item.title}/>
            }
            <div className={"p-5"}>
                <h4 className={"font-medium text-lg mb-3"}>{props.item.title}</h4>
                <p>{props.item.excerpt}</p>
            </div>
        </div>
    );
}

export default NewsItem;
