import React, {useEffect} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {IoChevronForward, IoCall} from 'react-icons/all';

function MobileSupport(props) {
    const settings = useSelector(state => state.root.settings.options);

    return (
        <div>
            <div className={"mt-5 bg-gray-100 px-3 py-3 rounded-lg"}>
                <div className={"flex flex-row items-center justify-between"}>
                    <div>
                        <IoCall size={30} color="#6F6F6F" />
                    </div>
                    <div>
                        <p className={"text-lg mb-2 font-medium"}>
                            Hotline hỗ trợ 24/7
                        </p>
                        <p className={"text-xs"}>
                            Sẵn sàng giải đáp mọi thắc mắc của
                        </p>
                        <p className={"text-xs"}>
                            quý khách về đặt hàng và sản phẩm Tảo.
                        </p>
                    </div>
                    <div>
                        <IoChevronForward size={30} color="#6F6F6F" />
                    </div>
                </div>
            </div>
        </div>

    );
}

export default MobileSupport;
