import React, {useState} from 'react';
import PropTypes from 'prop-types';
import Button from 'antd/es/button';
import history from '../utils/history';
import {RiArrowRightSFill, TiLocation} from 'react-icons/all'
import { MinusCircleOutlined, PlusCircleOutlined } from '@ant-design/icons';
import { useDispatch } from 'react-redux';
import { hideInfo } from '../containers/App/actions';

LocationListSidebar.propTypes = {

};

function LocationListSidebar(props) {
    const dispatch = useDispatch()
    const [viewAll, setViewAll] = useState(false);
    console.log(props.locations);

    console.log(props.currentId);

    const menuItem = (item) => {
        return (
                <div
                    className={`flex items-center justify-between py-2 px-3 cursor-pointer hover:bg-gray-100 ${Number(props.currentId) === Number(item.id) && 'bg-red-100 border-l-2 border-red-300 hover:bg-red-100'}`}
                    onClick={() => {
                        history.push(`${props.type === 'tour' ? '/tour-du-lich/' : props.type === 'combo' ? '/combo-du-lich/' : props.type === 'ticket' && '/ve-vui-choi/'}${item.id}/${item.slug}`);
                        dispatch(hideInfo())
                    }}
                >
                    {item.name}
                </div>
            )
    }

    return (
        <div className={"bg-gray-50 rounded-md border border-gray-100"}>
            <div className={"p-3 flex items-center border-b border-gray-100"}>
                <TiLocation size={20} className={"mr-2 text-red-500"}/>
                <h2 className={"font-medium text-base text-gray-600"}>Tìm {props.type === 'tour' ? 'tour' : props.type === 'combo' ? 'combo' : props.type === 'ticket' && 'vé' } theo địa điểm</h2>
            </div>
            {props.locations && props.locations.slice(0, 8).map((item, index) => (
                menuItem(item)
            ))}
            {viewAll && props.locations && props.locations.slice(8).map((item, index) => (
                menuItem(item)
            ))}
            <div className={"mb-2"}>
                <Button type={"link"} block onClick={() => setViewAll(!viewAll)} icon={viewAll ? <MinusCircleOutlined /> : <PlusCircleOutlined />}>
                    {viewAll ? 'Ẩn đi' : 'Xem thêm'}
                </Button>
            </div>

        </div>
    );
}

export default LocationListSidebar;
