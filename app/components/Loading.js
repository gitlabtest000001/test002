import {Spin} from "antd";
import { LoadingOutlined } from '@ant-design/icons';
import React from "react";

const Loading = () => <Spin indicator={<LoadingOutlined />} />;

export default Loading;