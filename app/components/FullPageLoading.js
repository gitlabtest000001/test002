import React from 'react';
import PropTypes from 'prop-types';
import {LoadingOutlined, SyncOutlined} from "@ant-design/icons";
import {Spin} from "antd";
import {useSelector} from "react-redux";

FullPageLoading.propTypes = {

};

function FullPageLoading(props) {
    const loading = useSelector(state => state.root.utility.loading);

    return (
        loading ?
        <div className={"fixed top-0 left-0 right-0 bottom-0 bg-gray-800 bg-opacity-80"} style={{zIndex: 2000}}>
            <Spin
                indicator={<SyncOutlined spin className={"text-white"} />}
                className={"fixed mr-auto ml-auto top-1/2 left-0 right-0 text-white"}
                tip="Vui lòng chờ..."
            />
        </div>
            : null
    );
}

export default FullPageLoading;
