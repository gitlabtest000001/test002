import React from 'react';
import Button from 'antd/es/button';
import { ArrowUpOutlined } from '@ant-design/icons';
import { Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll';

function BackToTop(props) {
    return (
        <div className={"fixed bottom-10 right-10"}>
            <Button
                icon={<ArrowUpOutlined />}
                onClick={() => {
                    scroll.scrollToTop();
                }}
            />
        </div>
    );
}

export default BackToTop;
