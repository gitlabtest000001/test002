import React, {useEffect} from 'react';
import PropTypes from 'prop-types';
import { Layout, Row, Col } from 'antd';
import styled from 'styled-components';
import {Link} from "react-router-dom";
import {Helmet} from "react-helmet";
import background from '../images/spirulina.jpeg';
import {useDispatch, useSelector} from "react-redux";
import Logo from '../images/logo.png'

const AuthForm = styled.div`
    max-with: 450px;
    z-index: 2;
    min-width: 300px;
`;

const LeftCol = styled.div`
    background-image: url(${background});
    background-size: cover;
    background-position: center center;
    min-height: 100vh;
    @media (max-width: 768px) {
        display: none;
      }
`

AuthLayout.propTypes = {
    title: PropTypes.string,
};

function AuthLayout({children}) {

    return (
        <Layout className="workspace">
            <Layout>
                <Row>
                    <Col lg={8} xs={24} md={12}>
                        <Row
                            type="flex"
                            align="middle"
                            justify="center"
                            className="px-3 bg-white min-h-screen"
                        >
                            <AuthForm>
                                <div className="text-center mb-10">
                                    <Link to="/">
                                        <img src={Logo} className={"w-40"} />
                                    </Link>
                                </div>
                                {children}
                            </AuthForm>
                        </Row>
                    </Col>
                    <Col lg={16} xs={24} md={12}>
                        <LeftCol>
                            &nbsp;
                        </LeftCol>
                    </Col>
                </Row>
            </Layout>

        </Layout>
    );
}

export default AuthLayout;
