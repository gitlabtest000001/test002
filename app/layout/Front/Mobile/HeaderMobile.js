import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import logo from '../../../images/logo.png';
import { useDispatch } from 'react-redux';
import Modal from 'antd/es/modal';
import history from '../../../utils/history';
import Search from 'antd/es/input/Search';
import { AppConfig } from '../../../appConfig';

const { confirm } = Modal;

HeaderMobile.propTypes = {};

function HeaderMobile(props) {
    const dispatch = useDispatch();

    const [scroll, setScroll] = useState(false);

    useEffect(() => {
        window.addEventListener("scroll", () => {
            setScroll(window.scrollY > 100);
        });
    }, []);

    function onSearch(value) {
        if (value && typeof value !== 'undefined') {
            history.push(`/tim-san-pham?q=${value}`)
        }
    }

    return (
        scroll &&
        <div className={"mb-5 bg-white px-3 py-2 shadow-lg fixed z-50 w-full"}>
            <div className={"flex items-center justify-between"}>
                <Link to={"/"} className={"mr-5"}>
                    <img src={props.settings && AppConfig.apiUrl+props.settings.logo} alt={"Đại Việt Group"} className={"w-20"}/>
                </Link>
                <Search
                    placeholder="Tìm sản phẩm"
                    onSearch={onSearch}
                    className={"w-full"}
                    style={{borderRadius: '100%'}}
                />
            </div>
        </div>
    );
}

export default HeaderMobile;
