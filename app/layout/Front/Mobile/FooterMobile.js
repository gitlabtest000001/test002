import React, { useEffect, useState } from 'react';
import {
    IoBagCheck,
    IoCall,
    IoIosCart,
    IoIosHome,
    IoMail,
    IoPerson,
    SiPhonepe,
    SiWhatsapp,
    SiZalo,
} from 'react-icons/all';
import { Link } from 'react-router-dom';
import Row from 'antd/es/grid/row';
import Col from 'antd/es/grid/col';
import { IoLogoFacebook, IoLogoYoutube } from 'react-icons/io5';
import AppStoreIcon from '../../../images/appstore.png';
import GGPlayIcon from '../../../images/ggplay.png';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';

FooterMobile.propTypes = {

};

function FooterMobile(props) {
    const checkCart = localStorage.getItem('dvg_shopping_cart');
    let currentCart;
    if (checkCart) {
        currentCart = JSON.parse(checkCart);
    }
    var pathname = window.location.pathname;
    console.log(pathname);
    const [policyMenu, setPolicyMenu] = useState([])
    const [informationMenu, setInformationMenu] = useState([])

    useEffect(() => {
        async function getRewards() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/menu`,
                params: {
                    location: 'policymenu',
                },
            }).then(function (response) {
                if (response.status === 200) {
                    setPolicyMenu(response.data);
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getRewards();
    }, [])

    useEffect(() => {
        async function getRewards() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/menu`,
                params: {
                    location: 'informationmenu',
                },
            }).then(function (response) {
                if (response.status === 200) {
                    setInformationMenu(response.data);
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getRewards();
    }, [])

    return (
        <>
            {/*{isIOS && <AppSuggestion type={"ios"} />}
            {isAndroid && <AppSuggestion type={"android"} />}*/}
            <div className={"bg-green-500 text-white"}>
                <div className={"max-w-6xl mx-auto"}>
                    <div className={"p-5"}>
                        <Row gutter={[16, 16]}>
                            <Col xs={24} lg={6} md={6}>
                                <h4 className={"font-bold text-base mb-2 mt-1 text-white uppercase"}>{props.settings && props.settings.company_name}</h4>
                                <div className={"text-white"}>
                                    <p>Hotline: <a href={`tel:${props.inviteInfo && props.inviteInfo.phone ? props.inviteInfo.phone :
                                        props.settings && props.settings.contact_hotline}`}>{props.inviteInfo && props.inviteInfo.phone ? props.inviteInfo.phone :
                                        props.settings && props.settings.contact_hotline}</a></p>
                                    <p>Email: <a href={`mailto:${props.inviteInfo && props.inviteInfo.email ? props.inviteInfo.email :
                                        props.settings && props.settings.contact_email}`}>{props.inviteInfo && props.inviteInfo.email ? props.inviteInfo.email :
                                        props.settings && props.settings.contact_email}</a></p>
                                    <p>Địa chỉ: {props.settings && props.settings.contact_address}</p>
                                </div>
                                <div className={"mt-3"}>
                                    {props.settings && props.settings.facebook &&
                                        <a className={"mr-2"} href={props.settings.facebook} target={"_blank"}>
                                            <IoLogoFacebook size={24} className={"hover:text-yellow-400"} />
                                        </a>
                                    }
                                    {props.settings && props.settings.youtube &&
                                        <a className={"mr-2"} href={props.settings.youtube} target={"_blank"}>
                                            <IoLogoYoutube size={24} className={"hover:text-yellow-400"} />
                                        </a>
                                    }
                                </div>
                            </Col>
                            <Col xs={12} lg={6} md={6}>
                                <h4 className={"font-bold text-base mb-2 mt-1 text-white uppercase"}>Chính sách</h4>
                                <div className={"text-white"}>
                                    <ul className={"list-circle pl-5"}>
                                        {policyMenu && policyMenu.length > 0 && policyMenu.map((item, index) => (
                                            <li className={"mb-2"} key={index}>
                                                <Link to={item.url} className={"hover:text-yellow-300"}>{item.title}</Link>
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                            </Col>
                            <Col xs={12} lg={6} md={6}>
                                <h4 className={"font-bold text-base mb-2 mt-1 text-white uppercase"}>Thông tin</h4>
                                <div className={"text-white"}>
                                    <ul className={"list-circle pl-5"}>
                                        {informationMenu && informationMenu.length > 0 && informationMenu.map((item, index) => (
                                            <li className={"mb-2"} key={index}>
                                                <Link to={item.url} className={"hover:text-yellow-300"}>{item.title}</Link>
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                            </Col>
                            <Col xs={24} lg={6} md={6}>
                                <h4 className={"font-bold text-base mb-2 mt-1 uppercase text-white"}>Ứng dụng di động</h4>
                                <p className={"mb-3"}>Tải ngay ứng dụng di động của {props.settings && props.settings.company_name} để sử dụng dịch vụ thuận tiện nhất!</p>
                                <a href={props.settings && props.settings.app_store} target={"_blank"} className={"mb-3"}>
                                    <img src={AppStoreIcon} className={"w-32"} alt={"App Store"}/>
                                </a>
                                <a href={props.settings && props.settings.google_play} target={"_blank"}>
                                    <img src={GGPlayIcon} className={"w-32"} alt={"Google Play"}/>
                                </a>
                            </Col>
                        </Row>
                    </div>
                </div>
            </div>
            <div className={"bg-green-600 pt-3 pb-20 px-3 text-white"}>
                <div className={"text-center"}>
                    <p>
                        &copy; 2022 - <b>{props.settings && props.settings.company_name}</b>
                    </p>
                </div>
            </div>
            <div className={"fixed bottom-20 right-5"}>
                <a href={`tel:${props.inviteInfo ? props.inviteInfo.phone : props.settings && props.settings.contact_whatsapp}`}>
                    <div className={"mb-3 bg-red-500 p-3 rounded-full border-2 border-white shadow-lg"}>
                        <IoCall size={24} className={"text-white"}/>
                    </div>
                </a>
                <a href={`https://wa.me/${props.inviteInfo ? props.inviteInfo.whatsapp : props.settings && props.settings.contact_whatsapp}`}>
                    <div className={"mb-3 bg-green-600 p-3 rounded-full border-2 border-white shadow-lg"}>
                        <SiWhatsapp size={24} className={"text-white"}/>
                    </div>
                </a>
                <a href={`https://zalo.me/${props.inviteInfo ? props.inviteInfo.zalo : props.settings && props.settings.contact_zalo}`}>
                    <div className={"bg-blue-500 p-3 rounded-full border-2 border-white shadow-lg"}>
                        <SiZalo size={24} className={"text-white"} />
                    </div>
                </a>
            </div>

                <div className={"fixed w-full pt-1 h-14 bg-white bottom-0 z-50 border-t border-gray-200"}>
                    <div className={"flex justify-between items-center my-1"}>
                        <Link to={'/'} className={"flex flex-col justify-center items-center w-1/5"}>
                            <IoIosHome size={24} className={`${pathname === '/' ? 'text-green-700' : 'text-gray-500'}`}/>
                            <p className={`text-xs ${pathname === '/' ? 'text-green-700' : 'text-gray-500'}`}>Trang chủ</p>
                        </Link>
                        <Link to={'/gio-hang'} className={"relative w-1/5"}>
                            <div className={"flex flex-col justify-center items-center"}>
                                <IoIosCart size={24} className={`${pathname === '/gio-hang' ? 'text-green-700' : 'text-gray-500'}`}/>
                                <p className={`text-xs ${pathname === '/gio-hang' ? 'text-green-700' : 'text-gray-500'}`}>Giỏ hàng</p>
                            </div>
                            {checkCart && currentCart && currentCart.items.length > 0 &&
                                <div className={"absolute top-0 right-2"}>
                                    <div className={"bg-red-600 rounded-full w-5 h-5 flex flex-col items-center"}>
                                        <span className={"text-white"}>{currentCart && currentCart.items.length}</span>
                                    </div>
                                </div>
                            }
                        </Link>
                        <Link to={'/san-pham'} className={"flex flex-col justify-center items-center w-1/5"}>
                            <IoBagCheck size={24} className={`${pathname === '/san-pham' ? 'text-green-700' : 'text-gray-500'}`}/>
                            <p className={`text-xs ${pathname === '/san-pham' ? 'text-green-700' : 'text-gray-500'}`}>Sản phẩm</p>
                        </Link>
                        <Link to={'/lien-he'} className={"flex flex-col justify-center items-center w-1/5"}>
                            <IoMail size={24} className={`${pathname === '/lien-he' ? 'text-green-700' : 'text-gray-500'}`}/>
                            <p className={`text-xs ${pathname === '/lien-he' ? 'text-green-700' : 'text-gray-500'}`}>Liên hệ</p>
                        </Link>
                        <Link to={'/taikhoan'} className={"flex flex-col justify-center items-center w-1/5"}>
                            <IoPerson size={24} className={`${pathname === '/taikhoan' ? 'text-green-700' : 'text-gray-500'}`}/>
                            <p className={`text-xs ${pathname === '/taikhoan' ? 'text-green-700' : 'text-gray-500'}`}>Tài khoản</p>
                        </Link>
                    </div>
                </div>
            </>
    );
}

export default FooterMobile;
