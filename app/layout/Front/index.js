import React, { useEffect, useState } from 'react';
import Header from './Desktop/Header';
import Footer from './Desktop/Footer';
import { useDispatch, useSelector } from 'react-redux';
import { FetchSettings } from '../../containers/App/actions/Settings';
import { GetMe } from '../../containers/Member/Auth/actions';
import HeaderMobile from './Mobile/HeaderMobile';
import { Desktop, TabletAndBelow } from '../../utils/responsive';
import FooterMobile from './Mobile/FooterMobile';
import {isIOS, isMacOs, isAndroid} from 'react-device-detect';
import AppSuggestion from '../component/appSuggestion';
import queryString from 'query-string';
import axios from 'axios';
import { AppConfig } from '../../appConfig';
import { hideLoader } from '../../containers/App/actions';
import { GetRefInfo } from '../../containers/App/actions/RefInfo';
import { useCookies } from 'react-cookie';

FrontLayout.propTypes = {

};

function FrontLayout(props) {
    var pathname = window.location.pathname;
    const settings = useSelector(state => state.root.settings);
    const currentUser = useSelector(state => state.root.currentUser.user);
    const dispatch = useDispatch();
    const [cookies, setCookie, removeCookie] = useCookies();
    const inviteInfo = cookies.dvg_invitee;

    useEffect(() => {
        async function checkLogin() {
            const userToken = localStorage.getItem('dvg_user_token');
            if (userToken) {
                dispatch(GetMe(userToken))
            }
        }
        checkLogin();
    }, [dispatch]);

    useEffect(() => {
        dispatch(FetchSettings());
    },[dispatch])

    return (
    <React.Fragment>
        <Desktop>
            <Header
                inviteInfo={inviteInfo}
                pathName={pathname}
                settings={settings && settings.options}
                currentUser={currentUser && currentUser}
            />
        </Desktop>
        {pathname === '/' &&
            <TabletAndBelow>
                <HeaderMobile inviteInfo={inviteInfo}/>
            </TabletAndBelow>
        }
        <div className={"pb-20 md:pb-0 bg-gray-50 md:bg-white"}>
            {props.children}
        </div>

        <Desktop>
            <Footer settings={settings && settings.options} inviteInfo={inviteInfo} />
        </Desktop>

        <TabletAndBelow>
            <FooterMobile settings={settings && settings.options} inviteInfo={inviteInfo} />
        </TabletAndBelow>

    </React.Fragment>
  );
}

export default FrontLayout;
