import React, { useEffect, useState } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { IoBag, IoPerson } from 'react-icons/io5';
import logo from '../../../images/logo.png';
import mienphiship from '../../../images/mienphiship.png';
import hotro247 from '../../../images/hotro247.png';
import giolamviec from '../../../images/giolamviec.png';
import { Affix } from 'antd';
import Search from 'antd/es/input/Search';
import history from '../../../utils/history';
import { hideLoader, showLoader } from '../../../containers/App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';

Header.propTypes = {};

function Header(props) {
    const [menuItems, setMenuItems] = useState([])

    useEffect(() => {
        async function getRewards() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/menu`,
                params: {
                    location: 'mainmenu',
                },
            }).then(function (response) {
                if (response.status === 200) {
                    setMenuItems(response.data);
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getRewards();
    }, [])

    const [cartItems, setCartItems] = useState([])

    useEffect(() => {
        if(typeof window !== "undefined") {
            const cartItems = localStorage.getItem('dvg_shopping_cart');
            if (cartItems) {
                setCartItems(JSON.parse(cartItems))
            }
        }
    }, [])

    const checkCart = localStorage.getItem('dvg_shopping_cart');
    let cartQuantity = 0;
    let currentCart;
    if (checkCart) {
        currentCart = JSON.parse(checkCart);
        if(currentCart.items.length > 0) {
            currentCart.items.map((el) => (
                cartQuantity += el.quantity
            ))
        }
    }

    function onSearch(value) {
        if (value && typeof value !== 'undefined') {
            history.push(`/tim-san-pham?q=${value}`)
        }
    }

    console.log(props);

    return (
        <div>
          {/*Top Bar */}
          <div className={"py-2 bg-green-600"}>
              <div className={"max-w-6xl mx-auto flex items-center justify-between text-white"}>
                  <div>
                      <span className={"mr-5"}><b>Hotline: </b>
                          <a href={`tel:${props.inviteInfo && props.inviteInfo.phone ? props.inviteInfo.phone :
                              props.settings && props.settings.contact_hotline}`}
                             className={"hover:text-white hover:underline"}
                          >
                              {props.inviteInfo && props.inviteInfo.phone ? props.inviteInfo.phone :
                                  props.settings && props.settings.contact_hotline}
                          </a>
                      </span>
                      <span><b>Địa chỉ: </b> {props.settings && props.settings.contact_address}</span>
                  </div>
                  <div>
                      {props.currentUser === null ?
                          <div>
                              <Link to={'/dang-nhap'} className={"hover:text-white hover:underline font-bold"}><IoPerson className={"-mt-1 mr-1"}/>Đăng nhập</Link>
                              <span className={"mx-2"}>hoặc</span>
                              <Link to={'/dang-ky'} className={"hover:text-white hover:underline font-bold"}>Đăng ký</Link>
                          </div>
                          : <Link to={'/taikhoan'} className={"hover:text-white hover:underline font-bold"}><IoPerson className={"-mt-1 mr-1"}/>Tài khoản của tôi</Link>}
                  </div>
              </div>
          </div>
          {/*Main header */}
          <div className={"max-w-6xl mx-auto flex items-center justify-between py-2"}>
              <div>
                  <Link to={props.currentUser !== null ? `/?ref=${props.currentUser.id}` : '/'}>
                      <img src={props.settings && AppConfig.apiUrl+props.settings.logo} alt={"Đại Việt Group"} className={"w-24"}/>
                  </Link>
              </div>
              <div className={"flex items-center justify-between"}>
                  <div className={"flex items-center"}>
                      <img src={mienphiship} className={"mr-3"} alt={"Miễn phí vận chuyển"}/>
                      <div>
                          <p className={"font-bold"}>Miễn phí vận chuyển</p>
                          <span className={"text-gray-600"}>Bán kính 100km</span>
                      </div>
                  </div>
                  <div className={"flex items-center mx-10"}>
                      <img src={hotro247} className={"mr-3"} alt={"Hỗ trợ 24/7"}/>
                      <div>
                          <p className={"font-bold"}>Hỗ trợ 24/7</p>
                          <span className={"text-gray-600"}>Hotline: <a href={`tel:${props.inviteInfo && props.inviteInfo.phone ? props.inviteInfo.phone :
                              props.settings && props.settings.contact_hotline}`} className={"hover:text-white hover:underline font-medium"}>{props.inviteInfo && props.inviteInfo.phone ? props.inviteInfo.phone :
                              props.settings && props.settings.contact_hotline}</a></span>
                      </div>
                  </div>
                  <div className={"flex items-center"}>
                      <img src={giolamviec} className={"mr-3"} alt={"Giờ làm việc"}/>
                      <div>
                          <p className={"font-bold"}>Giờ làm việc</p>
                          <span className={"text-gray-600"}>T2 - T7 Giờ hành chính</span>
                      </div>
                  </div>
              </div>
              <div>
                  <Link
                    to={'/gio-hang'}
                    className={'bg-yellow-500 text-white rounded-full px-5 py-2 hover:text-white'}
                  >
                      <IoBag size={18} className={"-mt-1"}/> Giỏ hàng <b>({cartQuantity})</b>
                  </Link>
              </div>
          </div>
            <Affix>
          {/*Bottom header */}
          <div className={"h-10 bg-green-700"}>
              <div className={"max-w-6xl mx-auto flex items-center justify-between text-white"}>
                  <div>
                      <div className={"flex items-center h-10"}>
                          {menuItems && menuItems.map((item, index) => (
                              item.type === 'Internal' ?
                                  <NavLink to={item.url} exact activeClassName="bg-blue-500" className={"uppercase h-10 pt-2 px-5 font-bold text-white hover:bg-blue-500 hover:text-white"}>
                                      {item.title}
                                  </NavLink>
                                  :
                                  <a href={item.url} target={"_blank"} className={"uppercase h-10 pt-2 px-5 font-bold text-white hover:bg-blue-500 hover:text-white"}>
                                      {item.title}
                                  </a>
                          ))}
                      </div>
                  </div>
                  <div>
                      <Search
                          placeholder="Tìm sản phẩm"
                          onSearch={onSearch}
                          className={"rounded-full w-64"}
                          style={{borderRadius: '100%'}}
                      />
                  </div>
              </div>
          </div>
            </Affix>
        </div>
    );
}

export default Header;
