import React, { useEffect, useState } from 'react';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import AppStoreIcon from '../../../images/appstore.png';
import GGPlayIcon from '../../../images/ggplay.png';
import { IoLogoFacebook, IoLogoYoutube } from 'react-icons/io5';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { SiWhatsapp, SiZalo } from 'react-icons/all';

Footer.propTypes = {

};

function Footer(props) {
    const [policyMenu, setPolicyMenu] = useState([])
    const [informationMenu, setInformationMenu] = useState([])

    useEffect(() => {
        async function getRewards() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/menu`,
                params: {
                    location: 'policymenu',
                },
            }).then(function (response) {
                if (response.status === 200) {
                    setPolicyMenu(response.data);
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getRewards();
    }, [])

    useEffect(() => {
        async function getRewards() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/menu`,
                params: {
                    location: 'informationmenu',
                },
            }).then(function (response) {
                if (response.status === 200) {
                    setInformationMenu(response.data);
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getRewards();
    }, [])
    return (
        <div>
            <div className={"bg-green-600 text-white"}>
                <div className={"max-w-6xl mx-auto"}>
                    <div className={"py-10"}>
                        <Row gutter={[16, 16]}>
                            <Col xs={12} lg={6} md={6}>
                                <h4 className={"font-bold text-base mb-2 mt-1 text-white uppercase"}>{props.settings && props.settings.company_name}</h4>
                                <div className={"text-white"}>
                                    <p>Hotline: <a href={`tel:${props.inviteInfo && props.inviteInfo.phone ? props.inviteInfo.phone :
                                        props.settings && props.settings.contact_hotline}`}>{props.inviteInfo && props.inviteInfo.phone ? props.inviteInfo.phone :
                                        props.settings && props.settings.contact_hotline}</a></p>
                                    <p>Email: <a href={`mailto:${props.inviteInfo && props.inviteInfo.email ? props.inviteInfo.email :
                                        props.settings && props.settings.contact_email}`}>{props.inviteInfo && props.inviteInfo.email ? props.inviteInfo.email :
                                        props.settings && props.settings.contact_email}</a></p>
                                    <p>Địa chỉ: {props.settings && props.settings.contact_address}</p>
                                </div>
                                <div className={"mt-3"}>
                                    {props.settings && props.settings.facebook &&
                                    <a className={"mr-2"} href={props.settings.facebook} target={"_blank"}>
                                        <IoLogoFacebook size={24} className={"hover:text-yellow-400"} />
                                    </a>
                                    }
                                    {props.settings && props.settings.youtube &&
                                    <a className={"mr-2"} href={props.settings.youtube} target={"_blank"}>
                                        <IoLogoYoutube size={24} className={"hover:text-yellow-400"} />
                                    </a>
                                    }
                                </div>
                            </Col>
                            <Col xs={12} lg={6} md={6}>
                                <h4 className={"font-bold text-base mb-2 mt-1 text-white uppercase"}>Chính sách &amp; Quy định</h4>
                                <div className={"text-white"}>
                                    <ul className={"list-circle pl-5"}>
                                        {policyMenu && policyMenu.length > 0 && policyMenu.map((item, index) => (
                                            <li className={"mb-2"} key={index}>
                                                <Link to={item.url} className={"hover:text-yellow-300"}>{item.title}</Link>
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                            </Col>
                            <Col xs={12} lg={6} md={6}>
                                <h4 className={"font-bold text-base mb-2 mt-1 text-white uppercase"}>Thông tin</h4>
                                <div className={"text-white"}>
                                    <ul className={"list-circle pl-5"}>
                                        {informationMenu && informationMenu.length > 0 && informationMenu.map((item, index) => (
                                            <li className={"mb-2"} key={index}>
                                                <Link to={item.url} className={"hover:text-yellow-300"}>{item.title}</Link>
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                            </Col>
                            <Col xs={12} lg={6} md={6}>
                                <h4 className={"font-bold text-base mb-2 mt-1 uppercase text-white"}>Ứng dụng di động</h4>
                                <p className={"mb-3"}>Tải ngay ứng dụng di động của {props.settings && props.settings.company_name} để sử dụng dịch vụ thuận tiện nhất!</p>
                                <a href={props.settings && props.settings.app_store} target={"_blank"} className={"mb-3"}>
                                    <img src={AppStoreIcon} className={"w-32"} alt={"App Store"}/>
                                </a>
                                <a href={props.settings && props.settings.google_play} target={"_blank"}>
                                    <img src={GGPlayIcon} className={"w-32"} alt={"Google Play"}/>
                                </a>
                            </Col>
                        </Row>
                    </div>
                </div>
            </div>
            <div className={"bg-green-700 py-3 text-white"}>
                <div className={"text-center"}>
                    <p>
                        &copy; 2022 - Bản quyền nội dung thuộc về <b>{props.settings && props.settings.company_name}</b>
                    </p>
                </div>
            </div>
            <div className={"fixed bottom-5 left-5"}>
                <a href={`tel:${props.inviteInfo && props.inviteInfo.phone ? props.inviteInfo.phone : props.settings && props.settings.contact_hotline}`}>
                    <div className="coccoc-alo-phone coccoc-alo-green coccoc-alo-show">
                        <div className="coccoc-alo-ph-circle"></div>
                        <div className="coccoc-alo-ph-circle-fill"></div>
                        <div className="coccoc-alo-ph-img-circle"></div>
                    </div>
                </a>

            </div>
            <div className={"fixed bottom-14 right-14"}>
                <a href={`https://wa.me/${props.inviteInfo ? props.inviteInfo.whatsapp : props.settings && props.settings.contact_whatsapp}`}>
                    <div className={"mb-3 bg-green-600 p-3 rounded-full border-2 border-white shadow-lg"}>
                        <SiWhatsapp size={32} className={"text-white"}/>
                    </div>
                </a>
                <a href={`https://zalo.me/${props.inviteInfo ? props.inviteInfo.zalo : props.settings && props.settings.contact_zalo}`}>
                    <div className={"bg-blue-500 p-3 rounded-full border-2 border-white shadow-lg"}>
                        <SiZalo size={32} className={"text-white"} />
                    </div>
                </a>
            </div>
        </div>

    );
}

export default Footer;
