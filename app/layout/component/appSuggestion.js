import React, {useState} from 'react';
import PropTypes from 'prop-types';
import Button from 'antd/es/button';
import { useDispatch } from 'react-redux';
import { Modal } from 'antd';
import AppStoreIcon from '../../images/appstore.png';
import GGStoreIcon from '../../images/ggplay.png';

AppSuggestion.propTypes = {

};

function AppSuggestion(props) {
    const dispatch = useDispatch();
    localStorage.setItem('app_suggestion', true);
    const [visible, setVisible] = useState(localStorage.getItem('app_suggestion'));

    function handleCancel() {
        setVisible(!visible)
        localStorage.setItem("app_suggestion", false);
    }

    return (
        <Modal
            visible={visible}
            onCancel={handleCancel}
            footer={false}
            destroyOnClose
            forceRender
            maskClosable={true}
        >
            <div>
                <p className={"mb-3"}>Cài đặt và sử dụng <strong>App BookBook</strong> trên {props.type === 'ios' ? 'iOS' : 'Android'} để có trải nghiệm tốt nhất!</p>
                <div className={"flex flex-col items-center text-center"}>
                    <img src={props.type === 'ios' ? AppStoreIcon : GGStoreIcon} alt={"store icon"} className={"w-52"}/>
                    <span className={"mt-2 text-gray-600"}>hoặc</span>
                    <Button
                        type={"link"}
                        className={"text-red-500 font-medium"}
                        onClick={handleCancel}
                    >
                        Tiếp tục sử dụng trình duyệt
                    </Button>
                </div>
            </div>
        </Modal>
    );
}

export default AppSuggestion;
