import React, {Fragment, lazy, Suspense} from 'react';
import {Redirect, Route, Switch} from 'react-router-dom';
import Loading from "../components/Loading";
import {useSelector} from "react-redux";
import {checkRole} from "../utils/helpers";
import FrontLayout from './Front';
import { adminGuest, userGuest } from '../guards/GuestGuard';
import { adminGuard, userGuard } from '../guards/AuthGuard';
import AuthLayout from './AuthLayout';
import AdminLayout from './Admin';

const routesConfig = [
    {
        exact: true,
        path: '/admin/login',
        guard: adminGuest,
        layout: AuthLayout,
        component: lazy(() => import('../containers/Admin/Auth/Login')),
    },
    {
        exact: true,
        path: '/admin/forgot-pass',
        guard: adminGuest,
        layout: AuthLayout,
        component: lazy(() => import('../containers/Admin/Auth/Forgot')),
    },
    {
        exact: true,
        path: '/admin/resetpass',
        guard: adminGuest,
        layout: AuthLayout,
        component: lazy(() => import('../containers/Admin/Auth/ResetPassword')),
    },
    {
        path: '/admin',
        guard: adminGuard,
        layout: AdminLayout,
        routes: [
            {
                exact: true,
                path: '/admin',
                component: () => <Redirect to="/admin/dashboard" />,
            },
            {
                exact: true,
                path: '/admin/dashboard',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Dashboard'))
            },
            {
                exact: true,
                path: '/admin/contact',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Contact'))
            },
            {
                exact: true,
                path: '/admin/investmentstats',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Dashboard/investmentStatistic'))
            },
            {
                exact: true,
                path: '/admin/account',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Auth/Account'))
            },
            {
                exact: true,
                path: '/admin/products/category',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Product/Category'))
            },
            {
                exact: true,
                path: '/admin/products/category/:id',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Product/Category/CategoryDetails'))
            },
            {
                exact: true,
                path: '/admin/products',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Product'))
            },
            {
                exact: true,
                path: '/admin/products/add',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Product/AddProduct'))
            },
            {
                exact: true,
                path: '/admin/products/edit/:id',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Product/EditProduct'))
            },
            {
                exact: true,
                path: '/admin/products/edit/:id/:priceId/stock',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Product/ProductStock'))
            },
            {
                exact: true,
                path: '/admin/orders',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Orders'))
            },
            {
                exact: true,
                path: '/admin/orders/pending',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Orders/PendingOrderList'))
            },
            {
                exact: true,
                path: '/admin/orders/approved',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Orders/ApprovedOrderList'))
            },
            {
                exact: true,
                path: '/admin/orders/canceled',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Orders/CanceledOrderList'))
            },
            {
                exact: true,
                path: '/admin/orders/canceled-statistic',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Orders/CanceledOrderStatistic'))
            },
            {
                exact: true,
                path: '/admin/order/:id',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Orders/OrderDetails'))
            },
            {
                exact: true,
                path: '/admin/transactions',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Transactions'))
            },
            {
                exact: true,
                path: '/admin/transaction/:id',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Transactions/TransactionDetails'))
            },
            {
                exact: true,
                path: '/admin/transactions/type/investment',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Transactions/InvestmentTransactions'))
            },
            {
                exact: true,
                path: '/admin/transactions/type/withdraw',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Transactions/WithdrawTransactions'))
            },
            {
                exact: true,
                path: '/admin/pages',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Page'))
            },
            {
                exact: true,
                path: '/admin/page/add',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Page/PageAdd'))
            },
            {
                exact: true,
                path: '/admin/page/edit/:id',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Page/PageDetails'))
            },
            {
                exact: true,
                path: '/admin/settings/website',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Settings/WebsiteSettings'))
            },
            {
                exact: true,
                path: '/admin/settings/affiliate',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Settings/AffiliateSettings'))
            },
            {
                exact: true,
                path: '/admin/settings/discounts',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Settings/DiscountSettings'))
            },
            {
                exact: true,
                path: '/admin/settings/upgradelevel',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Settings/UpgradeLevelSettings'))
            },
            {
                exact: true,
                path: '/admin/settings/reward',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Settings/RewardSettings'))
            },
            {
                exact: true,
                path: '/admin/settings/display',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Settings/ThemeSettings'))
            },
            {
                exact: true,
                path: '/admin/employee',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Employee'))
            },
            {
                exact: true,
                path: '/admin/employee/add',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Employee/AddEmployee'))
            },
            {
                exact: true,
                path: '/admin/employee/edit/:id',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Employee/EmployeeDetails'))
            },
            {
                exact: true,
                path: '/admin/posts',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Post'))
            },
            {
                exact: true,
                path: '/admin/posts/add',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Post/AddPost'))
            },
            {
                exact: true,
                path: '/admin/posts/edit/:id',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Post/EditPost'))
            },
            {
                exact: true,
                path: '/admin/posts/category',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Post/Category/CategoryListing'))
            },
            {
                exact: true,
                path: '/admin/post/category/:id',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Post/Category/CategoryDetails'))
            },
            {
                exact: true,
                path: '/admin/members',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Members'))
            },
            {
                exact: true,
                path: '/admin/members/add',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Members/AddMember'))
            },
            {
                exact: true,
                path: '/admin/members/edit/:id',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Members/EditMember'))
            },
            {
                exact: true,
                path: '/admin/members/detail/:id',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Members/MemberDetails'))
            },
            {
                exact: true,
                path: '/admin/members/tree/:id',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Members/MemberDetailTree'))
            },
            {
                exact: true,
                path: '/admin/members/wallet/stock/:id',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Members/Wallets/StockWallet'))
            },
            {
                exact: true,
                path: '/admin/members/tree',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Members/MemberTree'))
            },
            {
                exact: true,
                path: '/admin/settings/slideshow',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Settings/Slideshow'))
            },
            {
                exact: true,
                path: '/admin/settings/slideshow/edit/:id',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Settings/Slideshow/SlideshowDetails'))
            },
            {
                exact: true,
                path: '/admin/settings/feedback',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Settings/Feedback'))
            },
            {
                exact: true,
                path: '/admin/settings/feedback/edit/:id',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Settings/Feedback/FeedbackDetails'))
            },
            {
                exact: true,
                path: '/admin/stock',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Stock'))
            },
            {
                exact: true,
                path: '/admin/rewards/monthly',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Rewards/MonthlyRewardList'))
            },
            {
                exact: true,
                path: '/admin/rewards/monthly-office',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Rewards/MonthlyOfficeRewardList'))
            },
            {
                exact: true,
                path: '/admin/rewards/yearly',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Rewards/YearlyRewardList'))
            },
            {
                exact: true,
                path: '/admin/settings/menu',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Settings/MenuSettings'))
            },
            {
                exact: true,
                path: '/admin/settings/menu/:location',
                guard: adminGuard,
                component: lazy(() => import('../containers/Admin/Settings/MenuSettings/MenuBuilder'))
            },

        ]
    },
    {
        path: '/',
        layout: FrontLayout,
        routes: [
            {
                exact: true,
                path: '/404',
                component: lazy(() => import('../containers/NotFoundPage'))
            },
            {
                exact: true,
                path: '/gio-hang',
                component: lazy(() => import('../containers/Cart'))
            },
            {
                exact: true,
                path: '/thanh-toan',
                component: lazy(() => import('../containers/Checkout/CheckoutStepOne'))
            },
            {
                exact: true,
                path: '/dat-hang/:id',
                component: lazy(() => import('../containers/Checkout/CheckoutStepTwo'))
            },
            {
                exact: true,
                path: '/checkorder',
                component: lazy(() => import('../containers/CheckOrder'))
            },
            {
                exact: true,
                path: '/checkorder/:id',
                component: lazy(() => import('../containers/CheckOrder/CheckOrderResult'))
            },
            {
                exact: true,
                path: '/dang-nhap',
                guard: userGuest,
                component: lazy(() => import('../containers/Member/Auth/Login'))
            },
            {
                exact: true,
                path: '/dang-ky',
                guard: userGuest,
                component: lazy(() => import('../containers/Member/Auth/Register'))
            },
            {
                exact: true,
                path: '/quen-mat-khau',
                guard: userGuest,
                component: lazy(() => import('../containers/Member/Auth/ForgotPassword'))
            },
            {
                exact: true,
                path: '/resetpass',
                guard: userGuest,
                component: lazy(() => import('../containers/Member/Auth/ResetPassword'))
            },
            {
                exact: true,
                path: '/taikhoan',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Wallet'))
            },
            {
                exact: true,
                path: '/taikhoan/marketing',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Marketing'))
            },
            {
                exact: true,
                path: '/taikhoan/marketing/cat/:id',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Marketing/Category'))
            },
            {
                exact: true,
                path: '/taikhoan/marketing/post/:slug',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Marketing/PostDetail'))
            },
            {
                exact: true,
                path: '/taikhoan/vi',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Wallet'))
            },
            {
                exact: true,
                path: '/taikhoan/thuongthang',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Rewards/monthlyRewards'))
            },
            {
                exact: true,
                path: '/taikhoan/thuongnam',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Rewards/yearlyRewards'))
            },
            {
                exact: true,
                path: '/taikhoan/thongtin',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Account/Account'))
            },
            {
                exact: true,
                path: '/taikhoan/caidat',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Account/AccountSettings'))
            },
            {
                exact: true,
                path: '/taikhoan/don-hang',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Orders'))
            },
            {
                exact: true,
                path: '/taikhoan/don-hang-khach-le',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Orders/GuestOrders'))
            },
            {
                exact: true,
                path: '/taikhoan/don-hang-ctv',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Orders/ContributorOrders'))
            },
            {
                exact: true,
                path: '/taikhoan/don-hang/:id',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Orders/OrderDetails'))
            },
            {
                exact: true,
                path: '/taikhoan/giao-dich',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Transactions'))
            },
            {
                exact: true,
                path: '/taikhoan/vi-thuong',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Wallet/RewardWallet/RewardWallet'))
            },
            {
                exact: true,
                path: '/taikhoan/vi-tienmat',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Wallet/CashWallet/CashWallet'))
            },
            {
                exact: true,
                path: '/taikhoan/vi-tienhang',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Wallet/ProductWallet/ProductWallet'))
            },
            {
                exact: true,
                path: '/taikhoan/giao-dich/:id',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Transactions/TransactionDetails'))
            },
            {
                exact: true,
                path: '/taikhoan/dau-tu',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Investment'))
            },
            {
                exact: true,
                path: '/taikhoan/affiliate',
                guard: userGuard,
                component: lazy(() => import('../containers/Member/Affiliate'))
            },
            {
                exact: true,
                path: '/san-pham',
                component: lazy(() => import('../containers/Product'))
            },
            {
                exact: true,
                path: '/san-pham/:slug',
                component: lazy(() => import('../containers/Product/ProductDetail'))
            },
            {
                exact: true,
                path: '/tim-san-pham',
                component: lazy(() => import('../containers/Product/ProductSearchResult'))
            },
            {
                exact: true,
                path: '/danh-muc/:slug',
                component: lazy(() => import('../containers/Product/Category'))
            },
            {
                exact: true,
                path: '/lien-he',
                component: lazy(() => import('../containers/Contact'))
            },
            {
                exact: true,
                path: '/trang/:slug',
                component: lazy(() => import('../containers/Page'))
            },
            {
                exact: true,
                path: '/tin-tuc',
                component: lazy(() => import('../containers/Post'))
            },
            {
                exact: true,
                path: '/tin-tuc/:slug',
                component: lazy(() => import('../containers/Post/PostDetail'))
            },
            {
                exact: true,
                path: '/chuyen-muc/:slug',
                component: lazy(() => import('../containers/Post/PostByCategory'))
            },
            {
                exact: true,
                path: '/checkorder',
                component: lazy(() => import('../containers/CheckOrder'))
            },
            {
                exact: true,
                path: '/checkorder/:id',
                component: lazy(() => import('../containers/CheckOrder/CheckOrderResult'))
            },
            {
                exact: true,
                path: '/',
                component: lazy(() => import('../containers/HomePage')),
            },
        ],
    },
    {
        path: '/*',
        exact: true,
        layout: FrontLayout,
        component: lazy(() => import('../containers/NotFoundPage'))
    }
]

const renderRoutes = (routes, currentRole) =>
  routes ? (
    <Suspense fallback={<Loading />}>
      <Switch>
        {routes.map((route, i) => {
          const Layout = route.layout || Fragment;
          const Component = route.component || Fragment;
          const Guard = route.guard || Fragment;
          const Allow = route.allow;
          return (
            <Route
              key={i}
              path={route.path}
              exact={route.exact}
              render={props => (
                <Guard>
                  <Layout>
                    {route.routes ? (
                      renderRoutes(route.routes)
                    ) : (
                      <Component {...props} />
                    )}
                  </Layout>
                </Guard>
              )}
            />
          );
        })}
      </Switch>
    </Suspense>
  ) : null;

function Routes({currentRole}) {
  return renderRoutes(routesConfig, currentRole);
}

export default Routes;
