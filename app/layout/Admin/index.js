import React, {useEffect, useState} from 'react';
import Layout from "antd/es/layout/layout";
import DashboardHeader from "./Header";
import SidebarMenu from "./SidebarMenu";
import Routes from "./MenuItems";
import {Inner} from "./styles";
import {useDispatch, useSelector} from "react-redux";
import Loading from "../../components/Loading";
import {AppConfig} from "../../appConfig";
import axios from "axios";
import Button from "antd/es/button";
import {UpOutlined} from "@ant-design/icons";
import {IoCaretUpSharp} from "react-icons/io5";
import { Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll';
import { LoadDataAction } from '../../containers/Admin/Auth/actions';
import { FetchSettings } from '../../containers/App/actions/Settings';

const {Content} = Layout;

AdminLayout.propTypes = {

};

function AdminLayout({children}) {
    const dispatch = useDispatch();
    const [isLogin, setLogin] = useState(false);
    useEffect(() => {
        dispatch(FetchSettings());
    },[dispatch])

    useEffect(() => {
        async function checkLogin() {
            const adminToken = localStorage.getItem('dvg_manager_token');
            if (adminToken) {
                const configManager = {
                    method: 'get',
                    url: `${AppConfig.apiUrl}/auth/manager/me`,
                    headers: {Authorization: `Bearer ${adminToken}`}
                };

                const user = await axios(configManager);

                if (user.status === 200) {
                    dispatch(LoadDataAction(user));
                    setLogin(true);
                }
            }
        }

        checkLogin();
    }, [dispatch]);

    const currentUser = useSelector(state => state.root.AdminAuth.user);
    const isLoading = useSelector(state => state.root.AdminAuth.fetching);

    return (
        <>
            <Layout>
                <DashboardHeader currentUser={currentUser && currentUser.data} />
                <Layout className="mt-16">
                    <SidebarMenu
                        currentUser={currentUser && currentUser.data}
                        Routes={Routes}
                        sidebarIcons={true}
                    />
                    <Content>
                        <Inner className="content">
                            {children}
                        </Inner>
                    </Content>
                </Layout>
            </Layout>
        </>
    );
}

export default AdminLayout;
