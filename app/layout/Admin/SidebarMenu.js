import React, {useEffect, useState} from 'react';
import {Badge, Drawer, Layout, Menu} from "antd";
import {capitalize, lowercase} from "../../utils/helpers";
import {Link, withRouter} from "react-router-dom";
import {Sidebar} from "./styles";
import {useDispatch, useSelector} from "react-redux";
import {Desktop} from '../../utils/responsive';
import {hideDrawer} from "../../containers/App/actions";

const {Sider} = Layout;
const {SubMenu} = Menu;

let rootSubMenuKeys = [];

const getKey = (name, index) => {
  const string = `${name}-${index}`;
  let key = string.replace(' ', '-');
  return key.charAt(0).toLowerCase() + key.slice(1);
};

MainMenu.propTypes = {

};

function MainMenu({Routes, sidebarIcons, currentUser}) {
    const dispatch = useDispatch();
    const [openKeys, setOpenKeys] = useState([]);
    const [appRoutes] = useState(Routes);
    const pathname = window.location.pathname;
    const isVisible = useSelector(state => state.root.utility.showDrawer);

    const badgeTemplate = badge => <Badge count={badge.value} className="ml-2" />;

  useEffect(() => {
    appRoutes.forEach((route, index) => {
      const isCurrentPath =
          pathname.indexOf(lowercase(route.name)) > -1;
      const key = getKey(route.name, index);
      rootSubMenuKeys.push(key);
      if (isCurrentPath) setOpenKeys([...openKeys, key]);
    });
  }, [openKeys]);

  const onOpenChange = openKeys => {
    const latestOpenKey = openKeys.slice(-1);
    if (rootSubMenuKeys.indexOf(latestOpenKey) === -1) {
      setOpenKeys([...latestOpenKey]);
    } else {
      setOpenKeys(latestOpenKey ? [...latestOpenKey] : []);
    }
  };

  function handleCloseDrawer() {
      dispatch(hideDrawer());
  }

  const menu = (
      <>
        <Menu
            openKeys={openKeys}
            onOpenChange={onOpenChange}
        >
          {appRoutes.map((route, index) => {
            const hasChildren = !!route.children;
            if (!hasChildren)
              return (
                  <>
                      {currentUser && route.roles.includes(currentUser.roles) &&
                      <Menu.Item
                          key={getKey(route.name, index)}
                          className={
                              pathname === route.path ? 'ant-menu-item-selected' : ''
                          }
                          onClick={() => {
                              setOpenKeys([getKey(route.name, index)]);
                              handleCloseDrawer()
                          }}
                      >
                          <Link to={route.path}>
                              {sidebarIcons && (
                                  <span className="anticon">{route.icon}</span>
                              )}
                              <span className="mr-auto">{capitalize(route.name)}</span>
                              {route.badge && badgeTemplate(route.badge)}
                          </Link>
                      </Menu.Item>
                      }
                  </>
              );

            if (hasChildren)
              return (
                  <>
                      {currentUser && route.roles.includes(currentUser.roles) &&
                      <SubMenu
                          key={getKey(route.name, index)}
                          title={
                              <>
                                  {sidebarIcons && (
                                      <span className="anticon">{route.icon}</span>
                                  )}
                                  <span>{capitalize(route.name)}</span>
                              </>
                          }
                      >
                          {route.children.map((subitem, index) => (
                              <>
                                  {currentUser && subitem.roles.includes(currentUser.roles) &&
                                  <Menu.Item
                                      key={getKey(subitem.name, index)}
                                      className={
                                          pathname === subitem.path ? 'ant-menu-item-selected' : ''
                                      }
                                      onClick={handleCloseDrawer}
                                  >
                                      <Link to={`${subitem.path ? subitem.path : ''}`}>
                                        <span className="mr-auto">
                                          {capitalize(subitem.name)}
                                        </span>
                                      </Link>
                                  </Menu.Item>
                                  }
                              </>
                          ))}
                      </SubMenu>
                      }
                  </>
              );
          })}
        </Menu>
      </>
  )

  return (
      <>
        <Sidebar>
            <Desktop>
                <Sider
                    style={{
                        overflow: "auto",
                        height: "100vh",
                        position: "fixed",
                        left: 0,
                        paddingTop: 15
                    }}
                    theme="light"
                    width={240}
                >
                    {menu}
                </Sider>
            </Desktop>
          <Drawer
              closable={false}
              width={240}
              placement="left"
              onClose={handleCloseDrawer}
              visible={isVisible}
              destroyOnClose
              className="chat-drawer"
          >
              <Sidebar>
                  <div
                      style={{
                          overFlow: 'hidden',
                          flex: '1 1 auto',
                          flexDirection: 'column',
                          display: 'flex',
                          height: '100vh',
                          maxHeight: '-webkit-fill-available'
                      }}
                  >
                    {menu}
                  </div>
              </Sidebar>
          </Drawer>
        </Sidebar>
      </>
  );
}

export default withRouter(MainMenu);
