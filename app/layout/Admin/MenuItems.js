import React from 'react';
import {
    IoBagCheck,
    IoBarChart,
    IoCube,
    IoDocumentText,
    IoPeople,
    IoPerson,
    IoReader,
    IoReceipt,
    IoSettings,
    IoShapes,
    IoShieldSharp,
    IoMail
} from 'react-icons/all';
import { IoGift } from 'react-icons/io5';

export default [
    {
        path: '/admin/dashboard',
        name: 'Thống kê',
        icon: <IoBarChart size={18} className="text-gray-600"/>,
        roles: ['administrator', 'marketer', 'accountant'],
        children: [
            {
                path: '/admin/dashboard',
                name: 'Thống kê bán hàng',
                roles: ['administrator', 'marketer', 'accountant'],
            },
            {
                path: '/admin/investmentstats',
                name: 'Thống kê đầu tư',
                roles: ['administrator', 'marketer', 'accountant'],
            },
        ]
    },
    {
        path: '/admin/orders',
        name: 'Quản lý Đơn hàng',
        icon: <IoBagCheck size={18} className="text-gray-600"/>,
        roles: ['administrator', 'accountant'],
        children: [
            {
                path: '/admin/orders/pending',
                name: 'Đơn hàng chờ duyệt',
                roles: ['administrator', 'marketer', 'accountant'],
            },
            {
                path: '/admin/orders/approved',
                name: 'Đơn hàng đã duyệt',
                roles: ['administrator', 'marketer', 'accountant'],
            },
            {
                path: '/admin/orders/canceled',
                name: 'Đơn hàng đã huỷ',
                roles: ['administrator', 'marketer', 'accountant'],
            },
            {
                path: '/admin/orders/canceled-statistic',
                name: 'Thống kê Huỷ đơn',
                roles: ['administrator', 'marketer', 'accountant'],
            },
        ]
    },
    {
        path: '/admin/products',
        name: 'Quản lý Sản phẩm',
        icon: <IoShapes size={18} className="text-gray-600"/>,
        roles: ['administrator', 'marketer', 'accountant'],
        children: [
            {
                path: '/admin/products',
                name: 'Tất cả sản phẩm',
                roles: ['administrator', 'marketer', 'accountant'],
            },
            {
                path: '/admin/products/add',
                name: 'Thêm sản phẩm',
                roles: ['administrator', 'marketer', 'accountant'],
            },
            {
                path: '/admin/products/category',
                name: 'Danh mục sản phẩm',
                roles: ['administrator', 'marketer', 'accountant'],
            },
        ]
    },
    {
        path: '/admin/stock',
        name: 'Quản lý Kho hàng',
        icon: <IoCube size={18} className="text-gray-600"/>,
        roles: ['administrator', 'accountant']
    },
    {
        path: '/admin/members',
        name: 'Quản lý thành viên',
        icon: <IoPeople size={18} className="text-gray-600"/>,
        roles: ['administrator'],
    },
    {
        path: '/admin/transactions',
        name: 'Quản lý Giao dịch',
        icon: <IoReceipt size={18} className="text-gray-600"/>,
        roles: ['administrator', 'accountant'],
        children: [
            {
                path: '/admin/transactions',
                name: 'Tất cả giao dịch',
                roles: ['administrator', 'accountant'],
            },
            {
                path: '/admin/transactions/type/withdraw',
                name: 'Giao dịch Rút tiền mặt',
                roles: ['administrator', 'accountant'],
            },
            {
                path: '/admin/transactions/type/investment',
                name: 'Giao dịch cổ phần',
                roles: ['administrator', 'accountant'],
            },
        ]
    },
    {
        path: '/admin/rewards',
        name: 'Thưởng tháng, năm',
        icon: <IoGift size={18} className="text-gray-600"/>,
        roles: ['administrator', 'accountant'],
        children: [
            {
                path: '/admin/rewards/monthly',
                name: 'Danh sách thưởng tháng',
                roles: ['administrator', 'accountant'],
            },
            {
                path: '/admin/rewards/monthly-office',
                name: 'DS thưởng tháng Văn phòng',
                roles: ['administrator', 'accountant'],
            },
            {
                path: '/admin/rewards/yearly',
                name: 'Danh sách thưởng năm',
                roles: ['administrator', 'accountant'],
            },
        ]
    },
    {
        path: '/admin/employee',
        name: 'Quản trị viên',
        icon: <IoShieldSharp size={18} className="text-gray-600"/>,
        roles: ['administrator']
    },
    {
        path: '/admin/posts',
        name: 'Quản lý Tin tức',
        icon: <IoDocumentText size={18} className="text-gray-600"/>,
        roles: ['administrator', 'marketer'],
        children: [
            {
                path: '/admin/posts',
                name: 'Tất cả bài viết',
                roles: ['administrator', 'marketer'],
            },
            {
                path: '/admin/posts/add',
                name: 'Thêm bài viết',
                roles: ['administrator', 'marketer'],
            },
            {
                path: '/admin/posts/category',
                name: 'Danh mục bài viết',
                roles: ['administrator', 'marketer'],
            },
        ]
    },
    {
        path: '/admin/pages',
        name: 'Quản lý Trang',
        icon: <IoReader size={18} className="text-gray-600"/>,
        roles: ['administrator', 'marketer']
    },
    {
        path: '/admin/contact',
        name: 'Quản lý Liên hệ',
        icon: <IoMail size={18} className="text-gray-600"/>,
        roles: ['administrator', 'marketer']
    },
    {
        path: '/admin/settings',
        name: 'Cài đặt Website',
        icon: <IoSettings size={18} className="text-gray-600"/>,
        roles: ['administrator', 'marketer', 'accountant'],
        children: [
            {
                path: '/admin/settings/website',
                name: 'Cấu hình chung',
                roles: ['administrator', 'marketer', 'accountant'],
            },
            {
                path: '/admin/settings/affiliate',
                name: 'Cấu hình hệ thống Affiliate',
                roles: ['administrator', 'accountant'],
            },
            {
                path: '/admin/settings/upgradelevel',
                name: 'Cấu hình lên cấp',
                roles: ['administrator', 'accountant'],
            },
            {
                path: '/admin/settings/discounts',
                name: 'Cấu hình chiết khấu',
                roles: ['administrator', 'accountant'],
            },
            {
                path: '/admin/settings/reward',
                name: 'Cấu hình thưởng Doanh số',
                roles: ['administrator', 'accountant'],
            },
            {
                path: '/admin/settings/display',
                name: 'Cấu hình Hiển thị',
                roles: ['administrator', 'marketer', 'accountant'],
            },
            {
                path: '/admin/settings/menu',
                name: 'Cài đặt menu',
                roles: ['administrator', 'marketer', 'accountant'],
            },
            {
                path: '/admin/settings/slideshow',
                name: 'Cấu hình Slideshow',
                roles: ['administrator', 'marketer', 'accountant'],
            },
            {
                path: '/admin/settings/feedback',
                name: 'Cấu hình Feedback',
                roles: ['administrator', 'marketer', 'accountant'],
            },
        ]
    },
    {
        path: '/admin/account',
        name: 'Tài khoản',
        icon: <IoPerson size={18} className="text-gray-600"/>,
        roles: ['administrator', 'marketer', 'accountant'],
    },
];
