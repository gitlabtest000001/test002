import React from 'react';
import PropTypes from 'prop-types';
import {Link, Link as RouterLink} from 'react-router-dom';
import LogoutOutlined from "@ant-design/icons/lib/icons/LogoutOutlined";
import Menu from "antd/es/menu";
import {DashHeader} from "./styles";
import Layout from "antd/es/layout";
import Avatar from "antd/es/avatar";
import {Modal} from "antd";
import UserOutlined from "@ant-design/icons/lib/icons/UserOutlined";
import Dropdown from "antd/es/dropdown";
import {useDispatch} from "react-redux";
import { IoMenu, IoShieldCheckmark } from 'react-icons/io5';
import {showDrawer} from "../../containers/App/actions";
import Badge from "antd/es/badge";
import NotificationOutlined from "@ant-design/icons/lib/icons/NotificationOutlined";
import {BellFilled, BellTwoTone} from "@ant-design/icons";
import DownOutlined from "@ant-design/icons/lib/icons/DownOutlined";
import List from "antd/es/list";
import {Desktop} from "../../utils/responsive";
import { adminLogout } from '../../containers/Admin/Auth/actions';
import Logo from '../../images/logo.png'

const {confirm} = Modal;
const {Header} = Layout;

DashboardHeader.propTypes = {

};

function DashboardHeader(props) {
    const userData = props.currentUser;
    const dispatch = useDispatch();

    function showConfirm() {
        confirm({
            title: 'Bạn có chắc chắn muốn thoát tài khoản?',
            icon: <LogoutOutlined />,
            onOk() {
                dispatch(adminLogout())
            },
        });
    }

    function handleShowDrawer() {
        dispatch(showDrawer());
    }

    const menu = (
        <Menu>
            <Menu.Item>
                <RouterLink to="/admin/account">Tài khoản của tôi</RouterLink>
            </Menu.Item>
            <Menu.Item
                danger
                onClick={showConfirm}
            >
                Đăng xuất
            </Menu.Item>
        </Menu>
    );

    return (
        <DashHeader>
            <Header style={{ position: 'fixed', zIndex: 99, width: '100%' }}>
                <a
                    className="lg:hidden mr-5"
                    onClick={handleShowDrawer}
                >
                    <IoMenu size={20} strokeWidth={2} />
                </a>
                <RouterLink to="/">
                    {/*<img src={Logo} className={"w-16"}/> */}<IoShieldCheckmark size={20} className={"-mt-1 mr-2 text-green-700"} /> <span className={"text-orange-500 uppercase font-bold"}>Quản trị</span>
                </RouterLink>
                <span className="mr-auto" />
                <Dropdown overlay={menu} placement="bottomRight" arrow>
                    <a className="ant-dropdown-link" onClick={e => e.preventDefault()}>
                        <span className="font-medium mr-3">{userData && userData.name}</span>
                        <DownOutlined />
                    </a>
                </Dropdown>
            </Header>
        </DashHeader>
    );
}

export default DashboardHeader;
