import styled from 'styled-components';

export const DashHeader = styled.div`
  .ant-layout-header {
    position: relative;
    flex-direction: row;
    flex-wrap: nowrap;
    display: flex;
    align-items: center;
    min-height: 4.286rem;
    z-index: 11;
    padding: 0 1rem;
    box-shadow: 0 2px 2px rgba(0, 0, 0, 0.02), 0 1px 0 rgba(0, 0, 0, 0.02);
    background: #fff
  }
  .trigger {
    transform: rotate(90deg);
    margin-right: 1rem;
  }
  .menu-divider {
    position: relative;
  }
  .menu-divider:before {
    position: absolute;
    top: 0;
    left: 0;
    display: inline-block;
    width: 1px;
    height: 100%;
    content: '';
    background-color: #f9f9f9;
  }
  .brand {
    display: flex;
    align-items: center;
    margin-right: 1rem;
    font-size: 1.25rem;
    white-space: nowrap;
  }
  .brand > svg {
    fill: ${props => props.theme.primaryColor};
  }
  .ant-menu {
    font-family: inherit;
    line-height: inherit;
    box-shadow: none;
    display: inline-block;
    border: 0;
    margin-bottom: 1px;
  }
  .nav-link {
    display: initial;
    color: inherit;
  }
  .ant-list-header,
  .ant-list-footer {
    padding-left: 1rem;
    padding-right: 1rem;
  }
`;

export const Notification = styled.div`
  .ant-list-item {
    padding-left: 1rem;
    padding-right: 1rem;
  }
`;

export const Inner = styled.div`
  margin: 0 auto;
  padding: 1.5rem;
  position: relative;
  background-color: #f0f0f0;
  min-height: calc(100vh - 64px);
  @media (min-width: 992px) {
    &.content {
      margin-left: 240px;
    }
  }
`;

export const Sidebar = styled.div`
  height: 100%;
  display: flex;
  .ant-layout-sider {
    box-shadow: 0 0px 3px rgba(0, 0, 0, 0.02), 0 0 1px rgba(0, 0, 0, 0.05);
    z-index: 10;
  }
  .ant-menu-item > a {
    display: flex;
    align-items: center;
  }
  .ant-menu-item .anticon,
  .ant-menu-submenu-title .anticon {
    margin-right: 1rem;
  }
  .ant-layout-sider-zero-width-trigger {
    z-index: 9;
  }
  .ant-menu-inline-collapsed .ant-badge {
    max-width: 0;
    display: inline-block;
    opacity: 0;
  }
  .ant-menu-inline .ant-menu-item, .ant-menu-inline .ant-menu-submenu-title {
    width: calc(100%);
  }
`;
