import React from 'react';
import queryString from 'query-string';
import { Affix, Button, Form, Input } from 'antd';
import axios from 'axios';
import { AppConfig } from '../../appConfig';
import { hideLoader, showLoader } from '../App/actions';
import { useDispatch } from 'react-redux';
import { ErrorMessage } from '../../components/Message';
import history from '../../utils/history';
import { IoArrowBack } from 'react-icons/io5';
import { IoBagHandle, IoHome } from 'react-icons/all';
import { TabletAndBelow } from '../../utils/responsive';

function CheckOrder(props) {
    const dispatch = useDispatch()
    let params = queryString.parse(props.location.search);
    let code = params.code;

    const initialValues = {
        orderId: typeof code !== 'undefined' ? code : null
    }

    function handleSubmit(values) {
        dispatch(showLoader())
        axios({
            method: 'get',
            url: `${AppConfig.apiUrl}/order/check`,
            params: {orderId: values.orderId, orderPhone: values.orderPhone}
        }).then(function (response) {
            if (response.status === 200) {
                console.log(response);
                dispatch(hideLoader())
                history.push(`/checkorder/${response.data.order.id}`, {
                    result: response.data
                })
            }
        }).catch(function(error){
            ErrorMessage({
                message: error.response.data.message,
            });
            dispatch(hideLoader())
        })
    }

    return (
        <>
            <TabletAndBelow>
                <Affix offsetTop={0}>
                    <div className={"bg-white border-b border-gray-100"}>
                        <div className={"h-14 flex items-center justify-between mx-3 text-center"}>
                            <IoArrowBack size={24} onClick={() => history.goBack()}/>
                            <span className={"oneLineText w-1/2 font-medium text-lg"}>Tra cứu đơn hàng</span>
                            <IoHome  size={20} onClick={() => history.push('/')}/>
                        </div>
                    </div>
                </Affix>
            </TabletAndBelow>

            <div className={"min-h-screen"}>
                <div className={"max-w-md mx-auto px-3 md:px-0 py-10"}>
                    <div className={"rounded-md shadow-lg p-5 bg-white"}>
                        <h1 className={"mb-2 text-xl font-medium text-center hidden md:block"}>
                            Tra cứu đơn hàng
                        </h1>
                        <p className={"mb-5 text-gray-400 text-center"}>
                            Kiểm tra thông tin và trạng thái của đơn hàng nhanh nhất!
                        </p>
                        <Form
                            layout={"vertical"}
                            name={"checkOrder"}
                            onFinish={handleSubmit}
                            initialValues={initialValues}
                        >
                            <Form.Item
                                label="Mã đơn hàng"
                                name="orderId"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập thông tin'
                                    },
                                ]}
                            >
                                <Input
                                    size={"large"}
                                    className={"rounded h-12"}
                                />
                            </Form.Item>
                            <Form.Item
                                label="Số điện thoại"
                                name="orderPhone"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập số điện thoại'
                                    },
                                ]}
                            >
                                <Input
                                    size={"large"}
                                    className={"rounded h-12"}
                                />
                            </Form.Item>
                            <Button
                                type="primary"
                                size="large"
                                htmlType="submit"
                                block
                                className="rounded h-12"
                            >
                                Xem thông tin
                            </Button>
                        </Form>
                    </div>

                </div>
            </div>
        </>

    );
}

export default CheckOrder;
