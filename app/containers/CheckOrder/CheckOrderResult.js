import React, { useRef } from 'react';
import { Affix, Steps } from 'antd';
import history from '../../utils/history';
import Loading from '../../components/Loading';
import Button from 'antd/es/button';
import { IoArrowBack, IoCart, IoInformationCircle, IoPeople, IoPrint, IoWallet } from 'react-icons/io5';
import { useSelector } from 'react-redux';
import ReactToPrint from 'react-to-print';
import { OrderStatus, PaymentMethod } from '../../models/order.model';
import { formatVND } from '../../utils/helpers';
import { IoHome } from 'react-icons/all';
import { TabletAndBelow } from '../../utils/responsive';

const { Step } = Steps;

CheckOrderResult.propTypes = {

};

function CheckOrderResult(props) {
    const componentRef = useRef();
    const currentUser = useSelector(state => state.root.currentUser.user);
    const settings = useSelector(state => state.root.settings.options);
    const orderCode = props.match.params.id;
    const state = props.location.state;
    if (typeof state === 'undefined') {
        history.push('/checkorder')
    }
    let result = null;
    if (state) {
        result = state.result;
        if (String(orderCode) !== String(result.order.id)) {
            history.push('/checkorder')
        }
    }

    console.log('result', result);

    return (
        !result ? <Loading /> :
            <>
                <TabletAndBelow>
                    <Affix offsetTop={0}>
                        <div className={"bg-white border-b border-gray-100"}>
                            <div className={"h-14 flex items-center justify-between mx-3 text-center"}>
                                <IoArrowBack size={24} onClick={() => history.goBack()}/>
                                <span className={"oneLineText w-1/2 font-medium text-lg"}>Đơn hàng #{result.order.id}</span>
                                <IoHome  size={20} onClick={() => history.push('/')}/>
                            </div>
                        </div>
                    </Affix>
                </TabletAndBelow>
                <div className={"bg-gray-50 py-5 min-h-screen border-t border-b border-gray-200 mx-3"}>
                    <div className={"max-w-2xl mx-auto"}>
                        <div className={"mb-5 flex justify-between items-center"}>
                            <ReactToPrint
                                trigger={() =>
                                    <Button
                                        className={"rounded"}
                                        icon={<IoPrint className={"mr-2"} />}
                                        type={"primary"}
                                    >
                                        In hoá đơn
                                    </Button>
                                }
                                content={() => componentRef.current}
                            />
                        </div>

                        <div ref={componentRef}>
                            <div className={"bg-white rounded-xl px-6 py-5 shadow-lg border-t-4 border-green-500"}>
                                <div className={"text-center"}>
                                    <div>
                                        <h1 className={"font-medium mb-2"}>Đơn hàng #{result.order.id}</h1>
                                        <p className={"text-lg font-bold"}>{OrderStatus.map((item) => (item.code === result.order.status && item.name))}</p>
                                    </div>
                                </div>
                            </div>
                            <div id={"thong-tin-dich-vu"} className={"bg-white shadow-lg p-5 rounded-xl border-t-2 border-dashed border-gray-300"}>
                                <div className={"flex justify-between items-center mb-3 border-b border-gray-200 pb-3"}>
                                    <h2 className={"text-base font-bold text-gray-600"}><IoWallet size={20} className={"-mt-1 mr-2 text-red-600"} /> Thông tin thanh toán</h2>
                                </div>
                                <div>
                                    <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                        <p className={"text-gray-500"}>Hình thức thanh toán</p>
                                        <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>{PaymentMethod.map((item, index) => (item.code === result.order.paymentMethod && item.name))}</p>
                                    </div>
                                    <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                        <p className={"text-gray-500"}>Số tiền thanh toán</p>
                                        <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>{result && formatVND(result.order.amount)}</p>
                                    </div>
                                    {result && result.order.paymentMethod === 'COD' &&
                                        <div className={"bg-gray-50 border border-gray-200 px-5 py-3"}>
                                            Thanh toán tiền khi nhận hàng.
                                        </div>
                                    }
                                    {result && result.order.paymentMethod === 'BankTransfer' &&
                                        <div className={"bg-gray-50 border border-gray-200 px-5 py-3"}>
                                            <p className={"mb-2 font-bold"}>Quý khách thanh toán theo thông tin bên dưới:</p>
                                            <p>Số tiền thanh toán: <span className={"font-bold"}>{result && formatVND(result.order.amount)}</span></p>
                                            <div dangerouslySetInnerHTML={{__html: settings && settings.payment_info_company}}/>
                                            <p>Nội dung chuyển tiền: <b>HD-{result && result.order.id}-{result && result.order.guestPhone}</b></p>
                                        </div>
                                    }
                                </div>
                            </div>
                            <div id={"thong-tin-dich-vu"} className={"bg-white shadow-lg p-5 rounded-xl border-t-2 border-dashed border-gray-300"}>
                                <div className={"flex justify-between items-center mb-3 border-b border-gray-200 pb-3"}>
                                    <h2 className={"text-base font-bold text-gray-600"}><IoCart size={20} className={"-mt-1 mr-2 text-blue-400"} /> Thông tin đơn hàng</h2>
                                </div>
                                {result && result.orderItems && result.orderItems.length > 0 &&
                                    result.orderItems.map((item, index) => (
                                        <div>
                                            <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                                <p className={"text-gray-500"}>{item.productName} - {item.productPrice.name} <span className={"font-bold"}>x {item.quantity}</span></p>
                                                <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>{formatVND(item.totalAmount)}</p>
                                            </div>
                                        </div>
                                    ))
                                }
                                <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                    <p className={"text-gray-500"}>Tạm tính</p>
                                    <p>{formatVND(result && result.order.revenue)}</p>
                                </div>
                                {Number(result && result.order.discount) > 0 &&
                                    <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                        <p className={"text-gray-500"}>Chiết khấu</p>
                                        <p className={"text-red-400"}>-{formatVND(result.order.discount)}</p>
                                    </div>
                                }
                                <div className={"flex flex-row justify-between items-center mt-2"}>
                                    <span>Tổng tiền</span>
                                    <span className={"font-bold text-lg"}>{formatVND(result && result.order.amount)}</span>
                                </div>

                            </div>
                            <div id={"thong-tin-khach"} className={"bg-white shadow-lg p-5 rounded-xl border-t-2 border-dashed border-gray-300"}>
                                <div className={"flex justify-between items-center mb-3 border-b border-gray-200 pb-3"}>
                                    <h2 className={"text-base font-bold text-gray-600"}><IoPeople size={20} className={"mr-2 text-purple-400"} /> Thông tin nhận hàng</h2>
                                </div>
                                <div className={"mb-5"}>
                                    <div id={"thong-tin-lien-he"}>
                                        <div className={"flex py-2 border-b border-gray-100"}>
                                            <p className={"w-32 mr-2 text-gray-500"}>Họ tên:</p>
                                            <p className={"font-medium text-gray-700"}>{result.order.guestName}</p>
                                        </div>
                                        <div className={"flex py-2 border-b border-gray-100"}>
                                            <p className={"w-32 mr-2 text-gray-500"}>Số điện thoại:</p>
                                            <p className={"font-medium text-gray-700"}>{result.order.guestPhone}</p>
                                        </div>
                                        <div className={"flex py-2 border-b border-gray-100"}>
                                            <p className={"w-32 mr-2 text-gray-500"}>Email:</p>
                                            <p className={"font-medium text-gray-700"}>{result.order.guestEmail}</p>
                                        </div>
                                        <div className={"flex py-2 border-b border-gray-100"}>
                                            <p className={"w-32 mr-2 text-gray-500"}>Địa chỉ:</p>
                                            <p className={"font-medium text-gray-700"}>{result.order.guestAddress ? result.order.guestAddress : <span className={"text-gray-500 font-normal"}>Chưa có</span>}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id={"thong-tin-khach"} className={"bg-white shadow-lg p-5 rounded-xl border-t-2 border-dashed border-gray-300"}>
                                <div className={"flex justify-between items-center mb-3 border-b border-gray-200 pb-3"}>
                                    <h2 className={"text-base font-bold text-gray-600"}><IoInformationCircle size={20} className={"mr-1 text-yellow-400"} /> Ghi chú cho đơn hàng</h2>
                                </div>
                                {result.order.note ?
                                    <p className={"mb-5"}>{result.order.note}</p>
                                    : <p className={"mb-5 text-gray-500"}>Không có yêu cầu khác</p>
                                }
                            </div>
                        </div>

                        <div className={"py-10 text-center"}>
                            <Button
                                type={"primary"}
                                className={"rounded-md"}
                                size={"large"}
                                onClick={() => {history.push('/checkorder')}}
                            >
                                Quay lại
                            </Button>
                        </div>
                    </div>
                </div>
            </>

    );
}

export default CheckOrderResult;
