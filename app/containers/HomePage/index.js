import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import { Link } from 'react-router-dom';
import ReactPlayer from 'react-player';
import SliderGallery from '../../components/SliderGallery';
import { hideLoader, showLoader } from '../App/actions';
import axios from 'axios';
import { AppConfig } from '../../appConfig';
import ProductCategoryWidget from './Widgets/ProductCategory';
import Loading from '../../components/Loading';
import NewsItem from '../../components/NewsItem';
import { Desktop, TabletAndBelow } from '../../utils/responsive';
import logo from '../../images/logo.png';
import queryString from 'query-string';
import CategoryBlock from './Widgets/CategoryBlock';
import FeedbackHorizontalList from '../../components/FeedbackHorizontalList';
import { IoArrowForward } from 'react-icons/io5';
import { Helmet } from 'react-helmet/es/Helmet';
import Search from 'antd/es/input/Search';
import history from '../../utils/history';
import Skeleton from 'react-loading-skeleton';
import { UpdateRefInfo } from '../App/actions/RefInfo';
import Cookies from 'universal-cookie';
import { useCookies } from 'react-cookie';
import MetaTags from 'react-meta-tags';

export default function HomePage(props) {
    const dispatch = useDispatch();
    const [news, setNews] = useState();
    const [feedbacks, setFeedbacks] = useState();
    const [slideshows, setSlideshows] = useState();
    const settings = useSelector(state => state.root.settings.options);
    const params = queryString.parse(props.location.search)
    const [cookies, setCookie, removeCookie] = useCookies();

    useEffect(() => {
        if (params && params.ref) {
            async function getRefInfo() {
                axios({
                    method: 'get',
                    url: `${AppConfig.apiUrl}/user/refinfo/${params.ref}`,
                }).then(function (response) {
                    if (response.status === 200) {
                        dispatch(UpdateRefInfo({ref: params.ref, invitee: response.data}))
                        setCookie('dvg_ref', params.ref, {maxAge: 3600})
                        setCookie('dvg_invitee', response.data, {maxAge: 3600})
                    }
                }).catch(function(error){
                    console.log(error);
                    removeCookie('dvg_ref')
                    removeCookie('dvg_invitee')
                })
            }
            getRefInfo();
        }
    }, [params.ref])

    useEffect(() => {
        async function getProducts() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/post`,
                params: {
                    limit: 3,
                    catId: -1,
                    status: 'ACTIVE',
                    userOnly: 'FALSE'
                },
            }).then(function (response) {
                if (response.status === 200) {
                    setNews(response.data)
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getProducts();
    },[dispatch])

    useEffect(() => {
        async function getSlideshows() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/slideshow`,
            }).then(function (response) {
                if (response.status === 200) {
                    setSlideshows(response.data)
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getSlideshows();
    },[dispatch])

    useEffect(() => {
        async function getFeedbacks() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/feedback`,
            }).then(function (response) {
                if (response.status === 200) {
                    setFeedbacks(response.data)
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getFeedbacks();
    },[dispatch])

    function onSearch(value) {
        if (value && typeof value !== 'undefined') {
            history.push(`/tim-san-pham?q=${value}`)
        }
    }

    return (
        <div>
            <Helmet>
                <meta charSet="utf-8" />
                <title>{settings && settings.seoTitle}</title>
                <meta property="og:type" content="website" />
                <meta name="description" content={settings && settings.seoDescription ? settings.seoDescription : settings && settings.website_name} />
                <meta property="og:title" content={settings && settings.seoTitle ? settings.seoTitle : settings && settings.website_name} />
                <meta property="og:description" content={settings && settings.seoDescription ? settings.seoDescription : settings && settings.website_name} />
                <meta property="og:keyword" content={settings && settings.seoKeyword ? settings.seoKeyword : settings && settings.website_name} />
                <meta property="og:image" content={settings && settings.seoImage ? AppConfig.apiUrl+settings.seoImage : settings && AppConfig.apiUrl+settings.logo} />
            </Helmet>
            <TabletAndBelow>
                <div className={"mb-5 bg-white px-3 py-2 shadow-lg"}>
                    <div className={"flex items-center justify-between"}>
                        <Link to={"/"} className={"mr-5"}>
                            <img src={logo} alt={"Đại Việt Group"} className={"w-20"}/>
                        </Link>
                        <Search
                            placeholder="Tìm sản phẩm"
                            onSearch={onSearch}
                            className={"w-full"}
                            style={{borderRadius: '100%'}}
                        />
                    </div>
                </div>

            </TabletAndBelow>

            <div className={"mx-3 md:mx-auto max-w-6xl md:py-5"}>
                {/*<Helmet>
                    <title>{settings && settings.website_name}</title>
                </Helmet>*/}
                <div className={"mb-5 md:mb-10"}>
                    <Row gutter={[16, 16]}>
                        <Desktop>
                            <Col xs={24} lg={6} md={8}>
                                <ProductCategoryWidget />
                            </Col>
                        </Desktop>
                        <Col xs={24} lg={18} md={8}>
                            <div className={"rounded-xl"}>
                                {!slideshows ? <Skeleton height={465} width={860} className={"rounded-xl"} /> :
                                    <SliderGallery gallery={slideshows && slideshows}/>
                                }
                            </div>
                        </Col>
                    </Row>
                </div>

                <div className={"mb-10"}>
                    <Row gutter={[16, 16]}>
                        <Col xs={8} lg={8} md={8}>
                            <img src={settings && AppConfig.apiUrl+settings.home_banner_1} alt={"DVG"} className={"rounded-lg"}/>
                        </Col>
                        <Col xs={8} lg={8} md={8}>
                            <img src={settings && AppConfig.apiUrl+settings.home_banner_2} alt={"DVG"} className={"rounded-lg"} />
                        </Col>
                        <Col xs={8} lg={8} md={8}>
                            <img src={settings && AppConfig.apiUrl+settings.home_banner_3} alt={"DVG"} className={"rounded-lg"} />
                        </Col>
                    </Row>
                </div>

                {settings && settings.show_home_category_1 === 'true' &&
                    <CategoryBlock catId={settings.home_category_1} />
                }

                {settings && settings.show_home_category_2 === 'true' &&
                    <CategoryBlock catId={settings.home_category_2} />
                }

                {settings && settings.show_home_category_3 === 'true' &&
                    <CategoryBlock catId={settings.home_category_3} />
                }

                {settings && settings.show_home_category_4 === 'true' &&
                    <CategoryBlock catId={settings.home_category_4} />
                }

                {settings && settings.show_home_category_5 === 'true' &&
                    <CategoryBlock catId={settings.home_category_5} />
                }

                {settings && settings.show_home_category_6 === 'true' &&
                    <CategoryBlock catId={settings.home_category_6} />
                }

                <div className={"mb-10"}>
                    <Row gutter={[32, 32]}>
                        <Col xs={24} lg={12} md={12}>
                            <ReactPlayer
                                url={settings && settings.home_video}
                                width={"100%"}
                            />
                        </Col>
                        <Col xs={24} lg={12} md={12}>
                            <div className={"text-lg text-green-800"} dangerouslySetInnerHTML={{__html: settings && settings.home_intro}}/>
                        </Col>
                    </Row>
                </div>

                <div className={"mb-10"}>
                    <img src={settings && AppConfig.apiUrl+settings.home_banner_4} alt={"DVG"} className={"rounded-lg"}/>
                </div>

                <div className={"mb-10"}>
                    <div className={"text-center mb-5"}>
                        <h2><span className={"bg-green-600 text-white uppercase font-bold px-8 py-2 rounded-full text-lg"}>Tin cập nhật</span></h2>
                        <div className={"border-t-2 border-yellow-500 -mt-3 pt-8"}>
                            <span className={"pt-10 italic"}>Tin tức cập nhật mới nhất hàng ngày dành cho bạn</span>
                        </div>
                    </div>
                    <div>
                        <Row gutter={[32, 32]}>
                            {news && news.posts.map((item, index) => (
                                <Col xs={24} lg={8} md={8}>
                                    <NewsItem item={item} key={index}/>
                                </Col>
                            ))}
                        </Row>
                    </div>
                    <div className={"mt-5 text-center"}>
                        <Link to={`/tin-tuc`} className={"text-lg px-5 pb-1 border-b-2 border-green-600"}>
                            Xem tất cả <IoArrowForward className={"-mt-1"}/>
                        </Link>
                    </div>
                </div>

                <div>
                    <div className={"text-center mb-5"}>
                        <h2><span className={"bg-green-600 text-white uppercase font-bold px-8 py-2 rounded-full text-lg"}>Phản hồi của khách hàng</span></h2>
                        <div className={"border-t-2 border-yellow-500 -mt-3 pt-8"}>
                            <span className={"pt-10 italic"}>Phản hồi của khách hàng đã và đang sử dụng sản phẩm <br/> trong suốt những năm qua</span>
                        </div>
                    </div>
                    <div>
                        {feedbacks && feedbacks && <FeedbackHorizontalList items={feedbacks} itemShow={3} />}
                    </div>
                </div>
            </div>
        </div>
    );
}
