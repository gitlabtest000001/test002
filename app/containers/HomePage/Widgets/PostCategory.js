import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { IoArrowForwardCircle } from 'react-icons/all';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import Loading from '../../../components/Loading';

function PostCategoryWidget(props) {
    const [categories, setCategories] = useState()
    var pathname = window.location.pathname;
    useEffect(() => {
        async function getCategory() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/post-category`,
                params: {
                    userOnly: 'FALSE'
                }
            }).then(function (response) {
                if (response.status === 200) {
                    setCategories(response.data)
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getCategory();
    },[])

    return (
        !categories ? <Loading /> :
        <div className={"border border-gray-200 rounded-2xl mb-5"}>
            <h2 className={"rounded-2xl bg-blue-500 px-3 py-2 text-white uppercase font-bold text-center"}>Chuyên mục tin tức</h2>
            <div className={"px-5 pt-2"}>
                <ul className={"list-none"}>
                    {categories.map((item, index) => (
                        <li className={`py-3 border-b border-gray-200 group text-gray-700`}>
                            <Link to={`/chuyen-muc/${item.slug}`} className={"block w-full"}>
                                <IoArrowForwardCircle size={20} className={`${pathname === '/chuyen-muc/'+item.slug && 'text-blue-500'} mr-2 -mt-1 group-hover:text-blue-500`}/> <span className={`${pathname === '/chuyen-muc/'+item.slug && 'text-blue-500'} font-medium group-hover:text-blue-500`}>{item.name}</span>
                            </Link>
                        </li>
                    ))}
                </ul>

            </div>
        </div>
    );
}

export default PostCategoryWidget;
