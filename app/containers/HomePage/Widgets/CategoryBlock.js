import React, { useEffect, useState } from 'react';
import Row from 'antd/es/grid/row';
import Loading from '../../../components/Loading';
import Col from 'antd/es/grid/col';
import ProductItem from '../../../components/ProductItem';
import { useDispatch } from 'react-redux';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { Link } from 'react-router-dom';
import { IoArrowForward, IoFileTrayOutline } from 'react-icons/io5';
import Skeleton from 'react-loading-skeleton';

function CategoryBlock(props) {
    const dispatch = useDispatch()
    const [category, setCategory] = useState();
    const [products, setProducts] = useState();

    useEffect(() => {
        dispatch(showLoader());
        async function getCategory() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/category/${props.catId}`,
            }).then(function (response) {
                if (response.status === 200) {
                    setCategory(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getCategory();
    },[props.catId])

    useEffect(() => {
        dispatch(showLoader());
        async function getCategory() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/product`,
                params: {
                    category: [props.catId],
                    limit: 8,
                    status: 'ACTIVE'
                }
            }).then(function (response) {
                if (response.status === 200) {
                    setProducts(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getCategory();
    },[props.catId])

    return (
        <div className={"mb-8 md:mb-16 shadow-lg rounded-br-lg rounded-bl-lg"}>
            <div className={"md:mb-5 md:flex items-center justify-between"}>
                <Link to={`/danh-muc/${category && category.detail.slug}`}>
                    <h2 className={"bg-green-600 text-white uppercase font-bold px-8 py-2 rounded-full text-base text-center cursor-pointer"}>{category && category.detail && category.detail.name}</h2>
                </Link>
                <div className={"flex items-center whitespace-nowrap overflow-x-auto pb-3 mt-3 md:pb-0 md:mt-0"}>
                    {category && category.subCategory && category.subCategory.length > 0 && category.subCategory.map((item, index) => (
                        <Link to={`/danh-muc/${item.slug}`} className={"mx-2 border inline-block border-green-600 rounded-md px-3 py-1 hover:bg-green-600 hover:text-white"}>{item.name}</Link>
                    ))}
                    <Link to={`/danh-muc/${category && category.detail && category.detail.slug}`} className={"ml-2 border bg-green-600 border-green-600 text-white rounded-md px-3 py-1 hover:bg-green-600 hover:text-white"}>
                        Xem tất cả <IoArrowForward />
                    </Link>
                </div>
            </div>
            <div className={"p-2"}>
                <Row gutter={[16, 16]}>
                    {!products ?
                        <>
                            <Col xs={24} lg={6} md={12}>
                                <div>
                                    <Skeleton height={256} width={230} />
                                    <Skeleton height={20} width={230} />
                                    <Skeleton height={20} width={230} />
                                </div>
                            </Col>
                            <Col xs={24} lg={6} md={12}>
                                <div>
                                    <Skeleton height={256} width={230} />
                                    <Skeleton height={20} width={230} />
                                    <Skeleton height={20} width={230} />
                                </div>
                            </Col>
                            <Col xs={24} lg={6} md={12}>
                                <div>
                                    <Skeleton height={256} width={230} />
                                    <Skeleton height={20} width={230} />
                                    <Skeleton height={20} width={230} />
                                </div>
                            </Col>
                            <Col xs={24} lg={6} md={12}>
                                <div>
                                    <Skeleton height={256} width={230} />
                                    <Skeleton height={20} width={230} />
                                    <Skeleton height={20} width={230} />
                                </div>
                            </Col>
                        </>
                        :
                        products && products.list.length > 0  ? products.list.map((item, index) => (
                            <Col xs={24} lg={6} md={12}>
                                <ProductItem item={item} key={index} home={true}/>
                            </Col>
                        ))
                            :
                            <div className={"text-center w-full mb-5 text-yellow-600"}>
                                <p><IoFileTrayOutline size={50} className={"text-gray-200"}/></p>
                                <p>Chưa có sản phẩm</p>
                            </div>
                    }
                </Row>
            </div>
        </div>
    );
}

export default CategoryBlock;
