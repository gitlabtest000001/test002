import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { IoArrowForwardCircle } from 'react-icons/all';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import Loading from '../../../components/Loading';
import Skeleton from 'react-loading-skeleton';

function ProductCategoryWidget(props) {
    const [categories, setCategories] = useState()
    var pathname = window.location.pathname;
    useEffect(() => {
        async function getCategory() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/category?limit=100`,
            }).then(function (response) {
                if (response.status === 200) {
                    setCategories(response.data)
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getCategory();
    },[])

    return (
        !categories ? <Skeleton height={460} width={280} /> :
        <div className={"border border-gray-200 rounded-2xl"}>
            <h2 className={"rounded-2xl bg-green-600 px-3 py-2 text-white uppercase font-bold text-center"}>Danh mục sản phẩm</h2>
            <div className={"px-5 pt-2"}>
                <ul className={"list-none"}>
                    {categories.map((item, index) => (
                        <li className={`py-3 border-b border-gray-200 group text-gray-700`}>
                            <Link to={`/danh-muc/${item.slug}`} className={"block w-full"}>
                                <IoArrowForwardCircle size={20} className={`${pathname === '/danh-muc/'+item.slug && 'text-green-600'} mr-2 -mt-1 group-hover:text-green-600`}/> <span className={`${pathname === '/danh-muc/'+item.slug && 'text-green-600'} font-medium group-hover:text-green-600`}>{item.name}</span>
                            </Link>
                        </li>
                    ))}
                </ul>

            </div>
        </div>
    );
}

export default ProductCategoryWidget;
