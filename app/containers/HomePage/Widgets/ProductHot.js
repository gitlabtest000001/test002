import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { IoArrowForwardCircle } from 'react-icons/all';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import Loading from '../../../components/Loading';
import ProductWidgetItem from '../../../components/ProductWidgetItem';
import { Skeleton } from 'antd';

function ProductHotWidget(props) {
    const [products, setProducts] = useState()
    useEffect(() => {
        async function getProduct() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/product`,
                params: {
                    limit: 5,
                    status: 'ACTIVE'
                }
            }).then(function (response) {
                if (response.status === 200) {
                    setProducts(response.data)
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getProduct();
    },[])

    return (
        !products ? <Loading /> :
        <div className={"border border-gray-200 rounded-2xl mt-5"}>
            <h2 className={"rounded-2xl bg-green-600 px-3 py-2 text-white uppercase font-bold text-center"}>Sản phẩm nổi bật</h2>
            <div className={"px-5 pt-2"}>
                <ul className={"list-none"}>
                    {products && products.list.map((item, index) => (
                        <ProductWidgetItem item={item} />
                    ))}
                </ul>

            </div>
        </div>
    );
}

export default ProductHotWidget;
