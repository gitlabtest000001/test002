/**
 * NotFoundPage
 *
 * This is the page we show when the user visits a url that doesn't have a route
 *
 */

import React from 'react';
import { FormattedMessage } from 'react-intl';

import messages from './messages';
import Button from 'antd/es/button';
import { Link } from 'react-router-dom';
import { IoArrowBack, IoBanOutline } from 'react-icons/io5';
import { Helmet } from 'react-helmet/es/Helmet';
import Affix from 'antd/es/affix';
import history from '../../utils/history';
import { IoHome } from 'react-icons/all';
import { TabletAndBelow } from '../../utils/responsive';

export default function NotFound() {
  return (
      <>
          <TabletAndBelow>
              <Affix offsetTop={0}>
                  <div className={"bg-white border-b border-gray-100"}>
                      <div className={"h-14 flex items-center justify-between mx-3 text-center"}>
                          <IoArrowBack size={24} onClick={() => history.goBack()}/>
                          <span className={"oneLineText w-1/2 font-medium text-lg"}>404</span>
                          <IoHome  size={20} onClick={() => history.push('/')}/>
                      </div>
                  </div>
              </Affix>
          </TabletAndBelow>
          <div className={"mx-auto max-w-6xl my-10"}>
              <Helmet>
                  <title>Nội dung không tồn tại!</title>
              </Helmet>
              <div className={"text-center px-3 md:px-0"}>
                  <p className={"mb-5"}>
                      <IoBanOutline size={150} className={"text-red-100"} />
                  </p>
                  <h1 className={"text-gray-600 font-bold text-3xl mb-5"}>Nội dung không tồn tại</h1>
                  <p>Trang hoặc sản phẩm bạn đang tìm kiếm không tồn tại,<br/> vui lòng quay lại trang chủ hoặc tìm kiếm theo tiêu chí khác.</p>
                  <div className={"mt-5"}>
                      <Link to={'/'} className={"border-b-2 border-green-600 px-5 py-2 font-medium"}>
                          <IoArrowBack className={"mr-2 -mt-1"}/> Quay lại trang chủ
                      </Link>
                  </div>
              </div>
          </div>
      </>

  );
}
