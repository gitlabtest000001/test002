import * as constants from './constants';

const inititalState = {
    showDrawer: false,
    showInfo: false,
    showModal: false,
    component: null,
    loading: false,
    modalWidth: 520,
    showCart: false,
};

const utility = (state = inititalState, action) => {
    switch (action.type) {
        case constants.SHOW_DRAWER: {
            return {
                ...state,
                showDrawer: true,
            }
        }
        case constants.HIDE_DRAWER: {
            return {
                ...state,
                showDrawer: false,
            }
        }
        case constants.SHOW_CART: {
            return {
                ...state,
                showCart: true,
            }
        }
        case constants.HIDE_CART: {
            return {
                ...state,
                showCart: false,
            }
        }
        case constants.SHOW_INFO: {
            return {
                ...state,
                showInfo: true,
            }
        }
        case constants.HIDE_INFO: {
            return {
                ...state,
                showInfo: false,
            }
        }
        case constants.CHANGE_INFO_TITLE: {
            const {title} = action.payload;
            return {
                ...state,
                title: title,
            }
        }
        case constants.SET_MODAL_WIDTH: {
            const {width} = action.payload;
            return {
                ...state,
                modalWidth: width,
            }
        }
        case constants.CHANGE_INFO_CONTENT: {
            const {component} = action.payload;
            return {
                ...state,
                component: component,
            }
        }
        case constants.SHOW_MODAL: {
            return {
                ...state,
                showModal: true,
            }
        }
        case constants.HIDE_MODAL: {
            return {
                ...state,
                showModal: false,
            }
        }
        case constants.CHANGE_MODAL_TITLE: {
            const {title} = action.payload;
            return {
                ...state,
                title: title,
            }
        }
        case constants.CHANGE_MODAL_CONTENT: {
            const {component} = action.payload;
            return {
                ...state,
                component: component,
            }
        }

        case constants.SHOW_LOADER: {
            return {
                ...state,
                loading: true,
            }
        }
        case constants.HIDE_LOADER: {
            return {
                ...state,
                loading: false,
            }
        }

        default:
            return state;
    }
};

export default utility;
