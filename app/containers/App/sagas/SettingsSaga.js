import axios from 'axios';
import {all, call, put, takeLatest} from '@redux-saga/core/effects';
import {FetchSettingsSuccess, FetchSettingsFailed} from '../actions/Settings';
import { AppConfig } from '../../../appConfig';

async function fetchSettingsCall() {
    return axios({
        method: 'GET',
        url: `${AppConfig.apiUrl}/setting`,
    });
}

function* FetchSettings(action) {
    try {
        const response = yield call(fetchSettingsCall, action.payload);
        const {data} = response;
        yield put(FetchSettingsSuccess(data));
    } catch (e) {
        yield put(FetchSettingsFailed(e));
    }
}

export function* settingsSagas() {
    yield all([
        takeLatest('GET_SETTINGS', FetchSettings),
    ])
}
