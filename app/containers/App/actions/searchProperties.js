import * as constants from '../constants/searchProperties';

export const SetSearchQuery = (query, type) => ({
    type: constants.SET_SEARCH_QUERY,
    payload: {
        query,
        type,
    }
});

export const SetCategoryId = (id) => ({
    type: constants.SET_CATEGORY_ID,
    payload: {
        categoryId: id
    }
});

export const SetHotelId = (id) => ({
    type: constants.SET_HOTEL_ID,
    payload: {
        hotelId: id
    }
});

export const SetStartDate = (startDate) => ({
    type: constants.SET_START_DATE,
    payload: {
        startDate: startDate
    }
});

export const SetEndDate = (endDate) => ({
    type: constants.SET_END_DATE,
    payload: {
        endDate: endDate
    }
});

export const SetAdultQuantity = (quantity) => ({
    type: constants.SET_ADULT,
    payload: {
        quantity
    }
});

export const SetChildrenQuantity = (quantity) => ({
    type: constants.SET_CHILD,
    payload: {
        quantity
    }
});

export const SetBabyQuantity = (quantity) => ({
    type: constants.SET_BABY,
    payload: {
       quantity
    }
});

export const SetChildList = (childlist) => ({
    type: constants.SET_CHILD_LIST,
    payload: {
        ChildList: childlist
    }
});

export const SetRoomQuantity = (quantity) => ({
    type: constants.SET_ROOM,
    payload: {
        quantity
    }
});

export const showSetGuest = () => ({
    type: constants.SHOW_SET_GUEST,
});
export const hideSetGuest = () => ({
    type: constants.HIDE_SET_GUEST,
});
