import * as constants from '../constants/RefInfo';

export const GetRefInfo = () => ({
    type: constants.GET_REF_INFO,
});

export const UpdateRefInfo = data => ({
    type: constants.UPDATE_REF_INFO,
    payload: {
        data
    }
});
