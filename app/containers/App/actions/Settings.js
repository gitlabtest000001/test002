import * as constants from '../constants/Settings';

export const FetchSettings = () => ({
    type: constants.GET_SETTINGS,
});

export const FetchSettingsSuccess = data => ({
    type: constants.GET_SETTINGS_SUCCESS,
    payload: {
        data
    }
});

export const FetchSettingsFailed = error => ({
    type: constants.GET_SETTINGS_FAILED,
    payload: {
        error
    }
});
