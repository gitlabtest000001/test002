import { combineReducers } from 'redux';
import utility from './reducer';
import settingsReducer from './reducers/SettingsReducer';
import userAuth from '../Member/Auth/reducer';
import AdminAuth from '../Admin/Auth/reducer';
import RefReducer from './reducers/RefReducer';

const rootReducers = combineReducers({
    utility,
    settings: settingsReducer,
    currentUser: userAuth,
    AdminAuth: AdminAuth,
    ref: RefReducer
});

export default rootReducers;
