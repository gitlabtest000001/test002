/*
 * AppConstants
 * Each action has a corresponding type, which the reducer knows and picks up on.
 * To avoid weird typos between the reducer and the actions, we save them as
 * constants here. We prefix them with 'yourproject/YourComponent' so we avoid
 * reducers accidentally picking up actions they shouldn't.
 *
 * Follow this format:
 * export const YOUR_ACTION_CONSTANT = 'yourproject/YourContainer/YOUR_ACTION_CONSTANT';
 */

export const SHOW_LOADER = 'SHOW_LOADER';
export const HIDE_LOADER = 'HIDE_LOADER';

export const SHOW_DRAWER = 'SHOW_DRAWER';
export const HIDE_DRAWER = 'HIDE_DRAWER';

export const SHOW_CART = 'SHOW_CART';
export const HIDE_CART = 'HIDE_CART';

export const SET_MODAL_WIDTH = 'SET_MODAL_WIDTH';

export const SHOW_MODAL = 'SHOW_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';
export const CHANGE_MODAL_CONTENT = 'CHANGE_MODAL_CONTENT';
export const CHANGE_MODAL_TITLE = 'CHANGE_MODAL_TITLE';

export const SHOW_INFO = 'SHOW_INFO';
export const HIDE_INFO = 'HIDE_INFO';
export const CHANGE_INFO_CONTENT = 'CHANGE_INFO_CONTENT';
export const CHANGE_INFO_TITLE = 'CHANGE_INFO_TITLE';
