import {all} from 'redux-saga/effects';
import { settingsSagas } from './sagas/SettingsSaga';
import { AuthSagas } from '../Member/Auth/saga';
import { AdminAuthSagas } from '../Admin/Auth/saga';

export default function* rootSaga() {
    yield all([
        settingsSagas(),
        AuthSagas(),
        AdminAuthSagas()
    ]);
}
