import * as constants from '../constants/searchProperties';
import moment from 'moment';
import { produce } from 'immer';

const today = new Date();

const inititalState = {
    search_query: {},
    search_type: '',
    start_date: new Date(),
    end_date: new Date(today.getTime() + 86400000),
    AdultQuantity: 2,
    ChildrenQuantity: 0,
    BabyQuantity: 0,
    RoomQuantity: 1,
    ChildList: [],
    showSetGuest: false,
};

const searchReducer = (state = inititalState, action) =>
    produce(state, draft => {
        switch (action.type) {
            case constants.SET_SEARCH_QUERY: {
                return {
                    ...state,
                    search_query: action.payload.query,
                    search_type: action.payload.type,
                }
            }
            case constants.SET_CATEGORY_ID: {
                return {
                    ...state,
                    categoryId: action.payload.categoryId
                }
            }
            case constants.SET_HOTEL_ID: {
                return {
                    ...state,
                    hotelId: action.payload.hotelId
                }
            }
            case constants.SET_START_DATE: {
                return {
                    ...state,
                    start_date: action.payload.startDate
                }
            }
            case constants.SET_END_DATE: {
                return {
                    ...state,
                    end_date: action.payload.endDate
                }
            }
            case constants.SET_ADULT: {
                return {
                    ...state,
                    AdultQuantity: action.payload.quantity
                }
            }
            case constants.SET_CHILD: {
                return {
                    ...state,
                    ChildrenQuantity: action.payload.quantity
                }
            }
            case constants.SET_BABY: {
                return {
                    ...state,
                    BabyQuantity: action.payload.quantity
                }
            }
            case constants.SET_ROOM: {
                return {
                    ...state,
                    RoomQuantity: action.payload.quantity
                }
            }
            case constants.SET_CHILD_LIST: {
                return {
                    ...state,
                    ChildList: action.payload.ChildList
                }
            }
            case constants.SHOW_SET_GUEST: {
                return {
                    ...state,
                    showSetGuest: true,
                }
            }
            case constants.HIDE_SET_GUEST: {
                return {
                    ...state,
                    showSetGuest: false,
                }
            }
            default:
                return state;
        }
    })
export default searchReducer;
