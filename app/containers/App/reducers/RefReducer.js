import produce from 'immer/dist/immer';
import * as constants from '../constants/RefInfo';

export const initialState = {
    fetching: false,
    ref: null,
    invitee: null,
}

const refReducer = (state = initialState, action) =>
    produce(state, draft => {
        switch (action.type) {
            case constants.GET_REF_INFO: {
                return {
                    ...state,
                    fetching: true,
                }
            }
            case constants.UPDATE_REF_INFO: {
                const {data} = action.payload;
                return {
                    ...state,
                    fetching: false,
                    ref: data.ref,
                    invitee: data.invitee,
                }
            }
            default:
                return state;
        }
    })

export default refReducer;
