import produce from 'immer/dist/immer';
import * as constants from '../constants/Settings';

export const initialState = {
    fetching: false,
    success: false,
    error: null,
}

const settingsReducer = (state = initialState, action) =>
    produce(state, draft => {
        switch (action.type) {
            case constants.GET_SETTINGS: {
                return {
                    ...state,
                    fetching: true,
                }
            }
            case constants.GET_SETTINGS_SUCCESS: {
                const {data} = action.payload;
                return {
                    ...state,
                    fetching: false,
                    options: data,
                }
            }
            case constants.GET_SETTINGS_FAILED: {
                const {error} = action.payload;
                return {
                    ...state,
                    fetching: false,
                    error,
                }
            }
            default:
                return state;
        }
    })

export default settingsReducer;
