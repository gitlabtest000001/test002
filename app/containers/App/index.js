/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
//import { Switch, Route } from 'react-router-dom';
import {Router} from 'react-router-dom';
import {ConfigProvider} from "antd";
import moment from 'moment';
import history from '../../utils/history';
import 'moment/locale/vi';
import locale from 'antd/lib/locale/vi_VN';
import ScrollReset from "../../components/ScrollReset";

import AntInfo from "../../components/AntInfo";
import AntModal from "../../components/AntModal";
import FullPageLoading from "../../components/FullPageLoading";
import Routes from '../../layout/Routers';
import queryString from 'query-string';
import { CookiesProvider } from 'react-cookie';

moment.locale('vi');
moment.updateLocale('vi', {
  weekdays : [
    "Chủ nhật", "Thứ hai", "Thứ ba", "Thứ tư", "Thứ năm", "Thứ sáu", "Thứ bảy"
  ],
    months: [
        'Tháng 1', 'Tháng 2', 'Tháng 3', 'Tháng 4', 'Tháng 5', 'Tháng 6', 'Tháng 7', 'Tháng 8', 'Tháng 9', 'Tháng 10', 'Tháng 11', 'Tháng 12'
    ]
})

export default function App() {

    return (
    <>
      <FullPageLoading />
        <CookiesProvider>
      <ConfigProvider locale={locale}>
        <Router history={history}>
          <ScrollReset />
          <Routes />
        </Router>
        <AntInfo />
        <AntModal />
      </ConfigProvider>
        </CookiesProvider>
    </>
  );
}
