import * as constant from './constants';

export const showModal = () => ({
    type: constant.SHOW_MODAL,
});
export const hideModal = () => ({
    type: constant.HIDE_MODAL,
});

export const showCart = () => ({
    type: constant.SHOW_CART,
});
export const hideCart = () => ({
    type: constant.HIDE_CART,
});

export const changeModalContent = component => ({
    type: constant.CHANGE_MODAL_CONTENT,
    payload: {
        component,
    }
});
export const changeModalTitle = title => ({
    type: constant.CHANGE_MODAL_TITLE,
    payload: {
        title,
    }
});

export const setModalWidth = width => ({
    type: constant.SET_MODAL_WIDTH,
    payload: {
        width,
    }
});

export const showInfo = () => ({
    type: constant.SHOW_INFO,
});
export const hideInfo = () => ({
    type: constant.HIDE_INFO,
});
export const changeInfoContent = component => ({
    type: constant.CHANGE_INFO_CONTENT,
    payload: {
        component,
    }
});
export const changeInfoTitle = title => ({
    type: constant.CHANGE_INFO_TITLE,
    payload: {
        title,
    }
});

export const showDrawer = () => ({
    type: constant.SHOW_DRAWER,
});
export const hideDrawer = () => ({
    type: constant.HIDE_DRAWER,
});

export const showLoader = () => ({
    type: constant.SHOW_LOADER,
});
export const hideLoader = () => ({
    type: constant.HIDE_LOADER,
});

