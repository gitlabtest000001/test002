import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import history from '../../../utils/history';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader } from 'antd';
import PostForm from './PostForm';
import Loading from '../../../components/Loading';

function AddPost(props) {
    const dispatch = useDispatch();

    const [categoryList, setCategoryList] = useState();
    useEffect(() => {
        dispatch(showLoader());
        async function getCategory() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/post-category`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setCategoryList(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getCategory();
    },[])

    function handleSavePost(data) {
        dispatch(showLoader());
        const Token = localStorage.getItem('dvg_manager_token');
        axios({
            method: 'post',
            url: `${AppConfig.apiUrl}/manager/post`,
            data,
            headers: {Authorization: `Bearer ${Token}`}
        }).then(function (response) {
            if (response.status === 201) {
                history.push(`/admin/posts/edit/${response.data.id}`)
                dispatch(hideLoader());
            }
        }).catch(function(error){
            console.log(error);
        })
    }

    return (
        !categoryList ? <Loading /> :
        <div>
            <Helmet>
                <title>Tạo mới</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title={'Tạo bài viết'}
                className="mb-3"
                onBack={history.goBack}
            />
            <PostForm
                formType={"add"}
                categoryList={categoryList && categoryList}
                onSubmit={handleSavePost}
            />
        </div>
    );
}

export default AddPost;
