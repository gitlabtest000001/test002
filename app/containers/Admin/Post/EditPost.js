import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import history from '../../../utils/history';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader, Space } from 'antd';
import PostForm from './PostForm';
import Loading from '../../../components/Loading';
import Button from 'antd/es/button';
import { ErrorMessage, SuccessMessage } from '../../../components/Message';

function EditPost(props) {
    const dispatch = useDispatch();
    const postId = props.match.params.id;
    const [categoryList, setCategoryList] = useState();
    const [post, setPost] = useState();
    const settings = useSelector(state => state.root.settings.options);

    useEffect(() => {
        dispatch(showLoader());
        async function getCategory() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/post-category`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setCategoryList(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getCategory();
    },[])

    useEffect(() => {
        dispatch(showLoader());
        async function getCategory() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/post/${postId}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setPost(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getCategory();
    },[postId])

    function handleSavePost(data) {
        dispatch(showLoader());
        const Token = localStorage.getItem('dvg_manager_token');
        axios({
            method: 'put',
            url: `${AppConfig.apiUrl}/manager/post/${postId}`,
            data,
            headers: {Authorization: `Bearer ${Token}`}
        }).then(function (response) {
            if (response.status === 200) {
                history.push(`/admin/posts/edit/${response.data.id}`)
                dispatch(hideLoader());
                SuccessMessage({message: 'Đã lưu lại thông tin thay đổi'})
            }
        }).catch(function(error){
            console.log(error);
            ErrorMessage({message: error.data.message})
        })
    }

    function handleDeletePost() {
        dispatch(showLoader());
        const Token = localStorage.getItem('dvg_manager_token');
        axios({
            method: 'delete',
            url: `${AppConfig.apiUrl}/manager/post/${postId}`,
            headers: {Authorization: `Bearer ${Token}`}
        }).then(function (response) {
            history.push(`/admin/posts`)
            dispatch(hideLoader());
            SuccessMessage({message: 'Đã xoá bài viết'})
        }).catch(function(error){
            console.log(error);
        })
    }

    if (post) {
        var initialValues = {
            id: post.id,
            title: post.title,
            excerpt: post.excerpt,
            content: post.content,
            featureImage: post.featureImage,
            slug: post.slug,
            type: post.type,
            status: post.status,
            userOnly: post.userOnly,
            seoImage: post.seoImage,
            seoTitle: post.seoTitle,
            seoDescription: post.seoDescription,
            seoKeyword: post.seoKeyword,
            categories: post.categories.map((el) => el.id)
        }
    }

    return (
        !categoryList ? <Loading /> :
            <div>
                <Helmet>
                    <title>Sửa bài viết</title>
                </Helmet>
                <PageHeader
                    ghost={false}
                    title={'Sửa bài viết'}
                    className="mb-3"
                    onBack={() => history.push('/admin/posts')}
                    extra={
                        <Space size={"middle"}>
                            <a href={`${settings && settings.website_url}/tin-tuc/${post && post.slug}`} target={"_blank"}>
                                Xem thử
                            </a>
                            <Button
                                type={"primary"}
                                onClick={() => history.push('/admin/posts/add')}
                            >
                                Thêm mới
                            </Button>
                        </Space>
                    }
                />
                {!post ? <Loading/> :
                    <PostForm
                        initialValues={initialValues && initialValues}
                        formType={"edit"}
                        categoryList={categoryList && categoryList}
                        onEdit={handleSavePost}
                        onDelete={handleDeletePost}
                        settings={settings}
                    />
                }
            </div>
    );
}

export default EditPost;
