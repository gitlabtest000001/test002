import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import queryString from 'query-string';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { checkRole, formatDateTime } from '../../../utils/helpers';
import Tag from 'antd/es/tag';
import Space from 'antd/es/space';
import Button from 'antd/es/button';
import { EditTwoTone } from '@ant-design/icons';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader } from 'antd';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import Search from 'antd/es/input/Search';
import Table from 'antd/es/table';
import Loading from '../../../components/Loading';
import { AppConfig } from '../../../appConfig';

function PostListing(props) {
    const dispatch = useDispatch()
    let params = queryString.parse(props.location.search);
    let currentPage = params.page;
    let currentPageSize = params.limit;
    const [posts, setPosts] = useState();
    const [page, setPage] = useState(currentPage ? currentPage : 1);
    const [pageSize, setPageSize] = useState(currentPageSize ? currentPageSize : 10);
    const [searchQuery, setSearchQuery] = useState();

    useEffect(() => {
        dispatch(showLoader());
        async function getAllBookings() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/post`,
                params: {
                    catId: -1,
                    limit: pageSize,
                    page: page,
                    query: searchQuery,
                },
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setPosts(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getAllBookings();
    },[page, pageSize, searchQuery])

    function handleChangePage(page, pageSize) {
        setPage(page);
        setPageSize(pageSize);
    }
    const currentUser = useSelector(state => state.root.currentUser.user)

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Tiêu đề',
            dataIndex: 'title',
            key: 'title',
            render: (text, row) =>
                <Link to={`/admin/posts/edit/${row.id}`}>
                    <span className="text-blue-500">{text}</span>
                </Link>
        },
        {
            title: 'Ngày đăng',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: (key, row) => (
                formatDateTime(row.createdAt)
            )
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            key: 'status',
            align: 'right',
            render: (key, row) => (
                <>
                    {(() => {
                        switch(row.status) {
                            case 'ACTIVE':
                                return <Tag color="green">Đăng</Tag>;
                            case 'DEACTIVE':
                                return <Tag color="red">Nháp</Tag>;
                            default:
                                return null;
                        }
                    })()}
                </>
            ),
            filters: [
                {
                    text: 'Đăng',
                    value: 'ACTIVE',
                },
                {
                    text: 'Nháp',
                    value: 'DEACTIVE',
                },
            ],
            filterMultiple: false,
            onFilter: (value, record) => record.status.indexOf(value) === 0,
        },
        {
            title: 'Dành cho Thành viên',
            dataIndex: 'userOnly',
            key: 'userOnly',
            align: 'right',
            render: (key, row) => (
                <>
                    {(() => {
                        switch(row.userOnly) {
                            case 'TRUE':
                                return <Tag color="green">Đúng</Tag>;
                            case 'FALSE':
                                return <Tag color="red">Sai</Tag>;
                            default:
                                return null;
                        }
                    })()}
                </>
            ),
            filters: [
                {
                    text: 'Đúng',
                    value: 'TRUE',
                },
                {
                    text: 'Sai',
                    value: 'FALSE',
                },
            ],
            filterMultiple: false,
            onFilter: (value, record) => record.userOnly.indexOf(value) === 0,
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: (key, row) => (
                <Space size="middle">
                    {checkRole(currentUser.roles, ['administrator', 'marketer']) &&
                        <Link to={`/admin/posts/edit/${row.id}`}>
                            <Button type="primary" size="small" ghost icon={<EditTwoTone />}>
                                Sửa
                            </Button>
                        </Link>
                    }
                </Space>
            )
        },
    ]

    return (
        !posts ? <Loading /> :
        <div>
            <Helmet>
                <title>Bài viết</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title={'Bài viết'}
                extra={[
                    <>
                        {checkRole(currentUser.roles, ['administrator', 'marketer']) &&
                            <Link to={`/admin/posts/add`}>
                                <Button type="primary">
                                    Thêm mới
                                </Button>
                            </Link>
                        }
                    </>
                ]}
                className="mb-3"
            />
            <Row gutter={[20, 10]} className={"mb-3"}>
                <Col xs={24} lg={12} md={12}>
                    <Search
                        placeholder="Tìm kiếm..."
                        className={"w-full"}
                        onSearch={(value) => setSearchQuery(value)}
                        allowClear
                    />
                </Col>
                <Col xs={24} lg={12} md={12}>
                    <div className={"text-right sm:tex-center"}>
                        <p>Tìm thấy {posts && posts.count} bài viết</p>
                    </div>
                </Col>
            </Row>
            <Table
                scroll={{x: '100%'}}
                columns={columns}
                dataSource={posts && posts.posts}
                pagination={{
                    current: page,
                    pageSize: pageSize,
                    total: posts && posts.count,
                    onChange: ((page, pageSize) => handleChangePage(page, pageSize)),
                    position: ['bottomLeft']
                }}
            />
        </div>
    );
}

export default PostListing;
