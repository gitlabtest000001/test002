import React, { useEffect, useState } from 'react';
import {Link} from "react-router-dom";
import {Helmet} from "react-helmet/es/Helmet";
import { PageHeader, Table } from 'antd';
import Row from "antd/es/grid/Row";
import Col from "antd/es/grid/col";
import Space from "antd/es/space";
import Button from "antd/es/button";
import Popconfirm from "antd/es/popconfirm";
import CategoryForm from "./CategoryForm";
import {useDispatch, useSelector} from "react-redux";
import {QuestionCircleFilled, QuestionCircleTwoTone, QuestionOutlined} from "@ant-design/icons";
import Loading from '../../../../components/Loading';
import { hideLoader, showLoader } from '../../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';
import { ErrorMessage, SuccessMessage } from '../../../../components/Message';
import { IoCheckmarkCircle, IoRemoveCircle } from 'react-icons/all';

CategoryListing.propTypes = {

};

function CategoryListing(props) {
    const dispatch = useDispatch()
    const [categories, setCategories] = useState();
    const [flag, setFlag] = useState(true);
    useEffect(() => {
        dispatch(showLoader());
        async function getCategory() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/post-category`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setCategories(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getCategory();
    },[flag])

    console.log(categories);

    function handleAddCategory(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.post(
            `${AppConfig.apiUrl}/manager/post-category`,
            data,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Tạo danh mục thành công'});
            setCategories(categories.concat(response.data))
            dispatch(hideLoader())
            setFlag(!flag)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    function handleDeleteCategory(id) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.delete(
            `${AppConfig.apiUrl}/manager/post-category/${id}`,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Đã xoá danh mục'});
            const index = categories.findIndex(item => Number(item.id) === Number(id));
            if (index !== -1) {
                const newList = [
                    ...categories.slice(0, index),
                    response.data,
                    ...categories.slice(index + 1),
                ];
                setCategories(newList)
            }
            dispatch(hideLoader())
            setFlag(!flag)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    const columns = [
        {
            title: 'Tên',
            dataIndex: 'name',
            key: 'name',
            render: (text, row) =>
                <Link to={`/admin/post/category/${row.id}`}>
                    <span className="text-blue-500">{text}</span>
                </Link>
        },
        {
            title: 'Đường dẫn',
            dataIndex: 'slug',
            key: 'slug',
        },
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Chỉ dành cho thành viên',
            dataIndex: 'userOnly',
            key: 'userOnly',
            align: 'center',
            render: (text, row) =>
            row.userOnly === 'TRUE' ? <span><IoCheckmarkCircle className={"text-green-500"} /></span> : <span><IoRemoveCircle className={"text-red-500"}/></span>
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: (key, row) => (
                <Space size="middle">
                    <Link to={`/admin/post/category/${row.id}`}>
                        <Button type="link">
                            Sửa
                        </Button>
                    </Link>
                    <Popconfirm
                        title={`Bạn chắc chắn muốn xoá?`}
                        onConfirm={() => handleDeleteCategory(row.id)}
                        okText="Có"
                        cancelText="Không"
                    >
                        <Button danger type="link">
                            Xoá
                        </Button>
                    </Popconfirm>
                </Space>
            )
        },
    ]

    return (
        !categories ? <Loading /> :
        <>
            <Helmet>
                <title>Chuyên mục bài viết</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title="Chuyên mục bài viết"
                className="mb-3"
            />
            <Row gutter={[16, 16]}>
                <Col xs={24} lg={8} md={8}>
                    <CategoryForm formType="add" initialValues={{userOnly: 'FALSE'}} categoryList={categories && categories} onSubmit={handleAddCategory} />
                </Col>
                <Col xs={24} lg={16} md={16}>
                    <Table
                        dataSource={categories && categories}
                        columns={columns}
                        defaultExpandAllRows={true}
                    />
                </Col>
            </Row>
        </>
    );
}

export default CategoryListing;
