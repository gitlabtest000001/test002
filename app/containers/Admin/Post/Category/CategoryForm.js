import React, { useState } from 'react';
import Form from 'antd/es/form';
import Input from 'antd/es/input';
import Card from 'antd/es/card';
import { CheckOutlined, SaveOutlined } from '@ant-design/icons';
import Button from 'antd/es/button';
import AntTreeSelect from '../../../../components/AntTreeSelect';
import { useDispatch } from 'react-redux';
import SingleUpload from '../../../../components/SingleUpload';
import { Image } from 'antd';
import { AppConfig } from '../../../../appConfig';
import { changeModalContent, changeModalTitle, showModal } from '../../../App/actions';
import FileManager from '../../../../components/FileManager';
import Select from 'antd/es/select';

const {Option} = Select;

CategoryForm.propTypes = {

};

function CategoryForm(props) {
    const dispatch = useDispatch();
    const [parent, setParent] = useState(props.initialValues && props.initialValues.parent);
    const [featureImage, setFeatureImage] = useState(props.initialValues && props.initialValues.featureImage)

    function onChangeParent(value) {
        setParent(value)
    }

    function handleChooseMedia(image) {
        setFeatureImage(image)
    }

    function handleSubmit(values) {
        const body = {
            name: values.name,
            description: values.description,
            featureImage: featureImage,
            parent: parent,
            userOnly: values.userOnly
        }
        if (props.formType === 'add') {
            props.onSubmit(body)
        } else props.onSubmit(body)
    }

    function handleOpenFileMedia() {
        dispatch(showModal())
        dispatch(changeModalTitle("File Manager"))
        dispatch(changeModalContent(
            <FileManager
                onChooseFile={(value) => setFeatureImage(value[0].url)}
                multiple={false}
                manager={true}
            />
        ))
    }


    return (
        <Form
            layout="vertical"
            name="CategoryForm"
            onFinish={handleSubmit}
            initialValues={props.initialValues}
        >
            <Card
                title={props.formType === 'add' && 'Thêm chuyên mục'}
                bordered={false}
                size="small"
            >
                <Form.Item
                    label="Tên"
                    name="name"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập tiêu đề'},
                    ]}
                >
                    <Input
                        onFocus={(event) => event.target.select()}
                    />
                </Form.Item>
                {props.formType === 'edit' &&
                    <Form.Item
                        label="Đường dẫn"
                        name="slug"
                    >
                        <Input
                            onFocus={(event) => event.target.select()}
                        />
                    </Form.Item>
                }
                <Form.Item
                    label="Chuyên mục cha"
                    name="parent"
                >
                    <AntTreeSelect
                        hierarchy={props.categoryList && props.categoryList}
                        parent={parent}
                        onChange={onChangeParent}
                    />
                </Form.Item>
                <Form.Item
                    label="Mô tả"
                    name="description"
                >
                    <Input.TextArea
                        onFocus={(event) => event.target.select()}
                    />
                </Form.Item>
                <Form.Item name="userOnly" label="Chỉ dành cho thành viên">
                    <Select
                        style={{width: '100%'}}
                        placeholder="Chọn hiển thị"
                    >
                        <Option key={1} value="TRUE">Đúng</Option>
                        <Option key={2} value="FALSE">Sai</Option>
                    </Select>
                </Form.Item>

                <Form.Item>
                    {featureImage &&
                        <div className={"mb-5"}>
                            <Image src={AppConfig.apiUrl+featureImage} className={"w-24 h-24 object-cover"}/>
                        </div>
                    }
                    <Button onClick={handleOpenFileMedia}>
                        {featureImage ? 'Chọn ảnh khác' : "Chọn ảnh đại diện"}
                    </Button>
                </Form.Item>

                <Button
                    type="primary"
                    htmlType="submit"
                    icon={props.formType === 'add' ? <CheckOutlined /> : <SaveOutlined />}
                >
                    {props.formType === 'add' ? 'Thêm chuyên mục' : 'Lưu thay đổi'}
                </Button>
            </Card>

        </Form>
    );
}

export default CategoryForm;
