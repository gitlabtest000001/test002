import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import {Helmet} from "react-helmet/es/Helmet";
import {PageHeader} from "antd";
import CategoryForm from "./CategoryForm";
import Row from "antd/es/grid/Row";
import Col from "antd/es/grid/Col";
import {useDispatch, useSelector} from "react-redux";
import Loading from "../../../../components/Loading";
import axios from "axios";
import {AppConfig} from "../../../../appConfig";
import history from "../../../../utils/history";
import { hideLoader, showLoader } from '../../../App/actions';

CategoryDetails.propTypes = {

};

function CategoryDetails(props) {
    const dispatch = useDispatch();
    const id = props.match.params.id;

    const [categoryInfo, setCategoryInfo] = useState();
    const [categories, setCategories] = useState();
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        async function getCategoryInfo() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/post-category/${id}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setCategoryInfo(response.data);
                    setIsLoading(false);
                }
            }).catch(function(error){
                //history.push("/404")
            })
        }
        getCategoryInfo();
    }, [id])

    useEffect(() => {
        async function getCategoryInfo() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/post-category`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setCategories(response.data);
                    setIsLoading(false);
                }
            }).catch(function(error){
                //history.push("/404")
            })
        }
        getCategoryInfo();
    }, [id])

    console.log(categoryInfo);

    if (categoryInfo) {
        let parent;
        if (categoryInfo.parent !== null) {
            parent = categoryInfo.parent.id
        }
        var initialValues = {
            name: categoryInfo.name,
            description: categoryInfo.description,
            slug: categoryInfo.slug,
            parent: parent,
            featureImage: categoryInfo.featureImage,
            status: categoryInfo.status,
            userOnly: categoryInfo.userOnly,
        }
    }

    function handleEdit(data) {
        dispatch(showLoader());
        const Token = localStorage.getItem('dvg_manager_token');
        axios({
            method: 'put',
            url: `${AppConfig.apiUrl}/manager/post-category/${id}`,
            data,
            headers: {Authorization: `Bearer ${Token}`}
        }).then(function (response) {
            if (response.status === 200) {
                history.push(`/admin/posts/category`)
                dispatch(hideLoader());
            }
        }).catch(function(error){
            console.log(error);
            dispatch(hideLoader());
        })
    }

    return (
        !categories ? <Loading /> :
        <>
            <Helmet>
                <title>Chỉnh sửa chuyên mục</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title="Sửa chuyên mục"
                className="mb-3"
                onBack={history.goBack}
            />
            <Row gutter={[16, 16]}>
                <Col xs={24} lg={8} md={8}>
                    {!categoryInfo ? <Loading/> :
                        <CategoryForm formType="edit" categoryList={categories} initialValues={initialValues && initialValues} catId={id} onSubmit={handleEdit}/>
                    }
                </Col>
            </Row>
        </>
    );
}

export default CategoryDetails;
