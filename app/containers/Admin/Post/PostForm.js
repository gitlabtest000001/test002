import React, {useState, useEffect} from 'react';
import Form from "antd/es/form";
import Input from "antd/es/input";
import Card from "antd/es/card";
import Button from "antd/es/button";
import RichTextEditor from "../../../components/RTE";
import Col from "antd/es/grid/col";
import Row from "antd/es/grid/row";
import {CheckOutlined, SaveOutlined} from "@ant-design/icons";
import SingleUpload from "../../../components/SingleUpload";
import {useDispatch} from "react-redux";
import Select from "antd/es/select";
import AntTreeSelect from "../../../components/AntTreeSelect";
import Popconfirm from "antd/es/popconfirm";
import Affix from "antd/es/affix";
import useForm from "antd/es/form/hooks/useForm";
import {Image} from "antd";
import Checkbox from "antd/es/checkbox";
import axios from "axios";
import { changeModalContent, changeModalTitle, showModal } from '../../App/actions';
import FileManager from '../../../components/FileManager';
import { AppConfig } from '../../../appConfig';
import { IoRemoveCircleOutline } from 'react-icons/all';

const {Option} = Select;

PostForm.propTypes = {

};

function PostForm(props) {
    const [form] = useForm();
    const dispatch = useDispatch();
    const [featureImage, setFeatureImage] = useState(props.initialValues && props.initialValues.featureImage);
    const [seoImage, setSeoImage] = useState(props.initialValues && props.initialValues.seoImage);
    const [parent, setParent] = useState(props.initialValues && props.initialValues.categories);
    const [content, setContent] = useState(props.initialValues && props.initialValues.content);

    const [gallery, setGallery] = useState([])

    function onChangeParent(value) {
        setParent(value)
    }

    function handleSubmit(values) {
        const body = {
            title: values.title,
            videoUrl: values.videoUrl,
            excerpt: values.excerpt,
            content: content,
            slug: values.slug,
            featureImage: featureImage,
            status: values.status,
            seoTitle: values.seoTitle,
            seoDescription: values.seoDescription,
            seoKeyword: values.seoKeyword,
            seoImage: seoImage,
            userOnly: values.userOnly,
            categories: JSON.stringify(parent)
        }

        console.log(body);
        if (props.formType === 'add') {
            props.onSubmit(body)
        } else {
            props.onEdit(body)
        }
    }

    function handleOpenFileMediaFeature() {
        dispatch(showModal())
        dispatch(changeModalTitle("File Manager"))
        dispatch(changeModalContent(
            <FileManager
                onChooseFile={(value) => setFeatureImage(value[0].url)}
                multiple={false}
                manager={true}
            />
        ))
    }

    function handleOpenFileMedia() {
        dispatch(showModal())
        dispatch(changeModalTitle("File Manager"))
        dispatch(changeModalContent(
            <FileManager
                onChooseFile={(value) => setSeoImage(value[0].url)}
                multiple={false}
                manager={true}
            />
        ))
    }

    return (
        <div>
            <Form
                layout="vertical"
                name="PostForm"
                onFinish={handleSubmit}
                initialValues={props.initialValues}
                form={form}
            >
                <Row gutter={[16, 16]}>
                    <Col xs={24} md={17} lg={17}>
                        <Form.Item
                            name="title"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập tiêu đề'},
                            ]}
                            className="mb-3"
                        >
                            <Input
                                size="large"
                                placeHolder="Tiêu đề"
                                className="h-14 text-2xl"
                            />
                        </Form.Item>
                        {props.formType === 'edit' &&
                            <Form.Item
                                name="slug"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Liên kết tĩnh không được để trống'},
                                ]}
                                className="mb-5"
                            >
                                <Input
                                    size="small"
                                    addonBefore={`${props.settings && props.settings.website_url}/tin/`}
                                />
                            </Form.Item>
                        }
                        <Card
                            size="small"
                            bordered={false}
                            className="mb-5"
                            title="Nội dung"
                        >
                            <RichTextEditor
                                defaultValue={props.initialValues && props.initialValues.content}
                                onDataChange={(value) => setContent(value)}
                            />
                        </Card>
                        <Card
                            size="small"
                            bordered={false}
                            className={"mb-5"}
                        >
                            <Form.Item
                                label="Mô tả ngắn"
                                name="excerpt"
                                className="pb-0 mb-0"
                            >
                                <Input.TextArea/>
                            </Form.Item>
                        </Card>
                        <Card
                            size="small"
                            bordered={false}
                            title={"Cấu hình SEO"}
                        >
                            <Form.Item
                                label="Tiêu đề"
                                name="seoTitle"
                            >
                                <Input />
                            </Form.Item>
                            <Form.Item
                                label="Mô tả"
                                name="seoDescription"
                            >
                                <Input.TextArea/>
                            </Form.Item>
                            <Form.Item
                                label="Từ khoá"
                                name="seoKeyword"
                            >
                                <Input />
                            </Form.Item>
                            {seoImage &&
                                <div>
                                    <Image src={AppConfig.apiUrl+seoImage} className={"w-24 h-24 object-cover"}/>
                                </div>
                            }
                            <Button onClick={handleOpenFileMedia}>
                                {seoImage ? 'Chọn ảnh khác' : "Chọn ảnh chia sẻ"}
                            </Button>
                        </Card>
                    </Col>
                    <Col xs={24} md={7} lg={7}>
                        <Card
                            size="small"
                            bordered={false}
                            title="Ảnh đại diện"
                            className="mb-5"
                        >
                            {featureImage &&
                                <div className={"mb-5"}>
                                    <Image src={AppConfig.apiUrl+featureImage} className={"w-24 h-24 object-cover"}/>
                                </div>
                            }
                            <Button onClick={handleOpenFileMediaFeature}>
                                {featureImage ? 'Chọn ảnh khác' : "Chọn ảnh đại diện"}
                            </Button>
                        </Card>
                        <Card title={"Hàm thông tin thành viên"} size={'small'} className={"mb-5"}>
                            <div>
                                <div className={"flex items-center justify-between mb-2 border-b border-gray-100 pb-2"}>
                                    <span className={"text-red-500"}>[memberId]</span>
                                    <span>Mã thành viên</span>
                                </div>
                                <div className={"flex items-center justify-between mb-2 border-b border-gray-100 pb-2"}>
                                    <span className={"text-red-500"}>[memberName]</span>
                                    <span>Tên thành viên</span>
                                </div>
                                <div className={"flex items-center justify-between mb-2 border-b border-gray-100 pb-2"}>
                                    <span className={"text-red-500"}>[memberPhone]</span>
                                    <span>Số điện thoại của thành viên</span>
                                </div>
                                <div className={"flex items-center justify-between mb-2 border-b border-gray-100 pb-2"}>
                                    <span className={"text-red-500"}>[memberEmail]</span>
                                    <span>Email của thành viên</span>
                                </div>
                                <div className={"flex items-center justify-between mb-2 border-b border-gray-100 pb-2"}>
                                    <span className={"text-red-500"}>[memberAddress]</span>
                                    <span>Địa chỉ của thành viên</span>
                                </div>
                                <div className={"flex items-center justify-between mb-2 border-b border-gray-100 pb-2"}>
                                    <span className={"text-red-500"}>[memberRefLink]</span>
                                    <span>Link giới thiệu của thành viên</span>
                                </div>
                                <div className={"flex items-center justify-between mb-2 border-b border-gray-100 pb-2"}>
                                    <span className={"text-red-500"}>[memberZaloGroup]</span>
                                    <span>Link nhóm zalo</span>
                                </div>
                                <div className={"flex items-center justify-between mb-2 border-b border-gray-100 pb-2"}>
                                    <span className={"text-red-500"}>[memberZalo]</span>
                                    <span>Link Zalo cá nhân</span>
                                </div>
                                <div className={"flex items-center justify-between mb-2 border-b border-gray-100 pb-2"}>
                                    <span className={"text-red-500"}>[memberFacebook]</span>
                                    <span>Link Facebook</span>
                                </div>
                                <div className={"flex items-center justify-between mb-2 border-b border-gray-100 pb-2"}>
                                    <span className={"text-red-500"}>[memberWhatsapp]</span>
                                    <span>Link Whatsapp</span>
                                </div>
                                <div className={"flex items-center justify-between mb-2"}>
                                    <span className={"text-red-500"}>[memberYoutube]</span>
                                    <span>Link Youtube</span>
                                </div>
                            </div>
                        </Card>
                        <Affix offsetTop={80}>
                            <Card
                                size="small"
                                bordered={false}
                            >

                                <Form.Item
                                    label="Chuyên mục"
                                >
                                    <AntTreeSelect
                                        hierarchy={props.categoryList && props.categoryList}
                                        parent={parent}
                                        onChange={onChangeParent}
                                        multiple
                                        size="large"
                                    />
                                </Form.Item>
                                <Form.Item name="userOnly" label="Chỉ dành cho thành viên">
                                    <Select
                                        style={{width: '100%'}}
                                        placeholder="Chọn hiển thị"
                                    >
                                        <Option key={1} value="TRUE">Đúng</Option>
                                        <Option key={2} value="FALSE">Sai</Option>
                                    </Select>
                                </Form.Item>

                                {props.formType === 'edit' &&
                                    <Form.Item name="status" label="Trạng thái">
                                        <Select
                                            style={{width: '100%'}}
                                            placeholder="Chọn trạng thái"
                                        >
                                            <Option key={1} value="ACTIVE">Đăng</Option>
                                            <Option key={2} value="DEACTIVE">Nháp</Option>
                                        </Select>
                                    </Form.Item>
                                }
                                <Button
                                    type="primary"
                                    htmlType="submit"
                                    size="large"
                                    block
                                    icon={props.formType === 'add' ? <CheckOutlined /> : <SaveOutlined />}
                                >
                                    {props.formType === 'add' ? 'Xuất bản' : 'Lưu thay đổi'}
                                </Button>
                                {props.formType === 'edit' &&
                                    <Popconfirm
                                        title={`Bạn chắc chắn muốn xoá?`}
                                        onConfirm={props.onDelete}
                                        okText="Có"
                                        cancelText="Không"
                                    >
                                        <Button type="link" block danger className="mt-5" >
                                            Xoá bài viết
                                        </Button>
                                    </Popconfirm>
                                }
                            </Card>
                        </Affix>
                    </Col>
                </Row>
            </Form>
        </div>
    );
}

export default PostForm;
