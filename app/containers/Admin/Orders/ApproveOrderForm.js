import React from 'react';
import PropTypes from 'prop-types';
import Form from 'antd/es/form';
import { Select } from 'antd';
import { UserStatus } from '../../../models/member.model';
import { OrderStatus } from '../../../models/order.model';
import { Option } from 'antd/es/mentions';
import Button from 'antd/es/button';
import { CheckOutlined, SaveOutlined } from '@ant-design/icons';

ApproveOrderForm.propTypes = {

};

function ApproveOrderForm(props) {

    return (
        <Form
            layout="inline"
            name="UserForm"
            onFinish={props.onSubmit}
            initialValues={props.initialValues}
        >
            <Form.Item name="status">
                <Select
                    placeholder="Chọn trạng thái"
                >
                    {OrderStatus.map((item) =>
                        <Option value={item.code}>{item.name}</Option>
                    )}
                </Select>
            </Form.Item>
            <Button
                type="primary"
                htmlType="submit"
            >
                Xác nhận
            </Button>
        </Form>
    );
}

export default ApproveOrderForm;
