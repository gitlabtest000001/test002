import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { formatDateTime, formatDateUS, formatVND } from '../../../utils/helpers';
import Loading from '../../../components/Loading';
import Button from 'antd/es/button';
import history from '../../../utils/history';
import {
    IoArchive,
    IoArrowBack,
    IoCalendar,
    IoCart,
    IoInformation,
    IoInformationCircle,
    IoWallet,
} from 'react-icons/io5';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import { changeModalContent, changeModalTitle, hideLoader, hideModal, showLoader, showModal } from '../../App/actions';
import { Helmet } from 'react-helmet/es/Helmet';
import { message, PageHeader, Select, Space, Table } from 'antd';
import { OrderStatus, PaymentMethod } from '../../../models/order.model';
import ApproveOrderForm from './ApproveOrderForm';
import { ErrorMessage, SuccessMessage } from '../../../components/Message';
import { Link } from 'react-router-dom';
import { TransactionStatus, TransactionType } from '../../../models/transaction.model';
import { EditTwoTone } from '@ant-design/icons';
import Search from 'antd/es/input/Search';
import moment from 'moment';
import Card from 'antd/es/card';
import { MemberWallet } from '../../../models/member.model';
import Tooltip from 'antd/es/tooltip';
import { IoIosClock } from 'react-icons/all';
import Popconfirm from 'antd/es/popconfirm';
import CanceledOrderForm from './CanceledOrderForm';

function OrderDetails(props) {
    const dispatch = useDispatch();
    const id = props.match.params.id;
    const [result, setResult] = useState();
    const [flag, setFlag] = useState(true);
    const settings = useSelector(state => state.root.settings.options);

    useEffect(() => {
        dispatch(showLoader());
        async function getInfo() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/order/${id}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setResult(response.data)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                history.push('/404')
                dispatch(hideLoader())
                console.log(error);
            })
        }
        getInfo();
    }, [dispatch, id, flag])

    function handleQuickApproved(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios({
            method: 'put',
            url: `${AppConfig.apiUrl}/manager/order/approve/${data.id}`,
            headers: {Authorization: `Bearer ${Token}`},
            data: {
                status: 'COMPLETED',
            }
        }).then(function (response) {
            setFlag(!flag);
            dispatch(hideLoader())
            dispatch(hideModal())
            message.success(`Đã duyệt đơn hàng #${data.id}`)
        }).catch(function(error){
            console.log(error);
            dispatch(hideLoader())
        })
    }

    function handleQuickCanceled(data) {
        dispatch(showModal())
        dispatch(changeModalTitle('Huỷ đơn hàng'))
        dispatch(changeModalContent(
            <CanceledOrderForm
                data={data}
                onSubmit={handleCanceledSingleOrder}
            />
        ))
    }

    function handleCanceledSingleOrder(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios({
            method: 'put',
            url: `${AppConfig.apiUrl}/manager/order/approve/${data.orderId}`,
            headers: {Authorization: `Bearer ${Token}`},
            data: {
                reasonCode: data.reasonCode,
                reason: data.reason,
                status: 'CANCELED',
            }
        }).then(function (response) {
            setFlag(!flag);
            dispatch(hideLoader())
            dispatch(hideModal())
            message.success(`Đã huỷ bỏ đơn hàng #${data.orderId}`)
        }).catch(function(error){
            console.log(error);
            dispatch(hideLoader())
        })
    }

    console.log(result);

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Thành viên',
            dataIndex: 'owner',
            key: 'owner',
            render: (key, row) =>
                <Link to={`/admin/members/detail/${row.owner.id}`}>{row.owner.name} - {row.owner.phone}</Link>
        },
        {
            title: 'Số tiền',
            dataIndex: 'amount',
            key: 'amount',
            render: (text, row) =>
                <Tooltip title={row.note} color={'blue'}>
                    <span className={"font-medium"}>{formatVND(row.amount)}</span>
                </Tooltip>

        },
        {
            title: 'Phần trăm',
            dataIndex: 'percent',
            key: 'percent',
            render: (text, row) =>
                <span className={"font-medium"}>{row.percent}%</span>
        },
        {
            title: 'Loại',
            dataIndex: 'type',
            key: 'type',
            render: (key, row) => (
                TransactionType.map((item, index) => (item.code === row.type && item.name))
            ),
        },
        {
            title: 'Ví',
            dataIndex: 'wallet',
            key: 'wallet',
            render: (key, row) => (
                MemberWallet.map((item, index) => (item.code === row.wallet && item.name))
            ),
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            key: 'status',
            render: (key, row) => (
                TransactionStatus.map((item, index) => (item.code === row.status && item.name))
            ),
        },
        {
            title: 'Ngày tháng',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: (key, row) => formatDateTime(row.createdAt)
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: (key, row) => (
                <Space size={'middle'}>
                    <Button
                        type="primary"
                        size="small"
                        ghost
                        icon={<EditTwoTone />}
                        onClick={() => history.push(`/admin/transaction/${row.id}`)}
                    >
                        Chi tiết
                    </Button>
                </Space>
            )
        }
    ]

    return (
        !result ? <Loading /> :
            <div>
                <Helmet>
                    <title>Quản lý đơn hàng</title>
                </Helmet>
                <PageHeader
                    ghost={false}
                    title={`Đơn hàng #${result && result.order.id}`}
                    subTitle={OrderStatus.map((item, index) => (item.code === result.order.status && item.name))}
                    className={"mb-5"}
                    extra={
                        result.order.status === 'PENDING' &&
                        <Space size={'small'}>
                            <Popconfirm
                                title={`Chắc chắn duyệt đơn này?`}
                                onConfirm={() => handleQuickApproved({id: id, status: 'COMPLETED'})}
                                okText="Duyệt ngay"
                                cancelText="Không"
                            >
                                <Button
                                    type="primary"
                                >
                                    Duyệt
                                </Button>
                            </Popconfirm>
                            <Button
                                type="danger"
                                onClick={() => handleQuickCanceled({id: id, status: 'CANCELED'})}
                            >
                                Huỷ
                            </Button>
                        </Space>
                    }
                />
                <div className={"mb-5"}>
                    <Row gutter={[16, 16]}>
                        <Col xs={24} lg={5} md={5}>
                            <div id={"thong-tin-dich-vu"} className={"bg-white px-5 py-3 rounded border border-gray-300 mb-5"}>
                                <div className={"flex justify-between items-center mb-3 border-b border-gray-200 pb-3"}>
                                    <h2 className={"text-base font-bold text-gray-600"}><IoCalendar size={20} className={"-mt-1 mr-2 text-yellow-500"} /> Thông tin tạo đơn</h2>
                                </div>
                                <div>
                                    {result.order && result.order.createdBy &&
                                        <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                            <p className={"text-gray-500"}>Thành viên</p>
                                            <p className={"whitespace-nowrap ml-3 font-medium text-blue-500"}>
                                                <Link to={`/admin/members/detail/${result.order.createdBy.id}`}>#{result.order.createdBy.id}-{result.order.createdBy.name}</Link>
                                            </p>
                                        </div>
                                    }
                                    {result.order && result.order.referrer &&
                                        <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                            <p className={"text-gray-500"}>Người giới thiệu</p>
                                            <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>
                                                <Link to={`/admin/members/detail/${result.order.referrer.id}`}>#{result.order.referrer.id}-{result.order.referrer.name}</Link>
                                            </p>
                                        </div>
                                    }
                                    <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                        <p className={"text-gray-500"}>Trạng thái</p>
                                        <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>{OrderStatus.map((item, index) => (item.code === result.order.status && item.name))}</p>
                                    </div>
                                    <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                        <p className={"text-gray-500"}>Ngày tạo</p>
                                        <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>{result && formatDateTime(result.order.createdAt)}</p>
                                    </div>
                                    {result.order && result.order.approvedAt &&
                                        <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                            <p className={"text-gray-500"}>Ngày duyệt</p>
                                            <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>{result && formatDateTime(result.order.approvedAt)}</p>
                                        </div>
                                    }
                                    {result.order && result.order.approvedBy &&
                                        <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                            <p className={"text-gray-500"}>Người duyệt</p>
                                            <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>
                                                <Link to={`/admin/employee/edit/${result.order.approvedBy.id}`}>#{result.order.approvedBy.id}-{result.order.approvedBy.name}</Link>
                                            </p>
                                        </div>
                                    }
                                </div>
                            </div>
                            {result && result.orderHistory && result.orderHistory.length > 0 &&
                                <div id={"thong-tin-dich-vu"}
                                     className={"bg-white px-5 py-3 rounded border border-gray-300 mb-5"}>
                                    <div
                                        className={"flex justify-between items-center mb-3 border-b border-gray-200 pb-3"}>
                                        <h2 className={"text-base font-bold text-gray-600"}>
                                            <IoIosClock size={20} className={"-mt-1 mr-2 text-gray-600"}/>
                                            Lịch sử cập nhật</h2>
                                    </div>
                                    {result.orderHistory.map((item, index) => (
                                        <div className={"py-3 border-b border-gray-100"}>
                                            <div>
                                                <p>{item.reason}</p>
                                                <p className={"text-red-500"}>{OrderStatus.map((el) => el.code === item.status && el.name)}</p>
                                                <p className={"text-gray-500 italic text-xs"}>{formatDateTime(item.createdAt)}</p>
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            }
                        </Col>
                        <Col xs={24} lg={8} md={8}>
                            <div id={"thong-tin-dich-vu"} className={"bg-white px-5 py-3 rounded border border-gray-300 mb-5"}>
                                <div className={"flex justify-between items-center mb-3 border-b border-gray-200 pb-3"}>
                                    <h2 className={"text-base font-bold text-gray-600"}><IoArchive size={20} className={"-mt-1 mr-2 text-green-600"} /> Thông tin nhận hàng</h2>
                                </div>
                                <div>
                                    <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                        <p className={"text-gray-500"}>Họ tên</p>
                                        <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>{result && result.order.guestName}</p>
                                    </div>
                                    <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                        <p className={"text-gray-500"}>Số điện thoại</p>
                                        <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>{result && result.order.guestPhone}</p>
                                    </div>
                                    <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                        <p className={"text-gray-500"}>Email</p>
                                        <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>{result && result.order.guestEmail}</p>
                                    </div>
                                    <div className={"flex justify-between pt-3 flex-wrap"}>
                                        <p className={"text-gray-500"}>Địa chỉ</p>
                                        <p className={"font-medium text-gray-800"}>{result && result.order.guestAddress}</p>
                                    </div>
                                </div>
                            </div>
                            <div id={"thong-tin-dich-vu"} className={"bg-white px-5 py-3 rounded border border-gray-300"}>
                                <div className={"flex justify-between items-center mb-3 border-b border-gray-200 pb-3"}>
                                    <h2 className={"text-base font-bold text-gray-600"}><IoWallet size={20} className={"-mt-1 mr-2 text-red-600"} /> Thông tin thanh toán</h2>
                                </div>
                                <div>
                                    <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                        <p className={"text-gray-500"}>Hình thức thanh toán</p>
                                        <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>
                                            {PaymentMethod.map((item, index) => (
                                                item.code === result.order.paymentMethod && item.name
                                            ))}
                                        </p>
                                    </div>
                                    <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                        <p className={"text-gray-500"}>Số tiền thanh toán</p>
                                        <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>{result && formatVND(result.order.amount)}</p>
                                    </div>
                                    {result && result.order.paymentMethod === 'BankTransfer' &&
                                        <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                            <p className={"text-gray-500"}>Nội dung chuyển khoản </p>
                                            <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>HD-{result && result.order.id}-{result && result.order.guestPhone}</p>
                                        </div>
                                    }
                                </div>
                            </div>
                        </Col>
                        <Col xs={24} lg={11} md={11}>
                            <div id={"thong-tin-dich-vu"} className={"bg-white px-5 py-3 rounded border border-gray-300 mb-5"}>
                                <div className={"flex justify-between items-center mb-3 border-b border-gray-200 pb-3"}>
                                    <h2 className={"text-base font-bold text-gray-600"}><IoCart size={20} className={"-mt-1 mr-2 text-blue-400"} /> Thông tin đơn hàng</h2>
                                </div>
                                {result && result.orderItems && result.orderItems.length > 0 &&
                                    result.orderItems.map((item, index) => (
                                        <div>
                                            <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                                <p className={"text-gray-500"}>{item.productName} - {item.productPrice.name} <span className={"font-bold"}>x {item.quantity}</span></p>
                                                <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>{formatVND(item.totalAmount)}</p>
                                            </div>
                                        </div>
                                    ))
                                }
                                <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                    <p className={"text-gray-500"}>Tạm tính</p>
                                    <p>{formatVND(result && result.order.revenue)}</p>
                                </div>
                                {Number(result && result.order.discount) > 0 &&
                                    <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                        <p className={"text-gray-500"}>Chiết khấu</p>
                                        <p className={"text-red-400"}>-{formatVND(result.order.discount)}</p>
                                    </div>
                                }
                                <div className={"flex flex-row justify-between items-center mt-2"}>
                                    <span>Tổng tiền</span>
                                    <span className={"font-bold text-lg"}>{formatVND(result && result.order.amount)}</span>
                                </div>
                            </div>
                            <div id={"ghi-chu-don-hang"} className={"bg-white px-5 py-3 rounded border border-gray-300"}>
                                <div className={"flex justify-between items-center mb-3 border-b border-gray-200 pb-3"}>
                                    <h2 className={"text-base font-bold text-gray-600"}><IoInformationCircle size={20} className={"-mt-1 mr-2 text-purple-400"} /> Ghi chú cho đơn hàng</h2>
                                </div>
                                <div>
                                    {result && result.order.note ? result.order.note : 'Không có ghi chú nào'}
                                </div>
                            </div>
                        </Col>

                    </Row>
                </div>
                {result && result.transactions &&
                    <Card title={"Giao dịch liên quan"} size={'small'}>
                        <Table
                            size={"small"}
                            scroll={{ x: '100%' }}
                            columns={columns}
                            dataSource={result && result.transactions}
                        />
                    </Card>
                }
            </div>
    );
}

export default OrderDetails;
