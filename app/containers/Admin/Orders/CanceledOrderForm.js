import React, { useState } from 'react';
import Form from 'antd/es/form';
import Button from 'antd/es/button';
import { Input } from 'antd';
import { IoCheckmarkCircle, IoRadioButtonOff } from 'react-icons/all';
import { CanceledReasonList } from '../../../models/order.model';

const {TextArea} = Input;

OrderCanceledForm.propTypes = {

};

function OrderCanceledForm(props) {
    const [note, setNote] = useState('Không gọi được khách');
    const [reason, setReason] = useState('khonggoiduockhach');

    function handleSubmit(values) {
        props.onSubmit({
            reason: reason === 'lydokhac' ? values.reason : note,
            reasonCode: reason,
            orderId: props.type !== 'multiple' && props.data.id
        })
    }

    return (
        <Form
            layout="vertical"
            name="OrderCanceledForm"
            onFinish={handleSubmit}
            initialValues={props.initialValues}
        >
            {CanceledReasonList.map((item, index) => (
                <div className={"mb-2"}
                    onClick={() => {
                        setReason(item.code)
                        setNote(item.name)
                    }}
                >
                    {item.code === reason ? <IoCheckmarkCircle size={18} className={"-mt-1 text-blue-500 mr-1"}/> : <IoRadioButtonOff size={18} className={"-mt-1 mr-1"}/>}{item.name}
                </div>
            ))}

            {reason === 'lydokhac' &&
                <Form.Item
                    label="Nhập lý do huỷ đơn"
                    name="note"
                    rules={[
                        {
                            required: reason === 'lydokhac',
                            message: 'Vui lòng nhập lý do huỷ đơn'
                        },
                    ]}
                >
                    <Input.TextArea/>
                </Form.Item>
            }
            <Button
                className={"mt-3"}
                type="primary"
                htmlType="submit"
            >
                Xác nhận
            </Button>
        </Form>
    );
}

export default OrderCanceledForm;
