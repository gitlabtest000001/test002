import React, { useEffect, useState } from 'react';
import {
    convertPaymentMethod,
    convertStatus,
    formatDate,
    formatDateTime,
    formatDateUS,
    formatVND,
} from '../../../utils/helpers';
import { EditTwoTone, PlusCircleTwoTone } from '@ant-design/icons';
import Button from 'antd/es/button';
import { PageHeader, Select, Space, Table, Tag } from 'antd';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import { useDispatch } from 'react-redux';
import queryString from 'query-string';
import moment from 'moment';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import DatePicker from 'antd/es/date-picker';
import Search from 'antd/es/input/Search';
import { Link } from 'react-router-dom';
import history from '../../../utils/history';
import { hideLoader, showLoader } from '../../App/actions';
import { Helmet } from 'react-helmet/es/Helmet';
import { PaymentMethod, OrderStatus, CanceledReasonList } from '../../../models/order.model';
import { ExportXLS } from '../../../components/ExportXLS';
import Tooltip from 'antd/es/tooltip';
import Card from 'antd/es/card';
import { IoCheckmarkCircle, IoRemoveCircle } from 'react-icons/all';

const { RangePicker } = DatePicker;
const {Option} = Select;

function CanceledOrderStatistic(props) {
    const dispatch = useDispatch();
    let searchQuery = queryString.parse(props.location.search);
    const [statistics, setStatistics] = useState();
    const [limit, setLimit] = useState(searchQuery && searchQuery.limit ? searchQuery.limit : 10);
    const [page, setPage] = useState(searchQuery && searchQuery.page ? searchQuery.page : 1);
    const [reasonCode, setReasonCode] = useState('ALL');
    const [search, setSearch] = useState(null);
    const [dateRange, setDateRange] = useState(
        [
            moment.utc(moment().clone().startOf('month').format('YYYY-MM-DD')),
            moment.utc(moment().clone().endOf('month').format("YYYY-MM-DD"))
        ]
    )

    function handleSetRange (v) {
        setDateRange(v);
    }

    useEffect(() => {
        dispatch(showLoader())
        async function getPackageInfo() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/order-history`,
                params: {query:search, reasonCode, rangeStart: formatDateUS(dateRange[0]), rangeEnd: formatDateUS(dateRange[1])},
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setStatistics(response.data)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                history.push('/404')
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getPackageInfo();
    }, [dispatch, search, dateRange, reasonCode])

    const columns = [
        {
            title: 'Mã đơn',
            dataIndex: 'orderId',
            key: 'orderId',
            render: (key, row) =>
                <Link to={`/admin/order/${row.order.id}`}><span className={"text-blue-500"}>{row.order.id}</span></Link>
        },
        {
            title: 'Khách hàng',
            dataIndex: 'customer',
            key: 'customer',
            render: (key, row) =>
                <span>
                    {row.order.guestName} - {row.order.guestPhone}
                </span>
        },
        {
            title: 'Thành tiền',
            dataIndex: 'amount',
            key: 'amount',
            render: (text, row) =>
                <span className={"font-medium text-green-700"}>{formatVND(row.order.amount)}</span>
        },
        {
            title: 'Lý do',
            dataIndex: 'reasonCode',
            key: 'reasonCode',
            render: (key, row) =>
                <Tooltip title={row.reason} color={"red"}>
                    {CanceledReasonList.map((el) => el.code === row.reasonCode && el.name)}
                </Tooltip>
        },
        {
            title: 'Ngày huỷ',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: (key, row) => formatDateTime(row.createdAt)
        },
    ]

    function handleChangePage(page, pageSize) {
        setPage(page);
        setLimit(pageSize);
    }

    let exportData = []
    if(statistics) {
        exportData = statistics.list.map(item => {
            item = {
                'Mã đơn hàng': item.order.id,
                'Tên khách hàng': item.order.guestName,
                'Số điện thoại': item.order.guestPhone,
                'Email': item.order.guestEmail,
                'Địa chỉ': item.order.guestAddress,
                'Tạm tính': item.order.revenue,
                'Chiết khấu': item.order.discount,
                'Tổng tiền': item.order.amount,
                'Hình thức thanh toán': convertPaymentMethod(item.order.paymentMethod),
                'Trạng thái đơn hàng': convertStatus(item.order.status),
                'Ngày tạo': formatDateTime(item.order.createdAt),
                'Ngày huỷ': formatDateTime(item.createdAt),
                'Lý do huỷ': item.reason,
            }
            return item;
        })
    }

    console.log(statistics);

    return (
        <div>
            <Helmet>
                <title>Quản lý đơn hàng - Thống kê Huỷ đơn</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title={"Thống kê huỷ đơn"}
                className={"mb-5"}
            />
            <div className={"mb-5"}>
                <Row gutter={[16, 16]}>
                    <Col xs={24} lg={6} md={12}>
                        <Search
                            placeholder="Tìm đơn hàng..."
                            className={"w-full mt-2"}
                            onSearch={(value) => setSearch(value)}
                            allowClear
                        />
                    </Col>
                    <Col xs={24} lg={6} md={12}>
                        <RangePicker
                            ranges={{
                                'Hôm qua': [moment().subtract(1, 'days'), moment()],
                                'Hôm nay': [moment(), moment().add(1, 'days')],
                                'Tuần trước': [moment().startOf('week').subtract(7,'days'), moment().endOf('week').subtract(7, 'days')],
                                'Tuần này': [moment().startOf('week'), moment().endOf('week')],
                                'Tháng trước': [moment().subtract(1,'months').startOf('month'), moment().subtract(1,'months').endOf('month')],
                                'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                                'Năm trước': [moment().subtract(1,'years').startOf('year'), moment().subtract(1,'years').endOf('year')],
                                'Năm nay': [moment().startOf('year'), moment().endOf('year')],
                            }}
                            className={"mt-2"}
                            value={dateRange}
                            format={'DD/MM/YYYY'}
                            onChange={(v) => handleSetRange(v)}
                        />
                    </Col>
                </Row>
            </div>
            <div>
                <Row gutter={[16, 16]}>
                    <Col xs={24} lg={6} md={6}>
                        <Card
                            size={'small'}
                            title={`Thống kê từ ${formatDate(dateRange[0])} đến ${formatDate(dateRange[1])}`}
                        >
                            <div className={"flex items-center justify-between border-b border-gray-200 py-2 hover:bg-gray-100"}>
                                <p>Không gọi được khách</p>
                                <p className={"font-bold text-red-500"}>{statistics && statistics.stats.khonggoiduockhach}</p>
                            </div>
                            <div className={"flex items-center justify-between border-b border-gray-200 py-2 hover:bg-gray-100"}>
                                <p>Đặt nhầm sản phẩm</p>
                                <p className={"font-bold text-red-500"}>{statistics && statistics.stats.datnhamsanpham}</p>
                            </div>
                            <div className={"flex items-center justify-between border-b border-gray-200 py-2 hover:bg-gray-100"}>
                                <p>Đơn trùng</p>
                                <p className={"font-bold text-red-500"}>{statistics && statistics.stats.dontrung}</p>
                            </div>
                            <div className={"flex items-center justify-between border-b border-gray-200 py-2 hover:bg-gray-100"}>
                                <p>Hết hàng</p>
                                <p className={"font-bold text-red-500"}>{statistics && statistics.stats.hethang}</p>
                            </div>
                            <div className={"flex items-center justify-between border-b border-gray-200 py-2 hover:bg-gray-100"}>
                                <p>Khách không mua nữa</p>
                                <p className={"font-bold text-red-500"}>{statistics && statistics.stats.khachkhongmuanua}</p>
                            </div>
                            <div className={"flex items-center justify-between border-b border-gray-200 py-2 hover:bg-gray-100"}>
                                <p>Sai địa chỉ</p>
                                <p className={"font-bold text-red-500"}>{statistics && statistics.stats.saidiachi}</p>
                            </div>
                            <div className={"flex items-center justify-between py-2 hover:bg-gray-100"}>
                                <p>Lý do khác</p>
                                <p className={"font-bold text-red-500"}>{statistics && statistics.stats.lydokhac}</p>
                            </div>
                        </Card>
                    </Col>
                    <Col xs={24} lg={18} md={18}>
                        <Table
                            scroll={{x: '100%'}}
                            columns={columns}
                            dataSource={statistics && statistics.list}
                            /*pagination={{
                                current: page,
                                pageSize: limit,
                                //total: CanceledOrderStatistic && CanceledOrderStatistic.count,
                                onChange: ((page, pageSize) => handleChangePage(page, pageSize)),
                                position: ['bottomLeft']
                            }}*/
                        />
                    </Col>
                </Row>
            </div>
        </div>
    );
}

export default CanceledOrderStatistic;
