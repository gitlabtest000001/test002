import React, { useEffect, useState } from 'react';
import {
    convertPaymentMethod,
    convertStatus,
    formatDate,
    formatDateTime,
    formatDateUS,
    formatVND,
} from '../../../utils/helpers';
import { EditTwoTone, PlusCircleTwoTone } from '@ant-design/icons';
import Button from 'antd/es/button';
import { PageHeader, Select, Space, Table, Tag } from 'antd';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import { useDispatch } from 'react-redux';
import queryString from 'query-string';
import moment from 'moment';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import DatePicker from 'antd/es/date-picker';
import Search from 'antd/es/input/Search';
import { Link } from 'react-router-dom';
import history from '../../../utils/history';
import { hideLoader, showLoader } from '../../App/actions';
import { Helmet } from 'react-helmet/es/Helmet';
import { OrderStatus, PaymentMethod } from '../../../models/order.model';
import { ExportXLS } from '../../../components/ExportXLS';

const { RangePicker } = DatePicker;
const {Option} = Select;

function Orders(props) {
    const dispatch = useDispatch();
    let searchQuery = queryString.parse(props.location.search);
    const [orders, setTransactions] = useState();
    const [limit, setLimit] = useState(searchQuery && searchQuery.limit ? searchQuery.limit : 10);
    const [page, setPage] = useState(searchQuery && searchQuery.page ? searchQuery.page : 1);
    const [paymentMethod, setPaymentMethod] = useState(searchQuery && searchQuery.paymentMethod ? searchQuery.paymentMethod : 'ALL');
    const [status, setStatus] = useState(searchQuery && searchQuery.status? searchQuery.status : 'ALL');
    const [search, setSearch] = useState(null);
    const [dateRange, setDateRange] = useState(
        [
            moment.utc(moment().clone().startOf('month').format('YYYY-MM-DD')),
            moment.utc(moment().clone().endOf('month').format("YYYY-MM-DD"))
        ]
    )
    const [resultExport, setResultExport] = useState([])

    function handleSetRange (v) {
        setDateRange(v);
    }

    useEffect(() => {
        dispatch(showLoader())
        async function getPackageInfo() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/order`,
                params: {limit, page, paymentMethod, status, query:search, rangeStart: formatDateUS(dateRange[0]), rangeEnd: formatDateUS(dateRange[1])},
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setTransactions(response.data)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                history.push('/404')
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getPackageInfo();
    }, [dispatch, limit, page, paymentMethod, status, search, dateRange])

    useEffect(() => {
        dispatch(showLoader())
        async function getPackageInfo() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/order`,
                params: {limit: 10000000000, page: 1, paymentMethod, status, query:search, rangeStart: formatDateUS(dateRange[0]), rangeEnd: formatDateUS(dateRange[1])},
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setResultExport(response.data.list)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                history.push('/404')
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getPackageInfo();
    }, [dispatch, paymentMethod, status, search, dateRange])

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
            render: (key, row) =>
                <Link to={`/admin/order/${row.id}`}><span className={"text-blue-500"}>{row.id}</span></Link>
        },
        {
            title: 'Khách hàng',
            dataIndex: 'customer',
            key: 'customer',
            render: (key, row) =>
                <span>
                    {row.guestName} - {row.guestPhone}
                </span>
        },
        {
            title: 'Tạm tính',
            dataIndex: 'revenue',
            key: 'revenue',
            render: (text, row) =>
                <span className={"font-medium text-gray-600"}>{formatVND(row.revenue)}</span>
        },
        {
            title: 'Chiết khấu',
            dataIndex: 'discount',
            key: 'discount',
            render: (text, row) =>
                <span className={"text-red-500"}>- {formatVND(row.discount)}</span>
        },
        {
            title: 'Thành tiền',
            dataIndex: 'amount',
            key: 'amount',
            render: (text, row) =>
                <span className={"font-medium text-green-700"}>{formatVND(row.amount)}</span>
        },
        {
            title: 'Hình thức thanh toán',
            dataIndex: 'paymentMethod',
            key: 'paymentMethod',
            render: (key, row) => (
                PaymentMethod.map((item, index) => (
                    item.code === row.paymentMethod && item.name
                ))
            ),
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            key: 'status',
            render: (key, row) => (
                OrderStatus.map((item, index) =>(
                    item.code === row.status && item.name
                ))
            ),
        },
        {
            title: 'Ngày tháng',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: (key, row) => formatDateTime(row.createdAt)
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: (key, row) => (
                <Space size={'middle'}>
                    <Button
                        type="primary"
                        size="small"
                        onClick={() => history.push(`/admin/order/${row.id}`)}
                    >
                        Chi tiết
                    </Button>
                </Space>
            )
        }
    ]

    function handleChangePage(page, pageSize) {
        setPage(page);
        setLimit(pageSize);
    }

    let exportData = []
    if(resultExport) {
        exportData = resultExport.map(item => {
            item = {
                'Mã đơn hàng': item.id,
                'Tên khách hàng': item.guestName,
                'Số điện thoại': item.guestPhone,
                'Email': item.guestEmail,
                'Địa chỉ': item.guestAddress,
                'Tạm tính': item.revenue,
                'Chiết khấu': item.discount,
                'Tổng tiền': item.amount,
                'Hình thức thanh toán': convertPaymentMethod(item.paymentMethod),
                'Trạng thái đơn hàng': convertStatus(item.status),
                'Ngày duyệt': item.approvedAt ? formatDateTime(item.approvedAt) : null,
                'Ngày tạo': formatDateTime(item.createdAt),
            }
            return item;
        })
    }

    return (
        <div>
            <Helmet>
                <title>Quản lý đơn hàng</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title={"Quản lý đơn hàng"}
                className={"mb-5"}
                extra={<ExportXLS
                    csvData={exportData && exportData}
                    fileName={`Đơn hàng-${status}-${formatDate(dateRange[0])}-${formatDate(dateRange[1])}`}
                />}
            />
            <div className={"mb-5"}>
                <Row gutter={[16, 16]}>
                    <Col xs={24} lg={6} md={12}>
                        <Search
                            placeholder="Tìm kiếm..."
                            className={"w-full mt-2"}
                            onSearch={(value) => setSearch(value)}
                            allowClear
                        />
                    </Col>
                    <Col xs={24} lg={6} md={12}>
                        <Select
                            defaultValue="ALL"
                            className={"w-full mt-2"}
                            onChange={(value) => setStatus(value) }
                        >
                            <Option value="ALL">Tất cả trạng thái</Option>
                            {OrderStatus.map((item, index) => (
                                <Option value={item.code}>{item.name}</Option>
                            ))}
                        </Select>
                    </Col>
                    <Col xs={24} lg={6} md={12}>
                        <Select
                            defaultValue="ALL"
                            className={"w-full mt-2"}
                            onChange={(value) => setPaymentMethod(value) }
                        >
                            <Option value="ALL">Hình thức thanh toán</Option>
                            {PaymentMethod.map((item, index) => (
                                <Option value={item.code}>{item.name}</Option>
                            ))}
                        </Select>
                    </Col>
                    <Col xs={24} lg={6} md={12}>
                        <RangePicker
                            ranges={{
                                'Hôm qua': [moment().subtract(1, 'days'), moment()],
                                'Hôm nay': [moment(), moment().add(1, 'days')],
                                'Tuần trước': [moment().startOf('week').subtract(7,'days'), moment().endOf('week').subtract(7, 'days')],
                                'Tuần này': [moment().startOf('week'), moment().endOf('week')],
                                'Tháng trước': [moment().subtract(1,'months').startOf('month'), moment().subtract(1,'months').endOf('month')],
                                'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                                'Năm trước': [moment().subtract(1,'years').startOf('year'), moment().subtract(1,'years').endOf('year')],
                                'Năm nay': [moment().startOf('year'), moment().endOf('year')],
                            }}
                            className={"mt-2"}
                            value={dateRange}
                            format={'DD/MM/YYYY'}
                            onChange={(v) => handleSetRange(v)}
                        />
                    </Col>
                </Row>
            </div>
            <Table
                scroll={{x: '100%'}}
                columns={columns}
                dataSource={orders && orders.list}
                pagination={{
                    current: page,
                    pageSize: limit,
                    total: orders && orders.count,
                    onChange: ((page, pageSize) => handleChangePage(page, pageSize)),
                    position: ['bottomLeft']
                }}
            />
        </div>
    );
}

export default Orders;
