import React, { useEffect, useState } from 'react';
import Form from "antd/es/form";
import Input from "antd/es/input";
import Card from "antd/es/card";
import {CheckOutlined, SaveOutlined} from "@ant-design/icons";
import Button from "antd/es/button";
import AntTreeSelect from "../../../../components/AntTreeSelect";
import {useDispatch} from "react-redux";
import SingleUpload from "../../../../components/SingleUpload";
import ThumbnailUpload from '../../../../components/ThumbnailUpload';
import { Image } from 'antd';
import { AppConfig } from '../../../../appConfig';
import { changeModalContent, changeModalTitle, showModal } from '../../../App/actions';
import FileManager from '../../../../components/FileManager';

CategoryForm.propTypes = {

};

function CategoryForm({formType, onSubmit, categoryList, initialValues, catId, success }) {
    const dispatch = useDispatch()
    const [form] = Form.useForm();
    const [parent, setParent] = useState(initialValues && initialValues.parent);
    const [featureImage, setFeatureImage] = useState(initialValues && initialValues.featureImage)

    function onChangeParent(value) {
        setParent(value)
    }

    function handleChooseMedia(image) {
        setFeatureImage(image)
    }

    useEffect(() => {
        if (success) {
            setFeatureImage(null)
            form.resetFields()
        }

    }, [success])

    function handleSubmit(values) {
        const data = {
            name: values.name,
            description: values.description,
            featureImage: featureImage,
            parent: parent,
        }
        if (formType === 'add') {
            onSubmit(data)
        } else onSubmit(data)
    }

    function handleOpenFileMedia() {
        dispatch(showModal())
        dispatch(changeModalTitle("File Manager"))
        dispatch(changeModalContent(
            <FileManager
                onChooseFile={(value) => setFeatureImage(value[0].url)}
                multiple={false}
                manager={true}
            />
        ))
    }

    return (
        <Form
            form={form}
            layout="vertical"
            name="CategoryForm"
            onFinish={handleSubmit}
            initialValues={initialValues}
        >
            <Card
                title={formType === 'add' && 'Thêm'}
                bordered={false}
                size="small"
            >
                <Form.Item
                    label="Tên"
                    name="name"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập tiêu đề'},
                    ]}
                >
                    <Input onFocus={(event) => event.target.select()}/>
                </Form.Item>
                {formType === 'edit' &&
                    <Form.Item
                        label="Đường dẫn"
                        name="slug"
                    >
                        <Input onFocus={(event) => event.target.select()}/>
                    </Form.Item>
                }
                <Form.Item
                    label="Thuộc về"
                    name="parent"
                >
                    <AntTreeSelect
                        hierarchy={categoryList && categoryList}
                        parent={parent}
                        onChange={onChangeParent}
                    />
                </Form.Item>
                <Form.Item
                    label="Mô tả"
                    name="description"
                >
                    <Input.TextArea onFocus={(event) => event.target.select()} />
                </Form.Item>

                <Form.Item>
                    {featureImage &&
                        <div className={"mb-5"}>
                            <Image src={AppConfig.apiUrl+featureImage} className={"w-24 h-24 object-cover"}/>
                        </div>
                    }
                    <Button onClick={handleOpenFileMedia}>
                        {featureImage ? 'Chọn ảnh khác' : "Chọn ảnh đại diện"}
                    </Button>
                </Form.Item>

                <Button
                    type="primary"
                    htmlType="submit"
                    icon={formType === 'add' ? <CheckOutlined /> : <SaveOutlined />}
                >
                    {formType === 'add' ? 'Thêm' : 'Lưu thay đổi'}
                </Button>
            </Card>

        </Form>
    );
}

export default CategoryForm;
