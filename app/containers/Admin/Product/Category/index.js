import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { hideLoader, showLoader } from '../../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader, Space, Table } from 'antd';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import CategoryForm from './CategoryForm';
import { ErrorMessage, SuccessMessage } from '../../../../components/Message';
import Popconfirm from 'antd/es/popconfirm';
import Button from 'antd/es/button';
import { Link } from 'react-router-dom';
import Loading from '../../../../components/Loading';

function ProductCategory(props) {
    const dispatch = useDispatch();
    const [list, setList] = useState();
    const [flag, setFlag] = useState(true);
    const [limit, setLimit] = useState(1000);
    const [page, setPage] = useState(1);
    const [success, setSuccess] = useState(false);
    useEffect(() => {
        dispatch(showLoader());
        async function getCategory() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/category`,
                params: {
                    limit, page
                },
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setList(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getCategory();
    },[flag, limit, page])

    function handleAddCategory(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.post(
            `${AppConfig.apiUrl}/manager/category`,
            data,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Tạo danh mục thành công'});
            setList(list.concat(response.data))
            dispatch(hideLoader())
            setFlag(!flag)
            setSuccess(true)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    function handleDeleteCategory(id) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.delete(
            `${AppConfig.apiUrl}/manager/category/${id}`,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Đã xoá danh mục'});
            /*const index = list.findIndex(item => Number(item.id) === Number(id));
            if (index !== -1) {
                const newList = [
                    ...list.slice(0, index),
                    response.data,
                    ...list.slice(index + 1),
                ];
                setCategories(newList)
            }*/
            dispatch(hideLoader())
            setFlag(!flag)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    const columns = [
        {
            title: 'Tên',
            dataIndex: 'name',
            key: 'name',
            render: (text, row) =>
                <Link to={`/admin/products/category/${row.id}`}>
                    <span className="text-blue-500">{text}</span>
                </Link>
        },
        {
            title: 'Đường dẫn',
            dataIndex: 'slug',
            key: 'slug',
        },
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: (key, row) => (
                <Space size="middle">
                    <Link to={`/admin/products/category/${row.id}`}>
                        <Button type="link" size="small">
                            Sửa
                        </Button>
                    </Link>
                    <Popconfirm
                        title={`Bạn chắc chắn muốn xoá?`}
                        onConfirm={() => handleDeleteCategory(row.id)}
                        okText="Có"
                        cancelText="Không"
                    >
                        <Button danger type="link">
                            Xoá
                        </Button>
                    </Popconfirm>
                </Space>
            )
        },
    ]

    console.log(list);

    return (
        !list ? <Loading /> :
        <>
            <Helmet>
                <title>Danh mục sản phẩm</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title={"Danh mục sản phẩm"}
                className={"mb-5"}
            />
            <Row gutter={[16, 16]}>
                <Col xs={24} lg={8} md={8}>
                    <CategoryForm
                        formType={"add"}
                        categoryList={list}
                        onSubmit={handleAddCategory}
                        success={success}
                    />
                </Col>
                <Col xs={24} lg={16} md={16}>
                    <Table
                        dataSource={list && list}
                        columns={columns}
                        defaultExpandAllRows={true}
                        /*pagination={{
                            pageSize: 10,
                            total: list && list.count,
                            onChange: ((page, pageSize) => handleChangePage(page, pageSize)),
                            position: ['bottomLeft']
                        }}*/
                    />
                </Col>
            </Row>
        </>
    );
}

export default ProductCategory;
