import React, { useEffect, useState } from 'react';
import Form from "antd/es/form";
import Input from "antd/es/input";
import Card from "antd/es/card";
import {CheckOutlined, SaveOutlined} from "@ant-design/icons";
import Button from "antd/es/button";
import AntTreeSelect from "../../../../components/AntTreeSelect";
import {useDispatch} from "react-redux";
import SingleUpload from "../../../../components/SingleUpload";
import ThumbnailUpload from '../../../../components/ThumbnailUpload';
import { InputNumber, Select } from 'antd';
import { UserAffRoles } from '../../../../models/member.model';

const {Option} = Select;

ProductStockForm.propTypes = {

};

function ProductStockForm(props) {
    const [form] = Form.useForm();

    function handleSubmit(values) {
        props.onSubmit(values)
    }

    return (
        <Form
            form={form}
            layout="vertical"
            name="ProductStockForm"
            onFinish={handleSubmit}
            initialValues={props.initialValues}
        >
                <Form.Item name="type" label="Hành động">
                    <Select
                        style={{width: '100%'}}
                        placeholder="Hành động"
                    >
                        <Option value={'IN'}>Nhập kho</Option>
                        <Option value={'OUT'}>Xuất kho</Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    name={"quantity"}
                    label="Số lượng"
                >
                    <InputNumber
                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                        className={"w-full"}
                    />
                </Form.Item>
                <Form.Item
                    name={"note"}
                    label="Ghi chú"
                >
                    <Input.TextArea
                        rows={2}
                    />
                </Form.Item>
                <Button
                    type="primary"
                    htmlType="submit"
                    icon={<SaveOutlined />}
                >
                    Lưu lại
                </Button>

        </Form>
    );
}

export default ProductStockForm;
