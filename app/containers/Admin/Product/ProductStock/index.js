import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import {
    changeModalContent,
    changeModalTitle,
    hideLoader,
    hideModal,
    showLoader,
    showModal,
} from '../../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';
import Image from 'antd/es/image';
import {
    convertStatus,
    convertTransactionType,
    displayNumber,
    formatDate,
    formatDateTime,
} from '../../../../utils/helpers';
import Space from 'antd/es/space';
import Button from 'antd/es/button';
import { IoTrashBin } from 'react-icons/all';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader, Select, Table } from 'antd';
import { ExportXLS } from '../../../../components/ExportXLS';
import history from '../../../../utils/history';
import Card from 'antd/es/card';
import { formatNumber } from 'react-intl/src/format';
import ProductStockForm from './ProductStockForm';
import { ErrorMessage, SuccessMessage } from '../../../../components/Message';
import Loading from '../../../../components/Loading';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import { Link } from 'react-router-dom';

const {Option} = Select;

function ProductStock(props) {
    const dispatch = useDispatch();
    const priceId = props.match.params.priceId;
    const [productPrice, setProductPrice] = useState();
    const [stock, setStock] = useState();
    const [flag, setFlag] = useState(true);
    const [type, setType] = useState('ALL');

    useEffect(() => {
        dispatch(showLoader());
        async function getProductPrice() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/product-price/byId/${priceId}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setProductPrice(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getProductPrice();
    },[priceId, flag])

    useEffect(() => {
        dispatch(showLoader());
        async function getProductPriceStock() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/stock/byProductPrice/${priceId}`,
                params: {
                  type
                },
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setStock(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getProductPriceStock();
    },[priceId, flag, type])

    const columns = [
        {
            title: 'Id',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Số lượng',
            dataIndex: 'quantity',
            key: 'quantity',
            render: (key, row) => (
                <span className={row.type === 'IN' ? 'text-green-600' : 'text-red-600'}>
                    {displayNumber(row.quantity)}
                </span>
            )
        },
        {
            title: 'Đơn vị tính',
            dataIndex: 'unit',
            key: 'unit',
            render: (key, row) => (
                row.productPrice.unit
            )
        },
        {
            title: 'Hành động',
            dataIndex: 'type',
            key: 'type',
            render: (key, row) => (
                row.type === 'IN' ? <span className={"text-green-600"}>Nhập kho</span> : <span className={"text-red-600"}>Xuất kho</span>
            )
        },
        {
            title: 'Ngày tạo',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: (key, row) => (
                formatDateTime(row.createdAt)
            )
        },
        {
            title: 'Người tạo',
            dataIndex: 'createdBy',
            key: 'createdBy',
            render: (key, row) => (
                row.createdBy && row.createdBy.name
            )
        },
        {
            title: 'Ghi chú',
            dataIndex: 'note',
            key: 'note',
        },
    ]

    function handleStockModal() {
        dispatch(showModal())
        dispatch(changeModalTitle("Cập nhật kho"))
        dispatch(changeModalContent(
            <ProductStockForm
                initialValues={{
                    type: 'IN'
                }}
                onSubmit={handleUpdateStock}
            />
        ))
    }

    function handleUpdateStock(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.post(
            `${AppConfig.apiUrl}/manager/stock`,
            {...data, productPrice: priceId},
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Cập nhật kho thành công'});
            //setPrices(prices.concat(response.data))
            dispatch(hideLoader())
            dispatch(hideModal())
            setFlag(!flag)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    let exportData = []
    if(stock) {
        exportData = stock && stock.stock.map(item => {
            item = {
                'Id': item.id,
                'Số lượng': item.quantity,
                'Đơn vị tính': productPrice && productPrice.unit,
                'Hành động': item.type === 'IN' ? 'Nhập kho' : 'Xuất kho',
                'Ngày tạo': formatDateTime(item.createdAt),
                'Người tạo': item.createdBy.name,
                'Ghi chú': item.note,
            }
            return item;
        })
    }

    return (
        !productPrice ? <Loading /> :
        <div>
            <Helmet>
                <title>Quản lý xuất nhập kho</title>
            </Helmet>
            <PageHeader
                onBack={() => history.goBack()}
                className={"mb-5"}
                ghost={false}
                title={"Quản lý xuất/nhập kho"}
            />
            <Row gutter={[16, 16]}>
                <Col xs={24} lg={8} md={8}>
                    <Card title={'Thông tin sản phẩm'} size={'small'}>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Mã giá:
                            </span>
                            <span className={"ml-5 font-bold"}>
                                #{productPrice.id} - {productPrice.name}
                            </span>
                        </div>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Tên sản phẩm:
                            </span>
                            <span className={"ml-5 font-medium"}>
                                <Link to={`/admin/products/edit/${productPrice.product.id}`} className={"text-blue-500"}>{productPrice.product.name}</Link>
                            </span>
                        </div>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Tồn kho:
                            </span>
                            <span className={`ml-5 font-medium ${Number(productPrice.instock) <= 0 && 'text-red-600'}`}>
                                {displayNumber(productPrice.instock)} {productPrice.unit}
                            </span>
                        </div>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Tổng nhập:
                            </span>
                            <span className={`ml-5 font-medium text-green-600`}>
                                {displayNumber(stock && stock.totalIn)}
                            </span>
                        </div>
                        <div>
                            <span>
                                Tổng xuất:
                            </span>
                            <span className={`ml-5 font-medium text-red-600`}>
                                {displayNumber(stock && stock.totalOut)}
                            </span>
                        </div>
                    </Card>
                </Col>
                <Col xs={24} lg={16} md={16}>
                    <Card
                        title={"Lịch sử xuất/nhập kho"}
                        size={'small'}
                        extra={
                            <Space size={"middle"}>
                                <ExportXLS
                                    csvData={exportData && exportData}
                                    fileName={`Xuất Nhập Kho - ${type} - ${productPrice.product.name} - ${productPrice.name}`}
                                />
                                <Button
                                    type={"primary"}
                                    onClick={() => handleStockModal()}
                                >
                                    Cập nhật
                                </Button>
                            </Space>
                        }
                    >
                        <Select
                            defaultValue="ALL"
                            className={"mb-2"}
                            onChange={(value) => setType(value) }
                        >
                            <Option value="ALL">Tất cả hành động</Option>
                            <Option value="IN">Nhập kho</Option>
                            <Option value="OUT">Xuất kho</Option>
                        </Select>
                        <Table
                            size={'small'}
                            columns={columns}
                            dataSource={stock && stock.stock}
                        />
                    </Card>
                </Col>
            </Row>


        </div>
    );
}

export default ProductStock;
