import React, { useState } from 'react';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import Card from 'antd/es/card';
import Form from "antd/es/form";
import Input from 'antd/es/input';
import RichTextEditor from '../../../components/RTE';
import ThumbnailUpload from '../../../components/ThumbnailUpload';
import AntTreeSelect from '../../../components/AntTreeSelect';
import Select from 'antd/es/select';
import MultipleUpload from '../../../components/MultipleUpload';
import CheckOutlined from '@ant-design/icons/lib/icons/CheckOutlined';
import { SaveFilled } from '@ant-design/icons';
import Button from 'antd/es/button';
import { Image, InputNumber } from 'antd';
import Popconfirm from 'antd/es/popconfirm';
import ProductPrice from './ProductPrice';
import { changeModalContent, changeModalTitle, showModal } from '../../App/actions';
import FileManager from '../../../components/FileManager';
import { useDispatch } from 'react-redux';
import { IoRemoveCircleOutline } from 'react-icons/all';
import { AppConfig } from '../../../appConfig';

const {Option} = Select

function ProductForm(props) {
    const dispatch = useDispatch()
    const [gallery, setGallery] = useState(props.initialValues ? props.initialValues.gallery : []);
    const [content, setContent] = useState(props.initialValues && props.initialValues.content);
    const [description, setDescription] = useState(props.initialValues && props.initialValues.description);
    const [parent, setParent] = useState(props.initialValues && props.initialValues.categories);
    const [featureImage, setFeatureImage] = useState(props.initialValues && props.initialValues.featureImage);
    const [seoImage, setSeoImage] = useState(props.initialValues && props.initialValues.seoImage);

    function handleChooseFeatureImage(image) {
        setFeatureImage(image)
    }

    function handleChooseMedia(image) {
        setGallery(image)
    }

    function onChangeParent(value) {
        setParent(value)
    }

    function handleSubmit(values) {
        if (props.type === "add") {
            props.onSave(
                {
                    ...values,
                    content: content,
                    description: description,
                    gallery: gallery ? JSON.stringify(gallery) : null,
                    categories: parent ? JSON.stringify(parent) : null,
                    featureImage: featureImage,
                    seoImage: seoImage,
                }
            )
        } else {
            props.onEdit(
                {
                    ...values,
                    content: content,
                    gallery: gallery ? JSON.stringify(gallery) : null,
                    categories: parent ? JSON.stringify(parent) : null,
                    featureImage: featureImage,
                    seoImage: seoImage,
                }
            )
        }

    }

    function handleOpenFileMedia() {
        dispatch(showModal())
        dispatch(changeModalTitle("File Manager"))
        dispatch(changeModalContent(
            <FileManager
                onChooseFile={(value) => setFeatureImage(value[0].url)}
                multiple={false}
                manager={true}
            />
        ))
    }

    function handleOpenGallery() {
        dispatch(showModal())
        dispatch(changeModalTitle("Gallery Manager"))
        dispatch(changeModalContent(
            <FileManager
                onChooseFile={(value) => setGallery(gallery ? gallery.concat(value) : value)}
                multiple={true}
                manager={true}
            />
        ))
    }

    function handleOpenFileMediaSEO() {
        dispatch(showModal())
        dispatch(changeModalTitle("File Manager"))
        dispatch(changeModalContent(
            <FileManager
                onChooseFile={(value) => setSeoImage(value[0].url)}
                multiple={false}
                manager={true}
            />
        ))
    }

    function removeImageFromGallery(id) {
        setGallery(gallery.filter((item) => item.id !== id))
    }

    return (
        <Form
            layout={"vertical"}
            onFinish={handleSubmit}
            initialValues={props.initialValues}
        >
            <Row gutter={[32, 32]}>
                <Col xs={24} lg={16} md={16}>
                    <Form.Item
                        name="name"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập tên sản phẩm'},
                        ]}
                        className="mb-3"
                    >
                        <Input
                            size={"large"}
                            placeholder="Tên sản phẩm"
                        />
                    </Form.Item>
                    {props.type === 'edit' &&
                        <Form.Item
                            name="slug"
                            rules={[
                                {
                                    required: true,
                                    message: 'Liên kết tĩnh không được để trống'},
                            ]}
                            className="mb-3"
                        >
                            <Input
                                addonBefore={`${props.settings && props.settings.website_url}/san-pham/`}
                            />
                        </Form.Item>
                    }
                    {props.type === 'edit' &&
                        <Card
                            size="small"
                            title="Giá sản phẩm & Tồn kho"
                            className={"mb-5"}
                        >
                            <ProductPrice productId={props.productId} />
                        </Card>
                    }
                    <Card
                        size="small"
                        className="mb-5"
                        title="Mô tả sản phẩm"
                    >
                        <RichTextEditor
                            defaultValue={props.initialValues && props.initialValues.content}
                            onDataChange={(value) => setContent(value)}
                        />
                    </Card>

                    <Card
                        size="small"
                        title="Mô tả ngắn"
                        className={"mb-5"}
                    >
                        <RichTextEditor
                            defaultValue={props.initialValues && props.initialValues.description}
                            onDataChange={(value) => setDescription(value)}
                        />
                    </Card>
                    <Card
                        size="small"
                        bordered={false}
                        title="Gallery"
                        className="mb-5"
                        extra={
                            <Button onClick={handleOpenGallery}>
                                Thêm ảnh
                            </Button>
                        }
                    >
                        {gallery && gallery && gallery.length > 0 &&
                            <div className={"flex flex-wrap items-center gap-2"}>
                                {gallery.map((item, index) => (
                                    <div className={"mb-5 relative"}>
                                        <div className={"absolute top-0 right-0"}>
                                            <IoRemoveCircleOutline className={"text-red-600"} size={20} onClick={() => removeImageFromGallery(item.id)}/>
                                        </div>
                                        <img src={AppConfig.apiUrl+item.url} className={"w-24 h-24 object-cover"} alt={item.name}/>
                                    </div>
                                ))}
                            </div>
                        }
                    </Card>
                    <Card
                        size="small"
                        bordered={false}
                        title={"Cấu hình SEO"}
                        className={"mb-5"}
                    >
                        <Form.Item
                            label="Tiêu đề"
                            name="seoTitle"
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            label="Mô tả"
                            name="seoDescription"
                        >
                            <Input.TextArea/>
                        </Form.Item>
                        <Form.Item
                            label="Từ khoá"
                            name="seoKeyword"
                        >
                            <Input />
                        </Form.Item>
                        {seoImage &&
                            <div>
                                <Image src={AppConfig.apiUrl+seoImage} className={"w-24 h-24 object-cover"}/>
                            </div>
                        }
                        <Button onClick={handleOpenFileMediaSEO}>
                            {seoImage ? 'Chọn ảnh khác' : "Chọn ảnh chia sẻ"}
                        </Button>
                    </Card>
                    <Card
                        size="small"
                        title="Ghi chú"
                    >
                        <Form.Item
                            name="note"
                            className="pb-0 mb-0"
                        >
                            <Input.TextArea />
                        </Form.Item>
                    </Card>
                </Col>
                <Col xs={24} lg={8} md={8}>
                    <Card>
                        {featureImage &&
                            <div className={"mb-5"}>
                                <Image src={AppConfig.apiUrl+featureImage} className={"w-24 h-24 object-cover"}/>
                            </div>
                        }
                        <Button onClick={handleOpenFileMedia} className={"mb-5"}>
                            {featureImage ? 'Chọn ảnh khác' : "Chọn ảnh đại diện"}
                        </Button>
                        <Form.Item
                            label="Danh mục"
                        >
                            <AntTreeSelect
                                hierarchy={props.categoryList && props.categoryList}
                                parent={parent}
                                onChange={onChangeParent}
                                multiple
                                size="large"
                            />
                        </Form.Item>
                        <Form.Item name="feature" label="Sản phẩm nổi bật">
                            <Select
                                style={{width: '100%'}}
                                placeholder="Chọn trạng thái"
                                size="large"
                            >
                                <Option key={1} value="TRUE">Có</Option>
                                <Option key={2} value="FALSE">Không</Option>
                            </Select>
                        </Form.Item>
                        {props.type === 'edit' &&
                        <Form.Item name="status" label="Trạng thái">
                            <Select
                                style={{width: '100%'}}
                                placeholder="Chọn trạng thái"
                                size="large"
                            >
                                <Option key={1} value="ACTIVE">Đăng</Option>
                                <Option key={2} value="DEACTIVE">Nháp</Option>
                            </Select>
                        </Form.Item>
                        }
                        <Button
                            type="primary"
                            htmlType="submit"
                            size="large"
                            icon={props.type === 'add' ? <CheckOutlined /> : <SaveFilled />}
                        >
                            {props.type === 'add' ? 'Đăng sản phẩm' : 'Lưu thay đổi'}
                        </Button>
                        {props.type === 'edit' &&
                        <Popconfirm
                            title={`Bạn chắc chắn muốn xoá?`}
                            onConfirm={props.onDelete}
                            okText="Có"
                            cancelText="Không"
                        >
                            <Button danger type="link">
                                Xoá sản phẩm
                            </Button>
                        </Popconfirm>
                        }
                    </Card>

                </Col>
            </Row>
        </Form>
    );
}

export default ProductForm;
