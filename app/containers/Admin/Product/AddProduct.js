import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader } from 'antd';
import history from '../../../utils/history';
import ProductForm from './ProductForm';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import Loading from '../../../components/Loading';
import { SuccessMessage } from '../../../components/Message';

function AddProduct(props) {
    const dispatch = useDispatch();

    const [categoryList, setCategoryList] = useState();
    useEffect(() => {
        dispatch(showLoader());
        async function getCategory() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/category?limit=100`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setCategoryList(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getCategory();
    },[])

    function handleSaveProduct(data) {
        dispatch(showLoader());
        const Token = localStorage.getItem('dvg_manager_token');
        axios({
            method: 'post',
            url: `${AppConfig.apiUrl}/manager/product`,
            data,
            headers: {Authorization: `Bearer ${Token}`}
        }).then(function (response) {
            if (response.status === 201) {
                history.push(`/admin/products/edit/${response.data.id}`)
                dispatch(hideLoader());
                SuccessMessage({message: 'Đã tạo sản phẩm, vui lòng cấu hình giá bán'})
            }
        }).catch(function(error){
            console.log(error);
        })
    }

    return (
        !categoryList ? <Loading /> :
        <div>
            <Helmet>Thêm sản phẩm</Helmet>
            <PageHeader
                className={"mb-5"}
                title={"Thêm sản phẩm"}
                ghost={false}
                onBack={history.goBack}
            />
            <ProductForm
                type={"add"}
                initialValues={{
                    feature: 'FALSE'
                }}
                categoryList={categoryList && categoryList}
                onSave={handleSaveProduct}
            />
        </div>
    );
}

export default AddProduct;
