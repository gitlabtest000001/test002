import React, { useEffect, useState } from 'react';
import {
    changeInfoContent,
    changeInfoTitle,
    changeModalContent,
    changeModalTitle,
    hideLoader,
    hideModal, showInfo,
    showLoader,
    showModal,
} from '../../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';
import Loading from '../../../../components/Loading';
import Form from 'antd/es/form';
import Input from 'antd/es/input';
import { displayNumber, formatDateTime, formatVND } from '../../../../utils/helpers';
import Button from 'antd/es/button';
import { useDispatch } from 'react-redux';
import ProductPriceForm from './ProductPriceForm';
import { ErrorMessage, SuccessMessage } from '../../../../components/Message';
import { Space, Table } from 'antd';
import { Link } from 'react-router-dom';
import history from '../../../../utils/history';

function ProductPrice(props) {
    const dispatch = useDispatch()
    const [prices, setPrices] = useState()

    useEffect(() => {
        dispatch(showLoader());
        async function getProductPrices() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/product-price/${props.productId}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setPrices(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getProductPrices();
    },[props.productId])

    function handleCreate(data) {
        const values = {
            name: data.name,
            price: data.price,
            unit: data.unit,
            instock: data.instock,
            product: props.productId
        }
        console.log(values);
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.post(
            `${AppConfig.apiUrl}/manager/product-price`,
            values,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Tạo giá thành công'});
            setPrices(prices.concat(response.data))
            dispatch(hideLoader())
            dispatch(hideModal())
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    function handleEdit(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.put(
            `${AppConfig.apiUrl}/manager/product-price/${data.id}`,
            data.values,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Sửa giá thành công'});
            const index = prices.findIndex(item => item.id === data.id);
            if (index !== -1) {
                const newList = [
                    ...prices.slice(0, index),
                    response.data,
                    ...prices.slice(index+1)
                ]
                setPrices(newList)
            }
            dispatch(hideLoader())
            dispatch(hideModal())
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    function handleCreateModal() {
        dispatch(showModal());
        dispatch(changeModalTitle("Tạo giá"))
        dispatch(changeModalContent(
            <ProductPriceForm
                formType={"add"}
                onSubmit={handleCreate}
                productId={props.productId}
            />
        ))
    }

    function handleEditModal(data) {
        dispatch(showModal());
        dispatch(changeModalTitle("Sửa giá"))
        dispatch(changeModalContent(
            <ProductPriceForm
                formType={"edit"}
                initialValues={data}
                onSubmit={handleEdit}
            />
        ))
    }

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Định nghĩa',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Đơn vị tính',
            dataIndex: 'unit',
            key: 'unit',
        },
        {
            title: 'Giá',
            dataIndex: 'price',
            key: 'price',
            render: (key, row) => formatVND(row.price)
        },
        {
            title: 'Tồn kho',
            dataIndex: 'instock',
            key: 'instock',
            render: (key,row) => displayNumber(row.instock)
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: (key, row) => (
                <Space size="middle">
                    <Button
                        type="link"
                        size="small"
                        onClick={() => history.push(`/admin/products/edit/${props.productId}/${row.id}/stock`)}
                    >
                        Quản lý kho
                    </Button>
                    <Button
                        type="link"
                        size="small"
                        onClick={() => handleEditModal(row)}
                    >
                        Sửa giá
                    </Button>
                    <Button type="link" size="small" color={"danger"}>
                        Xoá
                    </Button>
                </Space>
            )
        },
    ]

    return (
        !prices ? <Loading /> :
        <div>
            <Button
                onClick={handleCreateModal}
                type={"primary"}
            >
                Tạo giá
            </Button>
            {prices && prices.length > 0 &&
                <Table
                    className={"mt-5"}
                    size={"small"}
                    columns={columns}
                    dataSource={prices}
                    pagination={false}
                />
            }
        </div>
    );
}

export default ProductPrice;
