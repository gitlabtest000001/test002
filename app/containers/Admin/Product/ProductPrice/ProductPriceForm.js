import React, { useEffect, useState } from 'react';
import Form from "antd/es/form";
import Input from "antd/es/input";
import Card from "antd/es/card";
import {CheckOutlined, SaveOutlined} from "@ant-design/icons";
import Button from "antd/es/button";
import AntTreeSelect from "../../../../components/AntTreeSelect";
import {useDispatch} from "react-redux";
import SingleUpload from "../../../../components/SingleUpload";
import ThumbnailUpload from '../../../../components/ThumbnailUpload';
import { InputNumber } from 'antd';

ProductPriceForm.propTypes = {

};

function ProductPriceForm(props) {
    const [form] = Form.useForm();

    function handleSubmit(values) {
        if (props.formType === 'add') {
            props.onSubmit(values)
        } else props.onSubmit({values, id: props.initialValues.id})
    }

    return (
        <Form
            form={form}
            layout="vertical"
            name="ProductPriceForm"
            onFinish={handleSubmit}
            initialValues={props.initialValues}
        >
                <Form.Item
                    label="Định nghĩa"
                    name="name"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập thông tin'},
                    ]}
                >
                    <Input placeholder={"Ví dụ: 100 viên"}/>
                </Form.Item>
                <Form.Item
                    label="Đơn vị tính"
                    name="unit"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập thông tin'},
                    ]}
                >
                    <Input placeholder={"Ví dụ: Hộp"}/>
                </Form.Item>
                <Form.Item
                    name={"price"}
                    label="Giá"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập thông tin'},
                    ]}
                >
                    <InputNumber
                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                        className={"w-full"}
                    />
                </Form.Item>
                {props.formType === 'add' &&
                    <Form.Item
                        name={"instock"}
                        label="Tồn kho"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập thông tin'},
                        ]}
                    >
                        <InputNumber
                            formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                            parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                            className={"w-full"}
                        />
                    </Form.Item>
                }
                <Button
                    type="primary"
                    htmlType="submit"
                    icon={props.formType === 'add' ? <CheckOutlined /> : <SaveOutlined />}
                >
                    {props.formType === 'add' ? 'Thêm' : 'Lưu thay đổi'}
                </Button>

        </Form>
    );
}

export default ProductPriceForm;
