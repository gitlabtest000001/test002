import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader, Space } from 'antd';
import history from '../../../utils/history';
import Button from 'antd/es/button';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { useDispatch, useSelector } from 'react-redux';
import Loading from '../../../components/Loading';
import ProductForm from './ProductForm';
import { ErrorMessage, SuccessMessage } from '../../../components/Message';

function EditProduct(props) {
    const dispatch = useDispatch()
    const productId = props.match.params.id;
    const [productInfo, setProductInfo] = useState();
    const settings = useSelector(state => state.root.settings.options);

    const [categoryList, setCategoryList] = useState();
    useEffect(() => {
        dispatch(showLoader());
        async function getCategory() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/category?limit=100`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setCategoryList(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getCategory();
    },[])

    useEffect(() => {
        dispatch(showLoader());
        async function getCategory() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/product/${productId}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setProductInfo(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getCategory();
    },[productId])

    if (productInfo) {
        let productCategories = [];
        if (productInfo.categories !== null && typeof productInfo.categories !== 'undefined' && productInfo.categories !== '') {
            productCategories = productInfo.categories.map((el) => el.id)
        }
        var initialValues = {
            id: productInfo.id,
            name: productInfo.name,
            content: productInfo.content,
            feature: productInfo.feature,
            description: productInfo.description,
            gallery: productInfo.gallery,
            slug: productInfo.slug,
            categories: productCategories,
            status: productInfo.status,
            note: productInfo.note,
            featureImage: productInfo.featureImage,
            seoImage: productInfo.seoImage,
            seoTitle: productInfo.seoTitle,
            seoDescription: productInfo.seoDescription,
            seoKeyword: productInfo.seoKeyword,
        }
    }

    function handleEditProduct(data) {
        dispatch(showLoader());
        const Token = localStorage.getItem('dvg_manager_token');
        axios({
            method: 'put',
            url: `${AppConfig.apiUrl}/manager/product/${productId}`,
            data,
            headers: {Authorization: `Bearer ${Token}`}
        }).then(function (response) {
            //history.push(`/admin/products/edit/${response.data.id}`)
            dispatch(hideLoader());
            SuccessMessage({message: 'Đã lưu thay đổi'})
        }).catch(function(error){
            console.log(error);
            ErrorMessage({message: error.data.message})
        })
    }

    function handleDeleteProduct() {
        dispatch(showLoader());
        const Token = localStorage.getItem('dvg_manager_token');
        axios({
            method: 'delete',
            url: `${AppConfig.apiUrl}/manager/product/${productId}`,
            headers: {Authorization: `Bearer ${Token}`}
        }).then(function (response) {
            history.push(`/admin/products`)
            dispatch(hideLoader());
            SuccessMessage({message: 'Đã xoá sản phẩm'})
        }).catch(function(error){
            console.log(error);
            ErrorMessage({message: error.data.message})
        })
    }

    return (
        !productInfo ? <Loading /> :
        <div>
            <Helmet>Sửa sản phẩm</Helmet>
            <PageHeader
                onBack={() => history.push('/admin/products')}
                ghost={false}
                title={"Sửa sản phẩm"}
                extra={
                    <Space size={"middle"}>
                        <a href={`${settings && settings.website_url}/san-pham/${productInfo && productInfo.slug}`} target={"_blank"}>
                            Xem thử
                        </a>
                        <Button
                            type={"primary"}
                            onClick={() => history.push('/admin/products/add')}
                        >
                            Thêm mới
                        </Button>
                    </Space>
                }
                className={"mb-5"}
            />
            {!categoryList ? <Loading/> :
                <ProductForm
                    settings={settings && settings}
                    productId={productId}
                    type={"edit"}
                    initialValues={initialValues}
                    categoryList={categoryList}
                    onEdit={handleEditProduct}
                    onDelete={handleDeleteProduct}
                />
            }
        </div>
    );
}

export default EditProduct;
