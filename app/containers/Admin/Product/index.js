import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader, Space, Table } from 'antd';
import Button from 'antd/es/button';
import history from '../../../utils/history';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import Loading from '../../../components/Loading';
import { Link } from 'react-router-dom';
import Popconfirm from 'antd/es/popconfirm';
import { convertStatus, convertTransactionType, formatDate, formatDateTime, formatVND } from '../../../utils/helpers';
import Search from 'antd/es/input/Search';
import Select from 'antd/es/select';
import AntTreeSelect from '../../../components/AntTreeSelect';
import { StarFilled, StarOutlined } from '@ant-design/icons';
import { ExportXLS } from '../../../components/ExportXLS';

const {Option} = Select;

function ProductList(props) {
    const dispatch = useDispatch();
    const [products, setProducts] = useState()

    const [limit, setLimit] = useState(10);
    const [page, setPage] = useState(1);
    const [status, setStatus] = useState('ALL');
    const [catId, setCatId] = useState('ALL');
    const [search, setSearch] = useState(null);
    const [categoryList, setCategoryList] = useState();
    const [resultExport, setResultExport] = useState([]);

    useEffect(() => {
        dispatch(showLoader());
        async function getCategory() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/category?limit=100`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setCategoryList(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getCategory();
    },[])

    useEffect(() => {
        dispatch(showLoader());
        async function getProducts() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/product`,
                params: {
                    page, limit, query: search, status, category: catId === 'ALL' ? 'ALL' : [catId]
                },
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setProducts(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getProducts();
    },[dispatch, limit, page, search, status, catId])

    useEffect(() => {
        dispatch(showLoader());
        async function getProducts() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/product`,
                params: {
                    page:1, limit:10000000000, query: search, status, category: catId === 'ALL' ? 'ALL' : [catId]
                },
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setResultExport(response.data.list)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getProducts();
    },[dispatch,search, status, catId])

    function handleUpdateFeatureProduct(data) {
        dispatch(showLoader());
        const Token = localStorage.getItem('dvg_manager_token');
        axios({
            method: 'put',
            url: `${AppConfig.apiUrl}/manager/product/${data.item.id}`,
            data: {feature: data.feature, status: data.item.status, name: data.item.name},
            headers: {Authorization: `Bearer ${Token}`}
        }).then(function (response) {
            dispatch(hideLoader());
            const index = products.list.findIndex(item => item.id === data.item.id);
            if (index !== -1) {
                const newList = [
                    ...products.list.slice(0,index),
                    response.data,
                    ...products.list.slice(index+1),
                ];
                setProducts({list: newList})
            }
        }).catch(function(error){
            console.log(error);
        })
    }

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Tên',
            dataIndex: 'name',
            key: 'name',
            render: (text, row) =>
                <Link to={`/admin/products/edit/${row.id}`}>
                    <span className="text-blue-500">{text}</span>
                </Link>
        },
        {
            title: 'Đường dẫn',
            dataIndex: 'slug',
            key: 'slug',
        },
        {
            title: 'Ngày tạo',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: (key, row) => formatDateTime(row.createdAt)
        },
        {
            title: 'Nổi bật',
            dataIndex: 'feature',
            key: 'feature',
            align: 'center',
            render: (key, row) =>
                row.feature === 'TRUE' ?
                    <StarFilled
                        className={"text-yellow-500"}
                        onClick={() => handleUpdateFeatureProduct({feature: 'FALSE', item: row})}
                    />
                    :
                    <StarOutlined
                        onClick={() => handleUpdateFeatureProduct({feature: 'TRUE', item: row})}
                    />
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: (key, row) => (
                <Space size="middle">
                    <Link to={`/admin/products/edit/${row.id}`}>
                        <Button type="link" size="small">
                            Sửa
                        </Button>
                    </Link>
                </Space>
            )
        },
    ]

    function handleChangePage(page, pageSize) {
        setPage(page);
        setLimit(pageSize);
    }

    let exportData = [];
    if(resultExport) {
        exportData = resultExport.map(item => {
            item = {
                'Id': item.id,
                'Tên sản phẩm': item.name,
                'Sản phẩm Nổi bật': item.feature === 'TRUE' ? 'Có' : 'Không',
                'Trạng thái': convertStatus(item.status),
                'Ghi chú': item.note,
                'Ngày tạo': formatDateTime(item.createdAt),
            }
            return item;
        })
    }

    return (
        !products ? <Loading /> :
        <div>
            <Helmet>
                <title>Quản lý sản phẩm</title>
            </Helmet>
            <PageHeader
                className={"mb-5"}
                ghost={false}
                title={"Danh sách sản phẩm"}
                extra={
                    <Space size={"middle"}>
                        <ExportXLS
                            csvData={exportData}
                            fileName={`Sản phẩm - ${status}`}
                        />
                        <Button
                            type={"primary"}
                            onClick={() => history.push('/admin/products/add')}
                        >
                            Thêm mới
                        </Button>
                    </Space>
                }
            />
            <div>
                <Row gutter={[16, 16]} className={"mb-5"}>
                    <Col xs={24} lg={8} md={8}>
                        <Search
                            placeholder="Tìm kiếm..."
                            className={"w-full mt-2"}
                            onSearch={(value) => setSearch(value)}
                            allowClear
                        />
                    </Col>
                    <Col xs={24} lg={8} md={8}>
                        {!categoryList ? <Loading/> :
                            <Select
                                defaultValue="ALL"
                                className={"w-full mt-2"}
                                onChange={(value) => setCatId(value)}
                            >
                                <Option value="ALL">Tất cả danh mục</Option>
                                {categoryList && categoryList.map((item, index) => (
                                    <Option value={item.id}>{item.name}</Option>
                                ))}
                            </Select>
                        }
                    </Col>
                    <Col xs={24} lg={8} md={8}>
                        <Select
                            defaultValue="ALL"
                            className={"w-full mt-2"}
                            onChange={(value) => setStatus(value) }
                        >
                            <Option value="ALL">Tất cả trạng thái</Option>
                            <Option value="ACTIVE">Hiển thị</Option>
                            <Option value="DEACTIVE">Đang ẩn</Option>
                        </Select>
                    </Col>
                </Row>
                <Table
                    columns={columns}
                    dataSource={products && products.list}
                    pagination={{
                        current: page,
                        pageSize: limit,
                        total: products && products.count,
                        onChange: ((page, pageSize) => handleChangePage(page, pageSize)),
                        position: ['bottomLeft']
                    }}
                />
            </div>
        </div>
    );
}

export default ProductList;
