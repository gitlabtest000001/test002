import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader } from 'antd';
import Button from 'antd/es/button';
import history from '../../../utils/history';
import PageForm from './PageForm';
import Loading from '../../../components/Loading';
import { ErrorMessage, SuccessMessage } from '../../../components/Message';

function PageDetails(props) {
    const dispatch = useDispatch()
    const id = props.match.params.id
    const settings = useSelector(state => state.root.settings.options);
    console.log(settings);
    const [page, setPage] = useState()

    useEffect(() => {
        dispatch(showLoader());
        async function getPage() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/page/${id}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setPage(response.data);
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getPage();
    }, [id, dispatch])

    console.log(page);

    function handleEditPage(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.put(
            `${AppConfig.apiUrl}/manager/page/${id}`,
            data,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Sửa trang thành công'});
            dispatch(hideLoader())
            //history.push(`/admin/page/edit/${response.data.id}`)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    function handleDeletePage(id) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.delete(
            `${AppConfig.apiUrl}/manager/page/${id}`,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Đã xoá trang'});
            dispatch(hideLoader())
            history.push(`/admin/pages`)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    return (
        !page ? <Loading /> :
        <div>
            <Helmet>
                <title>Quản lý trang</title>
            </Helmet>
            <PageHeader
                title={"Sửa trang"}
                className={"mb-5"}
                ghost={false}
                extra={
                    <Button
                        onClick={() => history.push('/admin/page/add')}
                    >
                        Thêm trang
                    </Button>
                }
            />
            <div>
                <PageForm
                    type={"edit"}
                    initialValues={page && page}
                    onSubmit={handleEditPage}
                    onDelete={handleDeletePage}
                    id={id}
                    settings={settings && settings}
                />
            </div>
        </div>
    );
}

export default PageDetails;
