import React, {useState} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useForm } from 'antd/es/form/Form';
import Form from 'antd/es/form';
import RichTextEditor from '../../../components/RTE';
import Input from 'antd/es/input';
import { AppConfig } from '../../../appConfig';
import Card from 'antd/es/card';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import Button from 'antd/es/button';
import { Image, Select } from 'antd';
import { CheckOutlined, SaveOutlined } from '@ant-design/icons';
import Popconfirm from 'antd/es/popconfirm';
import { changeModalContent, changeModalTitle, showModal } from '../../App/actions';
import FileManager from '../../../components/FileManager';

const {Option} = Select;

function PageForm(props) {
    const dispatch = useDispatch()
    const [form] = useForm()
    const [content, setContent] = useState(props.initialValues && props.initialValues.content);
    const [seoImage, setSeoImage] = useState(props.initialValues && props.initialValues.seoImage);

    function handleSubmit(values) {
        const body = {
            slug: values.slug,
            title: values.title,
            seoTitle: values.seoTitle,
            seoDescription: values.seoDescription,
            seoKeyword: values.seoKeyword,
            content,
            seoImage: seoImage,
            status: values.status
        }
        if (props.type === 'edit') {
            props.onSubmit(body, props.id)
        } else {
            props.onSubmit(body)
        }
    }

    function handleOpenFileMedia() {
        dispatch(showModal())
        dispatch(changeModalTitle("File Manager"))
        dispatch(changeModalContent(
            <FileManager
                onChooseFile={(value) => setSeoImage(value[0].url)}
                multiple={false}
                manager={true}
            />
        ))
    }

    return (
        <div>
            <Form
                layout="vertical"
                name="PostForm"
                onFinish={handleSubmit}
                initialValues={props.initialValues}
                form={form}
            >
                <Row gutter={[16, 16]}>
                    <Col xs={24} md={17} lg={17}>
                        <Form.Item
                            name="title"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập tiêu đề'},
                            ]}
                            className="mb-3"
                        >
                            <Input
                                size="large"
                                placeHolder="Tiêu đề"
                                className="h-14 text-2xl"
                            />
                        </Form.Item>
                        {props.type === 'edit' &&
                            <Form.Item
                                name="slug"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Liên kết tĩnh không được để trống'},
                                ]}
                                className="mb-5"
                            >
                                <Input
                                    size="small"
                                    addonBefore={`${props.settings && props.settings.website_url}/trang/`}
                                />
                            </Form.Item>
                        }
                        <Card
                            size="small"
                            bordered={false}
                            className="mb-5"
                            title="Nội dung"
                        >
                            <RichTextEditor
                                defaultValue={props.initialValues && props.initialValues.content}
                                onDataChange={(value) => setContent(value)}
                            />
                        </Card>
                        <Card
                            size="small"
                            bordered={false}
                            title={"Cấu hình SEO"}
                        >
                            <Form.Item
                                label="Tiêu đề"
                                name="seoTitle"
                            >
                                <Input />
                            </Form.Item>
                            <Form.Item
                                label="Mô tả"
                                name="seoDescription"
                            >
                                <Input.TextArea/>
                            </Form.Item>
                            <Form.Item
                                label="Từ khoá"
                                name="seoKeyword"
                            >
                                <Input />
                            </Form.Item>
                            {seoImage &&
                                <div>
                                    <Image src={AppConfig.apiUrl+seoImage} className={"w-24 h-24 object-cover"}/>
                                </div>
                            }
                            <Button onClick={handleOpenFileMedia}>
                                {seoImage ? 'Chọn ảnh khác' : "Chọn ảnh chia sẻ"}
                            </Button>
                        </Card>
                    </Col>
                    <Col xs={24} md={7} lg={7}>
                        <Card>
                            {props.type === 'edit' &&
                                <Form.Item name="status" label="Trạng thái">
                                    <Select
                                        style={{width: '100%'}}
                                        placeholder="Chọn trạng thái"
                                    >
                                        <Option key={1} value="ACTIVE">Đăng</Option>
                                        <Option key={2} value="DEACTIVE">Nháp</Option>
                                    </Select>
                                </Form.Item>
                            }
                            <Button
                                type="primary"
                                htmlType="submit"
                                size="large"
                                block
                                icon={props.type === 'add' ? <CheckOutlined /> : <SaveOutlined />}
                            >
                                {props.type === 'add' ? 'Xuất bản' : 'Lưu thay đổi'}
                            </Button>
                            {props.type === 'edit' &&
                                <Popconfirm
                                    title={`Bạn chắc chắn muốn xoá?`}
                                    onConfirm={() => props.onDelete(props.id)}
                                    okText="Có"
                                    cancelText="Không"
                                >
                                    <Button type="link" block danger className="mt-5" >
                                        Xoá trang
                                    </Button>
                                </Popconfirm>
                            }
                        </Card>

                    </Col>
                </Row>
            </Form>
        </div>
    );
}

export default PageForm;
