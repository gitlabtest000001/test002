import React, {useState, useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import queryString from 'query-string';
import { AppConfig } from '../../../appConfig';
import axios from 'axios';
import { hideLoader, showLoader } from '../../App/actions';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader } from 'antd';
import Button from 'antd/es/button';
import history from '../../../utils/history';
import Table from 'antd/es/table';
import { formatDateTime } from '../../../utils/helpers';
import { EditTwoTone } from '@ant-design/icons';
import Space from 'antd/es/space';
import Tag from 'antd/es/tag';
import { Link } from 'react-router-dom';

function Pages(props) {
    const dispatch = useDispatch();
    let searchQuery = queryString.parse(props.location.search);
    const [result, setResult] = useState();
    const [limit, setLimit] = useState(searchQuery && searchQuery.limit ? searchQuery.limit : 10);
    const [page, setPage] = useState(searchQuery && searchQuery.page ? searchQuery.page : 1);
    const [search, setSearch] = useState(null);

    useEffect(() => {
        dispatch(showLoader());
        async function getPages() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/page`,
                params: {
                  limit, page
                },
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setResult(response.data);
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getPages();
    }, [limit, page])

    console.log(result);

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Tiêu đề',
            dataIndex: 'title',
            key: 'title',
            render: (key, row) =>
                <Link
                    className={"text-blue-500"}
                    to={`/admin/page/edit/${row.id}`}
                >
                    {row.title}
                </Link>
        },
        {
            title: 'Đường dẫn',
            dataIndex: 'slug',
            key: 'slug',
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            key: 'status',
            render: (key, row) => (
                <>
                    {(() => {
                        switch(row.status) {
                            case 'ACTIVE':
                                return <Tag color="green">Đăng</Tag>;
                            case 'DEACTIVE':
                                return <Tag color="red">Ẩn</Tag>;
                            default:
                                return null;
                        }
                    })()}
                </>
            ),
        },
        {
            title: 'Ngày tháng',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: (key, row) => formatDateTime(row.createdAt)
        },
        {
            title: 'Người tạo',
            dataIndex: 'createdBy',
            key: 'createdBy',
            render: (key, row) => row.createdBy.name
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: (key, row) => (
                <Space size={'middle'}>
                    <Button
                        type="primary"
                        size="small"
                        ghost
                        icon={<EditTwoTone />}
                        onClick={() => history.push(`/admin/page/edit/${row.id}`)}
                    >
                        Chi tiết
                    </Button>
                </Space>
            )
        }
    ]

    function handleChangePage(page, pageSize) {
        setPage(page);
        setLimit(pageSize);
    }

    return (
        <div>
            <Helmet>
                <title>Quản lý trang</title>
            </Helmet>
            <PageHeader
                title={"Quản lý trang"}
                className={"mb-5"}
                ghost={false}
                extra={
                    <Button
                        onClick={() => history.push('/admin/page/add')}
                    >
                        Thêm trang
                    </Button>
                }
            />
            <Table
                scroll={{x: '100%'}}
                columns={columns}
                dataSource={result && result.list}
                pagination={{
                    current: page,
                    pageSize: limit,
                    total: result && result.count,
                    onChange: ((page, pageSize) => handleChangePage(page, pageSize)),
                    position: ['bottomLeft']
                }}
            />
        </div>
    );
}

export default Pages;
