import React from 'react';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader } from 'antd';
import Button from 'antd/es/button';
import history from '../../../utils/history';
import PageForm from './PageForm';
import { useDispatch } from 'react-redux';
import axios from 'axios';
import { hideLoader, showLoader } from '../../App/actions';
import { AppConfig } from '../../../appConfig';
import { ErrorMessage, SuccessMessage } from '../../../components/Message';

function PageAdd(props) {
    const dispatch = useDispatch()

    function handleCreatePage(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.post(
            `${AppConfig.apiUrl}/manager/page`,
            data,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Tạo trang thành công'});
            dispatch(hideLoader())
            history.push(`/admin/page/edit/${response.data.id}`)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    return (
        <div>
            <Helmet>
                <title>Quản lý trang</title>
            </Helmet>
            <PageHeader
                title={"Tạo trang"}
                className={"mb-5"}
                ghost={false}
                onBack={() => history.goBack()}
            />
            <PageForm
                type={"add"}
                onSubmit={handleCreatePage}
            />
        </div>
    );
}

export default PageAdd;
