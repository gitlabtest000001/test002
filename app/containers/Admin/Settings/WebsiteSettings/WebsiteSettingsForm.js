import React, { useState } from 'react';
import Form from 'antd/es/form';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import Card from 'antd/es/card';
import Input from 'antd/es/input';
import { Image, InputNumber, Select } from 'antd';
import Button from 'antd/es/button';
import TextArea from 'antd/es/input/TextArea';
import RichTextEditor from '../../../../components/RTE';
import { changeModalContent, changeModalTitle, showModal } from '../../../App/actions';
import FileManager from '../../../../components/FileManager';
import { AppConfig } from '../../../../appConfig';
import { useDispatch } from 'react-redux';

function WebsiteSettingsForm(props) {
    const dispatch = useDispatch()
    const [paymentInfo, setPaymentInfo] = useState(props.initialValues && props.initialValues.payment_info_company);
    const [seoImage, setSeoImage] = useState(props.initialValues && props.initialValues.seoImage);

    function handleSubmit(values) {
        props.onSubmit({...values, payment_info_company: paymentInfo, seoImage: seoImage})
    }
    function handleOpenFileMedia() {
        dispatch(showModal())
        dispatch(changeModalTitle("File Manager"))
        dispatch(changeModalContent(
            <FileManager
                onChooseFile={(value) => setSeoImage(value[0].url)}
                multiple={false}
                manager={true}
            />
        ))
    }

    return (
        <Form
            layout="vertical"
            name="SettingsForm"
            onFinish={handleSubmit}
            initialValues={props.initialValues}
        >
            <Row gutter={[16, 16]} className="my-5">
                <Col lg={12} md={12} xs={24}>
                    <Card title="Thông tin chung" size="small">
                        <Row gutter={16}>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Tên website"
                                    name="website_name"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Website URL"
                                    name="website_url"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Số tiền ít nhất có thể RÚT"
                                    name="minimum_withdraw_amount"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Google Play Link"
                                    name="google_play"
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Apple Store Link"
                                    name="app_store"
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={24} md={24}>
                                <Form.Item
                                    label="Thông tin nhận tiền"
                                    name="payment_info_company"
                                >
                                    <RichTextEditor
                                        defaultValue={props.initialValues && props.initialValues.payment_info_company}
                                        onDataChange={(value) => setPaymentInfo(value)}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Card>
                </Col>

                <Col lg={12} md={12} xs={24}>
                    <Card
                        size="small"
                        bordered={false}
                        title={"Cấu hình SEO"}
                    >
                        <Form.Item
                            label="Tiêu đề"
                            name="seoTitle"
                        >
                            <Input />
                        </Form.Item>
                        <Form.Item
                            label="Mô tả"
                            name="seoDescription"
                        >
                            <Input.TextArea/>
                        </Form.Item>
                        <Form.Item
                            label="Từ khoá"
                            name="seoKeyword"
                        >
                            <Input />
                        </Form.Item>
                        {seoImage &&
                            <div>
                                <Image src={AppConfig.apiUrl+seoImage} className={"w-24 h-24 object-cover"}/>
                            </div>
                        }
                        <Button onClick={handleOpenFileMedia}>
                            {seoImage ? 'Chọn ảnh khác' : "Chọn ảnh chia sẻ"}
                        </Button>
                    </Card>
                </Col>

                <Col lg={12} md={12} xs={24}>
                    <Card title="Thông tin liên hệ" size="small">
                        <Row gutter={16}>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Tên công ty"
                                    name="company_name"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Email support"
                                    name="contact_email"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Hotline"
                                    name="contact_hotline"
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Whatsapp"
                                    name="contact_whatsapp"
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Zalo"
                                    name="contact_zalo"
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={24} md={24}>
                                <Form.Item
                                    label="Địa chỉ"
                                    name="contact_address"
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Card>
                </Col>
                <Col lg={12} md={12} xs={24}>
                    <Card title="Bán cổ phần" size="small">
                        <Row gutter={16}>

                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Giá trị của 1 cổ phần"
                                    name="stock_price"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"đ"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Số lượng cổ phần có thể bán"
                                    name="stock_available"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Số tiền ít nhất để Gán lấy sản phẩm"
                                    name="minimum_exchange_stock_amount"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>

                        </Row>
                    </Card>
                </Col>
                <Col lg={12} md={12} xs={24}>
                    <Card title="Social Media" size="small">
                        <Row gutter={16}>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Facebook Fanpage"
                                    name="facebook"
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Youtube Channel"
                                    name="youtube"
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>

                        </Row>
                    </Card>
                </Col>

            </Row>
            <Button type="primary" htmlType="submit" size="large">
                Lưu thay đổi
            </Button>
        </Form>
    );
}

export default WebsiteSettingsForm;
