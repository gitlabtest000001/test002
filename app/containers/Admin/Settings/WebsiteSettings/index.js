import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { hideLoader, showLoader } from '../../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader } from 'antd';
import Loading from '../../../../components/Loading';
import WebsiteSettingsForm from './WebsiteSettingsForm';
import { ErrorMessage, SuccessMessage } from '../../../../components/Message';

function WebsiteSettings(props) {
    const dispatch = useDispatch()
    const settings = useSelector(state => state.root.settings.options);

    if (settings) {
        var initialValues = {
            website_name: settings.website_name,
            website_url: settings.website_url,
            logo: settings.logo,
            google_play: settings.google_play,
            app_store: settings.app_store,
            payment_info_company: settings.payment_info_company,
            company_name: settings.company_name,
            contact_address: settings.contact_address,
            contact_hotline: settings.contact_hotline,
            contact_email: settings.contact_email,
            contact_whatsapp: settings.contact_whatsapp,
            contact_zalo: settings.contact_zalo,
            facebook: settings.facebook,
            youtube: settings.youtube,
            stock_price: settings.stock_price,
            stock_available: settings.stock_available,
            minimum_withdraw_amount: settings.minimum_withdraw_amount,
            minimum_exchange_stock_amount: settings.minimum_exchange_stock_amount,
            seoImage: settings.seoImage,
            seoTitle: settings.seoTitle,
            seoDescription: settings.seoDescription,
            seoKeyword: settings.seoKeyword,
        }
    }

    function handleUpdateSettings(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.put(
            `${AppConfig.apiUrl}/manager/setting`,
            data,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Đã lưu lại cài đặt'});
            dispatch(hideLoader())
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    return (
        <>
            <Helmet>
                <title>Cài đặt website</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title="Cài đặt website"
            />
            {!settings ? <Loading/> :
                <WebsiteSettingsForm initialValues={settings && initialValues} onSubmit={handleUpdateSettings}/>
            }
        </>
    );
}

export default WebsiteSettings;
