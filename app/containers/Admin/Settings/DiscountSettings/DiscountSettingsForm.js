import React, { useState } from 'react';
import Form from 'antd/es/form';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import Card from 'antd/es/card';
import Input from 'antd/es/input';
import { InputNumber, Select } from 'antd';
import Button from 'antd/es/button';
import TextArea from 'antd/es/input/TextArea';
import RichTextEditor from '../../../../components/RTE';

function DiscountSettingsForm(props) {

    function handleSubmit(values) {
        props.onSubmit(values)
    }

    return (
        <Form
            layout="vertical"
            name="SettingsForm"
            onFinish={handleSubmit}
            initialValues={props.initialValues}
        >
            <Row gutter={[16, 16]} className="my-5">
                <Col lg={12} md={12} xs={24}>
                    <Card title="Chiết khấu cho khách hàng" size="small">
                        <Row gutter={16} className={"border-b border-gray-200 pb-2 mb-5"}>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Giá trị đơn hàng"
                                    name="customer_order_value_lte"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"đ"}
                                        addonBefore={"<="}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Chiết khấu"
                                    name="customer_order_value_lte_percent"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                        <Row gutter={16}>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Giá trị đơn hàng"
                                    name="customer_order_value_gt"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"đ"}
                                        addonBefore={">"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Chiết khấu"
                                    name="customer_order_value_gt_percent"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Card>
                </Col>
                <Col lg={12} md={12} xs={24}>
                    <Card title="Chiết khấu cho Đại lý các cấp" size="small">
                        <Row gutter={16}>

                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Chiết khấu Đại lý cấp 1"
                                    name="agency_discount_level1"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Chiết khấu Đại lý cấp 2"
                                    name="agency_discount_level2"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Chiết khấu Đại lý cấp 3"
                                    name="agency_discount_level3"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Card>
                </Col>
                <Col lg={12} md={12} xs={24}>
                    <Card title="Chiết khấu cho Nhà đầu tư" size="small">
                        <Row gutter={16}>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Chiết khấu trực tiếp"
                                    name="investor_discount"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Card>
                </Col>
                <Col lg={12} md={12} xs={24}>
                    <Card title="Chiết khấu cho Đại lý" size="small">
                        <Row gutter={16}>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Giá trị đơn hàng"
                                    name="agency_order_value_gt"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"đ"}
                                        addonBefore={">="}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Chiết khấu"
                                    name="agency_order_value_gt_percent"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Card>
                </Col>
            </Row>
            <Button type="primary" htmlType="submit" size="large">
                Lưu thay đổi
            </Button>
        </Form>
    );
}

export default DiscountSettingsForm;
