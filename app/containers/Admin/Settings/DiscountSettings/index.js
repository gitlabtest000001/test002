import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { hideLoader, showLoader } from '../../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader } from 'antd';
import Loading from '../../../../components/Loading';
import DiscountSettingsForm from './DiscountSettingsForm';
import { ErrorMessage, SuccessMessage } from '../../../../components/Message';

function DiscountSettings(props) {
    const dispatch = useDispatch()
    const settings = useSelector(state => state.root.settings.options);

    if (settings) {
        var initialValues = {
            customer_order_value_lte: settings.customer_order_value_lte,
            customer_order_value_lte_percent: settings.customer_order_value_lte_percent,
            customer_order_value_gt: settings.customer_order_value_gt,
            customer_order_value_gt_percent: settings.customer_order_value_gt_percent,
            //
            agency_order_value_gt: settings.agency_order_value_gt,
            agency_order_value_gt_percent: settings.agency_order_value_gt_percent,
            agency_discount_level1: settings.agency_discount_level1,
            agency_discount_level2: settings.agency_discount_level2,
            agency_discount_level3: settings.agency_discount_level3,
            //
            investor_discount: settings.investor_discount,

        }
    }

    function handleUpdateSettings(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.put(
            `${AppConfig.apiUrl}/manager/setting`,
            data,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Đã lưu lại cài đặt'});
            dispatch(hideLoader())
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    return (
        <>
            <Helmet>
                <title>Cài đặt Chiết khấu</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title="Cài đặt Chiết khấu"
            />
                <>
                    {!settings ? <Loading/> :
                        <DiscountSettingsForm initialValues={settings && initialValues} onSubmit={handleUpdateSettings}/>
                    }
                </>
        </>
    );
}

export default DiscountSettings;
