import React from 'react';
import { Helmet } from 'react-helmet/es/Helmet';
import PageHeader from 'antd/es/page-header';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import { Link } from 'react-router-dom';

function MenuSettings(props) {
    const menuLocations = [
        {name: 'Menu Ngang', location: 'MainMenu'},
        {name: 'Menu Thông tin', location: 'InformationMenu'},
        {name: 'Menu Chính sách', location: 'PolicyMenu'},
        {name: 'Menu Thông tin Mobile', location: 'InformationMobileMenu'},
    ]
    return (
        <div>
            <Helmet>Quản lý menu</Helmet>
            <PageHeader
                ghost={false}
                title={"Quản lý menu"}
                className={"mb-5"}
            />
            <Row gutter={[32, 32]}>
                {menuLocations.map((item, index) => (
                    <Col xs={24} lg={6} md={12}>
                        <div className={"flex items-center justify-between bg-white px-5 py-3"}>
                            {item.name}
                            <Link className={"text-blue-500"} to={`/admin/settings/menu/${item.location.toLowerCase()}`}>
                                Sửa
                            </Link>
                        </div>
                    </Col>
                ))}
            </Row>
        </div>
    );
}

export default MenuSettings;
