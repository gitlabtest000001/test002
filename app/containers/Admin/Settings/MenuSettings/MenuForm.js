import React from 'react';
import Card from 'antd/es/card';
import Form from 'antd/es/form';
import Input from 'antd/es/input';
import AntTreeSelect from '../../../../components/AntTreeSelect';
import { Image } from 'antd';
import { AppConfig } from '../../../../appConfig';
import Button from 'antd/es/button';
import { CheckOutlined, SaveOutlined } from '@ant-design/icons';
import Select from 'antd/es/select';

const {Option} = Select;

function MenuForm(props) {

    function handleSubmit(values) {
        if (props.formType === "add") {
            props.onSubmit(values)
        } else props.onSubmit({data: values, id: props.initialValues.id})

    }

    return (
        <div>
            <Form
                layout="vertical"
                name="CategoryForm"
                onFinish={handleSubmit}
                initialValues={props.initialValues}
            >
                <Card
                    title={props.formType === 'add' && 'Thêm'}
                    bordered={false}
                    size="small"
                >
                    <Form.Item
                        label="Tiêu đề"
                        name="title"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập tiêu đề'},
                        ]}
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item
                        label="Liên kết"
                        name="url"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập tiêu đề'},
                        ]}
                        help="Liên kết nội bộ không cần nhập tên miền. Ví dụ: /trang/gioi-thieu"
                    >
                        <Input />
                    </Form.Item>
                    <Form.Item name="type" label="Loại liên kết">
                        <Select
                            style={{width: '100%'}}
                            placeholder="Chọn loại"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập tiêu đề'},
                            ]}
                        >
                            <Option key={1} value="Internal">Liên kết nội bộ</Option>
                            <Option key={2} value="External">Liên kết bên ngoài</Option>
                        </Select>
                    </Form.Item>
                    <Button
                        type="primary"
                        htmlType="submit"
                        icon={props.formType === 'add' ? <CheckOutlined /> : <SaveOutlined />}
                    >
                        {props.formType === 'add' ? 'Thêm' : 'Lưu thay đổi'}
                    </Button>
                </Card>

            </Form>
        </div>
    );
}

export default MenuForm;
