import React, {useState, useEffect} from 'react';
import { useDispatch } from 'react-redux';
import { Helmet } from 'react-helmet/es/Helmet';
import PageHeader from 'antd/es/page-header';
import history from '../../../../utils/history';
import {
    changeModalContent,
    changeModalTitle,
    hideLoader,
    hideModal,
    showLoader,
    showModal,
} from '../../../App/actions';
import '../../../../styles/sort-style.css';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import MenuForm from './MenuForm';
import { ErrorMessage, SuccessMessage } from '../../../../components/Message';
import Loading from '../../../../components/Loading';
import { arrayMove, SortableContainer, SortableElement } from 'react-sortable-hoc';
import Card from 'antd/es/card';
import Button from 'antd/es/button';

function MenuBuilder(props) {
    const dispatch = useDispatch()
    const location = props.match.params.location;
    const [list, setList] = useState()
    const [flag, setFlag] = useState(true);

    useEffect(() => {
        dispatch(showLoader());
        async function getRewards() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/menu`,
                params: {
                    location,
                },
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setList(response.data);
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getRewards();
    }, [dispatch, location, flag])

    function handleAdd(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.post(
            `${AppConfig.apiUrl}/manager/menu`,
            {...data, location},
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Đã thêm vào menu'});
            setList([response.data, ...list])
            dispatch(hideLoader())
            setFlag(!flag)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    function handleDelete(id) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.delete(
            `${AppConfig.apiUrl}/manager/menu/${id}`,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Đã xoá menu'});
            const index = list.findIndex(item => Number(item.id) === Number(id));
            if (index !== -1) {
                const newList = [
                    ...list.slice(0, index),
                    response.data,
                    ...list.slice(index + 1),
                ];
                setList(newList)
            }
            dispatch(hideLoader())
            setFlag(!flag)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    function handleEditMenu(data) {
        console.log(data.values);
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.put(
            `${AppConfig.apiUrl}/manager/menu/${data.id}`,
            data.data,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Đã sửa menu'});
            //setList([response.data, ...list])
            dispatch(hideLoader())
            dispatch(hideModal())
            setFlag(!flag)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    function handleEditModal(item) {
        dispatch(showModal())
        dispatch(changeModalTitle("Sửa menu"))
        dispatch(changeModalContent(
            <MenuForm
                initialValues={item}
                formType={"edit"}
                onSubmit={handleEditMenu}
            />
        ))
    }

    const MenuItem = SortableElement(({item}) =>
        <div className={"bg-white px-5 py-3 flex items-center justify-between border border-gray-200 mb-2"}>
            <div>
                <p className={"font-medium"}>{item.title}</p>
                <p className={"italic text-xs"}>{item.url}</p>
            </div>
            <div>
                <Button
                    type={"link"}
                    onClick={() => handleEditModal(item)}
                >
                    Sửa
                </Button>
                <Button
                    type={"link"}
                    danger
                    onClick={() => handleDelete(item.id)}
                >
                    Xoá
                </Button>
            </div>
        </div>
    )

    const MenuList = SortableContainer(({items}) =>
        <div>
            {items.map((item, index) => (
                <MenuItem key={`section-${index}`} index={index} item={item} />
            ))}
        </div>
    )

    const onSortEnd = ({oldIndex, newIndex}) => {
        console.log('oldIndex', oldIndex);
        console.log('newIndex', newIndex);
        if (oldIndex !== newIndex) {
            const newData = arrayMove([].concat(list), oldIndex, newIndex).filter(el => !!el);
            const arrSort = [];
            for (const el of newData) {
                const dataSort = {
                    sortId: newData.indexOf(el),
                    id: el.id
                }
                arrSort.push(dataSort)
            }

            console.log(arrSort);
            //dispatch(SortComboPrice(newData))
            //dispatch(SortComboPrice(newData, {priceData: JSON.stringify(arrSort)}))
            dispatch(showLoader())
            const Token = localStorage.getItem('dvg_manager_token');
            axios.put(
                `${AppConfig.apiUrl}/manager/menu/updateSort`,
                { menuData: JSON.stringify(arrSort) },
                {headers: {Authorization: `Bearer ${Token}`}}
            ).then(function (response) {
                SuccessMessage({message: 'Đã lưu sắp xếp'});
                dispatch(hideLoader())
                setFlag(!flag)
            }).catch((function (error) {
                ErrorMessage(error.response.data)
                dispatch(hideLoader())
            }))
        }
    }

    return (
        !list ? <Loading /> :
        <div>
            <Helmet><title>Menu Builder</title></Helmet>
            <PageHeader
                className={"mb-5"}
                title={location.toUpperCase()}
                ghost={false}
                onBack={() => history.goBack()}
            />
            <Row gutter={[32, 32]}>
                <Col xs={24} lg={8} md={8}>
                    <MenuForm
                        formType={"add"}
                        onSubmit={handleAdd}
                        initialValues={{
                            type: 'Internal'
                        }}
                    />
                </Col>
                <Col xs={24} lg={16} md={16}>
                    {list && list.length > 0 &&
                        <Card>
                            <MenuList
                                pressDelay={200}
                                items={list}
                                onSortEnd={onSortEnd}
                                lockAxis={"y"}
                                helperClass="row-dragging"
                            />
                        </Card>
                    }
                </Col>
            </Row>
        </div>
    );
}

export default MenuBuilder;
