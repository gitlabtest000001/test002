import React, { useState } from 'react';
import Form from 'antd/es/form';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import Card from 'antd/es/card';
import Input from 'antd/es/input';
import { InputNumber, Select } from 'antd';
import Button from 'antd/es/button';
import TextArea from 'antd/es/input/TextArea';
import RichTextEditor from '../../../../components/RTE';

function UpgradeLevelSettingsForm(props) {

    function handleSubmit(values) {
        props.onSubmit(values)
    }

    return (
        <Form
            layout="vertical"
            name="SettingsForm"
            onFinish={handleSubmit}
            initialValues={props.initialValues}
        >
            <Row gutter={[16, 16]} className="my-5">
                <Col lg={12} md={12} xs={24}>
                    <Card title="Điều kiện doanh thu để lên cấp" size="small">
                        <Row gutter={16}>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Doanh số lên Đại lý"
                                    name="upgrade_to_agency_value"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"đ"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Doanh số lên Đại lý cấp 1"
                                    name="upgrade_to_agency_level1_value"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"đ"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Doanh số lên Đại lý cấp 2"
                                    name="upgrade_to_agency_level2_value"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"đ"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Doanh số lên Đại lý cấp 3"
                                    name="upgrade_to_agency_level3_value"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"đ"}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Card>
                </Col>
            </Row>
            <Button type="primary" htmlType="submit" size="large">
                Lưu thay đổi
            </Button>
        </Form>
    );
}

export default UpgradeLevelSettingsForm;
