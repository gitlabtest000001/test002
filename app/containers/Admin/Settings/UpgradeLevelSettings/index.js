import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { hideLoader, showLoader } from '../../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader } from 'antd';
import Loading from '../../../../components/Loading';
import UpgradeLevelSettingsForm from './UpgradeLevelSettingsForm';
import { ErrorMessage, SuccessMessage } from '../../../../components/Message';

function UpgradeLevelSettings(props) {
    const dispatch = useDispatch()
    const settings = useSelector(state => state.root.settings.options);

    if (settings) {
        var initialValues = {
            upgrade_to_agency_value: settings.upgrade_to_agency_value,
            upgrade_to_agency_level1_value: settings.upgrade_to_agency_level1_value,
            upgrade_to_agency_level2_value: settings.upgrade_to_agency_level2_value,
            upgrade_to_agency_level3_value: settings.upgrade_to_agency_level3_value,

        }
    }

    function handleUpdateSettings(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.put(
            `${AppConfig.apiUrl}/manager/setting`,
            data,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Đã lưu lại cài đặt'});
            dispatch(hideLoader())
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    return (
        <>
            <Helmet>
                <title>Cài đặt Điều kiện lên cấp</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title="Cài đặt Điều kiện lên cấp"
            />
                <>
                    {!settings ? <Loading/> :
                        <UpgradeLevelSettingsForm initialValues={settings && initialValues} onSubmit={handleUpdateSettings}/>
                    }
                </>
        </>
    );
}

export default UpgradeLevelSettings;
