import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { hideLoader, showLoader } from '../../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader } from 'antd';
import Loading from '../../../../components/Loading';
import RewardSettingsForm from './RewardSettingsForm';
import { ErrorMessage, SuccessMessage } from '../../../../components/Message';

function RewardSettings(props) {
    const dispatch = useDispatch()
    const settings = useSelector(state => state.root.settings.options);

    if (settings) {
        var initialValues = {
            // thưởng đại lý khi mua hàng
            reward_agency_level1: settings.reward_agency_level1,
            reward_agency_level2: settings.reward_agency_level2,
            reward_agency_level3: settings.reward_agency_level3,
            // thưởng hàng tháng NĐT
            investor_monthly_reward: settings.investor_monthly_reward,
            // thưởng hàng tháng theo doanh thu
            reward_monthly_level1: settings.reward_monthly_level1,
            reward_monthly_level2: settings.reward_monthly_level2,
            reward_monthly_level3: settings.reward_monthly_level3,
            reward_monthly_level4: settings.reward_monthly_level4,
            reward_monthly_level5: settings.reward_monthly_level5,
            reward_monthly_level6: settings.reward_monthly_level6,
            reward_monthly_level1_percent: settings.reward_monthly_level1_percent,
            reward_monthly_level2_percent: settings.reward_monthly_level2_percent,
            reward_monthly_level3_percent: settings.reward_monthly_level3_percent,
            reward_monthly_level4_percent: settings.reward_monthly_level4_percent,
            reward_monthly_level5_percent: settings.reward_monthly_level5_percent,
            reward_monthly_level6_percent: settings.reward_monthly_level6_percent,
            monthly_office_revenue: settings.monthly_office_revenue,
            reward_monthly_office_percent: settings.reward_monthly_office_percent,
            // thưởng hàng năm theo doanh thu
            reward_yearly_level1: settings.reward_yearly_level1,
            reward_yearly_level2: settings.reward_yearly_level2,
            reward_yearly_level3: settings.reward_yearly_level3,
            reward_yearly_level4: settings.reward_yearly_level4,
            reward_yearly_level1_percent: settings.reward_yearly_level1_percent,
            reward_yearly_level2_percent: settings.reward_yearly_level2_percent,
            reward_yearly_level3_percent: settings.reward_yearly_level3_percent,
            reward_yearly_level4_percent: settings.reward_yearly_level4_percent,
        }
    }

    function handleUpdateSettings(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.put(
            `${AppConfig.apiUrl}/manager/setting`,
            data,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Đã lưu lại cài đặt'});
            dispatch(hideLoader())
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    return (
        <>
            <Helmet>
                <title>Cài đặt Thưởng</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title="Cài đặt Thưởng"
            />
            {!settings ? <Loading/> :
                <RewardSettingsForm initialValues={settings && initialValues} onSubmit={handleUpdateSettings}/>
            }
        </>
    );
}

export default RewardSettings;
