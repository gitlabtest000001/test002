import React, { useState } from 'react';
import Form from 'antd/es/form';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import Card from 'antd/es/card';
import Input from 'antd/es/input';
import { InputNumber, Select } from 'antd';
import Button from 'antd/es/button';
import TextArea from 'antd/es/input/TextArea';
import RichTextEditor from '../../../../components/RTE';

function RewardSettingsForm(props) {

    function handleSubmit(values) {
        props.onSubmit(values)
    }

    return (
        <Form
            layout="vertical"
            name="SettingsForm"
            onFinish={handleSubmit}
            initialValues={props.initialValues}
        >
            <Row gutter={[16, 16]} className="my-5">
                <Col lg={12} md={12} xs={24}>
                    <Card title="Thưởng hàng tháng" size="small">
                        <Row gutter={16}>
                            <Col xs={24} lg={24} md={24}>
                                <Row gutter={16} className={"border-b border-gray-200 mb-5"}>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Doanh số cộng dồn 1"
                                            name="reward_monthly_level1"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"đ"}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Thưởng 1"
                                            name="reward_monthly_level1_percent"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"%"}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs={24} lg={24} md={24}>
                                <Row gutter={16} className={"border-b border-gray-200 mb-5"}>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Doanh số cộng dồn 2"
                                            name="reward_monthly_level2"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"đ"}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Thưởng 2"
                                            name="reward_monthly_level2_percent"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"%"}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs={24} lg={24} md={24}>
                                <Row gutter={16} className={"border-b border-gray-200 mb-5"}>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Doanh số cộng dồn 3"
                                            name="reward_monthly_level3"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"đ"}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Thưởng 3"
                                            name="reward_monthly_level3_percent"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"%"}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs={24} lg={24} md={24}>
                                <Row gutter={16} className={"border-b border-gray-200 mb-5"}>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Doanh số cộng dồn 4"
                                            name="reward_monthly_level4"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"đ"}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Thưởng 4"
                                            name="reward_monthly_level4_percent"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"%"}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs={24} lg={24} md={24}>
                                <Row gutter={16} className={"border-b border-gray-200 mb-5"}>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Doanh số cộng dồn 5"
                                            name="reward_monthly_level5"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"đ"}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Thưởng 5"
                                            name="reward_monthly_level5_percent"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"%"}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs={24} lg={24} md={24}>
                                <Row gutter={16} className={"border-b border-gray-200 mb-5"}>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Doanh số cộng dồn 6"
                                            name="reward_monthly_level6"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"đ"}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Thưởng 6"
                                            name="reward_monthly_level6_percent"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"%"}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs={24} lg={24} md={24}>
                                <Row gutter={16} className={"border-b border-gray-200 mb-5"}>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Doanh số cộng dồn Văn phòng kinh doanh"
                                            name="monthly_office_revenue"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"đ"}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Thưởng Văn phòng kinh doanh"
                                            name="reward_monthly_office_percent"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"%"}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>

                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Thưởng tháng cho nhà đầu tư"
                                    name="investor_monthly_reward"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Card>
                </Col>
                <Col lg={12} md={12} xs={24}>
                    <Card title="Thưởng năm" size="small">
                        <Row gutter={16}>
                            <Col xs={24} lg={24} md={24}>
                                <Row gutter={16} className={"border-b border-gray-200 mb-5"}>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Doanh số cộng dồn 1"
                                            name="reward_yearly_level1"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"đ"}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Thưởng 1"
                                            name="reward_yearly_level1_percent"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"%"}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs={24} lg={24} md={24}>
                                <Row gutter={16} className={"border-b border-gray-200 mb-5"}>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Doanh số cộng dồn 2"
                                            name="reward_yearly_level2"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"đ"}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Thưởng 2"
                                            name="reward_yearly_level2_percent"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"%"}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs={24} lg={24} md={24}>
                                <Row gutter={16} className={"border-b border-gray-200 mb-5"}>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Doanh số cộng dồn 3"
                                            name="reward_yearly_level3"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"đ"}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Thưởng 3"
                                            name="reward_yearly_level3_percent"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"%"}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>
                            <Col xs={24} lg={24} md={24}>
                                <Row gutter={16}>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Doanh số cộng dồn 4"
                                            name="reward_yearly_level4"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"đ"}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Thưởng 4"
                                            name="reward_yearly_level4_percent"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"%"}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Card>
                </Col>
                <Col lg={12} md={12} xs={24}>
                    <Card title="Thưởng Đại lý các cấp khi mua hàng" size="small">
                        <Row gutter={16}>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Đại lý cấp 1"
                                    name="reward_agency_level1"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Đại lý cấp 2"
                                    name="reward_agency_level2"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Đại lý cấp 3"
                                    name="reward_agency_level3"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Card>
                </Col>
            </Row>
            <Button type="primary" htmlType="submit" size="large">
                Lưu thay đổi
            </Button>
        </Form>
    );
}

export default RewardSettingsForm;
