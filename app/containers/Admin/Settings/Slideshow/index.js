import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { hideLoader, showLoader } from '../../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';
import { Helmet } from 'react-helmet/es/Helmet';
import PageHeader from 'antd/es/page-header';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import SlideshowForm from './SlideshowForm';
import Space from 'antd/es/space';
import { Link } from 'react-router-dom';
import Button from 'antd/es/button';
import Popconfirm from 'antd/es/popconfirm';
import Table from 'antd/es/table';
import Loading from '../../../../components/Loading';
import history from '../../../../utils/history';
import { SuccessMessage } from '../../../../components/Message';
import Image from 'antd/es/image';

function SlideshowSettings(props) {
    const dispatch = useDispatch();
    const [slideshows, setSlideshows] = useState()
    const [flag, setFlag] = useState(true)

    useEffect(() => {
        dispatch(showLoader());
        async function getSlideshow() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/slideshow`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setSlideshows(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getSlideshow();
    },[dispatch, flag])
    console.log(slideshows);

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Title',
            dataIndex: 'title',
            key: 'title',
        },
        {
            title: 'Image',
            dataIndex: 'image',
            key: 'image',
            render: (key, row) =>
                <Image src={AppConfig.apiUrl+row.image} className={"w-20 h-20 object-cover"}/>
        },
        {
            title: 'Link',
            dataIndex: 'link',
            key: 'link',
        },
        {
            title: 'Action',
            dataIndex: 'action',
            key: 'action',
            render: (key, row) =>
                <Space>
                    <Link to={`/admin/settings/slideshow/edit/${row.id}`}>
                        <Button type="link" size="small">
                            Sửa
                        </Button>
                    </Link>
                    <Popconfirm
                        title={`Bạn chắc chắn muốn xoá?`}
                        onConfirm={() => handleDeleteSlideshow(row.id)}
                        okText="Có"
                        cancelText="Không"
                    >
                        <Button danger type="link">
                            Xoá
                        </Button>
                    </Popconfirm>
                </Space>
        },
    ]

    function handleDeleteSlideshow(id) {
        dispatch(showLoader());
        const Token = localStorage.getItem('dvg_manager_token');
        axios({
            method: 'delete',
            url: `${AppConfig.apiUrl}/manager/slideshow/${id}`,
            headers: {Authorization: `Bearer ${Token}`}
        }).then(function (response) {
            SuccessMessage({message: 'Đã xoá slideshow'});
            dispatch(hideLoader());
            setFlag(!flag)
        }).catch(function(error){
            console.log(error);
            dispatch(hideLoader())
        })
    }

    function handleAddSlideshow(data) {
        dispatch(showLoader());
        const Token = localStorage.getItem('dvg_manager_token');
        axios({
            method: 'post',
            url: `${AppConfig.apiUrl}/manager/slideshow`,
            data,
            headers: {Authorization: `Bearer ${Token}`}
        }).then(function (response) {
            if (response.status === 201) {
                SuccessMessage({message: 'Tạo slideshow thành công'});
                dispatch(hideLoader());
                setSlideshows(slideshows.concat(response.data))
            }
        }).catch(function(error){
            console.log(error);
            dispatch(hideLoader())
        })
    }

    return (
        !slideshows ? <Loading /> :
        <div>
            <Helmet>
                <title>Quản lý slideshow</title>
            </Helmet>
            <PageHeader
                ghost={false}
                className={"mb-5"}
                title={"Quản lý slideshow"}
            />
            <Row gutter={[16, 16]}>
                <Col xs={24} lg={8} md={8}>
                    <SlideshowForm
                        formType={"add"}
                        onSubmit={handleAddSlideshow}
                    />
                </Col>
                <Col xs={24} lg={16} md={16}>
                    <Table
                        columns={columns}
                        dataSource={slideshows && slideshows}
                    />
                </Col>
            </Row>
        </div>
    );
}

export default SlideshowSettings;
