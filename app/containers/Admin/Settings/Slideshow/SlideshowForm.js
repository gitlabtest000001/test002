import React, { useState } from 'react';
import { changeModalContent, changeModalTitle, showModal } from '../../../App/actions';
import FileManager from '../../../../components/FileManager';
import useForm from 'antd/es/form/hooks/useForm';
import { useDispatch } from 'react-redux';
import Form from 'antd/es/form';
import { Image } from 'antd';
import { AppConfig } from '../../../../appConfig';
import Button from 'antd/es/button';
import Input from 'antd/es/input';
import { CheckOutlined, SaveOutlined } from '@ant-design/icons';
import Card from 'antd/es/card';

function SlideshowForm(props) {
    const [form] = useForm();
    const dispatch = useDispatch();
    const [image, setImage] = useState(props.initialValues && props.initialValues.image);

    function handleOpenFileMedia() {
        dispatch(showModal())
        dispatch(changeModalTitle("File Manager"))
        dispatch(changeModalContent(
            <FileManager
                onChooseFile={(value) => setImage(value[0].url)}
                multiple={false}
                manager={true}
            />
        ))
    }

    function handleSubmit(values) {
        if (props.formType === 'add') {
            props.onSubmit({...values, image})
        } else {
            props.onEdit({...values, image})
        }
    }

    return (
        <Card>
            <Form
                layout="vertical"
                name="PostForm"
                onFinish={handleSubmit}
                initialValues={props.initialValues}
                form={form}
            >
                <Form.Item>
                    {image &&
                        <div className={"mb-5"}>
                            <Image src={AppConfig.apiUrl+image} className={"w-24 h-24 object-cover"}/>
                        </div>
                    }
                    <Button onClick={handleOpenFileMedia}>
                        {image ? 'Chọn ảnh khác' : "Chọn hình ảnh"}
                    </Button>
                </Form.Item>
                <Form.Item
                    label="Tiêu đề"
                    name="title"
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Đường dẫn"
                    name="link"
                >
                    <Input />
                </Form.Item>
                <Button
                    type="primary"
                    htmlType="submit"
                    size="large"
                    block
                    icon={props.formType === 'add' ? <CheckOutlined /> : <SaveOutlined />}
                >
                    {props.formType === 'add' ? 'Xuất bản' : 'Lưu thay đổi'}
                </Button>
            </Form>
        </Card>

    );
}

export default SlideshowForm;
