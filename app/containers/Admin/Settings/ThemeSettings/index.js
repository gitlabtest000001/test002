import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { hideLoader, showLoader } from '../../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader } from 'antd';
import Loading from '../../../../components/Loading';
import ThemeSettingsForm from './ThemeSettingsForm';
import { ErrorMessage, SuccessMessage } from '../../../../components/Message';

function ThemeSettings(props) {
    const dispatch = useDispatch()
    const settings = useSelector(state => state.root.settings.options);
    const [categories, setCategories] = useState();
    useEffect(() => {
        dispatch(showLoader());
        async function getCategory() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/category`,
                params: {
                    limit: 1000, page: 1
                },
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setCategories(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getCategory();
    },[dispatch])

    console.log(categories);

    if (settings) {
        var initialValues = {
            logo: settings.logo,
            home_category_2: settings.home_category_2,
            home_category_3: settings.home_category_3,
            home_category_4: settings.home_category_4,
            home_category_5: settings.home_category_5,
            show_home_category_1: settings.show_home_category_1,
            show_home_category_2: settings.show_home_category_2,
            show_home_category_3: settings.show_home_category_3,
            show_home_category_4: settings.show_home_category_4,
            show_home_category_5: settings.show_home_category_5,
            home_video: settings.home_video,
            home_intro: settings.home_intro,
            home_banner_1: settings.home_banner_1,
            home_banner_2: settings.home_banner_2,
            home_banner_3: settings.home_banner_3,
            home_banner_4: settings.home_banner_4,
        }
    }

    function handleUpdateSettings(data) {
        console.log(data);
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.put(
            `${AppConfig.apiUrl}/manager/setting`,
            data,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Đã lưu lại cài đặt'});
            dispatch(hideLoader())
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    return (
        <>
            <Helmet>
                <title>Cài đặt Hiển thị</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title="Cài đặt Hiển thị"
            />
            {!categories ? <Loading/> :
                <>
                    {!settings ? <Loading/> :
                        <ThemeSettingsForm initialValues={settings && initialValues} categories={categories && categories} onSubmit={handleUpdateSettings}/>
                    }
                </>
            }
        </>
    );
}

export default ThemeSettings;
