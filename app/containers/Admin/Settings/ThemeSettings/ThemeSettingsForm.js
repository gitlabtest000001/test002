import React, { useState } from 'react';
import Form from 'antd/es/form';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import Card from 'antd/es/card';
import Input from 'antd/es/input';
import { Image, InputNumber, Select } from 'antd';
import Button from 'antd/es/button';
import TextArea from 'antd/es/input/TextArea';
import RichTextEditor from '../../../../components/RTE';
import AntTreeSelect from '../../../../components/AntTreeSelect';
import MultipleUpload from '../../../../components/MultipleUpload';
import { changeModalContent, changeModalTitle, showModal } from '../../../App/actions';
import FileManager from '../../../../components/FileManager';
import { useDispatch } from 'react-redux';
import { AppConfig } from '../../../../appConfig';
import Checkbox from 'antd/es/checkbox';
import { IoCheckbox, IoSquare, IoSquareOutline } from 'react-icons/all';

function ThemeSettingsForm(props) {
    const dispatch = useDispatch();

    const [homeCategory1, setHomeCategory1] = useState(props.initialValues && props.initialValues.home_category_1);
    const [homeCategory2, setHomeCategory2] = useState(props.initialValues && props.initialValues.home_category_2);
    const [homeCategory3, setHomeCategory3] = useState(props.initialValues && props.initialValues.home_category_3);
    const [homeCategory4, setHomeCategory4] = useState(props.initialValues && props.initialValues.home_category_4);
    const [homeCategory5, setHomeCategory5] = useState(props.initialValues && props.initialValues.home_category_5);
    const [homeCategory6, setHomeCategory6] = useState(props.initialValues && props.initialValues.home_category_6);
    const [homeCategory7, setHomeCategory7] = useState(props.initialValues && props.initialValues.home_category_7);
    const [showHomeCategory1, setShowHomeCategory1] = useState(props.initialValues && props.initialValues.show_home_category_1);
    const [showHomeCategory2, setShowHomeCategory2] = useState(props.initialValues && props.initialValues.show_home_category_2);
    const [showHomeCategory3, setShowHomeCategory3] = useState(props.initialValues && props.initialValues.show_home_category_3);
    const [showHomeCategory4, setShowHomeCategory4] = useState(props.initialValues && props.initialValues.show_home_category_4);
    const [showHomeCategory5, setShowHomeCategory5] = useState(props.initialValues && props.initialValues.show_home_category_5);
    const [showHomeCategory6, setShowHomeCategory6] = useState(props.initialValues && props.initialValues.show_home_category_6);
    const [showHomeCategory7, setShowHomeCategory7] = useState(props.initialValues && props.initialValues.show_home_category_7);
    const [logo, setLogo] = useState(props.initialValues && props.initialValues.logo);
    const [homeBanner1, setHomeBanner1] = useState(props.initialValues && props.initialValues.home_banner_1);
    const [homeBanner2, setHomeBanner2] = useState(props.initialValues && props.initialValues.home_banner_2);
    const [homeBanner3, setHomeBanner3] = useState(props.initialValues && props.initialValues.home_banner_3);
    const [homeBanner4, setHomeBanner4] = useState(props.initialValues && props.initialValues.home_banner_4);
    const [homeIntro, setHomeIntro] = useState(props.initialValues && props.initialValues.home_intro);

    function handleSubmit(values) {
        props.onSubmit({
            home_category_1: homeCategory1,
            home_category_2: homeCategory2,
            home_category_3: homeCategory3,
            home_category_4: homeCategory4,
            home_category_5: homeCategory5,
            home_category_6: homeCategory6,
            home_category_7: homeCategory7,
            show_home_category_1: showHomeCategory1,
            show_home_category_2: showHomeCategory2,
            show_home_category_3: showHomeCategory3,
            show_home_category_4: showHomeCategory4,
            show_home_category_5: showHomeCategory5,
            show_home_category_6: showHomeCategory6,
            show_home_category_7: showHomeCategory7,
            logo: logo,
            home_banner_1: homeBanner1,
            home_banner_2: homeBanner2,
            home_banner_3: homeBanner3,
            home_banner_4: homeBanner4,
            home_video: values.home_video,
            home_intro: homeIntro,
        })
    }

    function handleOpenFileLogo() {
        dispatch(showModal())
        dispatch(changeModalTitle("File Manager"))
        dispatch(changeModalContent(
            <FileManager
                onChooseFile={(value) => setLogo(value[0].url)}
                multiple={('false')}
                manager={true}
            />
        ))
    }
    function handleOpenFileBanner1() {
        dispatch(showModal())
        dispatch(changeModalTitle("File Manager"))
        dispatch(changeModalContent(
            <FileManager
                onChooseFile={(value) => setHomeBanner1(value[0].url)}
                multiple={('false')}
                manager={true}
            />
        ))
    }
    function handleOpenFileBanner2() {
        dispatch(showModal())
        dispatch(changeModalTitle("File Manager"))
        dispatch(changeModalContent(
            <FileManager
                onChooseFile={(value) => setHomeBanner2(value[0].url)}
                multiple={('false')}
                manager={true}
            />
        ))
    }
    function handleOpenFileBanner3() {
        dispatch(showModal())
        dispatch(changeModalTitle("File Manager"))
        dispatch(changeModalContent(
            <FileManager
                onChooseFile={(value) => setHomeBanner3(value[0].url)}
                multiple={('false')}
                manager={true}
            />
        ))
    }
    function handleOpenFileBanner4() {
        dispatch(showModal())
        dispatch(changeModalTitle("File Manager"))
        dispatch(changeModalContent(
            <FileManager
                onChooseFile={(value) => setHomeBanner4(value[0].url)}
                multiple={('false')}
                manager={true}
            />
        ))
    }

    console.log('homeCategory1', homeCategory1);

    return (
        <Form
            layout="vertical"
            name="SettingsForm"
            onFinish={handleSubmit}
            initialValues={props.initialValues}
        >
            <Row gutter={[16, 16]} className="my-5">
                <Col lg={12} md={12} xs={24}>
                    <Card title="Block Danh mục sản phẩm trang chủ" size="small" className={"mb-5"}>
                        <Row gutter={16}>
                            <Col xs={24} lg={12} md={12}>
                                {showHomeCategory1 === 'true' ?
                                    <div
                                        onClick={() => setShowHomeCategory1(('false'))}
                                    >
                                        <IoCheckbox size={20} className={"text-blue-500"} /> Block Danh mục 1
                                    </div>
                                    :
                                    <div
                                        onClick={() => setShowHomeCategory1('true')}
                                    >
                                        <IoSquareOutline size={20} /> Block Danh mục 1
                                    </div>
                                }
                                {showHomeCategory1 === 'true' &&
                                    <Form.Item
                                        className={"mt-2"}
                                        name="home_category_1"
                                    >
                                        <AntTreeSelect
                                            hierarchy={props.categories && props.categories}
                                            parent={homeCategory1}
                                            onChange={(value) => setHomeCategory1(value)}
                                            onClear={() => setHomeCategory1(null)}
                                        />
                                    </Form.Item>
                                }
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                {showHomeCategory2 === 'true' ?
                                    <div
                                        onClick={() => setShowHomeCategory2(('false'))}
                                    >
                                        <IoCheckbox size={20} className={"text-blue-500"} /> Block Danh mục 2
                                    </div>
                                    :
                                    <div
                                        onClick={() => setShowHomeCategory2('true')}
                                    >
                                        <IoSquareOutline size={20} /> Block Danh mục 2
                                    </div>
                                }
                                {showHomeCategory2 === 'true' &&
                                    <Form.Item
                                        className={"mt-2"}
                                        name="home_category_2"
                                    >
                                        <AntTreeSelect
                                            hierarchy={props.categories && props.categories}
                                            parent={homeCategory2}
                                            onChange={(value) => setHomeCategory2(value)}
                                        />
                                    </Form.Item>
                                }
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                {showHomeCategory3 === 'true' ?
                                    <div
                                        onClick={() => setShowHomeCategory3(('false'))}
                                    >
                                        <IoCheckbox size={20} className={"text-blue-500"} /> Block Danh mục 3
                                    </div>
                                    :
                                    <div
                                        onClick={() => setShowHomeCategory3('true')}
                                    >
                                        <IoSquareOutline size={20} /> Block Danh mục 3
                                    </div>
                                }
                                {showHomeCategory3 === 'true' &&
                                    <Form.Item
                                        className={"mt-2"}
                                        name="home_category_3"
                                    >
                                        <AntTreeSelect
                                            hierarchy={props.categories && props.categories}
                                            parent={homeCategory3}
                                            onChange={(value) => setHomeCategory3(value)}
                                        />
                                    </Form.Item>
                                }
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                {showHomeCategory4 === 'true' ?
                                    <div
                                        onClick={() => setShowHomeCategory4(('false'))}
                                    >
                                        <IoCheckbox size={20} className={"text-blue-500"} /> Block Danh mục 4
                                    </div>
                                    :
                                    <div
                                        onClick={() => setShowHomeCategory4('true')}
                                    >
                                        <IoSquareOutline size={20} /> Block Danh mục 4
                                    </div>
                                }
                                {showHomeCategory4 === 'true' &&
                                    <Form.Item
                                        className={"mt-2"}
                                        name="home_category_4"
                                    >
                                        <AntTreeSelect
                                            hierarchy={props.categories && props.categories}
                                            parent={homeCategory4}
                                            onChange={(value) => setHomeCategory4(value)}
                                        />
                                    </Form.Item>
                                }
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                {showHomeCategory5 === 'true' ?
                                    <div
                                        onClick={() => setShowHomeCategory5(('false'))}
                                    >
                                        <IoCheckbox size={20} className={"text-blue-500"} /> Block Danh mục 5
                                    </div>
                                    :
                                    <div
                                        onClick={() => setShowHomeCategory5('true')}
                                    >
                                        <IoSquareOutline size={20} /> Block Danh mục 5
                                    </div>
                                }
                                {showHomeCategory5 === 'true' &&
                                    <Form.Item
                                        className={"mt-2"}
                                        name="home_category_5"
                                    >
                                        <AntTreeSelect
                                            hierarchy={props.categories && props.categories}
                                            parent={homeCategory5}
                                            onChange={(value) => setHomeCategory5(value)}
                                        />
                                    </Form.Item>
                                }
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                {showHomeCategory6 === 'true' ?
                                    <div
                                        onClick={() => setShowHomeCategory6(('false'))}
                                    >
                                        <IoCheckbox size={20} className={"text-blue-500"} /> Block Danh mục 6
                                    </div>
                                    :
                                    <div
                                        onClick={() => setShowHomeCategory6('true')}
                                    >
                                        <IoSquareOutline size={20} /> Block Danh mục 6
                                    </div>
                                }
                                {showHomeCategory6 === 'true' &&
                                    <Form.Item
                                        className={"mt-2"}
                                        name="home_category_6"
                                    >
                                        <AntTreeSelect
                                            hierarchy={props.categories && props.categories}
                                            parent={homeCategory6}
                                            onChange={(value) => setHomeCategory6(value)}
                                        />
                                    </Form.Item>
                                }
                            </Col>
                        </Row>
                    </Card>

                    <Card title="Giới thiệu trên trang chủ" size="small" className={"mb-5"}>
                        <Form.Item
                            label="Video link"
                            name="home_video"
                        >
                            <Input />
                        </Form.Item>
                        <RichTextEditor
                            defaultValue={props.initialValues && props.initialValues.home_intro}
                            onDataChange={(value) => setHomeIntro(value)}
                        />
                    </Card>
                </Col>
                <Col lg={12} md={12} xs={24}>
                    <Card title="Banner trang chủ" size="small">
                        <Row gutter={[32, 32]}>
                            <Col xs={24} lg={12} md={12}>
                                {logo &&
                                    <div className={"mb-5"}>
                                        <Image src={AppConfig.apiUrl+logo} className={"w-24 h-24 object-cover"}/>
                                    </div>
                                }
                                <Button onClick={handleOpenFileLogo}>
                                    {logo ? 'Chọn ảnh Logo khác' : "Chọn ảnh Logo"}
                                </Button>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                {homeBanner1 &&
                                    <div className={"mb-5"}>
                                        <Image src={AppConfig.apiUrl+homeBanner1} className={"w-24 h-24 object-cover"}/>
                                    </div>
                                }
                                <Button onClick={handleOpenFileBanner1}>
                                    {homeBanner1 ? 'Chọn ảnh Banner 1 khác' : "Chọn ảnh Banner 1"}
                                </Button>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                {homeBanner2 &&
                                    <div className={"mb-5"}>
                                        <Image src={AppConfig.apiUrl+homeBanner2} className={"w-24 h-24 object-cover"}/>
                                    </div>
                                }
                                <Button onClick={handleOpenFileBanner2}>
                                    {homeBanner2 ? 'Chọn ảnh Banner 2 khác' : "Chọn ảnh Banner 2"}
                                </Button>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                {homeBanner3 &&
                                    <div className={"mb-5"}>
                                        <Image src={AppConfig.apiUrl+homeBanner3} className={"w-24 h-24 object-cover"}/>
                                    </div>
                                }
                                <Button onClick={handleOpenFileBanner3}>
                                    {homeBanner3 ? 'Chọn ảnh Banner 3 khác' : "Chọn ảnh Banner 3"}
                                </Button>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                {homeBanner4 &&
                                    <div className={"mb-5"}>
                                        <Image src={AppConfig.apiUrl+homeBanner4} className={"w-24 h-24 object-cover"}/>
                                    </div>
                                }
                                <Button onClick={handleOpenFileBanner4}>
                                    {homeBanner4 ? 'Chọn ảnh Banner 4 khác' : "Chọn ảnh Banner 4"}
                                </Button>
                            </Col>
                        </Row>
                    </Card>
                </Col>
            </Row>
            <Button type="primary" htmlType="submit" size="large">
                Lưu thay đổi
            </Button>
        </Form>
    );
}

export default ThemeSettingsForm;
