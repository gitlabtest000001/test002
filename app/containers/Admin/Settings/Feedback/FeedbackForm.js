import React, { useState } from 'react';
import { changeModalContent, changeModalTitle, showModal } from '../../../App/actions';
import FileManager from '../../../../components/FileManager';
import useForm from 'antd/es/form/hooks/useForm';
import { useDispatch } from 'react-redux';
import Form from 'antd/es/form';
import { Image } from 'antd';
import { AppConfig } from '../../../../appConfig';
import Button from 'antd/es/button';
import Input from 'antd/es/input';
import { CheckOutlined, SaveOutlined } from '@ant-design/icons';
import Card from 'antd/es/card';

function FeedbackForm(props) {
    const [form] = useForm();
    const dispatch = useDispatch();
    const [avatar, setAvatar] = useState(props.initialValues && props.initialValues.avatar);

    function handleOpenFileMedia() {
        dispatch(showModal())
        dispatch(changeModalTitle("File Manager"))
        dispatch(changeModalContent(
            <FileManager
                onChooseFile={(value) => setAvatar(value[0].url)}
                multiple={false}
                manager={true}
            />
        ))
    }

    function handleSubmit(values) {
        if (props.formType === 'add') {
            props.onSubmit({...values, avatar})
        } else {
            props.onEdit({...values, avatar})
        }
    }

    return (
        <Card>
            <Form
                layout="vertical"
                name="PostForm"
                onFinish={handleSubmit}
                initialValues={props.initialValues}
                form={form}
            >
                <Form.Item>
                    {avatar &&
                        <div className={"mb-5"}>
                            <Image src={AppConfig.apiUrl+avatar} className={"w-24 h-24 object-cover"}/>
                        </div>
                    }
                    <Button onClick={handleOpenFileMedia}>
                        {avatar ? 'Chọn ảnh khác' : "Chọn hình ảnh"}
                    </Button>
                </Form.Item>
                <Form.Item
                    label="Họ tên"
                    name="name"
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Nghề nghiệp"
                    name="job"
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Nội dung feedback"
                    name="content"
                >
                    <Input.TextArea />
                </Form.Item>
                <Button
                    type="primary"
                    htmlType="submit"
                    size="large"
                    block
                    icon={props.formType === 'add' ? <CheckOutlined /> : <SaveOutlined />}
                >
                    {props.formType === 'add' ? 'Xuất bản' : 'Lưu thay đổi'}
                </Button>
            </Form>
        </Card>

    );
}

export default FeedbackForm;
