import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { hideLoader, showLoader } from '../../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';
import Loading from '../../../../components/Loading';
import { Helmet } from 'react-helmet/es/Helmet';
import PageHeader from 'antd/es/page-header';
import Row from 'antd/es/grid/row';
import Col from 'antd/es/grid/col';
import FeedbackForm from './FeedbackForm';
import Table from 'antd/es/table';
import { SuccessMessage } from '../../../../components/Message';
import history from '../../../../utils/history';

function FeedbackDetails(props) {
    const dispatch = useDispatch()
    const id = props.match.params.id;
    const [item, setItem] = useState()
    const [flag, setFlag] = useState(true)

    useEffect(() => {
        dispatch(showLoader());
        async function getSlideshow() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/feedback/${id}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setItem(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getSlideshow();
    },[dispatch, flag, id])

    function handleEdit(data) {
        dispatch(showLoader());
        const Token = localStorage.getItem('dvg_manager_token');
        axios({
            method: 'put',
            url: `${AppConfig.apiUrl}/manager/feedback/${id}`,
            data,
            headers: {Authorization: `Bearer ${Token}`}
        }).then(function (response) {
            SuccessMessage({message: 'Cập nhật feedback thành công'});
            dispatch(hideLoader());
            history.push(`/admin/settings/feedback`)
        }).catch(function(error){
            console.log(error);
            dispatch(hideLoader())
        })
    }

    return (
        !item ? <Loading /> :
        <div>
            <Helmet>
                <title>Quản lý feedback</title>
            </Helmet>
            <PageHeader
                ghost={false}
                className={"mb-5"}
                title={"Sửa feedback"}
            />
            <Row gutter={[16, 16]}>
                <Col xs={24} lg={8} md={8}>
                    <FeedbackForm
                        formType={"edit"}
                        onEdit={handleEdit}
                        initialValues={item && item}
                    />
                </Col>
            </Row>
        </div>
    );
}

export default FeedbackDetails;
