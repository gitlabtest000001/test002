import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { hideLoader, showLoader } from '../../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';
import { Helmet } from 'react-helmet/es/Helmet';
import PageHeader from 'antd/es/page-header';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import FeedbackForm from './FeedbackForm';
import Space from 'antd/es/space';
import { Link } from 'react-router-dom';
import Button from 'antd/es/button';
import Popconfirm from 'antd/es/popconfirm';
import Table from 'antd/es/table';
import Loading from '../../../../components/Loading';
import history from '../../../../utils/history';
import { SuccessMessage } from '../../../../components/Message';
import Image from 'antd/es/image';

function FeedbackSettings(props) {
    const dispatch = useDispatch();
    const [feedbacks, setFeedbacks] = useState()
    const [flag, setFlag] = useState(true)

    useEffect(() => {
        dispatch(showLoader());
        async function getSlideshow() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/feedback`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setFeedbacks(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getSlideshow();
    },[dispatch, flag])

    console.log(feedbacks);

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Name',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Job',
            dataIndex: 'job',
            key: 'job',
        },
        {
            title: 'Avatar',
            dataIndex: 'avatar',
            key: 'avatar',
            render: (key, row) =>
                <Image src={AppConfig.apiUrl+row.avatar} className={"w-20 h-20 object-cover"}/>
        },
        {
            title: 'Action',
            dataIndex: 'action',
            key: 'action',
            render: (key, row) =>
                <Space>
                    <Link to={`/admin/settings/feedback/edit/${row.id}`}>
                        <Button type="link" size="small">
                            Sửa
                        </Button>
                    </Link>
                    <Popconfirm
                        title={`Bạn chắc chắn muốn xoá?`}
                        onConfirm={() => handleDeleteFeedback(row.id)}
                        okText="Có"
                        cancelText="Không"
                    >
                        <Button danger type="link">
                            Xoá
                        </Button>
                    </Popconfirm>
                </Space>
        },
    ]

    function handleDeleteFeedback(id) {
        dispatch(showLoader());
        const Token = localStorage.getItem('dvg_manager_token');
        axios({
            method: 'delete',
            url: `${AppConfig.apiUrl}/manager/feedback/${id}`,
            headers: {Authorization: `Bearer ${Token}`}
        }).then(function (response) {
            SuccessMessage({message: 'Đã xoá feedback'});
            dispatch(hideLoader());
            setFlag(!flag)
        }).catch(function(error){
            console.log(error);
            dispatch(hideLoader())
        })
    }

    function handleAddFeedback(data) {
        dispatch(showLoader());
        const Token = localStorage.getItem('dvg_manager_token');
        axios({
            method: 'post',
            url: `${AppConfig.apiUrl}/manager/feedback`,
            data,
            headers: {Authorization: `Bearer ${Token}`}
        }).then(function (response) {
            if (response.status === 201) {
                SuccessMessage({message: 'Tạo feedback thành công'});
                dispatch(hideLoader());
                setFeedbacks(feedbacks.concat(response.data))
            }
        }).catch(function(error){
            console.log(error);
            dispatch(hideLoader())
        })
    }

    return (
        !feedbacks ? <Loading /> :
        <div>
            <Helmet>
                <title>Quản lý feedback</title>
            </Helmet>
            <PageHeader
                ghost={false}
                className={"mb-5"}
                title={"Quản lý feedback"}
            />
            <Row gutter={[16, 16]}>
                <Col xs={24} lg={8} md={8}>
                    <FeedbackForm
                        formType={"add"}
                        onSubmit={handleAddFeedback}
                    />
                </Col>
                <Col xs={24} lg={16} md={16}>
                    <Table
                        columns={columns}
                        dataSource={feedbacks && feedbacks}
                    />
                </Col>
            </Row>
        </div>
    );
}

export default FeedbackSettings;
