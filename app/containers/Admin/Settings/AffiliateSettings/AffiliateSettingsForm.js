import React, { useState } from 'react';
import Form from 'antd/es/form';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import Card from 'antd/es/card';
import Input from 'antd/es/input';
import { InputNumber, Select } from 'antd';
import Button from 'antd/es/button';
import TextArea from 'antd/es/input/TextArea';
import RichTextEditor from '../../../../components/RTE';

function AffiliateSettingsForm(props) {

    /*var initialValues = {
        director_direct_commission: settings.director_direct_commission,
        sale_direct_commission: settings.sale_direct_commission,
        contributor_sale_commission_level1: settings.contributor_sale_commission_level1,
        contributor_sale_commission_level2: settings.contributor_sale_commission_level2,
        contributor_sale_commission_level3: settings.contributor_sale_commission_level3,
        ///
        director_stock_commission: settings.director_stock_commission,
        sale_stock_commission: settings.sale_stock_commission,
        contributor_stock_commission_level1: settings.contributor_stock_commission_level1,
        contributor_stock_commission_level2: settings.contributor_stock_commission_level2,
        contributor_stock_commission_level3: settings.contributor_stock_commission_level3,
        ///
        contributor_investor_selling_level1: settings.contributor_investor_selling_level1,
        contributor_investor_selling_level2: settings.contributor_investor_selling_level2,
        contributor_investor_selling_level3: settings.contributor_investor_selling_level3,*/

    function handleSubmit(values) {
        props.onSubmit(values)
    }

    return (
        <Form
            layout="vertical"
            name="SettingsForm"
            onFinish={handleSubmit}
            initialValues={props.initialValues}
        >
            <Row gutter={[16, 16]} className="my-5">
                <Col lg={12} md={12} xs={24}>
                    <Card title="Hoa hồng bán hàng" size="small">
                        <Row gutter={16}>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Hoa hồng dành cho Giám đốc Aff"
                                    name="director_direct_commission"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Hoa hồng dành cho Nhân viên Aff"
                                    name="sale_direct_commission"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Hoa hồng Cộng tác viên 1"
                                    name="contributor_sale_commission_level1"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Hoa hồng Cộng tác viên 2"
                                    name="contributor_sale_commission_level2"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Hoa hồng Cộng tác viên 3"
                                    name="contributor_sale_commission_level3"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Card>
                </Col>
                <Col lg={12} md={12} xs={24}>
                    <Card title="Hoa hồng giới thiệu Nhà Đầu Tư" size="small">
                        <Row gutter={16}>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Hoa hồng dành cho Giám đốc Aff"
                                    name="director_stock_commission"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Hoa hồng dành cho Nhân viên Aff"
                                    name="sale_stock_commission"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Hoa hồng Cộng tác viên 1"
                                    name="contributor_stock_commission_level1"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Hoa hồng Cộng tác viên 2"
                                    name="contributor_stock_commission_level2"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Hoa hồng Cộng tác viên 3"
                                    name="contributor_stock_commission_level3"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Card>
                </Col>
                <Col lg={12} md={12} xs={24}>
                    <Card title="Hoa hồng cho CTV giới thiệu khách không có tài khoản" size="small">
                        <Row gutter={16}>
                            <Col xs={24} lg={24} md={24}>
                                <p className={"font-medium"}>CTV giới thiệu trực tiếp</p>
                                <Row gutter={16}>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Giá trị đơn hàng"
                                            name="guest_bonus_level1_value_1"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonBefore={"<"}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Hoa hồng"
                                            name="guest_bonus_level1_percent_1"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"%"}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row gutter={16} className={"border-b border-gray-200 mb-5"}>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Giá trị đơn hàng"
                                            name="guest_bonus_level1_value_2"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonBefore={">="}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Hoa hồng"
                                            name="guest_bonus_level1_percent_2"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"%"}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>

                            <Col xs={24} lg={24} md={24}>
                                <p className={"font-medium"}>CTV gián tiếp 1</p>
                                <Row gutter={16}>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Giá trị đơn hàng"
                                            name="guest_bonus_level2_value_1"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonBefore={"<"}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Hoa hồng"
                                            name="guest_bonus_level2_percent_1"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"%"}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row gutter={16} className={"border-b border-gray-200 mb-5"}>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Giá trị đơn hàng"
                                            name="guest_bonus_level2_value_2"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonBefore={">="}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Hoa hồng"
                                            name="guest_bonus_level2_percent_2"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"%"}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>

                            <Col xs={24} lg={24} md={24}>
                                <p className={"font-medium"}>CTV gián tiếp 2</p>
                                <Row gutter={16}>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Giá trị đơn hàng"
                                            name="guest_bonus_level3_value_1"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonBefore={"<"}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Hoa hồng"
                                            name="guest_bonus_level3_percent_1"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"%"}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row gutter={16} className={"border-b border-gray-200 mb-5"}>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Giá trị đơn hàng"
                                            name="guest_bonus_level3_value_2"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonBefore={">="}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Hoa hồng"
                                            name="guest_bonus_level3_percent_2"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'},
                                            ]}
                                        >
                                            <InputNumber
                                                formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                                parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                                className={"w-full"}
                                                size="large"
                                                addonAfter={"%"}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Card>
                </Col>
                <Col lg={12} md={12} xs={24}>
                    <Card title="Hoa hồng khi NĐT Kinh doanh tảo" size="small">
                        <Row gutter={16}>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Hoa hồng Cộng tác viên 1"
                                    name="contributor_investor_selling_level1"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Hoa hồng Cộng tác viên 2"
                                    name="contributor_investor_selling_level2"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Hoa hồng Cộng tác viên 3"
                                    name="contributor_investor_selling_level3"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập thông tin'},
                                    ]}
                                >
                                    <InputNumber
                                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                        className={"w-full"}
                                        size="large"
                                        addonAfter={"%"}
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Card>
                </Col>


            </Row>
            <Button type="primary" htmlType="submit" size="large">
                Lưu thay đổi
            </Button>
        </Form>
    );
}

export default AffiliateSettingsForm;
