import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { hideLoader, showLoader } from '../../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader } from 'antd';
import Loading from '../../../../components/Loading';
import AffiliateSettingsForm from './AffiliateSettingsForm';
import { ErrorMessage, SuccessMessage } from '../../../../components/Message';

function AffiliateSettings(props) {
    const dispatch = useDispatch()
    const settings = useSelector(state => state.root.settings.options);
    const [categories, setCategories] = useState();
    useEffect(() => {
        dispatch(showLoader());
        async function getCategory() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/category`,
                params: {
                    limit: 1000, page: 1
                },
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setCategories(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getCategory();
    },[dispatch])

    if (settings) {
        var initialValues = {
            director_direct_commission: settings.director_direct_commission,
            sale_direct_commission: settings.sale_direct_commission,
            contributor_sale_commission_level1: settings.contributor_sale_commission_level1,
            contributor_sale_commission_level2: settings.contributor_sale_commission_level2,
            contributor_sale_commission_level3: settings.contributor_sale_commission_level3,
            ///
            director_stock_commission: settings.director_stock_commission,
            sale_stock_commission: settings.sale_stock_commission,
            contributor_stock_commission_level1: settings.contributor_stock_commission_level1,
            contributor_stock_commission_level2: settings.contributor_stock_commission_level2,
            contributor_stock_commission_level3: settings.contributor_stock_commission_level3,
            ///
            contributor_investor_selling_level1: settings.contributor_investor_selling_level1,
            contributor_investor_selling_level2: settings.contributor_investor_selling_level2,
            contributor_investor_selling_level3: settings.contributor_investor_selling_level3,
            /// Nhận thêm khi khách không đăng nhập mua hàng
            guest_bonus_level1_value_1: settings.guest_bonus_level1_value_1,
            guest_bonus_level1_percent_1: settings.guest_bonus_level1_percent_1,
            guest_bonus_level1_value_2: settings.guest_bonus_level1_value_2,
            guest_bonus_level1_percent_2: settings.guest_bonus_level1_percent_2,
            guest_bonus_level2_value_1: settings.guest_bonus_level2_value_1,
            guest_bonus_level2_percent_1: settings.guest_bonus_level2_percent_1,
            guest_bonus_level2_value_2: settings.guest_bonus_level2_value_2,
            guest_bonus_level2_percent_2: settings.guest_bonus_level2_percent_2,
            guest_bonus_level3_value_1: settings.guest_bonus_level3_value_1,
            guest_bonus_level3_percent_1: settings.guest_bonus_level3_percent_1,
            guest_bonus_level3_value_2: settings.guest_bonus_level3_value_2,
            guest_bonus_level3_percent_2: settings.guest_bonus_level3_percent_2,
        }
    }

    function handleUpdateSettings(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.put(
            `${AppConfig.apiUrl}/manager/setting`,
            data,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Đã lưu lại cài đặt'});
            dispatch(hideLoader())
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    return (
        <>
            <Helmet>
                <title>Cài đặt Affiliate</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title="Cài đặt Affiliate"
            />
            {!categories ? <Loading/> :
                <>
                    {!settings ? <Loading/> :
                        <AffiliateSettingsForm initialValues={settings && initialValues} categories={categories && categories.list} onSubmit={handleUpdateSettings}/>
                    }
                </>
            }
        </>
    );
}

export default AffiliateSettings;
