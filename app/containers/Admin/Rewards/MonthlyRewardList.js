import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet/es/Helmet';
import PageHeader from 'antd/es/page-header';
import { DatePicker } from 'antd';
import { useDispatch } from 'react-redux';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import moment from 'moment';
import { Link } from 'react-router-dom';
import {
    convertStatus,
    convertTransactionType,
    convertUserWallet,
    formatDate,
    formatDateTime,
    formatVND,
} from '../../../utils/helpers';
import Table from 'antd/es/table';
import Loading from '../../../components/Loading';
import { ExportXLS } from '../../../components/ExportXLS';

function MonthlyRewardList(props) {
    const dispatch = useDispatch()
    const [rewards, setRewards] = useState();
    const [startDate, setStartDate] = useState(moment().subtract(1,'months').startOf('month').format('YYYY-MM-DD 00:00:00'))

    console.log(startDate);

    function onChange(date) {
        setStartDate(moment(date).startOf('month').format('YYYY-MM-DD 00:00:00'))
        console.log(moment(date).startOf('month').format('YYYY-MM-DD 00:00:00'));
    }

    useEffect(() => {
        dispatch(showLoader());
        async function getRewards() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/rewards`,
                params: {
                    type: 'MONTH',
                    rangeStart: startDate
                },
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setRewards(response.data);
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getRewards();
    }, [dispatch, startDate])

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Thành viên',
            dataIndex: 'owner',
            key: 'owner',
            render: (key, row) =>
                <Link to={`/admin/members/detail/${row.member.id}`}>{row.member.name} - {row.member.phone}</Link>
        },
        {
            title: 'Doanh số cộng dồn',
            dataIndex: 'sales',
            key: 'sales',
            render: (key, row) =>
                formatVND(row.sales)
        },
        {
            title: 'Tiền thưởng',
            dataIndex: 'amount',
            key: 'amount',
            render: (key, row) =>
                <strong>{formatVND(row.amount)}</strong>
        },
        {
            title: 'Được thưởng',
            dataIndex: 'percent',
            key: 'percent',
            render: (key, row) =>
                row.percent + '%'
        },
        {
            title: 'Từ ngày',
            dataIndex: 'startDate',
            key: 'startDate',
            render: (key, row) =>
                formatDateTime(row.startDate)
        },
        {
            title: 'Đến ngày',
            dataIndex: 'endDate',
            key: 'endDate',
            render: (key, row) =>
                formatDateTime(row.endDate)
        },
    ]

    let exportData = []
    if(rewards) {
        exportData = rewards.map(item => {
            item = {
                'Id': item.id,
                'Thành viên': item.member.name + '-' + item.member.phone,
                'Doanh số cộng dồn': item.sales,
                'Tiền thưởng': item.amount,
                'Được thưởng': item.percent + '%',
            }
            return item;
        })
    }

    return (
        !rewards ? <Loading /> :
        <div>
            <Helmet>
                <title>Danh sách thưởng theo tháng</title>
            </Helmet>
            <PageHeader
                title={`Danh sách thưởng tháng ${moment(startDate).format('MM/YYYY')}`}
                ghost={false}
                className={"mb-5"}
                extra={
                <div className={"flex items-center"}>
                    <span className={"mr-2"}>Chọn tháng</span>
                    <DatePicker
                        onChange={onChange}
                        picker="month"
                        format={'MM/YYYY'}
                        placeHolder={"Chọn tháng"}
                        value={moment(startDate)}
                        className={"w-full"}
                        allowClear={false}
                    />
                </div>
                }
            />
            <div>
                <div className={"mb-2"}>
                    <ExportXLS
                        csvData={exportData && exportData}
                        fileName={`Thưởng tháng - ${formatDate(startDate)}`}
                    />
                </div>

                <Table
                    columns={columns}
                    dataSource={rewards}
                />
            </div>
        </div>
    );
}

export default MonthlyRewardList;
