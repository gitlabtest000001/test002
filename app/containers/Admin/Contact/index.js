import React, {useState, useEffect} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import queryString from 'query-string';
import { AppConfig } from '../../../appConfig';
import axios from 'axios';
import { changeModalContent, changeModalTitle, hideLoader, showLoader, showModal } from '../../App/actions';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader } from 'antd';
import Button from 'antd/es/button';
import history from '../../../utils/history';
import Table from 'antd/es/table';
import { formatDateTime } from '../../../utils/helpers';
import { EditTwoTone } from '@ant-design/icons';
import Space from 'antd/es/space';
import Tag from 'antd/es/tag';

function Pages(props) {
    const dispatch = useDispatch();
    const [result, setResult] = useState();

    useEffect(() => {
        dispatch(showLoader());
        async function getPages() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/contact`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setResult(response.data);
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getPages();
    }, [dispatch])

    function showDetailModal(data) {
        dispatch(showModal());
        dispatch(changeModalTitle('Chi tiết liên hệ'));
        dispatch(changeModalContent(
            <div>
                {data.message}
            </div>
        ));
    }

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Họ tên',
            dataIndex: 'name',
            key: 'name',
        },
        {
            title: 'Số điện thoại',
            dataIndex: 'phone',
            key: 'phone',
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: 'Ngày tháng',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: (key, row) => formatDateTime(row.createdAt)
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: (key, row) => (
                <Space size={'middle'}>
                    <Button
                        type="primary"
                        size="small"
                        ghost
                        icon={<EditTwoTone />}
                        onClick={() => showDetailModal(row)}
                    >
                        Chi tiết
                    </Button>
                </Space>
            )
        }
    ]

    return (
        <div>
            <Helmet>
                <title>Quản lý Liên hệ</title>
            </Helmet>
            <PageHeader
                title={"Liên hệ"}
                className={"mb-5"}
                ghost={false}
            />
            <Table
                scroll={{x: '100%'}}
                columns={columns}
                dataSource={result && result}
            />
        </div>
    );
}

export default Pages;
