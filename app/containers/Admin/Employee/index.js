import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import queryString from 'query-string';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { hideLoader, showLoader } from '../../App/actions';
import { Link } from 'react-router-dom';
import Space from 'antd/es/space';
import { checkRole } from '../../../utils/helpers';
import Button from 'antd/es/button';
import { EditTwoTone } from '@ant-design/icons';
import Table from 'antd/es/table';
import Col from 'antd/es/grid/col';
import Row from 'antd/es/grid/Row';
import PlusOutlined from '@ant-design/icons/lib/icons/PlusOutlined';
import { PageHeader } from 'antd';
import { Helmet } from 'react-helmet/es/Helmet';
import Tag from 'antd/es/tag';
import Search from 'antd/es/input/Search';

function Employee(props) {
    const dispatch = useDispatch();
    let params = queryString.parse(props.location.search);
    let currentPage = params.page;
    let currentPageSize = params.limit;
    const [results, setResults] = useState();
    const [loading, setLoading] = useState(true);
    const [page, setPage] = useState(currentPage ? currentPage : 1);
    const [pageSize, setPageSize] = useState(currentPageSize ? currentPageSize : 10);
    const [searchQuery, setSearchQuery] = useState();

    useEffect(() => {
        dispatch(showLoader());
        async function getUsers() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/employee`,
                params: {
                    page, limit: pageSize, query: searchQuery
                },
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setResults(response.data)
                    setLoading(false);
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getUsers();
    },[page, pageSize, searchQuery])

    function handleChangePage(page, pageSize) {
        setPage(page);
        setPageSize(pageSize);
    }

    const currentUser = useSelector(state => state.root.currentUser.user)

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Tên',
            dataIndex: 'name',
            key: 'name',
            render: (text, row) =>
                <>
                    {checkRole(currentUser.roles, ['administrator', 'marketer']) ?
                        <Link to={`/admin/employee/edit/${row.id}`}>
                            <span className="text-blue-500">{text}</span>
                        </Link>
                        : <span>{text}</span>
                    }
                </>
        },
        {
            title: 'Số điện thoại',
            dataIndex: 'phone',
            key: 'phone',
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: 'Chức vụ',
            dataIndex: 'roles',
            key: 'roles',
            render: (key, row) => (
                <>
                    {(() => {
                        switch(row.roles) {
                            case 'administrator':
                                return 'Quản lý';
                            case 'marketer':
                                return 'Marketer';
                            case 'accountant':
                                return 'Kế toán';
                            default:
                                return null;
                        }
                    })()}
                </>
            ),
            filters: [
                {
                    text: 'Quản lý',
                    value: 'administrator',
                },
                {
                    text: 'Marketer',
                    value: 'marketer',
                },
                {
                    text: 'Kế toán',
                    value: 'accountant',
                },
            ],
            filterMultiple: true,
            onFilter: (value, record) => record.roles.indexOf(value) === 0,
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            key: 'status',
            align: 'right',
            render: (key, row) => (
                <>
                    {(() => {
                        switch(row.status) {
                            case 'ACTIVE':
                                return <Tag color="green">Hoạt động</Tag>;
                            case 'DEACTIVE':
                                return <Tag color="red">Khoá</Tag>;
                            default:
                                return null;
                        }
                    })()}
                </>
            ),
            filters: [
                {
                    text: 'Hoạt động',
                    value: 'ACTIVE',
                },
                {
                    text: 'Khoá',
                    value: 'DEACTIVE',
                },
            ],
            filterMultiple: false,
            onFilter: (value, record) => record.status.indexOf(value) === 0,
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: (key, row) => (
                <Space size="middle">
                    {checkRole(currentUser.roles, ['administrator', 'marketer']) &&
                        <Link to={`/admin/employee/edit/${row.id}`}>
                            <Button type="primary" size="small" ghost icon={<EditTwoTone />}>
                                Sửa
                            </Button>
                        </Link>
                    }
                </Space>
            )
        },
    ]

    return (
        <div>
            <Helmet>
                <title>Tài khoản quản lý</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title="Tài khoản Quản lý"
                extra={[
                    <>
                        {checkRole(currentUser.roles, ['administrator', 'marketer', 'sale']) &&
                            <Link to="/admin/employee/add">
                                <Button type="primary" icon={<PlusOutlined />} shape="round">
                                    Tạo tài khoản
                                </Button>
                            </Link>
                        }
                    </>
                ]}
                className="mb-3"
            />
            <Row gutter={[20, 10]} className={"mb-3"}>
                <Col xs={24} lg={12} md={12}>
                    <Search
                        placeholder="Tìm kiếm..."
                        className={"w-full"}
                        onSearch={(value) => setSearchQuery(value)}
                        allowClear
                    />
                </Col>
                <Col xs={24} lg={12} md={12}>
                    <div className={"text-right sm:tex-center"}>
                        <p>Tìm thấy {results && results.count} tài khoản</p>
                    </div>
                </Col>
            </Row>
            <Table
                scroll={{x: '100%'}}
                loading={loading}
                columns={columns}
                dataSource={results && results.employees}
                pagination={{
                    current: page,
                    pageSize: pageSize,
                    total: results && results.count,
                    onChange: ((page, pageSize) => handleChangePage(page, pageSize)),
                    position: ['bottomLeft']
                }}
            />
        </div>
    );
}

export default Employee;
