import React from 'react';
import EmployeeForm from './EmployeeForm';
import { PageHeader } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { Helmet } from 'react-helmet/es/Helmet';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { ErrorMessage, SuccessMessage } from '../../../components/Message';
import history from '../../../utils/history';

function AddEmployee(props) {
    const dispatch = useDispatch()
    const currentUser = useSelector(state => state.root.currentUser.user);

    function handleCreateUser(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.post(
            `${AppConfig.apiUrl}/manager/employee`,
            data,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Tạo tài khoản thành công'});
            dispatch(hideLoader())
            history.push(`/admin/employee/edit/${response.data.id}`)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    return (
        <div>
            <Helmet>
                <title>Tạo tài khoản</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title="Tạo tài khoản"
                className="mb-3"
                onBack={history.goBack}
            />
            <EmployeeForm
                type={"add"}
                onSubmit={handleCreateUser}
                initialValues={{
                    roles: 'administrator'
                }}
            />
        </div>
    );
}

export default AddEmployee;
