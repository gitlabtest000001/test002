import React, { useEffect } from 'react';
import Form from 'antd/es/form';
import Row from 'antd/es/grid/row';
import Col from 'antd/es/grid/col';
import { CheckOutlined, SaveOutlined } from '@ant-design/icons';
import Button from 'antd/es/button';
import Card from 'antd/es/card';
import Input from 'antd/es/input';
import Select from 'antd/es/select';
import TextArea from 'antd/es/input/TextArea';
import { EmployeeRoles, UserStatus } from '../../../models/member.model';

const { Option } = Select;

UserForm.propTypes = {

};

function UserForm(props) {
    const [form] = Form.useForm();

    useEffect(() => form.resetFields(), [props.initialValues]);

    function handleSubmit(values) {
        if (props.type === 'add') {
            props.onSubmit(values)
        } else props.onEdit(values)
    }

    return (
        <Form
            layout="vertical"
            name="UserForm"
            form={form}
            onFinish={handleSubmit}
            initialValues={props.initialValues}
        >
            <Row gutter={[16, 16]}>
                <Col xs={24} lg={12} md={12}>
                    <Card title="Thông tin tài khoản">
                        <Row gutter={[16]}>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Họ và tên"
                                    name="name"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập tên'},
                                    ]}
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Email"
                                    name="email"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập email'
                                        },
                                        {
                                            type: 'email',
                                            message: 'Vui lòng nhập đúng định dạng Email!',
                                        },
                                    ]}
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Số điện thoại"
                                    name="phone"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập tên'},
                                    ]}
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Mật khẩu"
                                    name="password"
                                    rules={[
                                        {
                                            required: props.type === 'add',
                                            message: 'Vui lòng tạo mật khẩu'
                                        },
                                    ]}
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            {props.type === 'edit' &&
                                <Col xs={24} lg={12} md={12}>
                                    <Form.Item name="status" label="Trạng thái">
                                        <Select
                                            style={{width: '100%'}}
                                            placeholder="Chọn trạng thái"
                                            size="large"
                                        >
                                            {UserStatus.map((item) =>
                                                <Option value={item.code}>{item.name}</Option>
                                            )}
                                        </Select>
                                    </Form.Item>
                                </Col>
                            }
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item name="roles" label="Chức vụ">
                                    <Select
                                        style={{width: '100%'}}
                                        placeholder="Chọn chức vụ"
                                        size="large"
                                    >
                                        {EmployeeRoles.map((item) =>
                                            <Option value={item.code}>{item.name}</Option>
                                        )}
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={24} md={24}>
                                <Form.Item
                                    label="Ghi chú"
                                    name="note"
                                >
                                    <TextArea rows={3} />
                                </Form.Item>
                            </Col>
                            <Button
                                type="primary"
                                htmlType="submit"
                                icon={props.type === 'add' ? <CheckOutlined /> : <SaveOutlined />}
                            >
                                {props.type === 'add' ? 'Tạo tài khoản' : 'Lưu thay đổi'}
                            </Button>
                        </Row>
                    </Card>
                </Col>
            </Row>
        </Form>
    );
}

export default UserForm;
