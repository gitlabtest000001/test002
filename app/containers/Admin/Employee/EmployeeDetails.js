import React, { useEffect, useState } from 'react';
import EmployeeForm from './EmployeeForm';
import { PageHeader } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { Helmet } from 'react-helmet/es/Helmet';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { ErrorMessage, SuccessMessage } from '../../../components/Message';
import history from '../../../utils/history';
import Loading from '../../../components/Loading';

function EmployeeDetails(props) {
    const userId = props.match.params.id;
    const dispatch = useDispatch();
    const [user, setUser] = useState();
    const currentUser = useSelector(state => state.root.currentUser.user);

    function handleEditUser(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.put(
            `${AppConfig.apiUrl}/manager/employee/${userId}`,
            data,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Đã lưu thông tin tài khoản'});
            dispatch(hideLoader())
            //history.push(`/admin/employee/edit/${response.data.id}`)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    useEffect(() => {
        dispatch(showLoader());
        async function getPage() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/employee/${userId}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setUser(response.data);
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getPage();
    }, [userId, dispatch])

    return ( !user ? <Loading /> :
        <div>
            <Helmet>
                <title>Sửa tài khoản</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title="Sửa tài khoản"
                className="mb-3"
                onBack={history.goBack}
            />
            <EmployeeForm
                type={"edit"}
                onEdit={handleEditUser}
                initialValues={user && user}
            />
        </div>
    );
}

export default EmployeeDetails;
