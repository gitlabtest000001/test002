import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet/es/Helmet';
import { useDispatch } from 'react-redux';
import queryString from 'query-string';
import moment from 'moment';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { formatDate, formatDateTime, formatDateUS, formatVND } from '../../../utils/helpers';
import history from '../../../utils/history';
import PageHeader from 'antd/es/page-header';
import { DatePicker, Select, Space } from 'antd';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import Loading from '../../../components/Loading';
import { Link } from 'react-router-dom';
import { TransactionStatus, TransactionType } from '../../../models/transaction.model';
import { MemberWallet } from '../../../models/member.model';
import Button from 'antd/es/button';
import { EditTwoTone } from '@ant-design/icons';
import Card from 'antd/es/card';
import Table from 'antd/es/table';
import { OrderStatus } from '../../../models/order.model';
import LineChart from '../../../components/Chart';

const { RangePicker } = DatePicker;
const { Option } = Select;

function InvestmentStatistic(props) {
    const dispatch = useDispatch();
    const [result, setResult] = useState();
    const [dateRange, setDateRange] = useState(
        [
            moment.utc(moment().clone().startOf('month').format('YYYY-MM-DD')),
            moment.utc(moment().clone().endOf('month').format("YYYY-MM-DD"))
        ]
    )

    function handleSetRange (v) {
        setDateRange(v);
    }

    useEffect(() => {
        dispatch(showLoader())
        async function getPackageInfo() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/statistic/investment`,
                params: {rangeStart: formatDateUS(dateRange[0]), rangeEnd: formatDateUS(dateRange[1])},
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setResult(response.data)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getPackageInfo();
    }, [dispatch, dateRange])

    let dateList;
    let series;

    if (result) {
        dateList = result && result.saleChart.labels;
        series = [
            {
                name: 'Đầu tư',
                data: result && result.saleChart.amount
            },
        ]
    }

    return (
        !result ? <Loading /> :
        <div>
            <Helmet>
                <title>Thống kê bán cổ phần</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title={"Thống kê bán cổ phần"}
                className={"mb-5"}
                extra={
                <Space size={"middle"}>
                    <RangePicker
                        ranges={{
                            'Hôm qua': [moment().subtract(1, 'days'), moment()],
                            'Hôm nay': [moment(), moment().add(1, 'days')],
                            'Tuần trước': [moment().startOf('week').subtract(7,'days'), moment().endOf('week').subtract(7, 'days')],
                            'Tuần này': [moment().startOf('week'), moment().endOf('week')],
                            'Tháng trước': [moment().subtract(1,'months').startOf('month'), moment().subtract(1,'months').endOf('month')],
                            'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                            'Năm trước': [moment().subtract(1,'years').startOf('year'), moment().subtract(1,'years').endOf('year')],
                            'Năm nay': [moment().startOf('year'), moment().endOf('year')],
                        }}
                        value={dateRange}
                        allowClear={false}
                        format={'DD/MM/YYYY'}
                        onChange={(v) => handleSetRange(v)}
                    />
                </Space>
                }
            />
            <Row gutter={[16, 16]}>
                <Col xs={12} lg={6} md={6}>
                    <div
                        className={"bg-white rounded-md px-5 py-3 cursor-pointer group hover:bg-green-600"}
                        onClick={() => history.push('/admin/transactions/type/investment?status=ACTIVE')}
                    >
                        <p className={"font-thin text-gray-600 group-hover:text-white"}>Đầu tư đã duyệt</p>
                        <p className={"font-bold text-xl text-green-600 group-hover:text-white"}>{result && formatVND(result.approvedInvestAmount)}</p>
                    </div>
                </Col>
                <Col xs={12} lg={6} md={6}>
                    <div
                        className={"bg-white rounded-md px-5 py-3 cursor-pointer group hover:bg-yellow-500"}
                        onClick={() => history.push('/admin/transactions/type/investment?status=PENDING')}
                    >
                        <p className={"font-thin text-gray-600 group-hover:text-white"}>Đầu tư chờ duyệt</p>
                        <p className={"font-bold text-xl text-yellow-500 group-hover:text-white"}>{result && formatVND(result.pendingInvestAmount)}</p>
                    </div>
                </Col>
                <Col xs={12} lg={6} md={6}>
                    <div className={"bg-white rounded-md px-5 py-3"}>
                        <p className={"font-thin text-gray-600"}>Cổ phần đã gán</p>
                        <p className={"font-bold text-xl text-blue-500"}>{result && formatVND(result.exchangedInvestAmount)}</p>
                    </div>
                </Col>
                <Col xs={12} lg={6} md={6}>
                    <div className={"bg-white rounded-md px-5 py-3"}>
                        <p className={"font-thin text-gray-600"}>Cổ phần đã thanh toán</p>
                        <p className={"font-bold text-xl text-blue-500"}>{result && formatVND(result.reinvestedAmount)}</p>
                    </div>
                </Col>
                <Col xs={24} lg={24} md={24}>
                    <LineChart dateList={dateList} series={series} title="Biều đồ" />
                </Col>
            </Row>
        </div>
    );
}

export default InvestmentStatistic;
