import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet/es/Helmet';
import { useDispatch } from 'react-redux';
import queryString from 'query-string';
import moment from 'moment';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import {
    convertStatus,
    convertTransactionType,
    formatDate,
    formatDateTime,
    formatDateUS,
    formatVND,
} from '../../../utils/helpers';
import history from '../../../utils/history';
import PageHeader from 'antd/es/page-header';
import { DatePicker, Select, Space } from 'antd';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import Loading from '../../../components/Loading';
import { Link } from 'react-router-dom';
import { TransactionStatus, TransactionType } from '../../../models/transaction.model';
import { MemberWallet } from '../../../models/member.model';
import Button from 'antd/es/button';
import { EditTwoTone } from '@ant-design/icons';
import Card from 'antd/es/card';
import Table from 'antd/es/table';
import { OrderStatus } from '../../../models/order.model';
import LineChart from '../../../components/Chart';
import { ExportXLS } from '../../../components/ExportXLS';

const { RangePicker } = DatePicker;
const { Option } = Select;

function SellingStatistic(props) {
    const dispatch = useDispatch();
    const [result, setResult] = useState();
    const [status, setStatus] = useState('ALL');
    const [dateRange, setDateRange] = useState(
        [
            moment.utc(moment().clone().startOf('month').format('YYYY-MM-DD')),
            moment.utc(moment().clone().endOf('month').format("YYYY-MM-DD"))
        ]
    )

    function handleSetRange (v) {
        setDateRange(v);
    }

    useEffect(() => {
        dispatch(showLoader())
        async function getPackageInfo() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/statistic/selling`,
                params: {orderStatus: status, rangeStart: formatDateUS(dateRange[0]), rangeEnd: formatDateUS(dateRange[1])},
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setResult(response.data)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getPackageInfo();
    }, [dispatch, status, dateRange])

    console.log(result);

    let listOrders = []
    let listProducts = []
    let dateList;
    let series;

    if (result) {
        listOrders = Object.entries(result && result.filterOrders)
        listProducts = Object.entries(result && result.productList)
        dateList = result && result.saleChart.labels;
        series = [
            {
                name: 'Số tiền thu',
                data: result && result.saleChart.revenue
            },
            {
                name: 'Chiết khấu',
                data: result && result.saleChart.discount
            },
            {
                name: 'Số tiền còn lại',
                data: result && result.saleChart.amount
            },
        ]
    }

    const orderColumns = [
        {
            title: 'Ngày tháng',
            dataIndex: 'index',
            key: 'index',
            render: (key, row) =>
                formatDate(row[0])
        },
        {
            title: 'Đơn hàng',
            dataIndex: 'index',
            key: 'index',
            render: (key, row) =>
                row[1].totalOrders
        },
        {
            title: 'Sản phẩm',
            dataIndex: 'index',
            key: 'index',
            render: (key, row) =>
                row[1].totalSales
        },
        {
            title: 'Số tiền thu',
            dataIndex: 'index',
            key: 'index',
            render: (key, row) =>
                formatVND(row[1].revenue)
        },
        {
            title: 'Chiết khấu',
            dataIndex: 'index',
            key: 'index',
            render: (key, row) =>
                formatVND(row[1].discount)
        },
        {
            title: 'Số tiền còn lại',
            dataIndex: 'index',
            key: 'index',
            render: (key, row) =>
                formatVND(row[1].amount)
        },
    ]
    const productColumns = [
        {
            title: 'Tên sản phẩm',
            dataIndex: 'index',
            key: 'index',
            render: (key, row) =>
                row[0]
        },
        {
            title: 'Số lượng',
            dataIndex: 'index',
            key: 'index',
            render: (key, row) =>
                row[1].qty
        },
        {
            title: 'Tạm tính',
            dataIndex: 'index',
            key: 'index',
            render: (key, row) =>
                formatVND(row[1].amount)
        },
    ]

    let exportOrders = []
    if (listOrders) {
        exportOrders = listOrders.map((item) => {
            item = {
                'Ngày tháng': formatDate(item[0]),
                'Số đơn hàng': item[1].totalOrders,
                'Số sản phẩm': item[1].totalSales,
                'Số tiền thu': item[1].revenue,
                'Chiết khấu': item[1].discount,
                'Số tiền còn lại': item[1].amount,
            }
            return item;
        })
    }

    let exportProducts = []
    if (listProducts) {
        exportProducts = listProducts.map((item) => {
            item = {
                'Tên sản phẩm': item[0],
                'Số lượng': item[1].qty,
                'Tạm tính': item[1].amount,
            }
            return item;
        })
    }

    return (
        !result ? <Loading /> :
        <div>
            <Helmet>
                <title>Thống kê bán hàng</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title={"Thống kê bán hàng"}
                className={"mb-5"}
                extra={
                <Space size={"middle"}>
                    <Select
                        defaultValue="ALL"
                        className={"w-full"}
                        onChange={(value) => setStatus(value) }
                    >
                        <Option value="ALL">Tất cả trạng thái</Option>
                        {OrderStatus.map((item, index) => (
                            <Option value={item.code}>{item.name}</Option>
                        ))}
                    </Select>
                    <RangePicker
                        ranges={{
                            'Hôm qua': [moment().subtract(1, 'days'), moment()],
                            'Hôm nay': [moment(), moment().add(1, 'days')],
                            'Tuần trước': [moment().startOf('week').subtract(7,'days'), moment().endOf('week').subtract(7, 'days')],
                            'Tuần này': [moment().startOf('week'), moment().endOf('week')],
                            'Tháng trước': [moment().subtract(1,'months').startOf('month'), moment().subtract(1,'months').endOf('month')],
                            'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                            'Năm trước': [moment().subtract(1,'years').startOf('year'), moment().subtract(1,'years').endOf('year')],
                            'Năm nay': [moment().startOf('year'), moment().endOf('year')],
                        }}
                        value={dateRange}
                        allowClear={false}
                        format={'DD/MM/YYYY'}
                        onChange={(v) => handleSetRange(v)}
                    />
                </Space>
                }
            />
            <Row gutter={[16, 16]}>
                <Col xs={12} lg={6} md={6}>
                    <div className={"bg-white rounded-md px-5 py-3"}>
                        <p className={"font-thin text-gray-600"}>Số tiền thu</p>
                        <p className={"font-bold text-xl text-green-600"}>{result && formatVND(result.CompletedOrders.totalRevenue)}</p>
                    </div>
                </Col>
                <Col xs={12} lg={6} md={6}>
                    <div className={"bg-white rounded-md px-5 py-3"}>
                        <p className={"font-thin text-gray-600"}>Tổng chiết khấu</p>
                        <p className={"font-bold text-xl text-red-600"}>{result && formatVND(result.CompletedOrders.totalDiscount)}</p>
                    </div>
                </Col>
                <Col xs={12} lg={6} md={6}>
                    <div className={"bg-white rounded-md px-5 py-3"}>
                        <p className={"font-thin text-gray-600"}>Số tiền còn lại</p>
                        <p className={"font-bold text-xl text-blue-500"}>{result && formatVND(result.CompletedOrders.totalAmount)}</p>
                    </div>
                </Col>
                <Col xs={12} lg={6} md={6}>
                    <div className={"bg-white rounded-md px-5 py-3"}>
                        <p className={"font-thin text-gray-600"}>Tổng đơn hàng</p>
                        <p className={"font-bold text-xl text-purple-500"}>{result && result.totalOrder}</p>
                    </div>
                </Col>
                <Col xs={24} lg={24} md={24}>
                    <LineChart dateList={dateList} series={series} title="Biều đồ" />
                </Col>
                <Col xs={24} lg={24} md={24}>
                    <Card
                        title={"Đơn hàng theo ngày"}
                        size={"small"}
                        extra={<ExportXLS
                            //csvData={transactions && transactions.list}
                            csvData={exportOrders}
                            fileName={`Đơn hàng theo ngày - ${status} - ${formatDate(dateRange[0])}-${formatDate(dateRange[1])}`}
                        />}
                    >
                        <Table
                            size={"small"}
                            columns={orderColumns}
                            dataSource={listOrders}
                            summary={() => (
                                <Table.Summary fixed>
                                    <Table.Summary.Row>
                                        <Table.Summary.Cell index={0}><span className={"font-bold"}>Tổng</span></Table.Summary.Cell>
                                        <Table.Summary.Cell index={1}><span className={"font-bold"}>{result.totalOrders}</span></Table.Summary.Cell>
                                        <Table.Summary.Cell index={2}><span className={"font-bold"}>{result.totalOrderItems}</span></Table.Summary.Cell>
                                        <Table.Summary.Cell index={3}><span className={"font-bold"}>{formatVND(result.totalRevenue)}</span></Table.Summary.Cell>
                                        <Table.Summary.Cell index={4}><span className={"font-bold"}>{formatVND(result.totalDiscount)}</span></Table.Summary.Cell>
                                        <Table.Summary.Cell index={5}><span className={"font-bold text-blue-500"}>{formatVND(result.totalAmount)}</span></Table.Summary.Cell>
                                    </Table.Summary.Row>
                                </Table.Summary>
                            )}
                        />
                    </Card>
                </Col>
                <Col xs={24} lg={24} md={24}>
                    <Card
                        title={"Sản phẩm bán được"}
                        size={"small"}
                        extra={<ExportXLS
                            //csvData={transactions && transactions.list}
                            csvData={exportProducts}
                            fileName={`Sản phẩm bán được - ${status} - ${formatDate(dateRange[0])}-${formatDate(dateRange[1])}`}
                        />}
                    >
                        <Table
                            size={"small"}
                            columns={productColumns}
                            dataSource={listProducts}
                        />
                    </Card>
                </Col>
            </Row>
        </div>
    );
}

export default SellingStatistic;
