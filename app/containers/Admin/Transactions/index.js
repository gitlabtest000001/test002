import React, { useEffect, useState } from 'react';
import {
    convertStatus, convertTransactionType,
    convertUserAffRoles,
    convertUserLevel, convertUserWallet,
    formatDate,
    formatDateTime,
    formatDateUS,
    formatVND,
} from '../../../utils/helpers';
import { EditTwoTone } from '@ant-design/icons';
import Button from 'antd/es/button';
import { PageHeader, Select, Space, Table, Tag } from 'antd';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import { useDispatch } from 'react-redux';
import queryString from 'query-string';
import moment from 'moment';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import DatePicker from 'antd/es/date-picker';
import history from '../../../utils/history';
import { hideLoader, showLoader } from '../../App/actions';
import { Helmet } from 'react-helmet/es/Helmet';
import { Link } from 'react-router-dom';
import Search from 'antd/es/input/Search';
import { TransactionStatus, TransactionType } from '../../../models/transaction.model';
import { MemberWallet } from '../../../models/member.model';
import { ExportXLS } from '../../../components/ExportXLS';
import Tooltip from 'antd/es/tooltip';

const { RangePicker } = DatePicker;
const {Option} = Select;

function Transactions(props) {
    const dispatch = useDispatch();
    let searchQuery = queryString.parse(props.location.search);
    const [transactions, setTransactions] = useState();
    const [limit, setLimit] = useState(searchQuery && searchQuery.limit ? searchQuery.limit : 10);
    const [page, setPage] = useState(searchQuery && searchQuery.page ? searchQuery.page : 1);
    const [type, setType] = useState(searchQuery && searchQuery.type ? searchQuery.type : 'ALL');
    const [status, setStatus] = useState(searchQuery && searchQuery.status? searchQuery.status : 'ALL');
    const [search, setSearch] = useState(null);
    const [dateRange, setDateRange] = useState(
        [
            moment.utc(moment().clone().startOf('month').format('YYYY-MM-DD')),
            moment.utc(moment().clone().endOf('month').format("YYYY-MM-DD"))
        ]
    )

    function handleSetRange (v) {
        setDateRange(v);
    }

    const [resultExport, setResultExport] = useState([])

    useEffect(() => {
        dispatch(showLoader())
        async function getPackageInfo() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/transaction`,
                params: {limit, page, type: type, status: status, query:search, rangeStart: formatDateUS(dateRange[0]), rangeEnd: formatDateUS(dateRange[1])},
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setTransactions(response.data)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                history.push('/404')
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getPackageInfo();
    }, [dispatch, limit, page, type, status, search, dateRange])

    useEffect(() => {
        dispatch(showLoader())
        async function getPackageInfo() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/transaction`,
                params: {limit:1000000000, page:1, type: type, status: status, query:search, rangeStart: formatDateUS(dateRange[0]), rangeEnd: formatDateUS(dateRange[1])},
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setResultExport(response.data.list)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                history.push('/404')
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getPackageInfo();
    }, [dispatch, type, status, search, dateRange])

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Thành viên',
            dataIndex: 'owner',
            key: 'owner',
            render: (key, row) =>
                <Link to={`/admin/members/detail/${row.owner.id}`}>{row.owner.name} - {row.owner.phone}</Link>
        },
        {
            title: 'Số tiền',
            dataIndex: 'amount',
            key: 'amount',
            render: (text, row) =>
                <Tooltip title={row.note} color={'blue'}>
                    <span className={"font-medium"}>{formatVND(row.amount)}</span>
                </Tooltip>
        },
        {
            title: 'Phần trăm',
            dataIndex: 'percent',
            key: 'percent',
            render: (text, row) =>
                row.percent > 0 &&
                <span className={"font-medium"}>{row.percent}%</span>
        },
        {
            title: 'Loại',
            dataIndex: 'type',
            key: 'type',
            render: (key, row) => (
                TransactionType.map((item, index) => (item.code === row.type && item.name))
            ),
        },
        {
            title: 'Ví',
            dataIndex: 'wallet',
            key: 'wallet',
            render: (key, row) => (
                MemberWallet.map((item, index) => (item.code === row.wallet && item.name))
            ),
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            key: 'status',
            render: (key, row) => (
                TransactionStatus.map((item, index) => (item.code === row.status && item.name))
            ),
        },
        {
            title: 'Ngày tháng',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: (key, row) => formatDateTime(row.createdAt)
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: (key, row) => (
                <Space size={'middle'}>
                    <Button
                        type="primary"
                        size="small"
                        ghost
                        icon={<EditTwoTone />}
                        onClick={() => history.push(`/admin/transaction/${row.id}`)}
                    >
                        Chi tiết
                    </Button>
                </Space>
            )
        }
    ]

    function handleChangePage(page, pageSize) {
        setPage(page);
        setLimit(pageSize);
    }

    let exportData = []
    if(resultExport) {
        exportData = resultExport.map(item => {
            item = {
                'Id': item.id,
                'Thành viên': item.owner.name + '-' + item.owner.phone,
                'Số tiền': item.amount,
                'Loại giao dịch': convertTransactionType(item.type),
                'Phần trăm': item.percent + '%',
                'Ví': convertUserWallet(item.wallet),
                'TT Thanh toán': 'Ngân hàng: ' + item.bank_name + '/STK: ' + item.bank_account + '/CTK: ' + item.bank_owner + '/Chi nhánh: ' + item.bank_brand,
                'Trạng thái': convertStatus(item.status),
                'Ghi chú': item.note,
                'Ngày tạo': formatDateTime(item.createdAt),
            }
            return item;
        })
    }

    return (
            <div>
                <Helmet>
                    <title>Quản lý Giao dịch</title>
                </Helmet>
                <PageHeader
                    ghost={false}
                    title={'Quản lý Giao dịch'}
                    className={"mb-5"}
                    extra={<ExportXLS
                        //csvData={transactions && transactions.list}
                        csvData={exportData && exportData}
                        fileName={`Giao dịch - ${formatDate(dateRange[0])}-${formatDate(dateRange[1])}`}
                    />}
                />
                <div className={"mb-5"}>
                    <Row gutter={[16, 16]}>
                        <Col xs={24} lg={6} md={12}>
                            <Search
                                placeholder="Tìm kiếm..."
                                className={"w-full mt-2"}
                                onSearch={(value) => setSearch(value)}
                                allowClear
                            />
                        </Col>
                        <Col xs={24} lg={6} md={12}>
                            <Select
                                defaultValue="ALL"
                                className={"w-full mt-2"}
                                onChange={(value) => setStatus(value) }
                            >
                                <Option value="ALL">Tất cả trạng thái</Option>
                                {TransactionStatus.map((item, index) => (
                                    <Option value={item.code}>{item.name}</Option>
                                ))}
                            </Select>
                        </Col>
                        <Col xs={24} lg={6} md={12}>
                            <Select
                                defaultValue="ALL"
                                className={"w-full mt-2"}
                                onChange={(value) => setType(value) }
                            >
                                <Option value="ALL">Tất cả các loại</Option>
                                {TransactionType.map((item, index) => (
                                    <Option value={item.code}>{item.name}</Option>
                                ))}
                            </Select>
                        </Col>
                        <Col xs={24} lg={6} md={12}>
                            <RangePicker
                                ranges={{
                                    'Hôm qua': [moment().subtract(1, 'days'), moment()],
                                    'Hôm nay': [moment(), moment().add(1, 'days')],
                                    'Tuần trước': [moment().startOf('week').subtract(7,'days'), moment().endOf('week').subtract(7, 'days')],
                                    'Tuần này': [moment().startOf('week'), moment().endOf('week')],
                                    'Tháng trước': [moment().subtract(1,'months').startOf('month'), moment().subtract(1,'months').endOf('month')],
                                    'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                                    'Năm trước': [moment().subtract(1,'years').startOf('year'), moment().subtract(1,'years').endOf('year')],
                                    'Năm nay': [moment().startOf('year'), moment().endOf('year')],
                                }}
                                className={"mt-2"}
                                value={dateRange}
                                format={'DD/MM/YYYY'}
                                onChange={(v) => handleSetRange(v)}
                            />
                        </Col>
                    </Row>
                </div>
                <Table
                    scroll={{x: '100%'}}
                    columns={columns}
                    dataSource={transactions && transactions.list}
                    pagination={{
                        current: page,
                        pageSize: limit,
                        total: transactions && transactions.count,
                        onChange: ((page, pageSize) => handleChangePage(page, pageSize)),
                        position: ['bottomLeft']
                    }}
                />
            </div>
    );
}

export default Transactions;
