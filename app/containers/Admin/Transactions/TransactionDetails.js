import React, {useEffect, useState} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { Alert, PageHeader } from 'antd';
import Loading from '../../../components/Loading';
import { formatDateTime, formatVND } from '../../../utils/helpers';
import { Link } from 'react-router-dom';
import Card from 'antd/es/card';
import Table from 'antd/es/table';
import Space from 'antd/es/space';
import Button from 'antd/es/button';
import EditTwoTone from '@ant-design/icons/lib/icons/EditTwoTone';
import history from '../../../utils/history';
import Tag from 'antd/es/tag';
import { hideLoader, showLoader } from '../../App/actions';
import { Helmet } from 'react-helmet/es/Helmet';
import { TransactionStatus, TransactionType } from '../../../models/transaction.model';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import ApproveTransactionForm from './ApproveTransactionForm';
import { ErrorMessage, SuccessMessage } from '../../../components/Message';
import { MemberWallet } from '../../../models/member.model';
import { Popconfirm } from 'antd';

function TransactionDetails(props) {
    const dispatch = useDispatch()
    const transactionId = props.match.params.id;
    const [transactionInfo, setTransactionInfo] = useState();
    const [flag, setFlag] = useState(true);
    const settings = useSelector(state => state.root.settings.options);

    useEffect(() => {
        dispatch(showLoader())
        async function getTransactionInfo() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/transaction/${transactionId}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setTransactionInfo(response.data)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                //history.push('/404')
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getTransactionInfo();
    }, [transactionId, flag])

    function handleUpdateTransaction(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.put(
            `${AppConfig.apiUrl}/manager/transaction/${transactionId}`,
            data,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Đã cập nhật trạng thái giao dịch'});
            dispatch(hideLoader())
            setFlag(!flag)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    function handleRevertInvestment() {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.post(
            `${AppConfig.apiUrl}/manager/transaction/createStockInvoice`,
            {parentId: transactionId},
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Đã cập nhật hoàn trả cổ phần vào ví cổ phần'});
            dispatch(hideLoader())
            setFlag(!flag)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    return (
        !transactionInfo ? <Loading /> :
            <div>
                <Helmet>
                    <title>Quản lý giao dịch</title>
                </Helmet>
                <PageHeader
                    ghost={false}
                    title={`Giao dịch #${transactionInfo && transactionInfo.id}`}
                    className={"mb-5"}
                    onBack={() => history.goBack()}
                />
                <Row gutter={[16, 16]}>
                    <Col xs={24} lg={16} md={16}>
                        <Card
                            title={"Thông tin giao dịch"}
                            size={"small"}
                            /*extra={transactionInfo.type === 'EXCHANGESTOCK' && transactionInfo.status !== 'PAID' &&
                                <Popconfirm
                                    title="Chắc chắn muốn hoàn trả cổ phần cho giao dịch thế chấp này?"
                                    onConfirm={handleRevertInvestment}
                                    //onCancel={cancel}
                                    okText="Chắc chắn"
                                    cancelText="Nghĩ lại"
                                >
                                    <Button
                                        type={"primary"}
                                    >
                                        Tạo giao dịch hoàn trả Cổ phần
                                    </Button>
                                </Popconfirm>
                            }*/
                        >

                            <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                <span>
                                    Số tiền:
                                </span>
                                <span className={"ml-5 font-medium"}>
                                    {formatVND(transactionInfo.amount)}
                                </span>
                                <span className={"ml-5 font-medium"}>
                                    {transactionInfo.percent > 0 && `(${transactionInfo.percent}%)`}
                                </span>
                            </div>
                            <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                <span>
                                    Loại giao dịch:
                                </span>
                                <span className={"ml-5 font-medium"}>
                                    {TransactionType.map((item) => (item.code === transactionInfo.type && item.name))}
                                </span>
                            </div>
                            {transactionInfo.type === 'INVESTMENT' &&
                                <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                    <span>
                                        Số lượng cổ phần:
                                    </span>
                                    <span className={"ml-5 font-medium"}>
                                        {Number(transactionInfo.stockQty)}
                                    </span>
                                </div>
                            }
                            {transactionInfo.wallet &&
                                <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                    <span>
                                        Ví:
                                    </span>
                                    <span className={"ml-5 font-medium text-red-500"}>
                                        {MemberWallet.map((item) => (item.code === transactionInfo.wallet && item.name))}
                                    </span>
                                </div>
                            }
                            {transactionInfo.order &&
                                <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                    <span>
                                        Tham chiếu đơn hàng:
                                    </span>
                                    <span className={"ml-5 font-medium text-blue-500"}>
                                        <Link to={`/admin/order/${transactionInfo.order.id}`}>#{transactionInfo.order.id}</Link>
                                    </span>
                                </div>
                            }
                            {transactionInfo.owner &&
                                <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                    <span>
                                        Chủ sở hữu giao dịch:
                                    </span>
                                    <span className={"ml-5 font-medium text-blue-500"}>
                                        <Link to={`/admin/members/detail/${transactionInfo.owner.id}`}>#{transactionInfo.owner.id}-{transactionInfo.owner.name}-{transactionInfo.owner.phone} </Link>
                                    </span>
                                </div>
                            }
                            {transactionInfo.createdBy &&
                                <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                    <span>
                                        Người tạo giao dịch:
                                    </span>
                                    <span className={"ml-5 font-medium text-blue-500"}>
                                        <Link to={`/admin/members/detail/${transactionInfo.createdBy.id}`}>#{transactionInfo.createdBy.id}-{transactionInfo.createdBy.name}-{transactionInfo.createdBy.phone}</Link>
                                    </span>
                                </div>
                            }
                            <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                <span>
                                    Trạng thái:
                                </span>
                                        <span className={"ml-5 font-medium"}>
                                    {TransactionStatus.map((item) => (item.code === transactionInfo.status && item.name))}
                                </span>
                                    </div>
                            <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                <span>
                                    Ngày tạo:
                                </span>
                                <span className={"ml-5 font-medium"}>
                                    {formatDateTime(transactionInfo.createdAt)}
                                </span>
                            </div>
                            {transactionInfo.updatedAt &&
                                <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                    <span>
                                        Ngày cập nhật:
                                    </span>
                                    <span className={"ml-5 font-medium"}>
                                        {formatDateTime(transactionInfo.updatedAt)}
                                    </span>
                                </div>
                            }
                            {transactionInfo && transactionInfo.type === 'WITHDRAW' &&
                                <div>
                                    <p className={"mb-3 font-bold"}>Thông tin nhận tiền</p>
                                    <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                        <span>
                                            Tên ngân hàng:
                                        </span>
                                            <span className={"ml-5 font-medium"}>
                                            {transactionInfo.bank_name}
                                        </span>
                                    </div>
                                    <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                        <span>
                                            Chủ tài khoản:
                                        </span>
                                            <span className={"ml-5 font-medium"}>
                                            {transactionInfo.bank_owner}
                                        </span>
                                    </div>
                                    <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                        <span>
                                            Số tài khoản:
                                        </span>
                                            <span className={"ml-5 font-medium"}>
                                            {transactionInfo.bank_account}
                                        </span>
                                    </div>
                                    {transactionInfo.bank_brand &&
                                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                            <span>
                                                Chi nhánh:
                                            </span>
                                                <span className={"ml-5 font-medium"}>
                                                {transactionInfo.bank_brand}
                                            </span>
                                        </div>
                                    }
                                </div>
                            }
                            <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                    <span>
                                        Ghi chú:
                                    </span>
                                <span className={"ml-5 font-medium"}>
                                        {transactionInfo.note}
                                    </span>
                            </div>
                        </Card>
                    </Col>
                    <Col xs={24} lg={8} md={8}>
                        {transactionInfo && transactionInfo.status === 'PENDING' &&
                            <ApproveTransactionForm
                                initialValues={{status: transactionInfo.type === 'WITHDRAW' ? 'PAID' : 'ACTIVE'}}
                                type={transactionInfo.type}
                                onSubmit={handleUpdateTransaction}
                            />
                        }
                    </Col>
                </Row>
            </div>

    );
}

export default TransactionDetails;
