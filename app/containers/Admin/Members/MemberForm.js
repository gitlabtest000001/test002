import React, { useEffect, useState } from 'react';
import Form from 'antd/es/form';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import Card from 'antd/es/card';
import Input from 'antd/es/input';
import { DatePicker, Select } from 'antd';
import ThumbnailUpload from '../../../components/ThumbnailUpload';
import TextArea from 'antd/es/input/TextArea';
import Button from 'antd/es/button';
import CheckOutlined from '@ant-design/icons/lib/icons/CheckOutlined';
import { SaveOutlined } from '@ant-design/icons';
import { MemberRoles, UserAffRoles, UserLevel, UserStatus } from '../../../models/member.model';
import SingleUpload from '../../../components/SingleUpload';
import { MaskedInput } from 'antd-mask-input';

const {Option} = Select;

function MemberForm(props) {
    const [avatar, setAvatar] = useState(props.initialValues && props.initialValues.avatar);
    const [form] = Form.useForm();

    function handleChooseAvatar(avatar) {
        setAvatar(avatar)
    }

    useEffect(() => form.resetFields(), [props.initialValues]);

    function handleSubmit(values) {
        if (props.formType === 'add') {
            props.onSubmit({...values, avatar})
        } else props.onEdit({...values, avatar})
    }

    return (
        <Form
            layout="vertical"
            name="UserForm"
            form={form}
            onFinish={handleSubmit}
            initialValues={props.initialValues}
        >
            <Row gutter={16}>
                <Col xs={24} lg={16} md={16}>
                    <Card title="Thông tin tài khoản">
                        <Row gutter={[16, 16]}>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Họ và tên"
                                    name="name"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập tên'},
                                    ]}
                                    className={"mb-0"}
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Email"
                                    name="email"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập email'
                                        },
                                        {
                                            type: 'email',
                                            message: 'Vui lòng nhập đúng định dạng Email!',
                                        },
                                    ]}
                                    className={"mb-0"}
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Số điện thoại"
                                    name="phone"
                                    rules={[
                                        {
                                            required: true,
                                            message: 'Vui lòng nhập tên'},
                                    ]}
                                    className={"mb-0"}
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Ngày sinh"
                                    name="dateofbirth"
                                    className={"mb-0"}
                                >
                                    <MaskedInput
                                        size={"large"}
                                        mask={"00/00/0000"}
                                        />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Địa chỉ"
                                    name="address"
                                    className={"mb-0"}
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Mật khẩu"
                                    name="password"
                                    className={"mb-0"}
                                    rules={[
                                        {
                                            required: props.formType === 'add',
                                            message: 'Vui lòng tạo mật khẩu'
                                        },
                                    ]}
                                >
                                    <Input.Password
                                        placeHolder={props.formType === 'edit' && 'Chỉ nhập vào nếu muốn thay đổi'}
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            {props.formType === 'edit' &&
                                <Col xs={24} lg={12} md={12}>
                                    <Form.Item name="status" label="Trạng thái"
                                               className={"mb-0"}>
                                        <Select
                                            style={{width: '100%'}}
                                            placeholder="Chọn trạng thái"
                                            size="large"
                                        >
                                            {UserStatus.map((item, index) => (
                                                <Option value={item.code}>{item.name}</Option>
                                            ))}
                                        </Select>
                                    </Form.Item>
                                </Col>
                            }
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item name="level" label="Cấp bậc"
                                           className={"mb-0"}>
                                    <Select
                                        style={{width: '100%'}}
                                        placeholder="Chọn cấp bậc"
                                        size="large"
                                    >
                                        {UserLevel.map((item, index) => (
                                            <Option value={item.code}>{item.name}</Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item name="investorStatus" label="Nhà đầu tư"
                                           className={"mb-0"}>
                                    <Select
                                        style={{width: '100%'}}
                                        placeholder="Chọn trạng thái nhà đầu tư"
                                        size="large"
                                    >
                                        <Option value="TRUE">Có</Option>
                                        <Option value="FALSE">Không</Option>
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item name="officeStatus" label="Văn phòng kinh doanh"
                                           className={"mb-0"}>
                                    <Select
                                        style={{width: '100%'}}
                                        placeholder="Chọn trạng thái văn phòng"
                                        size="large"
                                    >
                                        <Option value="TRUE">Có</Option>
                                        <Option value="FALSE">Không</Option>
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item name="affRoles" label="Chức vụ"
                                           className={"mb-0"}>
                                    <Select
                                        style={{width: '100%'}}
                                        placeholder="Chọn chức vụ"
                                        size="large"
                                    >
                                        {UserAffRoles.map((item, index) => (
                                            <Option value={item.code}>{item.name}</Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Cấp trên trực tiếp"
                                    name="parent"
                                    className={"mb-0"}
                                >
                                    <Select
                                        allowClear
                                        style={{width: '100%'}}
                                        placeholder="Chọn cấp trên"
                                        showSearch={true}
                                        size="large"
                                        optionFilterProp="children"
                                    >
                                        {props.listUser && props.listUser.map((item) => (
                                            <Option key={item.id} value={item.id}>{item.name} - {item.phone}</Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                            </Col>
                        </Row>
                    </Card>

                    <Card title="Social Network" className={"mt-5"}>
                        <Row gutter={[16, 16]}>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Zalo"
                                    name="zalo"
                                    className={"mb-0"}
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Facebook"
                                    name="facebook"
                                    className={"mb-0"}
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Zalo Group"
                                    name="zaloGroup"
                                    className={"mb-0"}
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Whatsapp"
                                    name="whatsapp"
                                    className={"mb-0"}
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <Form.Item
                                    label="Youtube"
                                    name="youtube"
                                    className={"mb-0"}
                                >
                                    <Input
                                        size="large"
                                    />
                                </Form.Item>
                            </Col>
                        </Row>
                    </Card>
                </Col>
                <Col xs={24} lg={8} md={8}>
                    <Card>
                        <Form.Item>
                            <SingleUpload
                                currentImage={props.initialValues && props.initialValues.avatar}
                                uploadLabel={props.formType === 'add' ? 'Ảnh đại diện' : 'Đổi ảnh đại diện'}
                                onChooseMedia={handleChooseAvatar}
                                manager
                            />
                        </Form.Item>
                        <Form.Item
                            label="Ghi chú"
                            name="note"
                        >
                            <TextArea rows={3} />
                        </Form.Item>
                        <Button
                            type="primary"
                            htmlType="submit"
                            icon={props.formType === 'add' ? <CheckOutlined /> : <SaveOutlined />}
                        >
                            {props.formType === 'add' ? 'Tạo tài khoản' : 'Lưu thay đổi'}
                        </Button>
                    </Card>

                </Col>
            </Row>
        </Form>
    );
}

export default MemberForm;
