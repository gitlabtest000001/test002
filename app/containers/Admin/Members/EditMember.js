import React, { useEffect, useState } from 'react';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { ErrorMessage, SuccessMessage } from '../../../components/Message';
import { useDispatch } from 'react-redux';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader } from 'antd';
import history from '../../../utils/history';
import MemberForm from './MemberForm';
import Loading from '../../../components/Loading';

function EditMember(props) {
    const dispatch = useDispatch();
    const userId = props.match.params.id;
    const [memberInfo, setMemberInfo] = useState();
    const [listUser, setListUser] = useState()

    useEffect(() => {
        dispatch(showLoader());
        async function getUsers() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/user`,
                params: {
                    status: 'ACTIVE',
                    limit: 100000,
                    role: JSON.stringify(['member']),
                    excludeId: userId
                },
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setListUser(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getUsers();
    },[dispatch])

    function handleEditUser(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.put(
            `${AppConfig.apiUrl}/manager/user/${userId}`,
            data,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Đã lưu thông tin tài khoản'});
            dispatch(hideLoader())
            history.push(`/admin/members/detail/${response.data.id}`)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    useEffect(() => {
        dispatch(showLoader());
        async function getPage() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/user/${userId}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setMemberInfo(response.data);
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getPage();
    }, [userId, dispatch])

    return (
        <div>
            <Helmet>
                <title>Sửa tài khoản</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title="Sửa tài khoản"
                className="mb-3"
                onBack={history.goBack}
            />
            {!memberInfo ? <Loading/> :
                <MemberForm
                    formType={"edit"}
                    onEdit={handleEditUser}
                    initialValues={{ ...memberInfo, parent: memberInfo.parent && memberInfo.parent.id }}
                    listUser={listUser && listUser.users}
                />
            }
        </div>
    );
}

export default EditMember;
