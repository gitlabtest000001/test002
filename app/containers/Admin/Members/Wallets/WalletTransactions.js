import React, { useEffect, useState } from 'react';
import { hideLoader, showLoader } from '../../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';
import history from '../../../../utils/history';
import { formatDateTime, formatVND } from '../../../../utils/helpers';
import { TransactionStatus, TransactionType } from '../../../../models/transaction.model';
import { Space, Table } from 'antd';
import Button from 'antd/es/button';
import { EditTwoTone } from '@ant-design/icons';
import { useDispatch } from 'react-redux';
import Card from 'antd/es/card';
import { MemberWallet } from '../../../../models/member.model';

function WalletTransactions(props) {
    const dispatch = useDispatch()
    const [transactions, setTransactions] = useState();

    useEffect(() => {
        dispatch(showLoader())
        async function getPackageInfo() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/transaction`,
                params: {type: props.type, limit: 100000, page: 1, owner: props.memberId},
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setTransactions(response.data)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                //history.push('/404')
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getPackageInfo();
    }, [dispatch])

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Số tiền',
            dataIndex: 'amount',
            key: 'amount',
            render: (text, row) =>
                <span className={"font-medium"}>{formatVND(row.amount)}</span>
        },
        {
            title: 'Loại',
            dataIndex: 'type',
            key: 'type',
            render: (key, row) => (
                TransactionType.map((item, index) => (item.code === row.type && item.name))
            ),
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            key: 'status',
            render: (key, row) => (
                TransactionStatus.map((item, index) => (item.code === row.status && item.name))
            ),
        },
        {
            title: 'Ví',
            dataIndex: 'wallet',
            key: 'wallet',
            render: (key, row) => (
                MemberWallet.map((item, index) => (item.code === row.wallet && item.name))
            ),
        },
        {
            title: 'Ngày tháng',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: (key, row) => formatDateTime(row.createdAt)
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: (key, row) => (
                <Space size={'middle'}>
                    <Button
                        type="primary"
                        size="small"
                        ghost
                        icon={<EditTwoTone />}
                        onClick={() => history.push(`/admin/transaction/${row.id}`)}
                    >
                        Chi tiết
                    </Button>
                </Space>
            )
        }
    ]
    return (
        <div>
            <Card
                size={"small"}
                title={`Danh sách giao dịch ${props.title}`}
            >
                <Table
                    scroll={{x: '100%'}}
                    columns={columns}
                    dataSource={transactions && transactions.list}
                    pagination={false}
                />
            </Card>
        </div>
    );
}

export default WalletTransactions;
