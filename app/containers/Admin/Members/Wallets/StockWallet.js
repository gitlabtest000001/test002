import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { hideLoader, showLoader } from '../../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';
import Loading from '../../../../components/Loading';
import { Helmet } from 'react-helmet/es/Helmet';
import { InputNumber, PageHeader, Select } from 'antd';
import history from '../../../../utils/history';
import WalletTransactions from './WalletTransactions';
import Button from 'antd/es/button';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import Card from 'antd/es/card';
import Form from 'antd/es/form';
import { banks } from '../../../../utils/DefaultLists/bankList';
import Input from 'antd/es/input';
import CheckOutlined from '@ant-design/icons/lib/icons/CheckOutlined';
import { ErrorMessage, SuccessMessage } from '../../../../components/Message';
import { formatVND } from '../../../../utils/helpers';

function StockWallet(props) {
    const dispatch = useDispatch();
    const userId = props.match.params.id;
    const [memberInfo, setMemberInfo] = useState();
    const settings = useSelector(state => state.root.settings.options);

    useEffect(() => {
        dispatch(showLoader());
        async function getPage() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/user/${userId}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setMemberInfo(response.data);
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getPage();
    }, [userId, dispatch])

    function handleSubmit(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.post(
            `${AppConfig.apiUrl}/manager/transaction/exchangeStock`,
            {...data, userId},
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Đã tạo giao dịch gán cổ phần'});
            dispatch(hideLoader())
            history.push(`/admin/transaction/${response.data.id}`)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    return (
        !memberInfo ? <Loading /> :
        <div>
            <Helmet>
                <title>Ví cổ phần</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title={memberInfo && memberInfo.name +'-'+ memberInfo.phone}
                subTitle={"Ví cổ phần"}
                className="mb-3"
                onBack={history.goBack}
            />
            <Row gutter={[16, 16]}>
                {/*<Col xs={24} lg={6} md={6}>
                    <Card title={"Gán cổ phần lấy sản phẩm"} size={"small"}>
                        <h3 className={"mb-2 font-medium"}>Cổ phần hiện có: {formatVND(memberInfo.stock_wallet ? memberInfo.stock_wallet : 0)}</h3>
                        <h3 className={"mb-5 font-medium"}>Số tiền nhỏ nhất có thể gán: {formatVND(settings && settings.minimum_exchange_stock_amount)}</h3>
                        <Form
                            layout="vertical"
                            name="UserForm"
                            onFinish={handleSubmit}
                            initialValues={{
                                stockAmount: memberInfo.stock_wallet
                            }}
                        >
                            <Form.Item
                                label="Số cổ phần muốn gán"
                                name="stockAmount"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập số lượng'},
                                ]}
                            >
                                <InputNumber
                                    formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                                    parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                                    className={"w-full"}
                                    size="large"
                                />
                            </Form.Item>
                            <Button
                                type="primary"
                                htmlType="submit"
                                icon={<CheckOutlined />}
                            >
                                Xác nhận
                            </Button>
                        </Form>
                    </Card>
                </Col>*/}
                <Col xs={24} lg={18} md={18}>
                    <WalletTransactions
                        type={"INVESTMENT, REVERTINVESTMENT, EXCHANGESTOCK".split(', ')}
                        memberId={userId}
                        title={"Cổ phần"}
                    />
                </Col>
            </Row>
        </div>
    );
}

export default StockWallet;
