import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { Helmet } from 'react-helmet/es/Helmet';
import { PageHeader } from 'antd';
import history from '../../../utils/history';
import Row from 'antd/es/grid/Row';
import MemberInfo from './components/MemberInfo';
import Loading from '../../../components/Loading';
import MemberOrders from './components/MemberOrders';
import MemberTransactions from './components/MemberTransactions';
import { Link } from 'react-router-dom';

function MemberDetails(props) {
    const dispatch = useDispatch();
    const userId = props.match.params.id;
    const [memberInfo, setMemberInfo] = useState();

    useEffect(() => {
        dispatch(showLoader());
        async function getPage() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/user/${userId}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setMemberInfo(response.data);
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getPage();
    }, [userId, dispatch])

    return (
        !memberInfo ? <Loading /> :
        <div>
            <Helmet>
                <title>Thông tin tài khoản</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title="Thông tin tài khoản"
                className="mb-3"
                onBack={history.goBack}
                extra={
                    <Link to={`/admin/members/tree/${userId}`}>Sơ đồ hệ thống</Link>
                }
            />
            <div className={"mb-5"}>
                <MemberInfo
                    member={memberInfo}
                />
            </div>
            <MemberOrders
                userId={userId}
            />
            <MemberTransactions
                userId={userId}
            />
        </div>
    );
}

export default MemberDetails;
