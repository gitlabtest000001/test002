import React, { useEffect, useState } from 'react';
import { PageHeader } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { Helmet } from 'react-helmet/es/Helmet';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { ErrorMessage, SuccessMessage } from '../../../components/Message';
import history from '../../../utils/history';
import MemberForm from './MemberForm';

function AddMember(props) {
    const dispatch = useDispatch()
    const currentUser = useSelector(state => state.root.currentUser.user);
    const [listUser, setListUser] = useState()

    useEffect(() => {
        dispatch(showLoader());
        async function getUsers() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/user`,
                params: {
                    status: 'ACTIVE',
                    limit: 100000000,
                    role: JSON.stringify(['member'])
                },
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setListUser(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getUsers();
    },[dispatch])

    function handleCreateUser(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_manager_token');
        axios.post(
            `${AppConfig.apiUrl}/manager/user`,
            data,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Tạo tài khoản thành công'});
            dispatch(hideLoader())
            history.push(`/admin/members/edit/${response.data.id}`)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    return (
        <div>
            <Helmet>
                <title>Tạo tài khoản</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title="Tạo tài khoản"
                className="mb-3"
                onBack={history.goBack}
            />
            <MemberForm
                formType={"add"}
                onSubmit={handleCreateUser}
                initialValues={{
                    roles: 'member',
                    investorStatus: 'FALSE',
                    officeStatus: 'FALSE',
                }}
                listUser={listUser && listUser.users}
            />
        </div>
    );
}

export default AddMember;
