import React, { useEffect, useState } from 'react';
import { formatDateTime, formatDateUS, formatVND } from '../../../../utils/helpers';
import { EditTwoTone } from '@ant-design/icons';
import Button from 'antd/es/button';
import { PageHeader, Select, Space, Table, Tag } from 'antd';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import { useDispatch } from 'react-redux';
import queryString from 'query-string';
import moment from 'moment';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';
import DatePicker from 'antd/es/date-picker';
import history from '../../../../utils/history';
import { hideLoader, showLoader } from '../../../App/actions';
import { Helmet } from 'react-helmet/es/Helmet';
import { Link } from 'react-router-dom';
import Search from 'antd/es/input/Search';
import Card from 'antd/es/card';
import { OrderStatus } from '../../../../models/order.model';
import { TransactionStatus, TransactionType } from '../../../../models/transaction.model';
import Tooltip from 'antd/es/tooltip';
import { MemberWallet } from '../../../../models/member.model';

const { RangePicker } = DatePicker;
const {Option} = Select;

function MemberTransactions(props) {
    const dispatch = useDispatch();
    const [transactions, setTransactions] = useState();
    const [limit, setLimit] = useState(10);
    const [page, setPage] = useState(1);
    const [type, setType] = useState('ALL');
    const [status, setStatus] = useState('ALL');
    const [search, setSearch] = useState(null);
    const [dateRange, setDateRange] = useState(
        [
            moment.utc(moment().clone().startOf('month').format('YYYY-MM-DD')),
            moment.utc(moment().clone().endOf('month').format("YYYY-MM-DD"))
        ]
    )

    function handleSetRange (v) {
        setDateRange(v);
    }

    useEffect(() => {
        dispatch(showLoader())
        async function getPackageInfo() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/transaction`,
                params: {
                    limit,
                    page,
                    type: type,
                    status: status,
                    query:search,
                    rangeStart: formatDateUS(dateRange[0]),
                    rangeEnd: formatDateUS(dateRange[1]),
                    owner: props.userId,
                },
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setTransactions(response.data)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                history.push('/404')
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getPackageInfo();
    }, [dispatch, limit, page, type, status, search, dateRange, props.userId])

    console.log(transactions);

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Số tiền',
            dataIndex: 'amount',
            key: 'amount',
            render: (text, row) =>
                <Tooltip title={row.note} color={"blue"}>
                    <span className={"font-medium"}>{formatVND(row.amount)}</span>
                </Tooltip>
        },
        {
            title: 'Loại',
            dataIndex: 'type',
            key: 'type',
            render: (key, row) => (
                TransactionType.map((item, index) => (item.code === row.type && item.name))
            ),
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            key: 'status',
            render: (key, row) => (
                TransactionStatus.map((item, index) => (item.code === row.status && item.name))
            ),
        },
        {
            title: 'Ví',
            dataIndex: 'wallet',
            key: 'wallet',
            render: (key, row) => (
                MemberWallet.map((item, index) => (item.code === row.wallet && item.name))
            ),
        },
        {
            title: 'Ngày tháng',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: (key, row) => formatDateTime(row.createdAt)
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: (key, row) => (
                <Space size={'middle'}>
                    <Button
                        type="primary"
                        size="small"
                        ghost
                        icon={<EditTwoTone />}
                        onClick={() => history.push(`/admin/transaction/${row.id}`)}
                    >
                        Chi tiết
                    </Button>
                </Space>
            )
        }
    ]

    function handleChangePage(page, pageSize) {
        setPage(page);
        setLimit(pageSize);
    }
    return (
        <div className={"mb-5"}>
            <Card
                title={"Danh sách giao dịch"}
                size={"small"}
                extra={
                    <span>Tìm thấy {transactions && transactions.count} giao dịch</span>
                }
            >
                <Row gutter={[16, 16]}>
                    <Col xs={24} lg={6} md={12}>
                        <Search
                            placeholder="Tìm kiếm..."
                            className={"w-full mt-2"}
                            onSearch={(value) => setSearch(value)}
                            allowClear
                        />
                    </Col>
                    <Col xs={24} lg={6} md={12}>
                        <Select
                            defaultValue="ALL"
                            className={"w-full mt-2"}
                            onChange={(value) => setStatus(value) }
                        >
                            <Option value="ALL">Tất cả trạng thái</Option>
                            {TransactionStatus.map((item, index) => (
                                <Option value={item.code}>{item.name}</Option>
                            ))}
                        </Select>
                    </Col>
                    <Col xs={24} lg={6} md={12}>
                        <Select
                            defaultValue="ALL"
                            className={"w-full mt-2"}
                            onChange={(value) => setType(value) }
                        >
                            <Option value="ALL">Tất cả các loại</Option>
                            {TransactionType.map((item, index) => (
                                <Option value={item.code}>{item.name}</Option>
                            ))}
                        </Select>
                    </Col>
                    <Col xs={24} lg={6} md={12}>
                        <RangePicker
                            ranges={{
                                'Hôm qua': [moment().subtract(1, 'days'), moment()],
                                'Hôm nay': [moment(), moment().add(1, 'days')],
                                'Tuần trước': [moment().startOf('week').subtract(7,'days'), moment().endOf('week').subtract(7, 'days')],
                                'Tuần này': [moment().startOf('week'), moment().endOf('week')],
                                'Tháng trước': [moment().subtract(1,'months').startOf('month'), moment().subtract(1,'months').endOf('month')],
                                'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                                'Năm trước': [moment().subtract(1,'years').startOf('year'), moment().subtract(1,'years').endOf('year')],
                                'Năm nay': [moment().startOf('year'), moment().endOf('year')],
                            }}
                            className={"mt-2"}
                            value={dateRange}
                            format={'DD/MM/YYYY'}
                            onChange={(v) => handleSetRange(v)}
                        />
                    </Col>
                </Row>
            </Card>
            <Table
                scroll={{x: '100%'}}
                columns={columns}
                dataSource={transactions && transactions.list}
                pagination={{
                    current: page,
                    pageSize: limit,
                    total: transactions && transactions.count,
                    onChange: ((page, pageSize) => handleChangePage(page, pageSize)),
                    position: ['bottomLeft']
                }}
            />
        </div>
    );
}

export default MemberTransactions;
