import React from 'react';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import { formatDateTime, formatVND } from '../../../../utils/helpers';
import Card from 'antd/es/card';
import Button from 'antd/es/button';
import history from '../../../../utils/history';
import { IoBag, IoGift, IoPeopleCircle, IoShareSocial, IoWallet } from 'react-icons/io5';
import { IoBagCheck, IoCash } from 'react-icons/all';
import { Link } from 'react-router-dom';
import { UserAffRoles, UserLevel, UserStatus } from '../../../../models/member.model';
import { useSelector } from 'react-redux';

function MemberInfo(props) {
    const member = props.member;
    const settings = useSelector(state => state.root.settings.options);

    return (
        <div>
            <Row gutter={[16, 16]}>
                <Col xs={24} lg={12} md={12}>
                    <div className={"mb-10"}>
                        <h2 className={"font-medium text-lg mb-3"}>Các loại ví tiền</h2>
                        <Row gutter={[16, 16]}>
                            <Col xs={24} lg={12} md={12}>
                                <div className={"flex items-center shadow border-t border-gray-50 rounded bg-white px-3 py-2 cursor-pointer group hover:bg-green-600"}
                                    //onClick={() => history.push(`/admin/members/wallet/reward/${member.id}`)}
                                >
                                    <IoGift size={30} className={"mr-5 text-red-500 group-hover:text-white"}/>
                                    <div>
                                        <h4 className={"text-xs group-hover:text-white"}>Ví thưởng</h4>
                                        <p className={"font-bold text-xl text-gray-800 group-hover:text-white"}>{member && formatVND(member.reward_wallet)}</p>
                                    </div>
                                </div>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <div className={"flex items-center shadow border-t border-gray-50 rounded bg-white px-3 py-2 cursor-pointer group hover:bg-green-600"}
                                     //onClick={() => history.push(`/admin/members/wallet/cash/${member.id}`)}
                                >
                                    <IoCash size={30} className={"mr-5 text-blue-500 group-hover:text-white"}/>
                                    <div>
                                        <h4 className={"text-xs group-hover:text-white"}>Ví tiền mặt</h4>
                                        <p className={"font-bold text-xl text-gray-800 group-hover:text-white"}>{member && formatVND(member.cash_wallet)}</p>
                                    </div>
                                </div>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <div className={"flex items-center shadow border-t border-gray-50 rounded bg-white px-3 py-2 cursor-pointer group hover:bg-green-600"}
                                     onClick={() => history.push(`/admin/members/wallet/stock/${member.id}`)}
                                >
                                    <IoWallet size={30} className={"mr-5 text-green-500 group-hover:text-white"}/>
                                    <div>
                                        <h4 className={"text-xs group-hover:text-white"}>Số cổ phần</h4>
                                        <p className={"font-bold text-xl text-gray-800 group-hover:text-white"}>{member && member.stock}</p>
                                    </div>
                                </div>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <div className={"flex items-center shadow border-t border-gray-50 rounded bg-white px-3 py-2 cursor-pointer group hover:bg-green-600"}
                                     //onClick={() => history.push(`/admin/members/wallet/product/${member.id}`)}
                                >
                                    <IoBagCheck size={30} className={"mr-5 text-purple-500 group-hover:text-white"}/>
                                    <div>
                                        <h4 className={"text-xs group-hover:text-white"}>Ví tiền hàng</h4>
                                        <p className={"font-bold text-xl text-gray-800 group-hover:text-white"}>{member && formatVND(member.product_wallet)}</p>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </div>
                    <div className={"mb-5"}>
                        <h2 className={"font-medium text-lg mb-3"}>Thống kê doanh số</h2>
                        <Row gutter={[16, 16]}>
                            <Col xs={24} lg={12} md={12}>
                                <div className={"flex items-center shadow border-t border-gray-50 rounded bg-white px-3 py-2"}>
                                    <IoShareSocial size={30} className={"mr-5 text-red-500"}/>
                                    <div>
                                        <h4 className={"text-xs"}>Doanh số giới thiệu</h4>
                                        <p className={"font-bold text-xl text-gray-800"}>{member && formatVND(member.affiliateSales)}</p>
                                    </div>
                                </div>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <div className={"flex items-center shadow border-t border-gray-50 rounded bg-white px-3 py-2"}>
                                    <IoBag size={30} className={"mr-5 text-yellow-500"}/>
                                    <div>
                                        <h4 className={"text-xs"}>Doanh số mua hàng</h4>
                                        <p className={"font-bold text-xl text-gray-800"}>{member && formatVND(member.sales)}</p>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </div>
                </Col>
                <Col xs={24} lg={12} md={12}>
                    <Card
                        title={"Thông tin cá nhân"}
                        size={"small"}
                        extra={
                            <Button
                                type={"primary"}
                                onClick={() => history.push(`/admin/members/edit/${member.id}`)}
                            >
                                Sửa thông tin
                            </Button>
                        }
                    >
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Mã giới thiệu:
                            </span>
                            <span className={"ml-5 font-medium"}>
                                {member.id}
                            </span>
                            <a href={`${settings && settings.website_url}/dang-ky?ref=${member.id}`} target={"_blank"} className={"ml-5 font-bold text-red-500"}>
                                Link đăng ký
                            </a>
                        </div>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Họ tên:
                            </span>
                                <span className={"ml-5 font-medium"}>
                                {member.name}
                            </span>
                        </div>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Số điện thoại:
                            </span>
                                <span className={"ml-5 font-medium"}>
                                {member.phone}
                            </span>
                        </div>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Email:
                            </span>
                                <span className={"ml-5 font-medium"}>
                                {member.email}
                            </span>
                        </div>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Địa chỉ:
                            </span>
                                <span className={"ml-5 font-medium"}>
                                {member.address}
                            </span>
                        </div>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Trạng thái:
                            </span>
                                <span className={"ml-5 font-medium"}>
                                {UserStatus.map((item, index) => (
                                    item.code === member.status && item.name
                                ))}
                            </span>
                        </div>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Chức vụ:
                            </span>
                                <span className={"ml-5 font-medium"}>
                                {UserAffRoles.map((item, index) => (
                                    item.code === member.affRoles && item.name
                                ))}
                            </span>
                        </div>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Cấp bậc:
                            </span>
                                <span className={"ml-5 font-medium"}>
                                {UserLevel.map((item, index) => (
                                    item.code === member.level && item.name
                                ))}
                            </span>
                        </div>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Là nhà đầu tư:
                            </span>
                            <span className={"ml-5 font-medium"}>
                                {member.investorStatus === 'TRUE' ? 'Có' : 'Không'}
                            </span>
                        </div>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Văn phòng Đại diện:
                            </span>
                            <span className={"ml-5 font-medium"}>
                                {member.officeStatus === 'TRUE' ? 'Có' : 'Không'}
                            </span>
                        </div>
                        {member.parent &&
                            <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                <span>
                                    Người giới thiệu:
                                </span>
                                <Link to={`/admin/members/detail/${member.parent.id}`}>
                                    <span className={"ml-5 font-medium text-blue-500 text-underline"}>
                                        {member.parent.name}
                                    </span>
                                </Link>
                            </div>
                        }
                        <h3 className={"mt-5 font-medium mb-3 text-green-600"}>Thông tin thanh toán</h3>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Ngân hàng:
                            </span>
                                <span className={"ml-5 font-medium"}>
                                {member.bank_name}
                            </span>
                        </div>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Chủ tài khoản:
                            </span>
                                <span className={"ml-5 font-medium"}>
                                {member.bank_owner}
                            </span>
                        </div>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Số tài khoản:
                            </span>
                                <span className={"ml-5 font-medium"}>
                                {member.bank_account}
                            </span>
                        </div>
                        <div>
                            <span>
                                Chi nhánh:
                            </span>
                                <span className={"ml-5 font-medium"}>
                                {member.bank_brand}
                            </span>
                        </div>
                        <h3 className={"mt-5 font-medium mb-3 text-purple-600"}>Social Network</h3>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Zalo:
                            </span>
                                <span className={"ml-5 font-medium"}>
                                    <a href={`https://zalo.me/${member.zalo}`} target={"_blank"}>{member.zalo}</a>
                            </span>
                        </div>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Facebook:
                            </span>
                                <span className={"ml-5 font-medium"}>
                                    <a href={member.facebook} target={"_blank"}>{member.facebook}</a>
                            </span>
                        </div>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Zalo Group:
                            </span>
                                <span className={"ml-5 font-medium"}>
                                    <a href={member.zaloGroup} target={"_blank"}>{member.zaloGroup}</a>
                            </span>
                        </div>
                        <div className={"mb-2 border-b border-gray-200 pb-2"}>
                            <span>
                                Youtube:
                            </span>
                                <span className={"ml-5 font-medium"}>
                                    <a href={member.youtube} target={"_blank"}>{member.youtube}</a>
                            </span>
                        </div>
                        <div>
                            <span>
                                Whatsapp:
                            </span>
                                <span className={"ml-5 font-medium"}>
                                    <a href={`https://wa.me/${member.whatsapp}`} target={"_blank"}>{member.whatsapp}</a>
                            </span>
                        </div>
                    </Card>

                </Col>
            </Row>
        </div>
    );
}

export default MemberInfo;
