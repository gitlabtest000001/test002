import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import queryString from 'query-string';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { hideLoader, showLoader } from '../../App/actions';
import { Link } from 'react-router-dom';
import Space from 'antd/es/space';
import {
    checkRole,
    convertPaymentMethod,
    convertStatus, convertUserAffRoles, convertUserLevel,
    formatDate,
    formatDateTime,
    formatVND,
} from '../../../utils/helpers';
import Button from 'antd/es/button';
import { EditTwoTone } from '@ant-design/icons';
import Table from 'antd/es/table';
import Col from 'antd/es/grid/col';
import Row from 'antd/es/grid/Row';
import PlusOutlined from '@ant-design/icons/lib/icons/PlusOutlined';
import { PageHeader, Select } from 'antd';
import { Helmet } from 'react-helmet/es/Helmet';
import Tag from 'antd/es/tag';
import Search from 'antd/es/input/Search';
import { UserAffRoles, UserLevel, UserStatus } from '../../../models/member.model';
import { ExportXLS } from '../../../components/ExportXLS';
import moment from 'moment';

const {Option} = Select;

function Members(props) {
    const dispatch = useDispatch();
    let params = queryString.parse(props.location.search);
    let currentPage = params.page;
    let currentPageSize = params.limit;
    const [results, setResults] = useState();
    const [loading, setLoading] = useState(true);
    const [page, setPage] = useState(currentPage ? currentPage : 1);
    const [pageSize, setPageSize] = useState(currentPageSize ? currentPageSize : 10);
    const [searchQuery, setSearchQuery] = useState();
    const [level, setLevel] = useState('ALL')
    const [investorStatus, setInvestorStatus] = useState('ALL')
    const [affRoles, setAffRoles] = useState('ALL')
    const [status, setStatus] = useState('ALL')

    const [resultExport, setResultExport] = useState([])

    useEffect(() => {
        dispatch(showLoader());
        async function getUsers() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/user`,
                params: {
                    level,
                    status,
                    page,
                    affRoles,
                    investorStatus,
                    limit: pageSize,
                    query: searchQuery,
                    role: JSON.stringify(['member','guest'])
                },
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setResults(response.data)
                    setLoading(false);
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getUsers();
    },[page, pageSize, searchQuery, level, status, investorStatus, affRoles])

    useEffect(() => {
        dispatch(showLoader());
        async function getUsers() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/user`,
                params: {
                    level,
                    status,
                    page: 1,
                    affRoles,
                    investorStatus,
                    limit: 10000000,
                    query: searchQuery,
                    role: JSON.stringify(['member','guest'])
                },
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setResultExport(response.data.users)
                    setLoading(false);
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getUsers();
    },[searchQuery, level, status, investorStatus, affRoles])

    function handleChangePage(page, pageSize) {
        setPage(page);
        setPageSize(pageSize);
    }

    const currentUser = useSelector(state => state.root.currentUser.user)

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: (key, row) => (
                <Space size="middle">
                    {checkRole(currentUser.roles, ['administrator', 'marketer']) &&
                        <Link to={`/admin/members/edit/${row.id}`}>
                            <Button type="primary" size="small" ghost icon={<EditTwoTone />}>
                                Sửa
                            </Button>
                        </Link>
                    }
                </Space>
            )
        },
        {
            title: 'Tên',
            dataIndex: 'name',
            key: 'name',
            render: (text, row) =>
                <>
                    {checkRole(currentUser.roles, ['administrator', 'marketer']) ?
                        <Link to={`/admin/members/detail/${row.id}`}>
                            <span className="text-blue-500">{text}</span>
                        </Link>
                        : <span>{text}</span>
                    }
                </>
        },
        {
            title: 'Số điện thoại',
            dataIndex: 'phone',
            key: 'phone',
        },
        {
            title: 'Email',
            dataIndex: 'email',
            key: 'email',
        },
        {
            title: 'Ví thưởng',
            dataIndex: 'reward_wallet',
            key: 'reward_wallet',
            render: (key,row) => formatVND(row.reward_wallet)
        },
        {
            title: 'Ví hàng',
            dataIndex: 'product_wallet',
            key: 'product_wallet',
            render: (key,row) => formatVND(row.product_wallet)
        },
        {
            title: 'Ví tiền mặt',
            dataIndex: 'cash_wallet',
            key: 'cash_wallet',
            render: (key,row) => formatVND(row.cash_wallet)
        },
        {
            title: 'Cổ phần',
            dataIndex: 'stock',
            key: 'stock',
        },
        {
            title: 'Chức vụ',
            dataIndex: 'affRoles',
            key: 'affRoles',
            render: (key, row) => (
                UserAffRoles.map((item) => (
                    item.code === row.affRoles && item.name
                ))
            ),
        },
        {
            title: 'Cấp bậc',
            dataIndex: 'level',
            key: 'level',
            render: (key, row) => (
                UserLevel.map((item) => (
                    item.code === row.level && item.name
                ))
            ),
        },
        {
            title: 'Là nhà đầu tư',
            dataIndex: 'investorStatus',
            key: 'investorStatus',
            render: (key, row) => (
                row.investorStatus === 'TRUE' ? 'Có' : 'Không'
            ),
        },
        {
            title: 'Văn phòng kinh doanh',
            dataIndex: 'officeStatus',
            key: 'officeStatus',
            render: (key, row) => (
                row.officeStatus === 'TRUE' ? 'Có' : 'Không'
            ),
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            key: 'status',
            align: 'right',
            render: (key, row) => (
                UserStatus.map((item) => (item.code === row.status && item.name))
            ),
        },
    ]

    let exportData = []
    if(resultExport) {
        exportData = resultExport.map(item => {
            item = {
                'Id': item.id,
                'Họ tên': item.name,
                'Số điện thoại': item.phone,
                'Email': item.email,
                'Địa chỉ': item.guestAddress,
                'Chức vụ': convertUserAffRoles(item.affRoles),
                'Cấp bậc': convertUserLevel(item.level),
                'Doanh số': item.sales,
                'Doanh số Affiliate': item.affiliateSales,
                'Ví tiền mặt': item.cash_wallet,
                'Ví tiền hàng': item.product_wallet,
                'Ví tiền thưởng': item.reward_wallet,
                'Số cổ phần': item.stock,
                'TT Thanh toán': 'Ngân hàng: ' + item.bank_name + '/STK: ' + item.bank_account + '/CTK: ' + item.bank_owner + '/Chi nhánh: ' + item.bank_brand,
                'Trạng thái': convertStatus(item.status),
                'Ngày tham gia': formatDateTime(item.createdAt),
            }
            return item;
        })
    }

    return (
        <div>
            <Helmet>
                <title>Tài khoản thành viên</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title="Tài khoản thành viên"
                extra={[
                    <>
                        <Link to={'/admin/members/tree'}>
                            Sơ đồ thành viên
                        </Link>
                        <ExportXLS
                            //csvData={results && results.users}
                            csvData={exportData && exportData}
                            fileName={`Thành viên ${moment().format("DD/MM/YYYY")}`}
                        />
                        {checkRole(currentUser.roles, ['administrator', 'marketer', 'sale']) &&
                            <Link to="/admin/members/add">
                                <Button type="primary" icon={<PlusOutlined />} shape="round">
                                    Tạo tài khoản
                                </Button>
                            </Link>
                        }
                    </>
                ]}
                className="mb-3"
            />
            <Row gutter={[20, 10]} className={"mb-3"}>
                <Col xs={24} lg={4} md={12}>
                    <Search
                        placeholder="Tìm kiếm..."
                        className={"w-full"}
                        onSearch={(value) => setSearchQuery(value)}
                        allowClear
                    />
                </Col>
                <Col xs={24} lg={4} md={12}>
                    <Select
                        defaultValue="ALL"
                        className={"w-full"}
                        onChange={(value) => setStatus(value) }
                    >
                        <Option value="ALL">Tất cả trạng thái</Option>
                        {UserStatus.map((item) => (
                            <Option value={item.code}>{item.name}</Option>
                        ))}
                    </Select>
                </Col>
                <Col xs={24} lg={4} md={12}>
                    <Select
                        defaultValue="ALL"
                        className={"w-full"}
                        onChange={(value) => setAffRoles(value) }
                    >
                        <Option value="ALL">Tất cả Chức vụ</Option>
                        {UserAffRoles.map((item) => (
                            <Option value={item.code}>{item.name}</Option>
                        ))}
                    </Select>
                </Col>
                <Col xs={24} lg={4} md={12}>
                    <Select
                        defaultValue="ALL"
                        className={"w-full"}
                        onChange={(value) => setLevel(value) }
                    >
                        <Option value="ALL">Tất cả Cấp bậc</Option>
                        {UserLevel.map((item) => (
                            <Option value={item.code}>{item.name}</Option>
                        ))}
                    </Select>
                </Col>
                <Col xs={24} lg={4} md={12}>
                    <Select
                        defaultValue="ALL"
                        className={"w-full"}
                        onChange={(value) => setInvestorStatus(value) }
                    >
                        <Option value="ALL">Tất cả khách hàng</Option>
                        <Option value="TRUE">Là Nhà đầu tư</Option>
                        <Option value="FALSE">Không phải Nhà Đầu tư</Option>
                    </Select>
                </Col>
                <Col xs={24} lg={4} md={6}>
                    <div className={"text-right sm:tex-center"}>
                        <p>Tìm thấy {results && results.count} tài khoản</p>
                    </div>
                </Col>
            </Row>
            <Table
                scroll={{x: '100%'}}
                loading={loading}
                columns={columns}
                dataSource={results && results.users}
                pagination={{
                    current: page,
                    pageSize: pageSize,
                    total: results && results.count,
                    onChange: ((page, pageSize) => handleChangePage(page, pageSize)),
                    position: ['bottomLeft']
                }}
            />
        </div>
    );
}

export default Members;
