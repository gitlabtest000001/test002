import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet/es/Helmet';
import PageHeader from 'antd/es/page-header';
import history from '../../../utils/history';
import Button from 'antd/es/button';
import Tree from 'react-animated-tree'
import Card from 'antd/es/card';
import { useDispatch } from 'react-redux';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { hideLoader, showLoader } from '../../App/actions';
import Loading from '../../../components/Loading';
import { UserLevel } from '../../../models/member.model';

function MemberDetailTree(props) {
    const userId = props.match.params.id;
    const treeStyles = {
        width: '100%',
    }

    const dispatch = useDispatch()
    const [users, setUsers] = useState()

    useEffect(() => {
        dispatch(showLoader());
        async function getUsers() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/user/tree`,
                params: {
                    userId
                },
                headers: {Authorization: `Bearer ${Token}`},
            }).then(function (response) {
                if (response.status === 200) {
                    setUsers(response.data);
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getUsers();
    }, [dispatch, userId])

    const listTree = (arrItem) => {
        return arrItem.map((el) => {
            if(el.children && el.children.length === 0) {
                return <Tree
                    content={el.id + ' - ' + el.name + ' - ' + el.phone + ' (' + el.affRoles + ' - ' + el.level + ')'}
                    canHide
                    onClick={() => (history.push(`/admin/members/detail/${el.id}`))}
                />
            } else {
                return <Tree
                    content={el.id + ' - ' + el.name + ' - ' + el.phone + ' (' + el.affRoles + ' - ' + el.level + ')'}
                    canHide
                    onClick={() => (history.push(`/admin/members/detail/${el.id}`))}
                >
                    {listTree(el.children)}
                </Tree>
            }
        })

    }

    return (
        !users ? <Loading /> :
            <>
                <Helmet>
                    <title>Sơ đồ hệ thống</title>
                </Helmet>
                <PageHeader
                    onBack={() => history.goBack()}
                    ghost={false}
                    title={`Sơ đồ hệ thống`}
                    className="mb-5"
                />
                <Card>
                    {users && users.tree.map((el, index) => (
                        <Tree
                            content={el.id + ' - ' + el.name + ' - ' + el.phone + ' (' + el.affRoles + ' - ' + el.level + ')'}
                            canHide
                            open
                            style={treeStyles}
                            onClick={() => (history.push(`/admin/members/detail/${el.id}`))}
                        >
                            {el && el.children.length > 0 ? listTree(el.children): null}
                        </Tree>
                    ))}

                </Card>
            </>
    );
}

export default MemberDetailTree;
