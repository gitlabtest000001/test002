import React from 'react';
import Form from 'antd/es/form';
import { Select } from 'antd';
import TextArea from 'antd/es/input/TextArea';
import Input from 'antd/es/input';
import Button from 'antd/es/button';
import Card from 'antd/es/card';

const { Option } = Select;

function ExchangeStockForm(props) {
    return (
        <Card size={"small"} title={"Tạo giao dịch Gán cổ phần lấy sản phẩm"}>
            <Form
                layout="vertical"
                name="UserForm"
                onFinish={props.onSubmit}
                initialValues={props.initialValues}
            >
                <Form.Item name="status" label="Trạng thái">
                    <Select
                        style={{width: '100%'}}
                        placeholder="Chọn trạng thái"
                    >
                        {props.type === 'WITHDRAW' ?
                            <Option value='PAID'>Đã Thanh Toán</Option>
                            :
                            <Option value='ACTIVE'>Duyệt</Option>
                        }
                        <Option value='DEACTIVE'>Huỷ bỏ</Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Ghi chú"
                    name="note"
                >
                    <TextArea rows={3} />
                </Form.Item>
                <Form.Item
                    label="Xác nhận mật khẩu quản trị viên"
                    name="password"
                >
                    <Input.Password />
                </Form.Item>
                <Button
                    type="primary"
                    htmlType="submit"
                >
                    Cập nhật
                </Button>
            </Form>
        </Card>
    );
}

export default ExchangeStockForm;
