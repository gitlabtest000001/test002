import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { hideLoader, showLoader } from '../../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';

function ExchangeStock(props) {
    const dispatch = useDispatch();
    const [investors, setInvestors] = useState();

    useEffect(() => {
        dispatch(showLoader());
        async function getUsers() {
            const Token = localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/user`,
                params: {
                    level: 'INVESTOR',
                    status: 'ACTIVE',
                    limit: 1000,
                    role: JSON.stringify(['member','guest'])
                },
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setInvestors(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getUsers();
    },[dispatch])

    return (
        <div>

        </div>
    );
}

export default ExchangeStock;
