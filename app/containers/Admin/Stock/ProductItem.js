import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import Loading from '../../../components/Loading';
import history from '../../../utils/history';

function ProductItem(props) {
    const dispatch = useDispatch();
    const [prices, setPrices] = useState()

    useEffect(() => {
        async function getProductPrices() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/product-price/${props.item.id}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setPrices(response.data)
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getProductPrices();
    },[props.item.id])

    return (
        !props ? <Loading /> :
        <div onClick={() => history.push(`/admin/products/edit/${props.item.id}`)} className={"cursor-pointer"}>
            <div className={"border border-gray-200 rounded-md p-5 bg-white group"}>
                <h3 className={"group-hover:text-green-600 mb-5 font-bold"}>{props.item.name}</h3>
                {prices && prices.length > 0 ?
                    <div>
                        {prices.map((item, index) => (
                            <div className={"flex items-center justify-between border-b border-gray-100 pb-2 mb-2"}>
                                <p>{item.name}</p>
                                <p className={`${Number(item.instock) <= 0 && 'text-red-600 font-bold'}`}>{item.instock} - {item.unit}</p>
                            </div>
                        ))}
                    </div>
                    :
                    'Chưa có giá và số lượng'
                }
            </div>
        </div>
    );
}

export default ProductItem;
