import React, { useEffect, useState } from 'react';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { useDispatch } from 'react-redux';
import Loading from '../../../components/Loading';
import { Helmet } from 'react-helmet/es/Helmet';
import PageHeader from 'antd/es/page-header';
import ProductItem from './ProductItem';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import Search from 'antd/es/input/Search';
import Select from 'antd/es/select';
import Pagination from 'antd/es/pagination';

const {Option} = Select;

function StockManagement(props) {
    const dispatch = useDispatch();
    const [products, setProducts] = useState()

    const [limit, setLimit] = useState(16);
    const [page, setPage] = useState(1);
    const [status, setStatus] = useState('ALL');
    const [catId, setCatId] = useState('ALL');
    const [search, setSearch] = useState(null);
    const [categoryList, setCategoryList] = useState();

    useEffect(() => {
        dispatch(showLoader());
        async function getCategory() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/category?limit=10000`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setCategoryList(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getCategory();
    },[])

    useEffect(() => {
        dispatch(showLoader());
        async function getProducts() {
            const Token = await localStorage.getItem('dvg_manager_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/manager/product`,
                params: {
                    page, limit, query: search, status, category: catId === 'ALL' ? 'ALL' : [catId]
                },
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setProducts(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getProducts();
    },[dispatch, limit, page, search, status, catId])

    console.log(products);

    function handleChangePage(page, pageSize) {
        setPage(page);
        setLimit(pageSize);
    }

    return (
        !products ? <Loading /> :
        <div>
            <Helmet>
                <title>Quản lý kho hàng</title>
            </Helmet>
            <PageHeader
                title={"Quản lý kho hàng"}
                ghost={false}
                className={"mb-5"}
            />
            <div className={"mb-5"}>
                <Row gutter={[16, 16]}>
                    <Col xs={24} lg={8} md={8}>
                        <Search
                            placeholder="Tìm kiếm..."
                            className={"w-full mt-2"}
                            onSearch={(value) => setSearch(value)}
                            allowClear
                        />
                    </Col>
                    <Col xs={24} lg={8} md={8}>
                        {!categoryList ? <Loading/> :
                            <Select
                                defaultValue="ALL"
                                className={"w-full mt-2"}
                                onChange={(value) => setCatId(value)}
                            >
                                <Option value="ALL">Tất cả danh mục</Option>
                                {categoryList && categoryList.map((item, index) => (
                                    <Option value={item.id}>{item.name}</Option>
                                ))}
                            </Select>
                        }
                    </Col>
                    <Col xs={24} lg={8} md={8}>
                        <Select
                            defaultValue="ALL"
                            className={"w-full mt-2"}
                            onChange={(value) => setStatus(value) }
                        >
                            <Option value="ALL">Tất cả trạng thái</Option>
                            <Option value="ACTIVE">Hiển thị</Option>
                            <Option value="DEACTIVE">Đang ẩn</Option>
                        </Select>
                    </Col>
                </Row>
            </div>
            {products && products.count > 0 ?
                <div>
                    <Row gutter={[16, 16]} className={"mb-5"}>
                        {products && products.list.map((item, index) => (
                            <Col xs={24} lg={6} md={8}>
                                <ProductItem item={item}/>
                            </Col>
                        ))}
                    </Row>
                    <Pagination
                        defaultCurrent={page}
                        defaultPageSize={limit}
                        current={page}
                        onChange={handleChangePage}
                        pageSize={limit}
                        hideOnSinglePage
                        total={products && products.count}/>
                </div>
                :
                <div>
                    Chưa có sản phẩm
                </div>
            }
        </div>
    );
}

export default StockManagement;
