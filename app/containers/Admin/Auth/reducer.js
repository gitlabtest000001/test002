import produce from 'immer/dist/immer';
import * as constant from './constants';
import {ErrorMessage, SuccessMessage} from "../../../components/Message";

export const initialState = {
    user: null,
    fetching: false,
}

const AdminAuth = (state = initialState, action) =>
    produce(state, draft => {
        switch (action.type) {
            case constant.ADMIN_LOGIN: {
                return {
                    ...state,
                    fetching: true,
                };
            }
            case constant.ADMIN_LOGIN_SUCCESS: {
                SuccessMessage({message: 'Đăng nhập thành công'})
                return {
                    ...state,
                    fetching: false,
                };
            }
            case constant.ADMIN_LOGIN_FAILED: {
                const { error } = action.payload;
                ErrorMessage(error);
                return {
                    ...state,
                    fetching: false,
                    error,
                };
            }
            case constant.ADMIN_UPDATE_ACCOUNT: {
                return {
                    ...state,
                };
            }
            case constant.ADMIN_UPDATE_ACCOUNT_SUCCESS: {
                const {data} = action.payload;
                SuccessMessage({message: 'Cập nhật thành công'})
                return {
                    ...state,
                    user: {data: data},
                };
            }
            case constant.ADMIN_UPDATE_ACCOUNT_FAILED: {
                const { error } = action.payload;
                ErrorMessage(error);
                return {
                    ...state,
                    error,
                };
            }
            case constant.ADMIN_FORGOT_PASSWORD: {
                const { data } = action.payload;
                return {
                    ...state,
                    user: data,
                };
            }
            case constant.ADMIN_FORGOT_PASSWORD_SUCCESS: {
                const { data } = action.payload;
                SuccessMessage(data);
                return {
                    ...state,
                    user: data,
                    success: true,
                };
            }
            case constant.ADMIN_FORGOT_PASSWORD_FAILED: {
                const { error } = action.payload;
                ErrorMessage(error);
                return {
                    ...state,
                    error,
                    success: false,
                };
            }
            case constant.ADMIN_RESET_PASSWORD: {
                const { data } = action.payload;
                return {
                    ...state,
                    user: data,
                };
            }
            case constant.ADMIN_RESET_PASSWORD_SUCCESS: {
                const { data } = action.payload;
                SuccessMessage({message: 'Đã đặt mật khẩu mới, vui lòng đăng nhập lại'})
                return {
                    ...state,
                    success: true,
                };
            }
            case constant.ADMIN_RESET_PASSWORD_FAILED: {
                const { error } = action.payload;
                ErrorMessage(error);
                return {
                    ...state,
                    error,
                    success: false,
                };
            }
            case constant.ADMIN_GET_ME: {
                return {
                    ...state,
                    fetching: true,
                };
            }
            case constant.ADMIN_GET_ME_SUCCESS: {
                const { data } = action.payload;
                return {
                    ...state,
                    fetching: false,
                    user: data,
                };
            }
            case constant.ADMIN_GET_ME_FAILED: {
                const { error } = action.payload;
                ErrorMessage(error);
                return {
                    ...state,
                    fetching: false,
                    error,
                };
            }
            case constant.ADMIN_LOGOUT: {
                return {
                    ...state,
                };
            }
            case constant.ADMIN_LOGOUT_SUCCESS: {
                const { data } = action.payload;
                SuccessMessage(data)
                return {
                    ...state,
                    user: null,
                };
            }
            case constant.ADMIN_LOGOUT_FAILED: {
                const { error } = action.payload;
                ErrorMessage(error);
                return {
                    ...state,
                    error,
                };
            }
            case constant.ADMIN_USER_DATA: {
                return {
                    ...state,
                    user: action.user,
                    fetching: false,
                };
            }
            default:
                return state;
        }
    });

export default AdminAuth;
