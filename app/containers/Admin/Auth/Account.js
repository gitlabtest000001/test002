import React, {useState} from 'react';
import {PageHeader} from "antd";
import {Helmet} from "react-helmet/es/Helmet";
import Row from "antd/es/grid/row";
import Col from "antd/es/grid/col";
import Card from "antd/es/card";
import Form from "antd/es/form";
import Input from "antd/es/input";
import SingleUpload from "../../../components/SingleUpload";
import Button from "antd/es/button";
import {CheckOutlined} from "@ant-design/icons";
import {useDispatch, useSelector} from "react-redux";
import {updateAccount} from "./actions";
import Loading from "../../../components/Loading";

AccountSettings.propTypes = {

};

function AccountSettings(props) {
    const dispatch = useDispatch();
    const userInfo = useSelector(state => state.root.AdminAuth.user);
    const loading = useSelector(state => state.root.AdminAuth.fetching);

    if (userInfo) {
        var initialValues = {
            id: userInfo.data.id,
            name: userInfo.data.name,
            phone: userInfo.data.phone,
            email: userInfo.data.email,
            password: userInfo.data.password,
        }
    }

    function handleSubmit(values) {
        dispatch(updateAccount(initialValues.id, values.name, values.phone, values.email, values.password))
    }

    return ( !userInfo ? <Loading /> :
        <>
            <Helmet>
                <title>Cài đặt tài khoản</title>
            </Helmet>
            <PageHeader
                ghost={false}
                title="Cài đặt tài khoản"
                className={"mb-5"}
            />
            <Form
                layout="vertical"
                name="UserForm"
                onFinish={handleSubmit}
                initialValues={userInfo && initialValues}
            >
                <Row gutter={[16, 16]}>
                    <Col xs={24} lg={16} md={16}>
                        <Card title="Thông tin tài khoản">
                            <Row gutter={[16]}>
                                <Col xs={24} lg={12} md={12}>
                                    <Form.Item
                                        label="Họ và tên"
                                        name="name"
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng nhập tên'},
                                        ]}
                                    >
                                        <Input
                                            size="large"
                                        />
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12} md={12}>
                                    <Form.Item
                                        label="Email"
                                        name="email"
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng nhập email'
                                            },
                                            {
                                                type: 'email',
                                                message: 'Vui lòng nhập đúng định dạng Email!',
                                            },
                                        ]}
                                    >
                                        <Input
                                            size="large"
                                        />
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12} md={12}>
                                    <Form.Item
                                        label="Số điện thoại"
                                        name="phone"
                                        rules={[
                                            {
                                                required: true,
                                                message: 'Vui lòng nhập tên'},
                                        ]}
                                    >
                                        <Input
                                            size="large"
                                        />
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12} md={12}>
                                    <Form.Item
                                        label="Mật khẩu"
                                        name="password"
                                        extra="Chỉ nhập vào nếu muốn thay đổi mật khẩu."
                                    >
                                        <Input
                                            size="large"
                                        />
                                    </Form.Item>
                                </Col>
                                <Col xs={24} lg={12} md={12}>
                                    <Button
                                        type="primary"
                                        htmlType="submit"
                                        icon={<CheckOutlined />}
                                    >
                                        Lưu thay đổi
                                    </Button>
                                </Col>
                            </Row>
                        </Card>
                    </Col>
                </Row>
            </Form>
        </>
    );
}

export default AccountSettings;
