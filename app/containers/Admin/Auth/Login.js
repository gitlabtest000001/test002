import React from 'react';
import {Helmet} from "react-helmet/es/Helmet";
import {useDispatch} from "react-redux";
import Form from "antd/es/form";
import Input from "antd/es/input";
import Button from "antd/es/button";
import {Link} from "react-router-dom";
import {adminLogin} from "./actions";

Login.propTypes = {

};

function Login(props) {
    const dispatch = useDispatch()

    function handleSubmit(values) {
        dispatch(adminLogin(values.username, values.password))
    }

    return (
        <div>
            <Helmet>
                <title>Quản trị - Đăng nhập</title>
            </Helmet>
            <Form
                layout="vertical"
                name="login"
                onFinish={handleSubmit}
            >
                <Form.Item
                    label="Email / Số điện thoại"
                    name="username"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập email'
                        },
                    ]}
                >
                    <Input
                        size="large"
                        type="text"
                    />
                </Form.Item>

                <Form.Item
                    label="Mật khẩu"
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập mật khẩu'
                        }
                    ]}
                >
                    <Input.Password
                        size="large"
                        type="password"
                    />
                </Form.Item>

                <Button
                    type="danger"
                    size="large"
                    htmlType="submit"
                    block
                    className="bg-red-700 hover:bg-red-800 border-red-700 hover:border-red-800"
                >
                    Đăng nhập
                </Button>

                <div className="text-center mt-5">
                    <Link to="/admin/forgot-pass">
                        Quên mật khẩu?
                    </Link>
                </div>

            </Form>
        </div>
    );
}

export default Login;
