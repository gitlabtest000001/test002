import * as constants from './constants';

export const adminLogin = (username, password) => ({
    type: constants.ADMIN_LOGIN,
    payload: {
        username,
        password,
    },
});

export const adminLoginSuccess = data => ({
    type: constants.ADMIN_LOGIN_SUCCESS,
    payload: {
        data,
    },
});

export const adminLoginFailed = error => ({
    type: constants.ADMIN_LOGIN_FAILED,
    payload: {
        error,
    },
});

export const adminLogout = () => ({
    type: constants.ADMIN_LOGOUT,
});
export const adminLogoutSuccess = data => ({
    type: constants.ADMIN_LOGOUT_SUCCESS,
    payload: {
        data
    },
});
export const adminLogoutFailed = error => ({
    type: constants.ADMIN_LOGOUT_FAILED,
    payload: {
        error
    },
});

export const adminForgotPass = email => ({
    type: constants.ADMIN_FORGOT_PASSWORD,
    payload: {
        email,
    },
});
export const adminForgotPasswordSuccess = data => ({
    type: constants.ADMIN_FORGOT_PASSWORD_SUCCESS,
    payload: {
        data,
    },
});
export const adminForgotPasswordFailed = error => ({
    type: constants.ADMIN_FORGOT_PASSWORD_FAILED,
    payload: {
        error,
    },
});

export const adminResetPassword = (email, resetCode, password, passwordAgain) => ({
    type: constants.ADMIN_RESET_PASSWORD,
    payload: {
        email, resetCode, password, passwordAgain
    },
});

export const adminResetPasswordSuccess = data => ({
    type: constants.ADMIN_RESET_PASSWORD_SUCCESS,
    payload: {
        data,
    },
});

export const adminResetPasswordFailed = error => ({
    type: constants.ADMIN_RESET_PASSWORD_FAILED,
    payload: {
        error,
    },
});

export const GetMe = (token) => ({
    type: constants.ADMIN_GET_ME,
    payload: {
        token
    },
});
export const GetMeSuccess = data => ({
    type: constants.ADMIN_GET_ME_SUCCESS,
    payload: {
        data
    },
});
export const GetMeFailed = error => ({
    type: constants.ADMIN_GET_ME_FAILED,
    payload: {
        error
    },
});

export const LoadDataAction = user => ({
    type: constants.ADMIN_USER_DATA,
    user,
});

export const updateAccount = (id, name, phone, email, password ) => ({
    type: constants.ADMIN_UPDATE_ACCOUNT,
    payload: {
        id,
        body: {name, phone, email, password}
    },
});

export const updateAccountSuccess = data => ({
    type: constants.ADMIN_UPDATE_ACCOUNT_SUCCESS,
    payload: {
        data,
    },
});

export const updateAccountFailed = error => ({
    type: constants.ADMIN_UPDATE_ACCOUNT_FAILED,
    payload: {
        error,
    },
});
