import axios from 'axios';
import {all, call, delay, put, takeLatest} from "@redux-saga/core/effects";
import {AppConfig} from "../../../appConfig";
import history from "../../../utils/history";
import {
    adminForgotPasswordFailed,
    adminForgotPasswordSuccess,
    GetMeFailed,
    GetMeSuccess,
    adminLoginFailed,
    adminLoginSuccess,
    adminLogoutFailed,
    adminLogoutSuccess,
    adminResetPasswordFailed,
    adminResetPasswordSuccess, updateAccountSuccess, updateAccountFailed
} from "./actions";
import {hideLoader, showLoader} from "../../App/actions";

export const adminLoginCall = payload =>
    axios
        .post(`${AppConfig.apiUrl}/auth/manager/login`, payload)
        .then(response => ({response}))
        .catch(error => ({error}));

function storeToken(token) {
    try {
        localStorage.setItem('dvg_manager_token', token)
    } catch (e) {
        console.log(e)
    }
}

function removeToken() {
    try {
        localStorage.removeItem('dvg_manager_token')
    } catch (e) {
        console.log(e)
    }
}

async function getMe(payload) {
    return await axios.get(
        `${AppConfig.apiUrl}/auth/manager/me`,
        {headers: {Authorization: `Bearer ${payload}`}},
    );
}

function* Login(action) {
    yield put(showLoader());
    const {response, error} = yield call(adminLoginCall, action.payload);
    if (response) {
        yield call(storeToken, response.data.accessToken);
        const userData = yield call(getMe, response.data.accessToken);
        yield put(adminLoginSuccess(userData));
        yield delay(1500);
        history.push('/admin/dashboard');
    } else {
        yield put(adminLoginFailed(error.response.data));
    }
    yield put(hideLoader());
}

async function getMeCall(payload) {
    return axios({
        method: 'GET',
        url: `${AppConfig.apiUrl}/auth/manager/me`,
        headers: {Authorization: `Bearer ${payload.token}`},
    });
}

function* getMeData(action) {
    try {
        const response = yield call(getMeCall, action.payload);
        const {data} = response;
        yield put(GetMeSuccess(data));
    } catch (e) {
        yield put(GetMeFailed(error.response))
    }
}

function* logout() {
    yield put(showLoader());
    try {
        yield call(removeToken);
        yield put(adminLogoutSuccess({message: 'Đã đăng xuất khỏi hệ thống!'}));
        yield delay(1000);
        history.push('/admin/login')
    } catch (e) {
        yield put(adminLogoutFailed({message: 'Lỗi'}))
    }
    yield put(hideLoader());
}

// forgot password
export const forgotPasswordCall = payload =>
    axios
        .post(`${AppConfig.apiUrl}/member/managerforgot`, payload)
        .then(response => ({response}))
        .catch(error => ({error}));

function* forgotPassword(action) {
    yield put(showLoader());
    const {response, error} = yield call(forgotPasswordCall, action.payload);
    if (response) {
        localStorage.setItem('rsem', action.payload.email)
        yield put(adminForgotPasswordSuccess(response.data));
        yield delay(1000);
        history.push('/admin/resetpass')
    } else {
        yield put(adminForgotPasswordFailed(error.response.data));
    }
    yield put(hideLoader());
}

// forgot password
export const resetPasswordCall = payload =>
    axios
        .post(`${AppConfig.apiUrl}/member/managerreset`,
            {email: payload.email, resetCode: payload.resetCode, password: payload.password, passwordAgain: payload.passwordAgain}
            )
        .then(response => ({response}))
        .catch(error => ({error}));

function* resetPassword(action) {
    yield put(showLoader());
    const {response, error} = yield call(resetPasswordCall, action.payload);
    if (response) {
        yield put(adminResetPasswordSuccess(response.data));
        yield delay(1000);
        history.push('/admin/login')
    } else {
        yield put(adminResetPasswordFailed(error.response.data));
    }
    yield put(hideLoader());
}

// update account

const updateAccountCall = payload => {
    const Token = localStorage.getItem('dvg_manager_token');
    return axios.put(
        `${AppConfig.apiUrl}/manager/employee/me`,
        payload.body,
        {headers: {Authorization: `Bearer ${Token}`}}
    ).then(response => ({response})).catch(error => ({error}));
}

function* updateAccount(action) {
    yield put(showLoader());
    const {response, error} = yield call(updateAccountCall, action.payload);
    if (response) {
        yield put(updateAccountSuccess(response.data));
    } else {
        yield put(updateAccountFailed(error.response));
    }
    yield put(hideLoader());
}

export function* AdminAuthSagas() {
    yield all([
        takeLatest('ADMIN_LOGIN', Login),
        takeLatest('ADMIN_GET_ME', getMeData),
        takeLatest('ADMIN_LOGOUT', logout),
        takeLatest('ADMIN_FORGOT_PASSWORD', forgotPassword),
        takeLatest('ADMIN_RESET_PASSWORD', resetPassword),
        takeLatest('ADMIN_UPDATE_ACCOUNT', updateAccount),
    ])
}
