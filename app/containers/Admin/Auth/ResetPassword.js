import React from 'react';
import {Helmet} from "react-helmet/es/Helmet";
import {useDispatch} from "react-redux";
import Form from "antd/es/form";
import Input from "antd/es/input";
import Button from "antd/es/button";
import {adminResetPassword} from "./actions";

ResetPassword.propTypes = {

};

function ResetPassword(props) {
    const resetEmail = localStorage.getItem('rsem');
    const dispatch = useDispatch()

    function handleSubmit(values) {
        dispatch(adminResetPassword(resetEmail, values.resetCode, values.password, values.passwordAgain))
    }

    return (
        <div>
            <Helmet>
                <title>Quản trị - Đặt mật khẩu mới</title>
            </Helmet>
            <Form
                layout="vertical"
                name="resetPassword"
                onFinish={handleSubmit}
            >
                <Form.Item
                    label="Mã xác thực từ Email"
                    name="resetCode"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập mã xác thực từ email'
                        }
                    ]}
                >
                    <Input
                        size="large"
                    />
                </Form.Item>

                <Form.Item
                    label="Mật khẩu mới"
                    name="password"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập mật khẩu'
                        }
                    ]}
                >
                    <Input.Password
                        size="large"
                        type="password"
                    />
                </Form.Item>

                <Form.Item
                    label="Nhập lại mật khẩu mới"
                    name="passwordAgain"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập mật khẩu'
                        }
                    ]}
                >
                    <Input.Password
                        size="large"
                        type="password"
                    />
                </Form.Item>

                <Button
                    type="danger"
                    size="large"
                    htmlType="submit"
                    block
                    className="bg-red-700 hover:bg-red-800 border-red-700 hover:border-red-800"
                >
                    Xác nhận
                </Button>

            </Form>
        </div>
    );
}

export default ResetPassword;
