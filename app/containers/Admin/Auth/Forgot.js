import React from 'react';
import {Helmet} from "react-helmet/es/Helmet";
import {useDispatch} from "react-redux";
import {adminForgotPass} from "./actions";
import Form from "antd/es/form";
import Input from "antd/es/input";
import Button from "antd/es/button";
import {Link} from "react-router-dom";

Forgot.propTypes = {

};

function Forgot(props) {
    const dispatch = useDispatch();

    function handleSubmit(values) {
        dispatch(adminForgotPass(values.email))
    }

    return (
        <div>
            <Helmet>
                <title>Quản trị - Quên mật khẩu</title>
            </Helmet>
            <Form
                layout="vertical"
                name="forgot"
                onFinish={handleSubmit}
            >
                <Form.Item
                    label="Địa chỉ Email"
                    name="email"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập email'},
                        {
                            type: 'email',
                            message: 'Vui lòng nhập đúng định dạng Email!',
                        },
                    ]}
                >
                    <Input
                        size="large"
                        type="text"
                    />
                </Form.Item>

                <Button
                    type="danger"
                    size="large"
                    htmlType="submit"
                    block
                    className="bg-red-700 hover:bg-red-800 border-red-700 hover:border-red-800"
                >
                    Xác nhận
                </Button>

                <div className="text-center mt-5">
                    <Link to="/admin/login">
                        Trở về trang đăng nhập
                    </Link>
                </div>

            </Form>
        </div>
    );
}

export default Forgot;
