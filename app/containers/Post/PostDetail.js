import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { hideLoader, showLoader } from '../App/actions';
import axios from 'axios';
import { AppConfig } from '../../appConfig';
import Loading from '../../components/Loading';
import { Link } from 'react-router-dom';
import { IoAdd, IoChevronForward, IoHome, IoRemove, IoRemoveCircleOutline } from 'react-icons/all';
import Row from 'antd/es/grid/row';
import Col from 'antd/es/grid/col';
import SliderWithThumbnail from '../../components/SliderWithThumbnail';
import { formatDateTime, formatVND } from '../../utils/helpers';
import { InputNumber, Space } from 'antd';
import Button from 'antd/es/button';
import { IoArrowBack, IoCart } from 'react-icons/io5';
import { Tab, TabList, TabPanel, Tabs } from 'react-tabs';
import ProductCategoryWidget from '../HomePage/Widgets/ProductCategory';
import ProductHotWidget from '../HomePage/Widgets/ProductHot';
import { ClockCircleOutlined } from '@ant-design/icons';
import { Helmet } from 'react-helmet/es/Helmet';
import Affix from 'antd/es/affix';
import history from '../../utils/history';
import { Desktop, TabletAndBelow } from '../../utils/responsive';
import ProductItem from '../../components/ProductItem';
import NewsItem from '../../components/NewsItem';
import { useCookies } from 'react-cookie';
import { UpdateRefInfo } from '../App/actions/RefInfo';
import queryString from 'query-string';

function PostDetail(props) {
    const dispatch = useDispatch();
    const slug = props.match.params.slug;
    const [result, setResult] = useState();
    const params = queryString.parse(props.location.search)
    const [cookies, setCookie, removeCookie] = useCookies();

    useEffect(() => {
        if (params && params.ref) {
            async function getRefInfo() {
                axios({
                    method: 'get',
                    url: `${AppConfig.apiUrl}/user/refinfo/${params.ref}`,
                }).then(function (response) {
                    if (response.status === 200) {
                        dispatch(UpdateRefInfo({ref: params.ref, invitee: response.data}))
                        setCookie('dvg_ref', params.ref, {maxAge: 3600})
                        setCookie('dvg_invitee', response.data, {maxAge: 3600})
                    }
                }).catch(function(error){
                    console.log(error);
                    removeCookie('dvg_ref')
                    removeCookie('dvg_invitee')
                })
            }
            getRefInfo();
        }
    }, [params.ref])

    useEffect(() => {
        dispatch(showLoader());
        async function getProducts() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/post/${slug}`,
            }).then(function (response) {
                if (response.status === 200) {
                    setResult(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
                history.push('/404')
                dispatch(hideLoader());
            })
        }
        getProducts();
    },[slug])

    console.log(result);

    return (
        !result ? <Loading /> :
            <>
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>{result.post && result.post.seoTitle}</title>
                    <meta property="og:type" content="website" />
                    <meta name="description" content={result.post && result.post.seoDescription ? result.post.seoDescription : result.post && result.post.description} />
                    <meta property="og:title" content={result.post && result.post.seoTitle ? result.post.seoTitle : result.post && result.post.name} />
                    <meta property="og:description" content={result.post && result.post.seoDescription ? result.post.seoDescription : result.post && result.post.description} />
                    <meta property="og:keyword" content={result.post && result.post.seoKeyword ? result.post.seoKeyword : result.post && result.post.name} />
                    <meta property="og:image" content={result.post && result.post.seoImage ? AppConfig.apiUrl+result.post.seoImage : result.post && result.post.featureImage && AppConfig.apiUrl+result.post.featureImage} />
                </Helmet>
                <TabletAndBelow>
                    <Affix offsetTop={0}>
                        <div className={"bg-white border-b border-gray-100"}>
                            <div className={"h-14 flex items-center justify-between mx-3 text-center"}>
                                <IoArrowBack size={24} onClick={() => history.goBack()}/>
                                <span className={"oneLineText w-1/2 font-medium text-lg"}>{result.post && result.post.title}</span>
                                <IoHome size={20} onClick={() => history.push('/')}/>
                            </div>
                        </div>
                    </Affix>
                </TabletAndBelow>
                <div className={"max-w-6xl mx-3 md:mx-auto py-5"}>
                    <Helmet>
                        <title>{result.post && result.post.title}</title>
                    </Helmet>
                    <div className={"mb-5"}>
                        <Link to={'/'}><IoHome className={"-mt-1"}/> Trang chủ</Link> <IoChevronForward className={"-mt-1"} /> {result.post && result.post.categories.map((item, index) => <><Link to={`/chuyen-muc/${item.slug}`}>{item.name}</Link> <IoChevronForward className={"-mt-1"} /> </>)} <span className={"text-green-700"}>{result.post.title}</span>
                    </div>
                    <Row gutter={[32, 32]}>
                        <Col xs={24} lg={18} md={18}>
                            <div className={"border border-gray-200 rounded-md"}>
                                <div className={"pt-5 px-5"}>
                                    <h1 className={"font-bold text-2xl mb-1"}>{result.post.title}</h1>
                                    <p className={"mb-5 text-gray-500"}>
                                        <ClockCircleOutlined /> {formatDateTime(result.post.createdAt)}
                                    </p>
                                    {result.post.featureImage &&
                                        <img src={AppConfig.apiUrl+result.post.featureImage} alt={result.post.title} className={"w-full mb-5"}/>
                                    }
                                    <div className={"main-content"} dangerouslySetInnerHTML={{__html: result.post.content}}/>
                                </div>
                            </div>

                            {result && result.relatedPosts && result.relatedPosts.length > 0 &&
                                <div className={"mt-10"}>
                                    <h2 className={"font-bold text-lg mb-3 text-gray-600"}>Tin tức liên quan</h2>
                                    <Row gutter={[32, 32]}>
                                        {result.relatedPosts.map((item, index) => (
                                            <Col xs={24} lg={8} md={8}>
                                                <NewsItem item={item} key={index} />
                                            </Col>
                                        ))
                                        }
                                    </Row>
                                </div>
                            }
                        </Col>
                        <Desktop>
                            <Col xs={24} lg={6} md={6}>
                                <ProductCategoryWidget />
                                <ProductHotWidget />
                            </Col>
                        </Desktop>
                    </Row>
                </div>
            </>

    );
}

export default PostDetail;
