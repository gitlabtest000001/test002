import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { hideLoader, showLoader } from '../App/actions';
import axios from 'axios';
import { AppConfig } from '../../appConfig';
import Loading from '../../components/Loading';
import Row from 'antd/es/grid/row';
import Col from 'antd/es/grid/col';
import ProductCategoryWidget from '../HomePage/Widgets/ProductCategory';
import ProductHotWidget from '../HomePage/Widgets/ProductHot';
import { Link } from 'react-router-dom';
import ProductList from '../Product/Category/ProductList';
import HorizontalNewsItem from '../../components/HorizontalNewsItem';
import PostCategoryWidget from '../HomePage/Widgets/PostCategory';
import { IoChevronForward, IoHome } from 'react-icons/all';
import queryString from 'query-string';
import Pagination from 'antd/es/pagination';
import history from '../../utils/history';
import { Helmet } from 'react-helmet/es/Helmet';
import Affix from 'antd/es/affix';
import { IoArrowBack, IoBanOutline } from 'react-icons/io5';
import { Desktop, TabletAndBelow } from '../../utils/responsive';

function AllPosts(props) {
    const dispatch = useDispatch();
    const [result, setResult] = useState();
    let searchQuery = queryString.parse(props.location.search);
    const [page, setPage] = useState(searchQuery && searchQuery.page ? searchQuery.page : 1);

    useEffect(() => {
        dispatch(showLoader());
        async function getCategory() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/post`,
                params: {
                    catId: -1,
                    limit: 10,
                    page,
                    status: 'ACTIVE',
                    userOnly: 'FALSE'
                }
            }).then(function (response) {
                if (response.status === 200) {
                    setResult(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
                history.push('/404')
                dispatch(hideLoader());
            })
        }
        getCategory();
    },[page])

    const handleChangePage = page => {
        history.push(`/tin-tuc?page=${page}`)
        setPage(page)
        window.scrollTo({ top: 0, behavior: 'smooth' })
    };

    return (
        !result ? <Loading /> :
            <>
                <TabletAndBelow>
                    <Affix offsetTop={0}>
                        <div className={"bg-white border-b border-gray-100"}>
                            <div className={"h-14 flex items-center justify-between mx-3 text-center"}>
                                <IoArrowBack size={24} onClick={() => history.goBack()}/>
                                <span className={"oneLineText w-1/2 font-medium text-lg"}>Tin tức</span>
                                <IoHome size={20} onClick={() => history.push('/')}/>
                            </div>
                        </div>
                    </Affix>
                </TabletAndBelow>

                <div className={"max-w-6xl mx-auto py-5 px-3"}>
                    <Row gutter={[32, 32]}>
                        <Col xs={24} lg={6} md={6}>
                            <PostCategoryWidget />
                            <Desktop>
                                <ProductCategoryWidget />
                                <ProductHotWidget />
                            </Desktop>
                        </Col>
                        <Col xs={24} lg={18} md={18}>
                            <div className={"hidden md:block"}>
                                <div className={"mb-3"}>
                                    <Link to={'/'}><IoHome className={"-mt-1"}/> Trang chủ</Link> <IoChevronForward className={"-mt-1"} /> <span className={"text-green-700"}>Tin tức</span>
                                </div>
                                <div className={"flex items-center justify-between mb-5"}>
                                    <h1 className={"font-bold text-2xl text-gray-700"}>Tin tức</h1>
                                </div>
                            </div>

                            {result && result.count > 0 ?
                                <div>
                                    {result.posts.map((item, index) => (
                                        <HorizontalNewsItem item={item} key={index}/>
                                    ))}
                                    <Pagination
                                        defaultCurrent={page}
                                        defaultPageSize={10}
                                        current={page}
                                        onChange={handleChangePage}
                                        pageSize={10}
                                        hideOnSinglePage
                                        total={result && result.count}/>
                                </div>
                                :
                                <div className={"text-center"}>
                                    <p><IoBanOutline className={"text-red-50"} size={100} /></p>
                                    <p>Không tìm thấy</p>
                                </div>
                            }
                        </Col>
                    </Row>

                </div>
            </>

    );
}

export default AllPosts;
