import React from 'react';
import PropTypes from 'prop-types';
import {Helmet} from "react-helmet/es/Helmet";
import NotFoundImage from '../../images/Error404.png';
import Typography from "antd/es/typography";
import Button from "antd/es/button";
import history from "../../utils/history";
import {ArrowLeftOutlined} from "@ant-design/icons";

const { Title, Text } = Typography;

NotFound.propTypes = {

};

function NotFound(props) {
    return (
        <>
            <Helmet>
                <title>Không tìm thấy</title>
            </Helmet>
            <div className="flex h-auto text-center">
                <div className="m-auto">
                    <img src={NotFoundImage} alt="NotFound" />
                    <Title level={3}>Không tìm thấy</Title>
                    <Text mark>Trang mà bạn truy cập không tồn tại hoặc đã bị ẩn đi</Text>
                    <br/>
                    <Button className="mt-5" icon={<ArrowLeftOutlined />} onClick={() => history.goBack()}>
                        Quay lại
                    </Button>
                </div>
            </div>
        </>
    );
}

export default NotFound;
