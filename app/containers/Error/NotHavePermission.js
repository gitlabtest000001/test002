import React from 'react';
import PropTypes from 'prop-types';
import {Helmet} from "react-helmet/es/Helmet";

NotHavePermission.propTypes = {

};

function NotHavePermission(props) {
    return (
        <>
            <Helmet>
                <title>Hạn chế truy cập</title>
            </Helmet>
            Không có quyền truy cập
        </>
    );
}

export default NotHavePermission;
