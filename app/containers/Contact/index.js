import React, {useState} from 'react';
import Col from 'antd/es/grid/col';
import Row from 'antd/es/grid/Row';
import { IoCall, IoHome, IoMail, IoMap, IoPhoneLandscape } from 'react-icons/all';
import { useDispatch, useSelector } from 'react-redux';
import Loading from '../../components/Loading';
import Iframe from 'react-iframe';
import ContactForm from './ContactForm';
import { Helmet } from 'react-helmet/es/Helmet';
import Affix from 'antd/es/affix';
import { IoArrowBack } from 'react-icons/io5';
import history from '../../utils/history';
import { TabletAndBelow } from '../../utils/responsive';
import { hideLoader, showLoader } from '../App/actions';
import axios from 'axios';
import { AppConfig } from '../../appConfig';
import { ErrorMessage, SuccessMessage } from '../../components/Message';
import Button from 'antd/es/button';

function Contact(props) {
    const dispatch = useDispatch()
    const settings = useSelector(state => state.root.settings.options);
    const currentUser = useSelector(state => state.root.currentUser.user);
    const [showForm, setShowForm] = useState(true)

    console.log(currentUser);

    if (currentUser) {
        var initialValues = {
            name: currentUser && currentUser.name,
            email: currentUser && currentUser.email,
            phone: currentUser && currentUser.phone,
        }
    }

    function handleSend(data) {
        dispatch(showLoader())
        axios.post(
            `${AppConfig.apiUrl}/contact`,
            data,
        ).then(function (response) {
            dispatch(hideLoader())
            setShowForm(false)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    return (
        !settings ? <Loading /> :
            <>
                <TabletAndBelow>
                    <Affix offsetTop={0}>
                        <div className={"bg-white border-b border-gray-100"}>
                            <div className={"h-14 flex items-center justify-between mx-3 text-center"}>
                                <IoArrowBack size={24} onClick={() => history.goBack()}/>
                                <span className={"oneLineText w-1/2 font-medium text-lg"}>Liên hệ</span>
                                <IoHome  size={20} onClick={() => history.push('/')}/>
                            </div>
                        </div>
                    </Affix>
                </TabletAndBelow>
                <div className={"max-w-6xl mx-auto py-5 px-3 md:px-0"}>
                    <Helmet>
                        <title>Liên hệ {settings && settings.website_name}</title>
                    </Helmet>
                    <Row gutter={[32, 32]} className={"mb-10"}>
                        <Col xs={24} lg={12} md={12}>
                            <h1 className={"font-bold text-xl text-gray-700 mb-5"}>Gửi tin nhắn cho chúng
                                tôi</h1>
                                {showForm ?
                                    <ContactForm
                                        initialValues={initialValues}
                                        onSubmit={handleSend}
                                    />
                                :
                                <div>
                                    <div className={"mb-5 border border-green-100 bg-green-50 text-green-800 rounded p-3 bg-opacity-50"}>
                                        <p>Cảm ơn bạn đã gửi lời nhắn tới {settings && settings.website_name}. Bộ phận hỗ trợ sẽ liên hệ với bạn trong thời gian sớm nhất!</p>
                                    </div>
                                    <p>
                                        <Button
                                            type={"primary"}
                                            onClick={() => setShowForm(true)}
                                        >
                                            Gửi lời nhắn khác
                                        </Button>
                                    </p>
                                </div>
                            }

                        </Col>
                        <Col xs={24} lg={12} md={12}>
                            <div className={"flex items-center mb-5"}>
                                <span className="circle bg-green-600 mr-2">
                                    <span>
                                        <IoMap className={"text-white"} size={18}/>
                                    </span>
                                </span>
                                        <div>
                                            <p className={"font-bold text-gray-600"}>Địa chỉ liên hệ</p>
                                            <span>{settings && settings.contact_address}</span>
                                        </div>
                                    </div>
                                    <div className={"flex items-center mb-5"}>
                                <span className="circle bg-green-600 mr-2">
                                    <span>
                                        <IoCall className={"text-white"} size={18}/>
                                    </span>
                                </span>
                                        <div>
                                            <p className={"font-bold text-gray-600"}>Hotline</p>
                                            <span>{settings && settings.contact_hotline}</span>
                                        </div>
                                    </div>
                                    <div className={"flex items-center mb-5"}>
                                <span className="circle bg-green-600 mr-2">
                                    <span>
                                        <IoMail className={"text-white"} size={18}/>
                                    </span>
                                </span>
                                <div>
                                    <p className={"font-bold text-gray-600"}>Email</p>
                                    <span>{settings && settings.contact_email}</span>
                                </div>
                            </div>
                            <div>
                                <Iframe
                                    url={'https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7449.632079202113!2d105.79959200000002!3d21.00001!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x620d323f2193ff12!2zVmnhu4d0IMSQ4bupYyBDb21wbGV4!5e0!3m2!1svi!2sus!4v1647482241436!5m2!1svi!2sus'}
                                    width={'100%'}
                                    height={300}
                                />
                            </div>
                        </Col>
                    </Row>
                </div>
            </>

    );
}

export default Contact;
