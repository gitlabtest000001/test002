import React from 'react';
import Form from 'antd/es/form';
import { useDispatch, useSelector } from 'react-redux';
import Input from 'antd/es/input';
import Button from 'antd/es/button';
import { CheckOutlined } from '@ant-design/icons';

function ContactForm(props) {
    function handleSubmit(values) {
        props.onSubmit(values)
    }

    return (
        <Form
            layout="vertical"
            name="CategoryForm"
            onFinish={handleSubmit}
            initialValues={props.initialValues}
        >
            <Form.Item
                label="Tên"
                name="name"
                rules={[
                    {
                        required: true,
                        message: 'Vui lòng nhập tên'},
                ]}
                className={"mb-3"}
            >
                <Input size={"large"}/>
            </Form.Item>
            <Form.Item
                label="Số điện thoại"
                name="phone"
                rules={[
                    {
                        required: true,
                        message: 'Vui lòng nhập số điện thoại'},
                ]}
                className={"mb-3"}
            >
                <Input size={"large"} />
            </Form.Item>
            <Form.Item
                label="Email"
                name="email"
                className={"mb-3"}
            >
                <Input size={"large"} />
            </Form.Item>
            <Form.Item
                label="Tin nhắn"
                name="message"
            >
                <Input.TextArea  rows={5}/>
            </Form.Item>
            <Button
                type="primary"
                htmlType="submit"
                icon={<CheckOutlined />}
                size={"large"}
                className={"bg-green-600 border-green-600 hover:bg-green-500 rounded-md"}
            >
                Gửi đi
            </Button>
        </Form>
    );
}

export default ContactForm;
