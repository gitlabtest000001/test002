import React, { useEffect, useState } from 'react';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import Loading from '../../components/Loading';
import { hideLoader, showLoader } from '../App/actions';
import axios from 'axios';
import { AppConfig } from '../../appConfig';
import { useDispatch } from 'react-redux';
import ProductCategoryWidget from '../HomePage/Widgets/ProductCategory';
import ProductHotWidget from '../HomePage/Widgets/ProductHot';
import { Helmet } from 'react-helmet/es/Helmet';
import { Link } from 'react-router-dom';
import { IoChevronForward, IoHome } from 'react-icons/all';
import history from '../../utils/history';
import Affix from 'antd/es/affix';
import { IoArrowBack } from 'react-icons/io5';
import Search from 'antd/es/input/Search';
import { Desktop, TabletAndBelow } from '../../utils/responsive';
import { useCookies } from 'react-cookie';
import queryString from 'query-string';
import { UpdateRefInfo } from '../App/actions/RefInfo';

function Page(props) {
    const dispatch = useDispatch()
    const slug = props.match.params.slug;
    const [page, setPage] = useState();
    const params = queryString.parse(props.location.search)
    const [cookies, setCookie, removeCookie] = useCookies();

    useEffect(() => {
        if (params && params.ref) {
            async function getRefInfo() {
                axios({
                    method: 'get',
                    url: `${AppConfig.apiUrl}/user/refinfo/${params.ref}`,
                }).then(function (response) {
                    if (response.status === 200) {
                        dispatch(UpdateRefInfo({ref: params.ref, invitee: response.data}))
                        setCookie('dvg_ref', params.ref, {maxAge: 3600})
                        setCookie('dvg_invitee', response.data, {maxAge: 3600})
                    }
                }).catch(function(error){
                    console.log(error);
                    removeCookie('dvg_ref')
                    removeCookie('dvg_invitee')
                })
            }
            getRefInfo();
        }
    }, [params.ref])

    useEffect(() => {
        dispatch(showLoader());
        async function getPage() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/page/${slug}`,
            }).then(function (response) {
                if (response.status === 200) {
                    setPage(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
                history.push('/404')
                dispatch(hideLoader())
            })
        }
        getPage();
    },[dispatch, slug])

    console.log(page);

    return (
        !page ? <Loading /> :
            <>
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>{page && page.seoTitle}</title>
                    <meta property="og:type" content="website" />
                    <meta name="description" content={page && page.seoDescription ? page.seoDescription : page && page.description} />
                    <meta property="og:title" content={page && page.seoTitle ? page.seoTitle : page && page.name} />
                    <meta property="og:description" content={page && page.seoDescription ? page.seoDescription : page && page.description} />
                    <meta property="og:keyword" content={page && page.seoKeyword ? page.seoKeyword : page && page.name} />
                    <meta property="og:image" content={page && page.seoImage ? AppConfig.apiUrl+page.seoImage : page && page.featureImage && AppConfig.apiUrl+page.featureImage} />
                </Helmet>
                <TabletAndBelow>
                    <Affix offsetTop={0}>
                        <div className={"bg-white border-b border-gray-100"}>
                            <div className={"h-14 flex items-center justify-between mx-3 text-center"}>
                                <IoArrowBack size={24} onClick={() => history.goBack()}/>
                                <span className={"oneLineText w-1/2 font-medium text-lg"}>{page && page.title}</span>
                                <IoHome size={20} onClick={() => history.push('/')}/>
                            </div>
                        </div>
                    </Affix>
                </TabletAndBelow>
                <div className={"max-w-6xl mx-auto py-5"}>
                    <div>
                        <div className={"mb-5 hidden md:block"}>
                            <Link to={'/'}><IoHome className={"-mt-1"}/> Trang chủ</Link> <IoChevronForward className={"-mt-1"} /> <span className={"text-green-700"}>{page && page.title}</span>
                        </div>
                        <Row gutter={[16, 16]}>
                            <Col xs={24} lg={18} md={18}>
                                <div className={"mx-3 md:mx-0"}>
                                    <h1 className={"font-bold text-xl text-gray-700"}>{page && page.title}</h1>
                                    <div className={"mt-5"} dangerouslySetInnerHTML={{__html: page && page.content}}/>
                                </div>

                            </Col>
                            <Desktop>
                                <Col xs={24} lg={6} md={6}>
                                    <ProductCategoryWidget />
                                    <ProductHotWidget />
                                </Col>
                            </Desktop>

                        </Row>
                    </div>
                </div>
            </>
    );
}

export default Page;
