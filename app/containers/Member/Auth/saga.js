import axios from 'axios';
import {all, call, delay, put, takeLatest} from "@redux-saga/core/effects";
import {AppConfig} from "../../../appConfig";
import history from "../../../utils/history";
import {
    userForgotPasswordFailed,
    userForgotPasswordSuccess,
    GetMeFailed,
    GetMeSuccess,
    userLoginFailed,
    userLoginSuccess,
    userLogoutFailed,
    userLogoutSuccess,
    userResetPasswordFailed,
    userResetPasswordSuccess,
    updateAccountSuccess,
    updateAccountFailed,
    GetMe,
    userRegisterSuccess,
    userRegisterFailed,
    GetMeManagerSuccess,
} from './actions';
import {hideLoader, showLoader} from "../../App/actions";
import { SuccessMessage } from '../../../components/Message';

export const userLoginCall = payload =>
    axios
        .post(`${AppConfig.apiUrl}/auth/customer/login`, payload.data)
        .then(response => ({response}))
        .catch(error => ({error}));

function storeToken(token) {
    try {
        localStorage.setItem('dvg_user_token', token)
    } catch (e) {
        console.log(e)
    }
}

function removeToken() {
    try {
        localStorage.removeItem('dvg_user_token')
    } catch (e) {
        console.log(e)
    }
}

async function getMe(payload) {
    return await axios.get(
        `${AppConfig.apiUrl}/auth/customer/me`,
        {headers: {Authorization: `Bearer ${payload}`}},
    );
}

function* Login(action) {
    yield put(showLoader());
    const {response, error} = yield call(userLoginCall, action.payload);
    if (response) {
        yield call(storeToken, response.data.accessToken);
        yield delay(1500);
        yield put(GetMe(response.data.accessToken));
        yield put(userLoginSuccess());
        history.push('/taikhoan');
    } else {
        yield put(userLoginFailed(error.response.data));
    }
    yield put(hideLoader());
}

export const userRegisterCall = payload =>
    axios
        .post(`${AppConfig.apiUrl}/member/register`, payload.data)
        .then(response => ({response}))
        .catch(error => ({error}));

function* Register(action) {
    yield put(showLoader());
    const {response, error} = yield call(userRegisterCall, action.payload);
    if (response) {
        yield put(userRegisterSuccess());
        history.push('/dang-nhap');
    } else {
        yield put(userRegisterFailed(error.response.data));
    }
    yield put(hideLoader());
}

async function getMeCall(payload) {
    return axios({
        method: 'GET',
        url: `${AppConfig.apiUrl}/auth/customer/me`,
        headers: {Authorization: `Bearer ${payload.accessToken}`},
    });
}

function* getMeData(action) {
    try {
        const response = yield call(getMeCall, action.payload);
        const {data} = response;
        yield put(GetMeSuccess(data));
    } catch (e) {
        console.log(e);
    }
}

async function getMeManagerCall(payload) {
    return axios({
        method: 'GET',
        url: `${AppConfig.apiUrl}/auth/manager/me`,
        headers: {Authorization: `Bearer ${payload.accessToken}`},
    });
}

function* getMeManagerData(action) {
    try {
        const response = yield call(getMeManagerCall, action.payload);
        const {data} = response;
        yield put(GetMeManagerSuccess(data));
    } catch (e) {
        console.log(e);
    }
}

function* logout() {
    yield put(showLoader());
    try {
        yield call(removeToken);
        yield put(userLogoutSuccess({message: 'Đã đăng xuất khỏi hệ thống!'}));
        yield delay(1000);
        history.push('/dang-nhap')
    } catch (e) {
        yield put(userLogoutFailed({message: 'Lỗi'}))
    }
    yield put(hideLoader());
}

// forgot password
export const forgotPasswordCall = payload =>
    axios
        .post(`${AppConfig.apiUrl}/member/forgot`, payload)
        .then(response => ({response}))
        .catch(error => ({error}));

function* forgotPassword(action) {
    yield put(showLoader());
    const {response, error} = yield call(forgotPasswordCall, action.payload);
    if (response) {
        localStorage.setItem('rsem', action.payload.email)
        yield put(userForgotPasswordSuccess(response.data));
        yield delay(1000);
        history.push('/resetpass')
    } else {
        yield put(userForgotPasswordFailed(error.response.data));
    }
    yield put(hideLoader());
}

// forgot password
export const resetPasswordCall = payload =>
    axios
        .post(`${AppConfig.apiUrl}/member/reset`,
            {email: payload.email, resetCode: payload.resetCode, password: payload.password, passwordAgain: payload.passwordAgain}
            )
        .then(response => ({response}))
        .catch(error => ({error}));

function* resetPassword(action) {
    yield put(showLoader());
    const {response, error} = yield call(resetPasswordCall, action.payload);
    if (response) {
        console.log(response);
        SuccessMessage({message: 'Đã đặt mật khẩu mới, vui lòng đăng nhập lại'})
        //yield put(userResetPasswordSuccess(response.data));
        yield delay(1000);
        history.push('/dang-nhap')
    } else {
        yield put(userResetPasswordFailed(error.response.data));
    }
    yield put(hideLoader());
}

// update account

const updateAccountCall = payload => {
    const Token = localStorage.getItem('dvg_user_token');
    return axios.put(
        `${AppConfig.apiUrl}/user/me`,
        payload.body,
        {headers: {Authorization: `Bearer ${Token}`}}
    ).then(response => ({response})).catch(error => ({error}));
}

function* updateAccount(action) {
    yield put(showLoader());
    const {response, error} = yield call(updateAccountCall, action.payload);
    if (response) {
        yield put(updateAccountSuccess(response.data));
    } else {
        yield put(updateAccountFailed(error.response));
    }
    yield put(hideLoader());
}

export function* AuthSagas() {
    yield all([
        takeLatest('LOGIN', Login),
        takeLatest('REGISTER', Register),
        takeLatest('GET_ME', getMeData),
        takeLatest('GET_ME_MANAGER', getMeManagerData),
        takeLatest('LOGOUT', logout),
        takeLatest('FORGOT_PASSWORD', forgotPassword),
        takeLatest('RESET_PASSWORD', resetPassword),
        takeLatest('UPDATE_ACCOUNT', updateAccount),
    ])
}
