import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Button, Form, Input } from 'antd';
import { userRegister } from './actions';
import { IoCheckbox, IoSquareOutline } from 'react-icons/all';
import queryString from 'query-string';
import { Helmet } from 'react-helmet/es/Helmet';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { UpdateRefInfo } from '../../App/actions/RefInfo';
import { useCookies } from 'react-cookie';

Register.propTypes = {

};

function Register(props) {
    const params = queryString.parse(props.location.search)
    const [cookies, setCookie, removeCookie] = useCookies();

    useEffect(() => {
        if (params && params.ref) {
            async function getRefInfo() {
                axios({
                    method: 'get',
                    url: `${AppConfig.apiUrl}/user/refinfo/${params.ref}`,
                }).then(function (response) {
                    if (response.status === 200) {
                        dispatch(UpdateRefInfo({ref: params.ref, invitee: response.data}))
                        setCookie('dvg_ref', params.ref, {maxAge: 3600})
                        setCookie('dvg_invitee', response.data, {maxAge: 3600})
                    }
                }).catch(function(error){
                    console.log(error);
                    removeCookie('dvg_ref')
                    removeCookie('dvg_invitee')
                })
            }
            getRefInfo();
        }
    }, [params.ref])

    const dispatch = useDispatch();
    const [form] = Form.useForm();

    const initialValues = {
        referrer: params && params.ref ? params.ref : cookies.dvg_ref
    }

    function handleSubmit(values) {
        const data = {
            password: values.password,
            name: values.name,
            email: values.email,
            phone: values.phone,
            referrer: values.referrer,
        }
        dispatch(userRegister(data));
    }

    return (
        <div>
            <Helmet>
                <title>Tạo tài khoản</title>
            </Helmet>
            <div className={"max-w-md mx-auto px-3 md:px-0 py-10"}>
                <div className={"rounded-md shadow-lg p-5 bg-white border border-gray-200"}>
                    <h1 className={"mb-2 text-xl font-medium text-center"}>
                        Tạo tài khoản
                    </h1>
                    <Form
                        form={form}
                        layout={"vertical"}
                        name={"Register"}
                        onFinish={handleSubmit}
                        initialValues={initialValues}
                    >
                        <Form.Item
                            label="Họ và tên"
                            name="name"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập thông tin'
                                },
                            ]}
                        >
                            <Input
                                placeholder={"Tên của bạn"}
                                size={"large"}
                                className={"rounded h-12"}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Email"
                            name="email"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập thông tin'
                                },
                            ]}
                        >
                            <Input
                                placeholder={"Email"}
                                size={"large"}
                                className={"rounded h-12"}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Số điện thoại"
                            name="phone"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập thông tin'
                                },
                            ]}
                        >
                            <Input
                                placeholder={"Số điện thoại"}
                                size={"large"}
                                className={"rounded h-12"}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Mật khẩu"
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập mật khẩu'
                                },
                            ]}
                        >
                            <Input.Password
                                placeholder={"Mật khẩu"}
                                size={"large"}
                                className={"rounded h-12"}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Mã giới thiệu"
                            name="referrer"
                        >
                            <Input
                                placeholder={"Mã giới thiệu"}
                                size={"large"}
                                className={"rounded h-12"}
                            />
                        </Form.Item>
                        <Button
                            type="primary"
                            size="large"
                            htmlType="submit"
                            block
                            className="rounded border-0 bg-yellow-500 text-white hover:bg-yellow-600 hover:text-white focus:bg-yellow-600 focus:text-white h-12"
                        >
                            Đăng ký
                        </Button>
                        <div
                            className={"text-center mt-5"}
                        >
                            <Link to={"/dang-nhap"} className={"text-orange-400 text-base cursor-pointer hover:text-organge-500"}>
                                Đã có tài khoản? Đăng nhập ngay
                            </Link>
                        </div>
                    </Form>
                </div>

            </div>
        </div>
    );
}

export default Register;
