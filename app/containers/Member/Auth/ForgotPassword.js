import React from 'react';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Button, Form, Input } from 'antd';
import { userForgotPass } from './actions';
import { Helmet } from 'react-helmet/es/Helmet';

ForgotPassword.propTypes = {

};

function ForgotPassword(props) {
    const dispatch = useDispatch();
    const [form] = Form.useForm();

    function handleSubmit(values) {
        dispatch(userForgotPass(values.email));
    }

    return (
        <div>
            <Helmet>
                <title>Quên mật khẩu</title>
            </Helmet>
            <div className={"max-w-md mx-auto px-3 md:px-0 py-10"}>
                <div className={"rounded-md shadow-md p-5 bg-white border border-gray-200"}>
                    <h1 className={"mb-2 text-xl font-medium text-center"}>
                    Lấy lại mật khẩu
                </h1>
                <Form
                    form={form}
                    layout={"vertical"}
                    name={"ForgotPassword"}
                    onFinish={handleSubmit}
                >
                    <Form.Item
                        label="Email"
                        name="email"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập thông tin'
                            },
                        ]}
                    >
                        <Input
                            placeholder={"Email"}
                            size={"large"}
                            className={"rounded h-12"}
                        />
                    </Form.Item>
                    <Button
                        type="primary"
                        size="large"
                        htmlType="submit"
                        block
                        className="rounded bg-blue-400 hover:bg-blue-500 border-blue-500 h-12"
                    >
                        Tiếp tục
                    </Button>
                    <div
                        className={"text-center mt-5"}
                    >
                        <Link to={"/dang-nhap"} className={"text-blue-400 text-base cursor-pointer"}>
                            Quay lại trang đăng nhập
                        </Link>
                    </div>
                </Form>
            </div>
            </div>

        </div>
    );
}

export default ForgotPassword;
