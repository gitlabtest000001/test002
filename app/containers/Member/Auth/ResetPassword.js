import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Button, Form, Input } from 'antd';
import { userResetPassword } from './actions';
import OTPInput from 'otp-input-react';
import { Helmet } from 'react-helmet/es/Helmet';

ResetPassword.propTypes = {

};

function ResetPassword(props) {
    const dispatch = useDispatch();
    const [form] = Form.useForm();
    const resetEmail = localStorage.getItem('rsem');
    const [resetCode, setResetCode] = useState();

    function handleSubmit(values) {
        dispatch(userResetPassword(resetEmail, resetCode, values.password, values.passwordAgain))
    }

    return (
        <div>
            <Helmet>
                <title>Đặt mật khẩu mới</title>
            </Helmet>
            <div className={"max-w-md mx-auto px-3 md:px-0 py-10"}>
                <div className={"rounded-md shadow-md p-5 bg-white"}>
                    <h1 className={"mb-2 text-xl font-medium text-center"}>
                    Đặt mật khẩu mới
                </h1>
                <Form
                    form={form}
                    layout={"vertical"}
                    name={"ResetPassword"}
                    onFinish={handleSubmit}
                >
                    <div className={"mb-5"}>
                        <p className={"mb-2"}>Mã xác thực từ Email</p>
                        <OTPInput
                            onChange={setResetCode}
                            value={resetCode}
                            numberOnly={true}
                            autoFocus={true}
                            OTPLength={6}
                            inputClassName={"border border-gray-500 rounded"}
                            inputStyles={{width: 35, height: 40}}
                            className={"flex"}
                        />
                    </div>

                    <Form.Item
                        label="Mật khẩu mới"
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập thông tin'
                            },
                        ]}
                    >
                        <Input.Password
                            placeholder={"Mật khẩu mới"}
                            size={"large"}
                            className={"rounded h-12"}
                        />
                    </Form.Item>
                    <Form.Item
                        label="Nhập lại mật khẩu mới"
                        name="passwordAgain"
                        rules={[
                            {
                                required: true,
                                message: 'Vui lòng nhập thông tin'
                            },
                        ]}
                    >
                        <Input.Password
                            placeholder={"Mật khẩu mới"}
                            size={"large"}
                            className={"rounded h-12"}
                        />
                    </Form.Item>
                    <Button
                        type="primary"
                        size="large"
                        htmlType="submit"
                        block
                        className="rounded bg-blue-400 hover:bg-blue-500 border-blue-500 h-12"
                    >
                        Xác nhận
                    </Button>
                </Form>
            </div>
            </div>

        </div>
    );
}

export default ResetPassword;
