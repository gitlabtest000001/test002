import * as constants from './constants';

export const userLogin = (data) => ({
    type: constants.LOGIN,
    payload: {
        data
    },
});

export const userLoginSuccess = data => ({
    type: constants.LOGIN_SUCCESS,
    payload: {
        data,
    },
});

export const userLoginFailed = error => ({
    type: constants.LOGIN_FAILED,
    payload: {
        error,
    },
});

export const userRegister = (data) => ({
    type: constants.REGISTER,
    payload: {
        data
    },
});

export const userRegisterSuccess = data => ({
    type: constants.REGISTER_SUCCESS,
    payload: {
        data,
    },
});

export const userRegisterFailed = error => ({
    type: constants.REGISTER_FAILED,
    payload: {
        error,
    },
});

export const userLogout = () => ({
    type: constants.LOGOUT,
});
export const userLogoutSuccess = data => ({
    type: constants.LOGOUT_SUCCESS,
    payload: {
        data
    },
});
export const userLogoutFailed = error => ({
    type: constants.LOGOUT_FAILED,
    payload: {
        error
    },
});

export const userForgotPass = email => ({
    type: constants.FORGOT_PASSWORD,
    payload: {
        email,
    },
});
export const userForgotPasswordSuccess = data => ({
    type: constants.FORGOT_PASSWORD_SUCCESS,
    payload: {
        data,
    },
});
export const userForgotPasswordFailed = error => ({
    type: constants.FORGOT_PASSWORD_FAILED,
    payload: {
        error,
    },
});

export const userResetPassword = (email, resetCode, password, passwordAgain) => ({
    type: constants.RESET_PASSWORD,
    payload: {
        email, resetCode, password, passwordAgain
    },
});

export const userResetPasswordSuccess = data => ({
    type: constants.RESET_PASSWORD_SUCCESS,
    payload: {
        data,
    },
});

export const userResetPasswordFailed = error => ({
    type: constants.RESET_PASSWORD_FAILED,
    payload: {
        error,
    },
});

export const GetMe = (accessToken) => ({
    type: constants.GET_ME,
    payload: {
        accessToken
    },
});
export const GetMeSuccess = data => ({
    type: constants.GET_ME_SUCCESS,
    payload: {
        data
    },
});
export const GetMeFailed = error => ({
    type: constants.GET_ME_FAILED,
    payload: {
        error
    },
});

export const GetMeManager = (accessToken) => ({
    type: constants.GET_ME_MANAGER,
    payload: {
        accessToken
    },
});
export const GetMeManagerSuccess = data => ({
    type: constants.GET_ME_MANAGER_SUCCESS,
    payload: {
        data
    },
});
export const GetMeManagerFailed = error => ({
    type: constants.GET_ME_MANAGER_FAILED,
    payload: {
        error
    },
});

export const LoadDataAction = user => ({
    type: constants.USER_DATA,
    user,
});

export const updateAccount = body  => ({
    type: constants.UPDATE_ACCOUNT,
    payload: {
        body
    },
});

export const updateAccountSuccess = data => ({
    type: constants.UPDATE_ACCOUNT_SUCCESS,
    payload: {
        data,
    },
});

export const updateAccountFailed = error => ({
    type: constants.UPDATE_ACCOUNT_FAILED,
    payload: {
        error,
    },
});
