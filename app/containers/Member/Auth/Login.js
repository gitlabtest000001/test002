import React from 'react';
import { Link } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Button, Form, Input } from 'antd';
import { userLogin } from './actions';
import queryString from 'query-string';
import { Helmet } from 'react-helmet/es/Helmet';

Login.propTypes = {

};

function Login(props) {
    const dispatch = useDispatch();
    const [form] = Form.useForm();

    let params = queryString.parse(props.location.search);
    let returnURL = params.return;

    async function handleSubmit(values) {
        dispatch(userLogin(values))
    }

    return (
        <div>
            <Helmet>
                <title>Đăng nhập</title>
            </Helmet>
            <div className={"max-w-md mx-auto px-3 md:px-0 py-10"}>
                <div className={"rounded-md shadow-md p-5 bg-white border border-gray-200"}>
                    <h1 className={"mb-2 text-xl font-medium text-center"}>
                        Đăng nhập
                    </h1>
                    <p className={"mb-5 text-gray-400 text-center"}>
                        Đăng nhập để quản lý đơn hàng thuận tiện hơn
                    </p>
                    <Form
                        form={form}
                        layout={"vertical"}
                        name={"login"}
                        onFinish={handleSubmit}
                    >
                        <Form.Item
                            label="Email/Số điện thoại"
                            name="username"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập thông tin'
                                },
                            ]}
                        >
                            <Input
                                placeholder={"Email hoặc Số điện thoại"}
                                size={"large"}
                                className={"rounded h-12"}
                            />
                        </Form.Item>
                        <Form.Item
                            label="Mật khẩu"
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: 'Vui lòng nhập mật khẩu'
                                },
                            ]}
                        >
                            <Input.Password
                                placeholder={"Mật khẩu"}
                                size={"large"}
                                className={"rounded h-12"}
                            />
                        </Form.Item>
                        <Button
                            size="large"
                            htmlType="submit"
                            block
                            className="rounded border-0 bg-yellow-500 text-white hover:bg-yellow-600 hover:text-white focus:bg-yellow-600 focus:text-white h-12"
                        >
                            Đăng nhập
                        </Button>
                        <div
                            className={"flex flex-row justify-between items-center mt-5"}
                        >
                            <Link to={"/dang-ky"} className={"text-orange-400 text-base hover:text-orange-500"}>
                                Tạo tài khoản
                            </Link>
                            <Link to={"/quen-mat-khau"}>
                                Quên mật khẩu?
                            </Link>
                        </div>
                    </Form>
                </div>

            </div>
        </div>

    );
}

export default Login;
