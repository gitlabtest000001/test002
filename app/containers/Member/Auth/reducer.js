import produce from 'immer/dist/immer';
import * as constant from './constants';
import {ErrorMessage, SuccessMessage} from "../../../components/Message";

export const initialState = {
    user: null,
    fetching: false,
}

const userAuth = (state = initialState, action) =>
    produce(state, draft => {
        switch (action.type) {
            case constant.LOGIN: {
                return {
                    ...state,
                    fetching: true,
                };
            }
            case constant.LOGIN_SUCCESS: {
                SuccessMessage({message: 'Đăng nhập thành công'})
                return {
                    ...state,
                    fetching: false,
                };
            }
            case constant.LOGIN_FAILED: {
                const { error } = action.payload;
                ErrorMessage(error);
                return {
                    ...state,
                    fetching: false,
                    error,
                };
            }
            case constant.REGISTER: {
                return {
                    ...state,
                    fetching: true,
                };
            }
            case constant.REGISTER_SUCCESS: {
                SuccessMessage({message: 'Tạo tài khoản thành công, bạn có thể đăng nhập ngay!'})
                return {
                    ...state,
                    fetching: false,
                };
            }
            case constant.REGISTER_FAILED: {
                const { error } = action.payload;
                ErrorMessage(error);
                return {
                    ...state,
                    fetching: false,
                    error,
                };
            }
            case constant.UPDATE_ACCOUNT: {
                return {
                    ...state,
                };
            }
            case constant.UPDATE_ACCOUNT_SUCCESS: {
                const {data} = action.payload;
                SuccessMessage({message: 'Cập nhật thành công'})
                return {
                    ...state,
                    user: data,
                };
            }
            case constant.UPDATE_ACCOUNT_FAILED: {
                const { error } = action.payload;
                ErrorMessage(error);
                return {
                    ...state,
                    error,
                };
            }
            case constant.FORGOT_PASSWORD: {
                const { data } = action.payload;
                return {
                    ...state,
                    //user: data,
                };
            }
            case constant.FORGOT_PASSWORD_SUCCESS: {
                const { data } = action.payload;
                SuccessMessage(data);
                return {
                    ...state,
                    //user: data,
                    success: true,
                };
            }
            case constant.FORGOT_PASSWORD_FAILED: {
                const { error } = action.payload;
                ErrorMessage(error);
                return {
                    ...state,
                    error,
                    success: false,
                };
            }
            case constant.RESET_PASSWORD: {
                const { data } = action.payload;
                return {
                    ...state,
                    //user: data,
                };
            }
            case constant.RESET_PASSWORD_SUCCESS: {
                const { data } = action.payload;
                SuccessMessage({message: 'Đã đặt mật khẩu mới, vui lòng đăng nhập lại'})
                return {
                    ...state,
                    success: true,
                };
            }
            case constant.RESET_PASSWORD_FAILED: {
                const { error } = action.payload;
                ErrorMessage(error);
                return {
                    ...state,
                    error,
                    success: false,
                };
            }
            case constant.GET_ME: {
                return {
                    ...state,
                    fetching: true,
                };
            }
            case constant.GET_ME_SUCCESS: {
                const { data } = action.payload;
                return {
                    ...state,
                    fetching: false,
                    user: data,
                };
            }
            case constant.GET_ME_FAILED: {
                const { error } = action.payload;
                ErrorMessage(error);
                return {
                    ...state,
                    fetching: false,
                    error,
                };
            }
            case constant.GET_ME_MANAGER: {
                return {
                    ...state,
                    fetching: true,
                };
            }
            case constant.GET_ME_MANAGER_SUCCESS: {
                const { data } = action.payload;
                return {
                    ...state,
                    fetching: false,
                    user: data,
                };
            }
            case constant.GET_ME_MANAGER_FAILED: {
                const { error } = action.payload;
                ErrorMessage(error);
                return {
                    ...state,
                    fetching: false,
                    error,
                };
            }
            case constant.LOGOUT: {
                return {
                    ...state,
                };
            }
            case constant.LOGOUT_SUCCESS: {
                const { data } = action.payload;
                SuccessMessage(data)
                return {
                    ...state,
                    user: null,
                };
            }
            case constant.LOGOUT_FAILED: {
                const { error } = action.payload;
                ErrorMessage(error);
                return {
                    ...state,
                    error,
                };
            }
            case constant.USER_DATA: {
                return {
                    ...state,
                    user: {data: action.user},
                    fetching: false,
                };
            }
            default:
                return state;
        }
    });

export default userAuth;
