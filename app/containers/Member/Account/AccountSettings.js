import React from 'react';
import { useSelector } from 'react-redux';
import AccountUpdateForm from './components/AccountUpdateForm';
import AccountLayout from './components/AccountLayout';
import { Helmet } from 'react-helmet/es/Helmet';

AccountSettings.propTypes = {

};

function AccountSettings(props) {
    const currentUser = useSelector(state => state.root.currentUser.user);

    return (
        <AccountLayout path={props.location.pathname} title={"Cài đặt tài khoản"}>
            <Helmet>
                <title>Cài đặt tài khoản</title>
            </Helmet>
            <AccountUpdateForm initialValues={currentUser && currentUser}/>
        </AccountLayout>
    );
}

export default AccountSettings;
