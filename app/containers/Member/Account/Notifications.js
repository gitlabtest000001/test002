import React, {useEffect, useState} from 'react';
import AccountLayout from './components/AccountLayout';
import Button from 'antd/es/button';
import NotFound from '../../../components/NotFound';
import Row from 'antd/es/grid/row';
import history from '../../../utils/history';
import Rate from 'antd/es/rate';
import Col from 'antd/es/grid/col';
import Popconfirm from 'antd/es/popconfirm';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { useDispatch } from 'react-redux';
import queryString from 'query-string';
import Loading from '../../../components/Loading';
import Pagination from 'antd/es/pagination';
import { formatDateTime } from '../../../utils/helpers';
import {BsCircleFill, IoCheckmarkDoneOutline} from 'react-icons/all'
import { ErrorMessage, SuccessMessage } from '../../../components/Message';

Notifications.propTypes = {

};

function Notifications(props) {
    let params = queryString.parse(props.location.search);
    const dispatch = useDispatch()
    const [results, setResults] = useState([])
    const [page, setPage] = useState(params && params.page ? params.page : 1);
    const [readAll, setReadAll] = useState(false);


    useEffect(() => {
        dispatch(showLoader());
        async function getAllBookings() {
            const Token = await localStorage.getItem('dvg_user_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/customer/notification?limit=10&page=${page}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setResults(response.data)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getAllBookings();
    }, [page, readAll])

    function handleChangePage(page) {
        setPage(page);
        history.push(`${props.location.pathname}?page=${page}`);
        window.scrollTo({top: 0, left: 0, behavior: 'smooth' });
    }

    function handleReadAll() {
        async function editVin() {
            dispatch(showLoader())
            const Token = await localStorage.getItem('dvg_user_token');
            axios({
                method: 'put',
                url: `${AppConfig.apiUrl}/customer/notification/readAll`,
                headers: {Authorization: `Bearer ${Token}`},
            }).then(function (response) {
                SuccessMessage({message: 'Đã cập nhật trạng thái thông báo'})
                dispatch(hideLoader())
                setReadAll(!readAll);
            }).catch(function(error){
                ErrorMessage({message: 'Lỗi'})
                dispatch(hideLoader())
            })
        }
        editVin();
    }

    return (
        <AccountLayout path={props.location.pathname} title={"Thông báo"}>
            <div className={"px-3 pt-5 md:pt-0 md:pl-5"}>
                <div className={"flex justify-between items-start"}>
                    <h1 className={"hidden md:block text-2xl text-gray-700 mb-5"}>Thông báo</h1>
                    <Button
                        onClick={handleReadAll}
                        icon={<IoCheckmarkDoneOutline size={20} className={"mr-2"} />}
                        className={"rounded"}
                    >
                        Đã đọc tất cả
                    </Button>
                </div>
                <div className={"mt-5"}>
                    {results && results.list && results.list.length > 0 ?
                        <div>
                            <div className={"mb-5 flex justify-between"}>
                                <span className={"text-gray-500"}>Có <span className={"font-medium text-gray-700"}>{results.count}</span> thông báo</span>
                                Trang {page}
                            </div>
                            {results.list.map((item, index) => (
                                <div className={"mb-5 rounded shadow-md border border-gray-200"}>
                                    <div className={"md:flex justify-between items-center border-b border-gray-200 px-3 py-2 mb-2"}>
                                        <div>
                                            {item.seen === 'false' ?
                                                <BsCircleFill className={"text-blue-400 mr-2"} />
                                                :
                                                <BsCircleFill className={"text-gray-200 mr-2"} />
                                            }
                                            <span className={"font-medium"}>{item.title}</span>
                                        </div>
                                        <div className={"text-gray-500"}>
                                            {formatDateTime(item.createdAt)}
                                        </div>
                                    </div>
                                    <div className={"px-3 pb-3"}>
                                        <p>{item.message}</p>
                                    </div>
                                </div>
                            ))}
                            {results.count &&
                            results.count > 10 &&
                                <Pagination
                                    pageSize={10}
                                    defaultCurrent={page}
                                    total={results.count}
                                    onChange={(page, pageSize) => handleChangePage(page)}
                                />
                            }
                        </div>

                        :
                        <NotFound />
                    }
                </div>
            </div>
        </AccountLayout>
    );
}

export default Notifications;
