import React from 'react';
import PropTypes from 'prop-types';
import { useSelector } from 'react-redux';
import { formatVND } from '../../../../utils/helpers';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import { SuccessMessage } from '../../../../components/Message';
import { IoCard } from 'react-icons/io5';

PaymentBox.propTypes = {

};

function PaymentBox(props) {

    function PaymentInfo(payment_info) {
        const paymentInfo = JSON.parse(payment_info);
        return (
            <div>
                {paymentInfo && paymentInfo.map((item, index) => (
                    <React.Fragment>
                        <div className={"md:flex pb-2 border-b border-gray-100"}>
                            <p className={"text-gray-600 w-40"}>Ngân hàng:</p>
                            <p className={"font-medium"}>{item.bank_name}</p>
                        </div>
                        <div className={"md:flex py-2 border-b border-gray-100"}>
                            <p className={"text-gray-600 w-40"}>Số tài khoản:</p>
                            <p className={"font-medium"}>{item.bank_account}</p>
                        </div>
                        <div className={"md:flex py-2 border-b border-gray-100"}>
                            <p className={"text-gray-600 w-40"}>Chủ tài khoản:</p>
                            <p className={"font-medium"}>{item.bank_owner}</p>
                        </div>
                        {item.bank_brand !== '' &&
                            <div className={"md:flex py-2 border-b border-gray-100"}>
                                <p className={"text-gray-600 w-40"}>Chi nhánh:</p>
                                <p className={"font-medium"}>{item.bank_brand}</p>
                            </div>
                        }
                    </React.Fragment>
                ))}
            </div>
        )
    }

    return (
        <div id={"thong-tin-thanh-toan"} className={"mt-5"}>
            {props.exportTax ?
                Number(props.totalAmount) > 20000000 ?
                    PaymentInfo(props.settings.payment_info_company)
                    :
                    PaymentInfo(props.settings.payment_info_personal)
                :
                PaymentInfo(props.settings.payment_info_personal)
            }
            <div className={"md:flex py-2 border-b border-gray-100"}>
                <p className={"text-gray-600 w-40"}>Số tiền:</p>
                <p className={"font-bold text-green-500"}>{formatVND(props.totalAmount)}</p>
            </div>
            <div className={"md:flex pt-2"}>
                <p className={"text-gray-600 w-40"}>Nội dung:</p>
                <p className={"font-medium"}>Thanh toan {props.orderCode}</p>
            </div>
        </div>
    );
}

export default PaymentBox;
