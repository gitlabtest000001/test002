import React, {useState} from 'react';
import PropTypes from 'prop-types';
import CheckOutlined from '@ant-design/icons/lib/icons/CheckOutlined';
import Button from 'antd/es/button';
import SingleUpload from '../../../../components/SingleUpload';
import Card from 'antd/es/card';
import Col from 'antd/es/grid/col';
import Row from 'antd/es/grid/row';
import Form from 'antd/es/form';
import Input from 'antd/es/input';
import { updateAccount } from '../../Auth/actions';
import { useDispatch } from 'react-redux';
import SingleUploadButton from '../../../../components/SingleUploadButton';
import Image from 'antd/es/image';
import Avatar from 'antd/es/avatar';
import { banks } from '../../../../utils/DefaultLists/bankList';
import { Divider, Select } from 'antd';
import { UserAffRoles, UserLevel } from '../../../../models/member.model';
import { MaskedInput } from 'antd-mask-input';

const {Option} = Select;

AccountUpdateForm.propTypes = {

};

function AccountUpdateForm(props) {
    const dispatch = useDispatch();
    const [avatar, setAvatar] = useState(props.initialValues && props.initialValues.avatar);

    function handleChooseAvatar(avatar) {
        setAvatar(avatar)
    }

    function handleSubmit(values) {
        dispatch(updateAccount(
            {
                name: values.name,
                email: values.email,
                phone: values.phone,
                password: values.password,
                address: values.address,
                bank_name: values.bank_name,
                bank_brand: values.bank_brand,
                bank_account: values.bank_account,
                bank_owner: values.bank_owner,
                zalo: values.zalo,
                facebook: values.facebook,
                whatsapp: values.whatsapp,
                zaloGroup: values.zaloGroup,
                youtube: values.youtube,
                dateofbirth: values.dateofbirth,
                avatar: avatar
            }))
    }

    return (
        <div>
            <Form
                className="mt-5"
                layout="vertical"
                name="UserForm"
                onFinish={handleSubmit}
                initialValues={props.initialValues}
            >
                <Row gutter={[16, 16]}>
                    <Col xs={{ order: 2, span: 24 }} lg={16} md={16}>
                        <Card>
                            <Divider orientation="left">Thông tin cấp bậc</Divider>
                            {props.initialValues &&
                                UserAffRoles.map((item, index) => (
                                    item.code === props.initialValues.affRoles && <span>{item.name}</span>
                                ))
                            }
                            <span> - </span>
                            {props.initialValues &&
                                UserLevel.map((item, index) => (
                                    item.code === props.initialValues.level && item.name
                                ))
                            }
                            {props.initialValues &&
                                props.initialValues.investorStatus === 'TRUE' && ' - Nhà đầu tư'
                            }
                            <Divider orientation="left">Thông tin cá nhân</Divider>
                            <Form.Item
                                label="Họ và tên"
                                name="name"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập tên'},
                                ]}
                            >
                                <Input
                                    size="large"
                                />
                            </Form.Item>
                            <Form.Item
                                label="Email"
                                name="email"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập email'
                                    },
                                    {
                                        type: 'email',
                                        message: 'Vui lòng nhập đúng định dạng Email!',
                                    },
                                ]}
                            >
                                <Input
                                    size="large"
                                />
                            </Form.Item>
                            <Form.Item
                                label="Số điện thoại"
                                name="phone"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập số điện thoại'},
                                ]}
                            >
                                <Input
                                    size="large"
                                />
                            </Form.Item>
                            <Form.Item
                                label="Địa chỉ"
                                name="address"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Vui lòng nhập địa chỉ'},
                                ]}
                            >
                                <Input
                                    size="large"
                                />
                            </Form.Item>
                            <Form.Item
                                label="Ngày sinh"
                                name="dateofbirth"
                                className={"mb-0"}
                            >
                                <MaskedInput
                                    size={"large"}
                                    mask={"00/00/0000"}
                                />
                            </Form.Item>
                            <Form.Item
                                label="Mật khẩu"
                                name="password"
                                extra="Chỉ nhập vào nếu muốn thay đổi mật khẩu."
                            >
                                <Input
                                    size="large"
                                />
                            </Form.Item>

                            <Divider orientation="left">Thông tin nhận tiền</Divider>
                            <Form.Item
                                label="Tên ngân hàng"
                                name="bank_name"
                            >
                                <Select
                                    showSearch={true}
                                    allowClear
                                    style={{ width: '100%' }}
                                    size="large"
                                >
                                    {
                                        banks.map(bank =>(
                                            <Option key={bank.code} value={bank.bank_name}>{bank.bank_name}</Option>
                                        ))
                                    }
                                </Select>
                            </Form.Item>
                            <Form.Item
                                label="Chủ tài khoản"
                                name="bank_owner"
                            >
                                <Input
                                    size="large"
                                />
                            </Form.Item>
                            <Form.Item
                                label="Số tài khoản"
                                name="bank_account"
                            >
                                <Input
                                    size="large"
                                />
                            </Form.Item>
                            <Form.Item
                                label="Chi nhánh"
                                name="bank_brand"
                            >
                                <Input
                                    size="large"
                                />
                            </Form.Item>

                            <Divider orientation="left">Thông tin mạng xã hội</Divider>
                            <Form.Item
                                label="Số điện thoại Zalo"
                                name="zalo"
                            >
                                <Input
                                    size="large"
                                    placeholder={""}
                                />
                            </Form.Item>
                            <Form.Item
                                label="Đường dẫn nhóm Zalo"
                                name="zaloGroup"
                            >
                                <Input
                                    size="large"
                                    placeholder={"Ví dụ: https://zalo.me/group/AKXmdos"}
                                />
                            </Form.Item>
                            <Form.Item
                                label="Đường dẫn Facebook"
                                name="facebook"
                            >
                                <Input
                                    size="large"
                                    placeholder={"Ví dụ: http://facebook.me/daivietgroup"}
                                />
                            </Form.Item>
                            <Form.Item
                                label="Số điện thoại Whatsapp"
                                name="whatsapp"
                            >
                                <Input
                                    size="large"
                                />
                            </Form.Item>
                            <Form.Item
                                label="Youtube"
                                name="youtube"
                            >
                                <Input
                                    size="large"
                                />
                            </Form.Item>

                            <Button
                                type="primary"
                                htmlType="submit"
                                icon={<CheckOutlined />}
                            >
                                Lưu thay đổi
                            </Button>
                        </Card>
                    </Col>
                    <Col xs={{ order: 1, span: 24 }} lg={8} md={8}>
                        <Card bordered={true} className={"text-center mb-5"}>
                            {avatar &&
                                <Avatar src={avatar} className={"w-20 h-20 mr-5 md:w-32 md:h-32 object-cover border border-gray-100 shadow md:mb-5"}/>
                            }
                                <SingleUploadButton
                                    onChooseMedia={handleChooseAvatar}
                                    uploadLabel={'chọn ảnh đại diện'}
                                />
                        </Card>
                    </Col>
                </Row>
            </Form>
        </div>
    );
}

export default AccountUpdateForm;
