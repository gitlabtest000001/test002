import React from 'react';
import {Link} from 'react-router-dom';
import {
    IoPerson,
    IoReceipt,
    IoBarChart,
    IoSync,
    IoNotificationsSharp, IoLogOut,
} from 'react-icons/all';
import { useSelector } from 'react-redux';
import { checkRole } from '../../../../utils/helpers';

function AccountSidebar(props) {

    const currentUser = useSelector(state => state.root.currentUser.user);

    return (
        <div className={"py-10 -mr-4"}>
            <div>
                {props.menu.map((item, index) => (
                    currentUser && item.roles.includes(currentUser.roles) &&
                    <div
                        className={`group rounded-tl-md rounded-bl-md hover:bg-green-50 ${item.path === props.path && 'bg-gray-100 border-r-2 border-green-600'}`}
                    >
                        <Link to={item.path} className={"block w-full p-3 mb-2 text-black flex items-center"}>
                            {item.path === props.path ? item.activeIcon : item.icon } <span className={"text-base text-gray-800"}>{item.name}</span>
                        </Link>
                    </div>
                ))}
                <div
                    className={`group rounded-tl-md rounded-bl-md hover:bg-red-50 hover:border-r-2 border-red-600 cursor-pointer`}
                >
                    <div className={"block w-full p-3 mb-2 text-black flex items-center"} onClick={() => props.onLogout()}>
                        <IoLogOut size={20} className={"mr-2 text-red-300"} /> <span className={"text-base text-gray-800"}>Đăng xuất</span>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default AccountSidebar;
