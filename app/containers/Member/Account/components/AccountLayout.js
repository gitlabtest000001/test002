import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Row from 'antd/es/grid/row';
import Col from 'antd/es/grid/col';
import AccountSidebar from './AccountSidebar';
import { Desktop, TabletAndBelow } from '../../../../utils/responsive';
import {
    IoArrowBack,
    IoBag,
    IoCall,
    IoDiamond,
    IoGitNetwork,
    IoGrid,
    IoLogOutOutline,
    IoPaperPlane,
} from 'react-icons/io5';
import history from '../../../../utils/history';
import { Affix } from 'antd';
import {
    IoBarChart,
    IoNotificationsSharp,
    IoPerson,
    IoReceipt,
    IoSync,
    IoInformationCircle,
    IoShieldCheckmark,
    IoWallet,
    IoBagHandle, IoBagCheck,
} from 'react-icons/all';
import { Link } from 'react-router-dom';
import AccountMobileMenu from './AccountMobileMenu';
import { useDispatch } from 'react-redux';
import Modal from 'antd/es/modal';
import LogoutOutlined from '@ant-design/icons/lib/icons/LogoutOutlined';
import { userLogout } from '../../Auth/actions';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';

const { confirm } = Modal;

AccountLayout.propTypes = {

};

AccountLayout.defaultProps = {
    title: 'Tài khoản'
}

function AccountLayout(props) {
    const dispatch = useDispatch();

    const [informationMobileMenu, setInformationMobileMenu] = useState([])

    useEffect(() => {
        async function getRewards() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/menu`,
                params: {
                    location: 'informationmobilemenu',
                },
            }).then(function (response) {
                if (response.status === 200) {
                    setInformationMobileMenu(response.data);
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getRewards();
    }, [])

    const menu = [
        {
            id: 'dashboard',
            path: '/taikhoan',
            name: 'Dashboard',
            roles: ['member', 'guest'],
            icon: <IoGrid size={20} className={"mr-2 text-gray-400"} />,
            activeIcon: <IoGrid size={20} className={"mr-2 text-green-600"} />,
        },
        {
            id: 'dautu',
            path: '/taikhoan/dau-tu',
            name: 'Đầu tư',
            roles: ['member'],
            icon: <IoDiamond size={20} className={"mr-2 text-gray-400"} />,
            activeIcon: <IoDiamond size={20} className={"mr-2 text-green-600"} />,
        },
        {
            id: 'affiliate',
            path: '/taikhoan/affiliate',
            name: 'Cộng tác viên',
            roles: ['member'],
            icon: <IoGitNetwork size={20} className={"mr-2 text-gray-400"} />,
            activeIcon: <IoGitNetwork size={20} className={"mr-2 text-green-600"} />,
        },
        {
            id: 'donhang',
            path: '/taikhoan/don-hang',
            name: 'Đơn hàng',
            roles: ['member'],
            icon: <IoBag size={20} className={"mr-2 text-gray-400"} />,
            activeIcon: <IoBag size={20} className={"mr-2 text-green-600"} />,
        },
        {
            id: 'donhangctv',
            path: '/taikhoan/don-hang-ctv',
            name: 'Đơn hàng CTV',
            roles: ['member'],
            icon: <IoBagCheck size={20} className={"mr-2 text-gray-400"} />,
            activeIcon: <IoBagCheck size={20} className={"mr-2 text-green-600"} />,
        },
        {
            id: 'donhangkhachle',
            path: '/taikhoan/don-hang-khach-le',
            name: 'Đơn hàng khách lẻ',
            roles: ['member'],
            icon: <IoBagHandle size={20} className={"mr-2 text-gray-400"} />,
            activeIcon: <IoBagHandle size={20} className={"mr-2 text-green-600"} />,
        },
        {
            id: 'giaodich',
            path: '/taikhoan/giao-dich',
            name: 'Giao dịch',
            roles: ['member'],
            icon: <IoReceipt size={20} className={"mr-2 text-gray-400"} />,
            activeIcon: <IoReceipt size={20} className={"mr-2 text-green-600"} />,
        },
        {
            id: 'marketing',
            path: '/taikhoan/marketing',
            name: 'Marketing',
            roles: ['member'],
            icon: <IoPaperPlane size={20} className={"mr-2 text-gray-400"}/>,
            activeIcon: <IoPaperPlane size={20} className={"mr-2 text-green-600"}/>
        },
        {
            id: 'taikhoan',
            path: '/taikhoan/thongtin',
            name: 'Tài khoản',
            roles: ['member'],
            icon: <IoPerson size={20} className={"mr-2 text-gray-400"}/>,
            activeIcon: <IoPerson size={20} className={"mr-2 text-green-600"}/>
        },
    ]

    const menuMobile = [
        {
            id: 'dashboard',
            path: '/taikhoan/vi',
            name: 'Dashboard',
            roles: ['member'],
            icon: <IoGrid size={16} className={"mr-2 text-gray-400"}/>,
            activeIcon: <IoGrid size={16} className={"mr-2 text-green-600"}/>
        },
        {
            id: 'affiliate',
            path: '/taikhoan/affiliate',
            name: 'Cộng tác viên',
            roles: ['member'],
            icon: <IoGitNetwork size={16} className={"mr-2 text-gray-400"} />,
            activeIcon: <IoGitNetwork size={16} className={"mr-2 text-green-600"} />,
        },
        {
            id: 'donhang',
            path: '/taikhoan/don-hang',
            name: 'Đơn hàng',
            roles: ['member'],
            icon: <IoBag size={16} className={"mr-2 text-gray-400"} />,
            activeIcon: <IoBag size={16} className={"mr-2 text-green-600"} />,
        },
        {
            id: 'donhangkhachle',
            path: '/taikhoan/don-hang-khach-le',
            name: 'Đơn hàng khách lẻ',
            roles: ['member'],
            icon: <IoBagHandle size={16} className={"mr-2 text-gray-400"} />,
            activeIcon: <IoBagHandle size={16} className={"mr-2 text-green-600"} />,
        },
        {
            id: 'donhangctv',
            path: '/taikhoan/don-hang-ctv',
            name: 'Đơn hàng CTV',
            roles: ['member'],
            icon: <IoBagCheck size={16} className={"mr-2 text-gray-400"} />,
            activeIcon: <IoBagCheck size={16} className={"mr-2 text-green-600"} />,
        },
        {
            id: 'dautu',
            path: '/taikhoan/dau-tu',
            name: 'Đầu tư',
            roles: ['member'],
            icon: <IoDiamond size={16} className={"mr-2 text-gray-400"} />,
            activeIcon: <IoDiamond size={16} className={"mr-2 text-green-600"} />,
        },
        {
            id: 'giaodich',
            path: '/taikhoan/giao-dich',
            name: 'Giao dịch',
            roles: ['member'],
            icon: <IoReceipt size={16} className={"mr-2 text-gray-400"} />,
            activeIcon: <IoReceipt size={16} className={"mr-2 text-green-600"} />,
        },
        {
            id: 'marketing',
            path: '/taikhoan/marketing',
            name: 'Marketing',
            roles: ['member'],
            icon: <IoPaperPlane size={16} className={"mr-2 text-gray-400"}/>,
            activeIcon: <IoPaperPlane size={16} className={"mr-2 text-green-600"}/>
        },
        {
            id: 'taikhoan',
            path: '/taikhoan/caidat',
            name: 'Tài khoản',
            roles: ['member'],
            icon: <IoPerson size={16} className={"mr-2 text-gray-400"}/>,
            activeIcon: <IoPerson size={16} className={"mr-2 text-green-600"}/>
        },
    ]

    const menuInfo = [
        {
            id: 'gioithieu',
            path: '/trang/gioi-thieu',
            name: 'Giới thiệu',
            roles: ['member'],
            icon: <IoInformationCircle size={16} className={"mr-2 text-gray-400"}/>
        },
        {
            id: 'chinhsach',
            path: '/trang/chinh-sach',
            name: 'Chính sách bảo mật',
            roles: ['member'],
            icon: <IoShieldCheckmark size={16} className={"mr-2 text-gray-400"}/>
        },
        {
            id: 'huongdan',
            path: '/trang/huong-dan-thanh-toan',
            name: 'Hướng dẫn thanh toán',
            roles: ['member'],
            icon: <IoWallet size={16} className={"mr-2 text-gray-400"} />
        },
    ]

    function handleLogout() {
        confirm({
            title: 'Bạn có chắc chắn muốn thoát tài khoản?',
            icon: <LogoutOutlined />,
            onOk() {
                dispatch(userLogout())
            },
        });
    }

    return (
        <div>
            <Desktop>
                <div className={"max-w-6xl mx-auto"}>
                    <Row gutter={[32, 32]}>
                        <Col xs={24} lg={5} md={5} className={"min-h-screen"} style={{boxShadow: '10px 0px 15px -10px #eaeaea'}}>
                            <AccountSidebar path={props.path} menu={menu} onLogout={handleLogout} />
                        </Col>
                        <Col xs={24} lg={19} md={19} className={"my-10"}>
                            {props.children}
                        </Col>
                    </Row>
                </div>
            </Desktop>
            <TabletAndBelow>
                <div>
                    <Affix>
                        <div className={"bg-white border-b border-gray-100"}>
                            <div className={"h-14 flex items-center justify-between mx-3 text-center"}>
                                <IoArrowBack onClick={() => history.goBack()} size={22} className={"text-gray-800"} />
                                <span className={"font-medium text-base text-gray-800"}>{props.title}</span>
                                <IoLogOutOutline size={22} className={"text-gray-800"} onClick={handleLogout} />
                            </div>
                        </div>
                    </Affix>

                    {props.path === '/taikhoan'
                        ?
                        <AccountMobileMenu menu={menuMobile} path={props.path} menuInfo={informationMobileMenu && informationMobileMenu} onLogout={handleLogout} />
                        : props.children
                    }
                </div>
            </TabletAndBelow>
        </div>
    );
}

export default AccountLayout;
