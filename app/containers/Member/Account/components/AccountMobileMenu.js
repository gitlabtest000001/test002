import React from 'react';
import {Link} from 'react-router-dom';
import {
    IoPerson,
    IoReceipt,
    IoBarChart,
    IoSync,
    IoNotificationsSharp, IoLogOut, IoHeartCircle, IoHeartCircleOutline,
} from 'react-icons/all';
import { useSelector } from 'react-redux';
import { checkRole } from '../../../../utils/helpers';
import Loading from '../../../../components/Loading';
import MobileSupport from '../../../../components/MobileSupport';
import { AppConfig } from '../../../../appConfig';
import { IoPeopleCircle } from 'react-icons/io5';

function AccountMobileMenu(props) {

    const currentUser = useSelector(state => state.root.currentUser.user);

    return (
        !currentUser ? <Loading /> :
        <div>
            <div className={"bg-white mb-5 flex flex-row items-center p-3"}>
                <div>
                    {currentUser && currentUser.avatar ?
                        <img src={currentUser && currentUser.avatar} alt={"avatar"}
                             className={"w-20 h-20 rounded-full object-cover border border-gray-300"}/>
                        :
                        <IoHeartCircleOutline className={"text-green-600"} size={68}/>
                    }
                </div>
                <div className={"ml-5"}>
                    <h2 className={"font-bold text-base"}>{currentUser && currentUser.name}</h2>
                    <p>{currentUser && currentUser.email}</p>
                    <p>{currentUser && currentUser.phone}</p>
                </div>
            </div>
            <div className={"bg-white p-3"}>
                <p className={"text-xs font-bold text-gray-500 mb-2"}>Tài khoản của tôi</p>
                <div>
                    {props.menu.map((item, index) => (
                        currentUser && item.roles.includes(currentUser.roles) &&
                        <div
                            className={`group border-b border-gray-200 flex flex-rows items-center justify-between`}
                        >
                            <Link to={item.path} className={"block w-full py-2 mb-1 text-black flex items-center"}>
                                {item.icon} <span className={"text-gray-800"}>{item.name}</span>
                            </Link>
                        </div>
                    ))}
                </div>
                <div
                    className={`group border-b border-gray-200 flex flex-rows items-center justify-between`}
                    onClick={() => props.onLogout()}
                >
                    <div className={"block w-full py-2 mb-1 text-black flex items-center"}>
                        <IoLogOut size={16} className={"mr-2 text-red-500"}/> <span className={"text-gray-800"}>Đăng xuất</span>
                    </div>
                </div>
            </div>
            <div className={"bg-white p-3 mb-5"}>
                <p className={"text-xs font-bold text-gray-500 mb-2"}>Thông tin</p>
                <div>
                    {props.menuInfo.map((item, index) => (
                        <div
                            className={`group border-b border-gray-200 flex flex-rows items-center justify-between`}
                        >
                            <Link to={item.url} className={"block w-full py-2 mb-1 text-black flex items-center"}>
                                {item.icon} <span className={"text-gray-800"}>{item.title}</span>
                            </Link>
                        </div>
                    ))}
                </div>
            </div>
            <div className={"bg-white p-3"}>
                <MobileSupport />
            </div>

        </div>

    );
}

export default AccountMobileMenu;
