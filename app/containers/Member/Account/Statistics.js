import React, {useEffect, useState} from 'react';
import AccountLayout from './components/AccountLayout';
import moment from 'moment';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import {DatePicker} from "antd";
import LineChart from '../../../components/Chart';
import Stats from '../../../components/Stats';
import { Helmet } from 'react-helmet/es/Helmet';
import history from '../../../utils/history';
import { hideLoader } from '../../App/actions';

const { RangePicker } = DatePicker;

Statistics.propTypes = {

};

function Statistics(props) {
    const [stats, setStats] = useState();
    const [chart, setChart] = useState();
    const dateFormat = 'DD/MM/YYYY';
    const [dateRange, setDateRange] = useState(
        [
            moment.utc(moment().clone().startOf('month').format('YYYY-MM-DD')),
            moment.utc(moment().clone().endOf('month').format("YYYY-MM-DD"))
        ]
    )

    function handleSetRange (v) {
        setDateRange(v);
    }

    useEffect(() => {
        async function getBookingHistory() {
            const Token = await localStorage.getItem('dvg_user_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/statistic/booking?rangeStart=${moment(dateRange[0]).format("YYYY-MM-DD")}&rangeEnd=${moment(dateRange[1]).format("YYYY-MM-DD")}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setStats(response.data)
                }
            }).catch(function(error){
                console.log(error);
                history.push('/404')
                dispatch(hideLoader());
            })
        }
        getBookingHistory();
    }, [dateRange])

    useEffect(() => {
        async function getBookingHistory() {
            const Token = await localStorage.getItem('dvg_user_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/statistic/profitChart?rangeStart=${moment(dateRange[0]).format("YYYY-MM-DD")}&rangeEnd=${moment(dateRange[1]).format("YYYY-MM-DD")}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setChart(response.data)
                }
            }).catch(function(error){
                console.log(error);
                history.push('/404')
                dispatch(hideLoader());
            })
        }
        getBookingHistory();
    }, [dateRange])

    const dateList = chart && chart.labels;
    const series = [
        {
            name: 'Doanh thu',
            data: chart && chart.data
        }
    ]

    return (
        <AccountLayout path={props.location.pathname}>
            <Helmet>
                <title>Thống kê CTV</title>
            </Helmet>
            <div className={"p-3 md:pl-5"}>
                <div className={"flex justify-between items-start"}>
                    <h1 className={"text-2xl text-gray-700 mb-5"}>Thống kê Cộng tác viên</h1>
                    <RangePicker
                        value={dateRange}
                        format={dateFormat}
                        onChange={(v) => handleSetRange(v)}
                    />
                </div>
                <div className={"mt-5"}>
                    <Stats stats={stats && stats} />
                    <LineChart dateList={dateList} series={series} title="Biều đồ doanh thu" />
                </div>
            </div>
        </AccountLayout>
    );
}

export default Statistics;
