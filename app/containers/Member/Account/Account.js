import React from 'react';
import AccountLayout from './components/AccountLayout';
import { useSelector } from 'react-redux';
import AccountUpdateForm from './components/AccountUpdateForm';
import { Helmet } from 'react-helmet/es/Helmet';
import Loading from '../../../components/Loading';

Account.propTypes = {

};

function Account(props) {
    const currentUser = useSelector(state => state.root.currentUser.user);

    return (
        <AccountLayout path={props.location.pathname} title={"Tài khoản"}>
            <Helmet>
                <title>Thông tin tài khoản</title>
            </Helmet>
            <div className={"p-3 md:pl-5"}>
                <h1 className={"text-2xl text-gray-700"}>Thông tin tài khoản</h1>
                <div className={"mt-5"}>
                    {!currentUser ? <Loading/> :
                        <AccountUpdateForm initialValues={currentUser && currentUser}/>
                    }
                </div>
            </div>
        </AccountLayout>
    );
}

export default Account;
