import React, {useState, useEffect} from 'react';
import AccountLayout from '../Account/components/AccountLayout';
import { useDispatch, useSelector } from 'react-redux';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import { IoCash, IoPersonAdd, IoStatsChart } from 'react-icons/all';
import { formatDateTime, formatDateUS, formatVND } from '../../../utils/helpers';
import { changeModalContent, changeModalTitle, hideLoader, hideModal, showLoader, showModal } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import history from '../../../utils/history';
import { Space, Table, Tag } from 'antd';
import Button from 'antd/es/button';
import { EditTwoTone } from '@ant-design/icons';
import Card from 'antd/es/card';
import { ErrorMessage, SuccessMessage } from '../../../components/Message';
import { TransactionStatus, TransactionType } from '../../../models/transaction.model';
import { Helmet } from 'react-helmet/es/Helmet';

function MonthlyRewards(props) {
    const dispatch = useDispatch()
    const settings = useSelector(state => state.root.settings.options);
    const currentUser = useSelector(state => state.root.currentUser.user);
    const [transactions, setTransactions] = useState();

    useEffect(() => {
        dispatch(showLoader())
        async function getPackageInfo() {
            const Token = localStorage.getItem('dvg_user_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/customer/rewards`,
                params: {type: "MONTH"},
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setTransactions(response.data)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                history.push('/404')
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getPackageInfo();
    }, [dispatch])

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Doanh số',
            dataIndex: 'sales',
            key: 'sales',
            render: (text, row) =>
                <span className={"font-medium"}>{formatVND(row.sales)}</span>
        },
        {
            title: 'Tiền thưởng',
            dataIndex: 'amount',
            key: 'amount',
            render: (text, row) =>
                <span className={"font-medium"}>{formatVND(row.amount)}</span>
        },
        {
            title: 'Phần trăm',
            dataIndex: 'percent',
            key: 'percent',
            render: (text, row) =>
                <span className={"font-medium"}>{row.percent}%</span>
        },
        {
            title: 'Từ ngày',
            dataIndex: 'startDate',
            key: 'startDate',
            render: (key, row) => formatDateTime(row.startDate)
        },
        {
            title: 'Đến ngày',
            dataIndex: 'endDate',
            key: 'endDate',
            render: (key, row) => formatDateTime(row.endDate)
        },
    ]

    return (
        <AccountLayout path={props.location.pathname} title={"Thưởng tháng"}>
            <Helmet>
                <title>Thưởng tháng</title>
            </Helmet>
            <div className={"p-3 md:pl-5"}>
                <div className={"mb-5 flex items-center justify-between"}>
                    <h1 className={"text-2xl text-gray-700"}>Thưởng tháng</h1>
                </div>
                <div>
                    <Card
                        size={"small"}
                        title={"Danh sách thưởng tháng"}
                    >
                        <Table
                            scroll={{x: '100%'}}
                            columns={columns}
                            dataSource={transactions && transactions}
                            pagination={false}
                        />
                    </Card>

                </div>
            </div>
        </AccountLayout>
    );
}

export default MonthlyRewards;
