import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { formatDateUS, formatVND } from '../../../utils/helpers';
import AccountLayout from '../Account/components/AccountLayout';
import Loading from '../../../components/Loading';
import Button from 'antd/es/button';
import history from '../../../utils/history';
import { IoArchive, IoArrowBack, IoCart, IoWallet } from 'react-icons/io5';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import { hideLoader, showLoader } from '../../App/actions';
import { OrderStatus, PaymentMethod } from '../../../models/order.model';

function OrderDetails(props) {
    const dispatch = useDispatch();
    const id = props.match.params.id;
    const [result, setResult] = useState();
    const settings = useSelector(state => state.root.settings.options);

    useEffect(() => {
        dispatch(showLoader());
        async function getInfo() {
            const Token = localStorage.getItem('dvg_user_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/customer/order/${id}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setResult(response.data)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                history.push('/404')
                dispatch(hideLoader())
                console.log(error);
            })
        }
        getInfo();
    }, [dispatch, id])

    return (
        !result ? <Loading /> :
        <AccountLayout path={props.location.pathname} title={"Chi tiết đơn hàng"}>
            <div className={"p-3 md:pl-5"}>
                <div className={"flex items-center justify-between mb-5"}>
                    <h1 className={"text-2xl text-gray-700"}>Đơn hàng #{result.order.id}<span className={"text-sm font-medium block"}>{OrderStatus.map((item) => (item.code === result.order.status && item.name))}</span></h1>
                    <Button
                        className={"rounded"}
                        icon={<IoArrowBack className={"mr-2"} />}
                        onClick={() => history.goBack()}
                    >
                        Quay lại
                    </Button>
                </div>
                <div>
                    <Row gutter={[16, 16]}>
                        <Col xs={24} lg={12} md={12}>
                            <div id={"thong-tin-dich-vu"} className={"bg-white px-5 py-3 rounded border border-gray-300 mb-5"}>
                                <div className={"flex justify-between items-center mb-3 border-b border-gray-200 pb-3"}>
                                    <h2 className={"text-base font-bold text-gray-600"}><IoArchive size={20} className={"-mt-1 mr-2 text-green-600"} /> Thông tin nhận hàng</h2>
                                </div>
                                <div>
                                    <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                        <p className={"text-gray-500"}>Họ tên</p>
                                        <p className={"font-medium text-gray-800"}>{result && result.order.guestName}</p>
                                    </div>
                                    <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                        <p className={"text-gray-500"}>Số điện thoại</p>
                                        <p className={"font-medium text-gray-800"}>{result && result.order.guestPhone}</p>
                                    </div>
                                    <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                        <p className={"text-gray-500"}>Email</p>
                                        <p className={"font-medium text-gray-800"}>{result && result.order.guestEmail}</p>
                                    </div>
                                    <div className={"flex justify-between flex-wrap pt-3"}>
                                        <p className={"text-gray-500"}>Địa chỉ</p>
                                        <p className={"font-medium text-gray-800"}>{result && result.order.guestAddress}</p>
                                    </div>
                                </div>
                            </div>
                            <div id={"thong-tin-dich-vu"} className={"bg-white px-5 py-3 rounded border border-gray-300"}>
                                <div className={"flex justify-between items-center mb-3 border-b border-gray-200 pb-3"}>
                                    <h2 className={"text-base font-bold text-gray-600"}><IoWallet size={20} className={"-mt-1 mr-2 text-red-600"} /> Thông tin thanh toán</h2>
                                </div>
                                <div>
                                    <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                        <p className={"text-gray-500"}>Hình thức thanh toán</p>
                                        <p className={"font-medium text-gray-800"}>{PaymentMethod.map((item, index) => (item.code === result.order.paymentMethod && item.name))}</p>
                                    </div>
                                    <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                        <p className={"text-gray-500"}>Số tiền thanh toán</p>
                                        <p className={"font-medium text-gray-800"}>{result && formatVND(result.order.amount)}</p>
                                    </div>
                                    {result && result.order.paymentMethod === 'COD' &&
                                        <div className={"bg-gray-50 border border-gray-200 px-5 py-3"}>
                                            Thanh toán tiền khi nhận hàng.
                                        </div>
                                    }
                                    {result && result.order.paymentMethod === 'BankTransfer' &&
                                        <div className={"bg-gray-50 border border-gray-200 px-5 py-3"}>
                                            <p className={"mb-2 font-bold"}>Quý khách thanh toán theo thông tin bên dưới:</p>
                                            <p>Số tiền thanh toán: <span className={"font-bold"}>{result && formatVND(result.order.amount)}</span></p>
                                            <div dangerouslySetInnerHTML={{__html: settings && settings.payment_info_company}}/>
                                            <p>Nội dung chuyển tiền: <b>HD-{result && result.order.id}-{result && result.order.guestPhone}</b></p>
                                        </div>
                                    }
                                </div>
                            </div>
                        </Col>
                        <Col xs={24} lg={12} md={12}>
                            <div id={"thong-tin-dich-vu"} className={"bg-white px-5 py-3 rounded border border-gray-300"}>
                                <div className={"flex justify-between items-center mb-3 border-b border-gray-200 pb-3"}>
                                    <h2 className={"text-base font-bold text-gray-600"}><IoCart size={20} className={"-mt-1 mr-2 text-blue-400"} /> Thông tin đơn hàng</h2>
                                </div>
                                {result && result.orderItems && result.orderItems.length > 0 &&
                                    result.orderItems.map((item, index) => (
                                        <div>
                                            <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                                <p className={"text-gray-500"}>{item.productName} - {item.productPrice.name} <span className={"font-bold"}>x {item.quantity}</span></p>
                                                <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>{formatVND(item.totalAmount)}</p>
                                            </div>
                                        </div>
                                    ))
                                }
                                <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                    <p className={"text-gray-500"}>Tạm tính</p>
                                    <p>{formatVND(result && result.order.revenue)}</p>
                                </div>
                                {Number(result && result.order.discount) > 0 &&
                                    <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                        <p className={"text-gray-500"}>Chiết khấu</p>
                                        <p className={"text-red-400"}>-{formatVND(result.order.discount)}</p>
                                    </div>
                                }
                                <div className={"flex flex-row justify-between items-center mt-2"}>
                                    <span>Tổng tiền</span>
                                    <span className={"font-bold text-lg"}>{formatVND(result && result.order.amount)}</span>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>

            </div>
        </AccountLayout>
    );
}

export default OrderDetails;
