import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet/es/Helmet';
import Button from 'antd/es/button';
import AccountLayout from '../Account/components/AccountLayout';
import { useDispatch, useSelector } from 'react-redux';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import history from '../../../utils/history';
import Loading from '../../../components/Loading';
import { IoAdd, IoChevronBack, IoDocumentOutline, IoFolder, IoPulse } from 'react-icons/all';
import { Link } from 'react-router-dom';
import InnerHTML from 'dangerously-set-html-content'

function PostDetail(props) {
    const slug = props.match.params.slug;
    const dispatch = useDispatch();
    const [result, setResult] = useState()
    const currentUser = useSelector(state => state.root.currentUser.user);
    const settings = useSelector(state => state.root.settings.options);

    useEffect(() => {
        dispatch(showLoader())
        async function getPackageInfo() {
            const Token = localStorage.getItem('dvg_user_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/post/${slug}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setResult(response.data)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                history.push('/404')
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getPackageInfo();
    }, [dispatch, slug])

    console.log(result);

    if (result) {
        var content = result && result.post.content
    }

    var mapText = {
        '[memberPhone]': currentUser && currentUser.phone,
        '[memberId]': currentUser && currentUser.id,
        '[memberZaloGroup]': `<a href=${currentUser && currentUser.zaloGroup}>${currentUser && currentUser.zaloGroup}</a>`,
        '[memberZalo]': `<a href="https://zalo.me/${currentUser && currentUser.zalo}" target="_blank">https://zalo.me/${currentUser && currentUser.zalo}</a>`,
        '[memberFacebook]': `<a href=${currentUser && currentUser.facebook} target="_blank">${currentUser && currentUser.facebook}</a>`,
        '[memberYoutube]': `<a href=${currentUser && currentUser.youtube} target="_blank">${currentUser && currentUser.youtube}</a>`,
        '[memberWhatsapp]': `<a href="https://wa.me/${currentUser && currentUser.whatsapp}" target="_blank">https://wa.me/${currentUser && currentUser.whatsapp}</a>`,
        '[memberEmail]': `<a href="mailto:${currentUser && currentUser.email}" target="_blank">${currentUser && currentUser.email}</a>`,
        '[memberAddress]': currentUser && currentUser.address,
        '[memberRefLink]': `<a href=${settings && settings.website_url}?ref=${currentUser && currentUser.id} target="_blank">${settings && settings.website_url}?ref=${currentUser && currentUser.id}</a>`,
        '[memberName]': currentUser && currentUser.name,
    }

    content = content && content.replace(/\[memberPhone\]|\[memberId\]|\[memberZaloGroup\]|\[memberZalo\]|\[memberFacebook\]|\[memberYoutube\]|\[memberWhatsapp\]|\[memberEmail\]|\[memberAddress\]|\[memberRefLink\]|\[memberName\]/gi, function(matched){
        return mapText[matched]
    })

    return (
        !result ? <Loading /> :
            <AccountLayout path={props.location.pathname} title={result.post.title}>
                <Helmet>
                    <title>{result.post.title}</title>
                </Helmet>
                <div className={"p-3 md:pl-5"}>
                    <div className={"mb-10 hidden md:flex items-center"}>
                        <div
                            onClick={() => history.goBack()}
                            className={"mr-2"}
                        >
                            <IoChevronBack size={24}/>
                        </div>
                        <h1 className={"text-2xl text-gray-700"}>{result.post.title}</h1>
                    </div>

                    {content &&
                        /*<div className={"main-content"} dangerouslySetInnerHTML={{ __html: content && content }}/>*/
                        <InnerHTML html={content && content} />
                    }
                    {result && result.relatedPosts && result.relatedPosts.length > 0 &&
                        <div className={"mt-10"}>
                            <h2 className={"font-bold text-lg mb-3 text-gray-600"}>Bài viết liên quan</h2>
                            {result.relatedPosts.map((item, index) => (
                                <div className={"mb-2 border-b border-gray-100 pb-2"} key={index}>
                                    <Link
                                        to={`/taikhoan/marketing/post/${item.slug}`}
                                    >
                                        <IoAdd className={"-mt-1"} /> {item.title}
                                    </Link>
                                </div>
                            ))}
                        </div>
                    }
                </div>
            </AccountLayout>
    );
}

export default PostDetail;
