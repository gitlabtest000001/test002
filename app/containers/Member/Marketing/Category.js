import React, { useEffect, useState } from 'react';
import { Helmet } from 'react-helmet/es/Helmet';
import Button from 'antd/es/button';
import AccountLayout from '../Account/components/AccountLayout';
import { useDispatch } from 'react-redux';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { formatDateUS } from '../../../utils/helpers';
import history from '../../../utils/history';
import Loading from '../../../components/Loading';
import { IoAdd, IoChevronBack, IoDocumentOutline, IoFolder, IoPulse } from 'react-icons/all';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import { Link } from 'react-router-dom';
import { IoArrowForward } from 'react-icons/io5';

function Marketing(props) {
    const id = props.match.params.id;
    const dispatch = useDispatch();
    const [result, setResult] = useState()

    useEffect(() => {
        dispatch(showLoader())
        async function getPackageInfo() {
            const Token = localStorage.getItem('dvg_user_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/customer/post-category/${id}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setResult(response.data)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                history.push('/404')
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getPackageInfo();
    }, [dispatch, id])

    console.log(result);

    return (
        !result ? <Loading /> :
        <AccountLayout path={props.location.pathname} title={result.category.name}>
            <Helmet>
                <title>{result.category.name}</title>
            </Helmet>
            <div className={"p-3 md:pl-5"}>
                <div className={"mb-5 hidden md:flex items-center"}>
                    <div
                        onClick={() => history.goBack()}
                        className={"mr-2"}
                    >
                        <IoChevronBack size={24}/>
                    </div>
                    <h1 className={"text-2xl text-gray-700"}>{result.category.name}</h1>
                </div>

                {result.childs && result.childs.length > 0 &&
                    <Row gutter={[16, 16]} className={"mb-5"}>
                        {result.childs.map((item, index) => (
                            <Col xs={24} lg={8} md={8} key={index}>
                                <div className={"shadow-lg p-3 border-t border-gray-200 rounded"}>
                                    <Link
                                        to={`/taikhoan/marketing/cat/${item.id}`}
                                    >
                                        <IoFolder className={"-mt-1 mr-1 text-blue-500"}/> {item.name}
                                    </Link>
                                    {item.children && item.children.length > 0 &&
                                        <div className={"mt-2"}>
                                            {item.children.map((child, index) => (
                                                <p key={index} className={"ml-5 mb-2"}>
                                                    <Link
                                                        to={`/taikhoan/marketing/cat/${child.id}`}
                                                    >
                                                        <IoAdd className={"-mt-1 mr-1"}/> {child.name}
                                                    </Link>
                                                </p>
                                            ))}
                                        </div>
                                    }
                                </div>
                            </Col>
                        ))}
                    </Row>
                }

                {result.posts && result.posts.length > 0 ?
                    <div className={"mt-5"}>
                        <p className={"font-bold mb-5"}>Danh sách bài viết</p>
                        {result.posts.map((item, index) => (
                            <div className={"mb-2 border-b border-gray-100 pb-2"}>
                                <Link
                                    to={`/taikhoan/marketing/post/${item.slug}`}
                                >
                                    <IoAdd className={"-mt-1"} /> {item.title}
                                </Link>
                            </div>
                        ))}
                    </div>
                    :
                    <div className={"text-center my-5 text-gray-500"}>
                        <IoDocumentOutline size={30} className={"mb-3"}/>
                        <p>Chưa có nội dung</p>
                    </div>
                }

            </div>
        </AccountLayout>
    );
}

export default Marketing;
