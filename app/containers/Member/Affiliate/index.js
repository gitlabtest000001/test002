import React, { useEffect, useState } from 'react';
import AccountLayout from '../Account/components/AccountLayout';
import Row from 'antd/es/grid/row';
import Col from 'antd/es/grid/col';
import { IoBagCheck, IoBagHandle, IoCash, IoDownload, IoIosContacts, IoPersonAdd, IoStatsChart } from 'react-icons/all';
import { formatDateTime, formatDateUS, formatVND } from '../../../utils/helpers';
import { useDispatch, useSelector } from 'react-redux';
import { IoShareSocial, IoWallet } from 'react-icons/io5';
import Button from 'antd/es/button';
import { changeModalContent, changeModalTitle, hideLoader, hideModal, showLoader, showModal } from '../../App/actions';
import WithdrawForm from '../Wallet/CashWallet/WithdrawForm';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { ErrorMessage, SuccessMessage } from '../../../components/Message';
import history from '../../../utils/history';
import { message, Form, Space, Select, Table } from 'antd';
import { CopyToClipboard } from 'react-copy-to-clipboard';
import { CopyOutlined } from '@ant-design/icons';
import Input from 'antd/es/input';
import { Helmet } from 'react-helmet/es/Helmet';
import MemberTree from './MemberTree';
import Loading from '../../../components/Loading';
import queryString from 'query-string';
import moment from 'moment';
import { Link } from 'react-router-dom';
import { OrderStatus, PaymentMethod } from '../../../models/order.model';
import Search from 'antd/es/input/Search';
import DatePicker from 'antd/es/date-picker';

const { RangePicker } = DatePicker;
const {Option} = Select;

function Affiliate(props) {
    const dispatch = useDispatch()
    const settings = useSelector(state => state.root.settings.options);
    const currentUser = useSelector(state => state.root.currentUser.user);
    const [users, setUsers] = useState()
    let searchQuery = queryString.parse(props.location.search);
    const [orders, setOrders] = useState();
    const [limit, setLimit] = useState(searchQuery && searchQuery.limit ? searchQuery.limit : 10);
    const [page, setPage] = useState(searchQuery && searchQuery.page ? searchQuery.page : 1);
    const [paymentMethod, setPaymentMethod] = useState(searchQuery && searchQuery.paymentMethod ? searchQuery.paymentMethod : 'ALL');
    const [status, setStatus] = useState(searchQuery && searchQuery.status? searchQuery.status : 'ALL');
    const [search, setSearch] = useState(null);
    const [dateRange, setDateRange] = useState(
        [
            moment.utc(moment().clone().startOf('month').format('YYYY-MM-DD')),
            moment.utc(moment().clone().endOf('month').format("YYYY-MM-DD"))
        ]
    )

    function handleSetRange (v) {
        setDateRange(v);
    }

    useEffect(() => {
        dispatch(showLoader())
        async function getPackageInfo() {
            const Token = await localStorage.getItem('dvg_user_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/customer/order/child`,
                params: {limit, page, paymentMethod, status, query:search, rangeStart: formatDateUS(dateRange[0]), rangeEnd: formatDateUS(dateRange[1])},
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setOrders(response.data)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                history.push('/404')
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getPackageInfo();
    }, [dispatch, limit, page, paymentMethod, status, search, dateRange])

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
            render: (key, row) =>
                <Link to={`/taikhoan/don-hang/${row.id}`}><span className={"text-blue-500"}>{row.id}</span></Link>
        },
        {
            title: 'Người mua',
            dataIndex: 'buyer',
            key: 'buyer',
            render: (text, row) =>
                row.guestName +' - '+ row.guestPhone
        },
        {
            title: 'Tạm tính',
            dataIndex: 'revenue',
            key: 'revenue',
            render: (text, row) =>
                <span className={"font-medium text-gray-600"}>{formatVND(row.revenue)}</span>
        },
        {
            title: 'Chiết khấu',
            dataIndex: 'discount',
            key: 'discount',
            render: (text, row) =>
                <span className={"text-red-500"}>- {formatVND(row.discount)}</span>
        },
        {
            title: 'Thành tiền',
            dataIndex: 'amount',
            key: 'amount',
            render: (text, row) =>
                <span className={"font-medium text-green-700"}>{formatVND(row.amount)}</span>
        },
        {
            title: 'Hình thức thanh toán',
            dataIndex: 'paymentMethod',
            key: 'paymentMethod',
            render: (key, row) => (
                PaymentMethod.map((item, index) => (
                    item.code === row.paymentMethod && item.name
                ))
            ),
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            key: 'status',
            render: (key, row) => (
                OrderStatus.map((item, index) =>(
                    item.code === row.status && item.name
                ))
            ),
        },
        {
            title: 'Ngày tháng',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: (key, row) => formatDateTime(row.createdAt)
        }
    ]

    function handleChangePage(page, pageSize) {
        setPage(page);
        setLimit(pageSize);
    }

    useEffect(() => {
        dispatch(showLoader());
        async function getUsers() {
            const Token = await localStorage.getItem('dvg_user_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/user/tree`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setUsers(response.data);
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
                history.push('/404')
                dispatch(hideLoader());
            })
        }
        getUsers();
    }, [dispatch])

    function handleCreateWithdraw(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_user_token');
        axios.post(
            `${AppConfig.apiUrl}/customer/transaction/withdraw`,
            data,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Tạo giao dịch thành công'});
            //setTransactions(transactions.list.concat(response.data))
            dispatch(hideLoader())
            dispatch(hideModal())
            history.push(`/taikhoan/giao-dich/${response.data.id}`)
        }).catch((function (error) {
            message.error(error.response.data.message)
            dispatch(hideLoader())
        }))
    }

    function handleOpenModal() {
        dispatch(showModal())
        dispatch(changeModalTitle("Rút tiền"))
        dispatch(changeModalContent(
            <WithdrawForm
                initialValues={{
                    bank_name: currentUser && currentUser.bank_name,
                    bank_account: currentUser && currentUser.bank_account,
                    bank_owner: currentUser && currentUser.bank_owner,
                    bank_brands: currentUser && currentUser.bank_brands,
                    amount: currentUser && currentUser.wallet
                }}
                onSubmit={handleCreateWithdraw}
            />
        ))
    }

    return (
        <AccountLayout path={props.location.pathname} title={"Cộng tác viên"}>
            <Helmet>
                <title>Cộng tác viên</title>
            </Helmet>
            <div className={"p-3 md:pl-5"}>
                <div className={"mb-5 flex items-center justify-between hidden md:flex"}>
                    <h1 className={"text-2xl text-gray-700"}>Cộng tác viên</h1>
                </div>
                <div className={"mb-5"}>
                    <Row gutter={[16, 16]}>
                        <Col xs={24} lg={8} md={8}>
                            <div className={"flex items-center shadow border-t border-gray-50 rounded bg-white px-3 py-2"}>
                                <IoIosContacts size={30} className={"mr-5 text-red-500"}/>
                                <div>
                                    <h4 className={"text-xs"}>Giới thiệu trực tiếp</h4>
                                    <p className={"font-bold text-xl text-gray-800"}>{users && users.countF1}</p>
                                </div>
                            </div>
                        </Col>
                        <Col xs={24} lg={8} md={8}>
                            <div className={"flex items-center shadow border-t border-gray-50 rounded bg-white px-3 py-2"}>
                                <IoShareSocial size={30} className={"mr-5 text-green-500"}/>
                                <div>
                                    <h4 className={"text-xs"}>Doanh số giới thiệu</h4>
                                    <p className={"font-bold text-xl text-gray-800"}>{currentUser && formatVND(currentUser.affiliateSales)}</p>
                                </div>
                            </div>
                        </Col>
                        <Col xs={24} lg={8} md={8}>
                            <div className={"flex items-center shadow border-t border-gray-50 rounded bg-white px-3 py-2"}>
                                <IoBagHandle size={30} className={"mr-5 text-blue-500"}/>
                                <div>
                                    <h4 className={"text-xs"}>Doanh số mua hàng</h4>
                                    <p className={"font-bold text-xl text-gray-800"}>{currentUser && formatVND(currentUser.sales)}</p>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>

                <div className={"mb-5 bg-white border border-gray-200 rounded px-3 py-2"}>
                    <div className={"mb-3"}>Đường dẫn giới thiệu</div>
                    <Input
                        size={"large"}
                        value={`${settings.website_url}?ref=${currentUser && currentUser.id}`}
                        className={"mb-3"}
                    />
                    <CopyToClipboard
                        text={`${settings.website_url}?ref=${currentUser && currentUser.id}`}
                        onCopy={() => SuccessMessage({message: 'Đã sao chép liên kết'})}
                    >
                        <Button type={"primary"} icon={<CopyOutlined />}>
                            Sao chép
                        </Button>
                    </CopyToClipboard>
                </div>
                {/*<div className={"mb-5 bg-white border border-gray-200 rounded px-3 py-2"}>
                    <div className={"mb-3"}><span className={"font-medium"}>Đơn hàng giới thiệu</span></div>
                    <div className={"mb-5"}>
                        <Row gutter={[16, 16]}>
                            <Col xs={24} lg={8} md={12}>
                                <Search
                                    placeholder="Tìm kiếm..."
                                    className={"w-full mt-2"}
                                    onSearch={(value) => setSearch(value)}
                                    allowClear
                                />
                            </Col>
                            <Col xs={24} lg={6} md={12}>
                                <Select
                                    defaultValue="ALL"
                                    className={"w-full mt-2"}
                                    onChange={(value) => setStatus(value) }
                                >
                                    <Option value="ALL">Tất cả trạng thái</Option>
                                    {OrderStatus.map((item, index) => (
                                        <Option value={item.code}>{item.name}</Option>
                                    ))}
                                </Select>
                            </Col>
                            <Col xs={24} lg={6} md={12}>
                                <Select
                                    defaultValue="ALL"
                                    className={"w-full mt-2"}
                                    onChange={(value) => setPaymentMethod(value) }
                                >
                                    <Option value="ALL">Hình thức thanh toán</Option>
                                    {PaymentMethod.map((item, index) => (
                                        <Option value={item.code}>{item.name}</Option>
                                    ))}
                                </Select>
                            </Col>
                            <Col xs={24} lg={12} md={12}>
                                <RangePicker
                                    ranges={{
                                        'Hôm qua': [moment().subtract(1, 'days'), moment()],
                                        'Hôm nay': [moment(), moment().add(1, 'days')],
                                        'Tuần trước': [moment().startOf('week').subtract(7,'days'), moment().endOf('week').subtract(7, 'days')],
                                        'Tuần này': [moment().startOf('week'), moment().endOf('week')],
                                        'Tháng trước': [moment().subtract(1,'months').startOf('month'), moment().subtract(1,'months').endOf('month')],
                                        'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                                        'Năm trước': [moment().subtract(1,'years').startOf('year'), moment().subtract(1,'years').endOf('year')],
                                        'Năm nay': [moment().startOf('year'), moment().endOf('year')],
                                    }}
                                    className={"mt-2"}
                                    value={dateRange}
                                    format={'DD/MM/YYYY'}
                                    onChange={(v) => handleSetRange(v)}
                                />
                            </Col>
                        </Row>
                    </div>
                    <Table
                        scroll={{x: '100%'}}
                        columns={columns}
                        dataSource={orders && orders.list}
                        pagination={{
                            current: page,
                            pageSize: limit,
                            total: orders && orders.count,
                            onChange: ((page, pageSize) => handleChangePage(page, pageSize)),
                            position: ['bottomLeft']
                        }}
                    />
                </div>*/}
                {!users ? <Loading/> :
                    <MemberTree users={users}/>
                }
            </div>
        </AccountLayout>
    );
}

export default Affiliate;
