import React, { useState, useEffect } from 'react';
import { Helmet } from 'react-helmet/es/Helmet';
import PageHeader from 'antd/es/page-header';
import history from '../../../utils/history';
import Button from 'antd/es/button';
import Tree from 'react-animated-tree'
import Card from 'antd/es/card';
import { useDispatch } from 'react-redux';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { hideLoader, showLoader } from '../../App/actions';
import Loading from '../../../components/Loading';
import { UserLevel } from '../../../models/member.model';

function MemberTree(props) {
    const treeStyles = {
        width: '100%',
    }

    const listTree = (arrItem) => {
        return arrItem.map((el) => {
            if(el.children && el.children.length === 0) {
                return <Tree
                    content={el.name + ' - ' + el.phone + ' - ' + el.level}
                    //canHide
                    //onClick={() => (history.push(`/admin/members/detail/${el.id}`))}
                />
            } else {
                return <Tree
                    content={el.name + ' - ' + el.phone + ' - ' + el.level}
                    //canHide
                    //onClick={() => (history.push(`/admin/members/detail/${el.id}`))}
                >
                    {listTree(el.children)}
                </Tree>
            }
        })

    }

    return (
        !props.users ? <Loading /> :
            <>
                <Card>
                    {props.users && props.users.tree.map((el, index) => (
                        <Tree
                            content={el.name + ' - ' + el.phone + ' - ' + el.level}
                            //canHide
                            open
                            style={treeStyles}
                            //onClick={() => (history.push(`/admin/members/detail/${item.id}`))}
                        >
                            {el && el.children.length > 0 ? listTree(el.children): null}
                        </Tree>
                    ))}

                </Card>
            </>
    );
}

export default MemberTree;
