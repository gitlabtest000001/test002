import React from 'react';
import Form from 'antd/es/form';
import Input from 'antd/es/input';
import { InputNumber } from 'antd';
import CheckOutlined from '@ant-design/icons/lib/icons/CheckOutlined';
import Button from 'antd/es/button';

function InvestmentForm(props) {

    function handleSubmit(values) {
        props.onSubmit(values)
    }

    return (
        <Form
            layout="vertical"
            name="UserForm"
            onFinish={handleSubmit}
            initialValues={props.initialValues}
        >
            <Form.Item
                label="Số lượng cổ phần"
                name="quantity"
                rules={[
                    {
                        required: true,
                        message: 'Vui lòng nhập số lượng'},
                ]}
            >
                <InputNumber
                    formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                    parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                    className={"w-full"}
                    size="large"
                />
            </Form.Item>
            <Button
                type="primary"
                htmlType="submit"
                icon={<CheckOutlined />}
            >
                Xác nhận đầu tư
            </Button>
        </Form>
    );
}

export default InvestmentForm;
