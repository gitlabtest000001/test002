import React, {useState, useEffect} from 'react';
import AccountLayout from '../Account/components/AccountLayout';
import { useDispatch, useSelector } from 'react-redux';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import { IoCash, IoPersonAdd, IoStatsChart } from 'react-icons/all';
import { formatDateTime, formatDateUS, formatVND } from '../../../utils/helpers';
import { changeModalContent, changeModalTitle, hideLoader, hideModal, showLoader, showModal } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import history from '../../../utils/history';
import { Space, Table, Tag } from 'antd';
import Button from 'antd/es/button';
import { EditTwoTone } from '@ant-design/icons';
import Card from 'antd/es/card';
import { ErrorMessage, SuccessMessage } from '../../../components/Message';
import InvestmentForm from './InvestmentForm';
import { TransactionStatus, TransactionType } from '../../../models/transaction.model';
import { Helmet } from 'react-helmet/es/Helmet';

function Investments(props) {
    const dispatch = useDispatch()
    const settings = useSelector(state => state.root.settings.options);
    const currentUser = useSelector(state => state.root.currentUser.user);
    const [transactions, setTransactions] = useState();

    useEffect(() => {
        dispatch(showLoader())
        async function getPackageInfo() {
            const Token = localStorage.getItem('dvg_user_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/customer/transaction`,
                params: {type: ['INVESTMENT', 'REVERTINVESTMENT', 'EXCHANGESTOCK'], limit: 100000, page: 1},
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setTransactions(response.data)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                history.push('/404')
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getPackageInfo();
    }, [dispatch])

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Số tiền',
            dataIndex: 'amount',
            key: 'amount',
            render: (text, row) =>
                <span className={"font-medium"}>{formatVND(row.amount)}</span>
        },
        {
            title: 'Loại',
            dataIndex: 'type',
            key: 'type',
            render: (key, row) => (
                TransactionType.map((item, index) => (item.code === row.type && item.name))
            ),
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            key: 'status',
            render: (key, row) => (
                TransactionStatus.map((item, index) => (item.code === row.status && item.name))
            ),
        },
        {
            title: 'Ngày tháng',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: (key, row) => formatDateTime(row.createdAt)
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: (key, row) => (
                <Space size={'middle'}>
                    <Button
                        type="primary"
                        size="small"
                        ghost
                        icon={<EditTwoTone />}
                        onClick={() => history.push(`/taikhoan/giao-dich/${row.id}`)}
                    >
                        Chi tiết
                    </Button>
                </Space>
            )
        }
    ]

    function createInvestment(data) {
        dispatch(showLoader())
        const Token = localStorage.getItem('dvg_user_token');
        axios.post(
            `${AppConfig.apiUrl}/customer/transaction/investment`,
            data,
            {headers: {Authorization: `Bearer ${Token}`}}
        ).then(function (response) {
            SuccessMessage({message: 'Tạo giao dịch thành công'});
            setTransactions(transactions.list.concat(response.data))
            dispatch(hideLoader())
            dispatch(hideModal())
            history.push(`/taikhoan/giao-dich/${response.data.id}`)
        }).catch((function (error) {
            ErrorMessage(error.response.data)
            dispatch(hideLoader())
        }))
    }

    function handleOpenModal() {
        dispatch(showModal());
        dispatch(changeModalTitle("Đầu tư cổ phần"))
        dispatch(changeModalContent(
            <div>
                <p>Giá cổ phần hiện tại: {settings && formatVND(settings.stock_price)}</p>
                <InvestmentForm
                    settings={settings}
                    onSubmit={createInvestment}
                />
            </div>
        ))
    }

    return (
        <AccountLayout path={props.location.pathname} title={"Đầu tư cổ phần"}>
            <Helmet>
                <title>Đầu tư cổ phần</title>
            </Helmet>
            <div className={"p-3 md:pl-5"}>
                <div className={"mb-5 flex items-center justify-between"}>
                    <h1 className={"text-2xl text-gray-700"}>Đầu tư cổ phần</h1>
                    <Button
                        type={"primary"}
                        className={"bg-green-600 rounded"}
                        onClick={handleOpenModal}
                    >
                        Đầu tư ngay
                    </Button>
                </div>
                <div className={"mb-5"}>
                    <Row gutter={[16, 16]}>
                        <Col xs={24} lg={8} md={8}>
                            <div className={"flex items-center shadow border-t border-gray-50 rounded bg-white px-3 py-2"}>
                                <IoPersonAdd size={30} className={"mr-5 text-blue-500"}/>
                                <div>
                                    <h4 className={"text-xs"}>Cổ phần sở hữu</h4>
                                    <p className={"font-bold text-xl text-gray-800"}>{currentUser ? Number(currentUser.stock).toFixed(0) : 0}</p>
                                </div>
                            </div>
                        </Col>
                        <Col xs={24} lg={8} md={8}>
                            <div className={"flex items-center shadow border-t border-gray-50 rounded bg-white px-3 py-2"}>
                                <IoStatsChart size={30} className={"mr-5 text-red-500"}/>
                                <div>
                                    <h4 className={"text-xs"}>Giá cổ phần</h4>
                                    <p className={"font-bold text-xl text-gray-800"}>{settings && settings.stock_price && formatVND(settings.stock_price)}</p>
                                </div>
                            </div>
                        </Col>
                        <Col xs={24} lg={8} md={8}>
                            <div className={"flex items-center shadow border-t border-gray-50 rounded bg-white px-3 py-2"}>
                                <IoCash size={30} className={"mr-5 text-green-500"}/>
                                <div>
                                    <h4 className={"text-xs"}>Giá trị quy đổi</h4>
                                    <p className={"font-bold text-xl text-gray-800"}>{currentUser && currentUser.stock ? formatVND(Number(currentUser.stock)*Number(settings.stock_price)) : 0}</p>
                                </div>
                            </div>
                        </Col>
                    </Row>
                </div>
                <div>
                    <Card
                        size={"small"}
                        title={"Danh sách giao dịch đầu tư"}
                    >
                        <Table
                            scroll={{x: '100%'}}
                            columns={columns}
                            dataSource={transactions && transactions.list}
                            pagination={false}
                        />
                    </Card>

                </div>
            </div>
        </AccountLayout>
    );
}

export default Investments;
