import React from 'react';
import { IoAdd, IoDocumentOutline, IoIosContact, IoIosVolumeHigh, IoNewspaper, RiBarChart2Fill } from 'react-icons/all';
import { Link } from 'react-router-dom';

function NewsUpdate(props) {
    const postsList = props.posts
    return (
        <div className={"rounded shadow px-3 py-2"}>
            <div className={"flex items-center justify-between mb-3"}>
                <h3 className={"font-medium"}><IoIosVolumeHigh className={"-mt-1 text-red-500"} size={18} /> Tin tức &amp; Thông báo</h3>
                {/*{postsList && postsList.length > 1 &&
                    <Link className={"text-blue-500"}>Xem tất cả <IoAdd className={"-mt-1"}/></Link>
                }*/}
            </div>
            {postsList && postsList.length > 0 ?
                postsList.map((item, index) => (
                    <div className={`${postsList.length > 1 && 'mb-5 border-b border-gray-200 pb-5'}`}>
                        <h3 className={"font-bold text-green-600 mb-5 text-lg"}>{item.title}</h3>
                        <div className={"main-content"} dangerouslySetInnerHTML={{__html: item.content}}/>
                    </div>
                ))
                :
                <div className={"text-center my-3 text-gray-500"}>
                    <IoDocumentOutline size={30} className={"mb-3"}/>
                    <p>Chưa có nội dung</p>
                </div>
            }

        </div>
    );
}

export default NewsUpdate;
