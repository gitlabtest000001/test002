import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { useDispatch, useSelector } from 'react-redux';
import moment from 'moment';
import { hideLoader, showLoader } from '../../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../../appConfig';
import { formatDateTime, formatDateUS, formatVND } from '../../../../utils/helpers';
import history from '../../../../utils/history';
import { Space, Table, Tag } from 'antd';
import Button from 'antd/es/button';
import { EditTwoTone } from '@ant-design/icons';
import { Helmet } from 'react-helmet/es/Helmet';
import AccountLayout from '../../Account/components/AccountLayout';
import Card from 'antd/es/card';
import DatePicker from 'antd/es/date-picker';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import { ErrorMessage, SuccessMessage } from '../../../../components/Message';
import { IoGift } from 'react-icons/io5';
import { GetMe, LoadDataAction } from '../../Auth/actions';
import WithdrawForm from './WithdrawForm';
import { IoCash } from 'react-icons/all';
import { TransactionStatus, TransactionType } from '../../../../models/transaction.model';
import Loading from '../../../../components/Loading';
import Tooltip from 'antd/es/tooltip';

const { RangePicker } = DatePicker;

CashWallet.propTypes = {

};

function CashWallet(props) {
    const dispatch = useDispatch()
    const settings = useSelector(state => state.root.settings.options);
    const currentUser = useSelector(state => state.root.currentUser.user);
    const [transactions, setTransactions] = useState();
    const [limit, setLimit] = useState(10);
    const [page, setPage] = useState(1);
    const [flag, setFlag] = useState(true);
    const [dateRange, setDateRange] = useState(
        [
            moment.utc(moment().clone().startOf('month').format('YYYY-MM-DD')),
            moment.utc(moment().clone().endOf('month').format("YYYY-MM-DD"))
        ]
    )

    function handleSetRange (v) {
        setDateRange(v);
    }

    useEffect(() => {
        dispatch(showLoader())
        async function getPackageInfo() {
            const Token = await localStorage.getItem('dvg_user_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/customer/transaction`,
                params: {
                    limit,
                    page,
                    rangeStart: formatDateUS(dateRange[0]),
                    rangeEnd: formatDateUS(dateRange[1]),
                    wallet: 'CashWallet'
                },
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setTransactions(response.data)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                history.push('/404')
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getPackageInfo();
    }, [dispatch, limit, page, dateRange, flag])

    console.log(transactions);

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Số tiền',
            dataIndex: 'amount',
            key: 'amount',
            render: (text, row) =>
                <Tooltip title={row.note} color={'blue'}>
                    <span className={"font-medium"}>{formatVND(row.amount)}</span>
                </Tooltip>
        },
        {
            title: 'Loại',
            dataIndex: 'type',
            key: 'type',
            render: (key, row) => (
                TransactionType.map((item, index) => (item.code === row.type && item.name))
            ),
        },
        {
            title: 'Trạng thái',
            dataIndex: 'status',
            key: 'status',
            render: (key, row) => (
                TransactionStatus.map((item, index) => (item.code === row.status && item.name))
            ),
        },
        {
            title: 'Ngày tháng',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: (key, row) => formatDateTime(row.createdAt)
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: (key, row) => (
                <Space size={'middle'}>
                    <Button
                        type="primary"
                        size="small"
                        ghost
                        icon={<EditTwoTone />}
                        onClick={() => history.push(`/taikhoan/giao-dich/${row.id}`)}
                    >
                        Chi tiết
                    </Button>
                </Space>
            )
        }
    ]

    function handleChangePage(page, pageSize) {
        setPage(page);
        setLimit(pageSize);
    }

    function handleWithdraw(data) {
        dispatch(showLoader());
        const Token = localStorage.getItem('dvg_user_token');
        axios({
            method: 'post',
            url: `${AppConfig.apiUrl}/customer/transaction/withdraw`,
            data,
            headers: {Authorization: `Bearer ${Token}`}
        }).then(function (response) {
            if (response.status === 201) {
                //history.push(`/admin/posts/edit/${response.data.id}`)
                SuccessMessage({message: 'Đã thực hiện yêu cầu rút tiền về ngân hàng, vui lòng chờ xét duyệt.'})
                setFlag(!flag);
                dispatch(hideLoader());
                dispatch(GetMe(Token))
            }
        }).catch(function(error){
            ErrorMessage(error.response.data)
            console.log(error);
            dispatch(hideLoader())
        })
    }

    return (
        !currentUser ? <Loading /> :
        <AccountLayout path={props.location.pathname} title={"Ví tiền mặt"}>
            <Helmet>
                <title>Ví tiền mặt</title>
            </Helmet>
            <div className={"p-3 md:pl-5"}>
                <div className={"mb-5 flex items-center justify-between hidden md:flex"}>
                    <h1 className={"text-2xl text-gray-700"}>Ví tiền mặt</h1>
                    <Button
                        onClick={() => history.goBack()}
                    >
                        Quay lại
                    </Button>
                </div>
                <div className={"mb-5"}>
                    <Row gutter={[16, 16]}>
                        <Col xs={24} lg={8} md={8}>
                            <div>
                                <div
                                    className={"flex items-center shadow border-t border-gray-50 rounded bg-white px-3 py-2 cursor-pointer group bg-blue-500 mb-5"}
                                >
                                    <IoCash size={30} className={"mr-5 text-white"}/>
                                    <div>
                                        <h4 className={"text-xs text-white"}>Số dư khả dụng</h4>
                                        <p className={"font-bold text-lg text-white"}>{currentUser && formatVND(currentUser.cash_wallet)}</p>
                                    </div>
                                </div>
                                <h3 className={"mb-5 font-medium"}>Số tiền nhỏ nhất có thể rút: {formatVND(settings && settings.minimum_withdraw_amount)}</h3>
                                <p>Sử dụng form rút tiền để rút tiền về tài khoản ngân hàng.</p>
                            </div>
                        </Col>
                        <Col xs={24} lg={16} md={16}>
                            <div className={"bg-white"}>
                                <WithdrawForm
                                    onSubmit={handleWithdraw}
                                    initialValues={{
                                        bank_name: currentUser && currentUser.bank_name,
                                        bank_account: currentUser && currentUser.bank_account,
                                        bank_owner: currentUser && currentUser.bank_owner,
                                        bank_brand: currentUser && currentUser.bank_brand,
                                        amount: currentUser && currentUser.cash_wallet
                                    }}
                                />
                            </div>
                        </Col>
                    </Row>
                </div>
                <div className={"mb-5"}>
                    <Card
                        title={"Giao dịch trong Ví tiền mặt"}
                        size={"small"}
                        extra={
                            <RangePicker
                                ranges={{
                                    'Hôm qua': [moment().subtract(1, 'days'), moment()],
                                    'Hôm nay': [moment(), moment().add(1, 'days')],
                                    'Tuần trước': [moment().startOf('week').subtract(7,'days'), moment().endOf('week').subtract(7, 'days')],
                                    'Tuần này': [moment().startOf('week'), moment().endOf('week')],
                                    'Tháng trước': [moment().subtract(1,'months').startOf('month'), moment().subtract(1,'months').endOf('month')],
                                    'Tháng này': [moment().startOf('month'), moment().endOf('month')],
                                    'Năm trước': [moment().subtract(1,'years').startOf('year'), moment().subtract(1,'years').endOf('year')],
                                    'Năm nay': [moment().startOf('year'), moment().endOf('year')],
                                }}
                                className={"mt-2"}
                                value={dateRange}
                                format={'DD/MM/YYYY'}
                                onChange={(v) => handleSetRange(v)}
                            />
                        }
                    >
                        <Table
                            size={"small"}
                            scroll={{x: '100%'}}
                            columns={columns}
                            dataSource={transactions && transactions.list}
                            pagination={{
                                current: page,
                                pageSize: limit,
                                total: transactions && transactions.count,
                                onChange: ((page, pageSize) => handleChangePage(page, pageSize)),
                                position: ['bottomLeft']
                            }}
                        />
                    </Card>
                </div>
            </div>
        </AccountLayout>
    );
}

export default CashWallet;
