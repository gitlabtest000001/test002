import React from 'react';
import Form from 'antd/es/form';
import Input from 'antd/es/input';
import { InputNumber, Select } from 'antd';
import CheckOutlined from '@ant-design/icons/lib/icons/CheckOutlined';
import Button from 'antd/es/button';
import { banks } from '../../../../utils/DefaultLists/bankList';

const {Option} = Select;

function WithdrawForm(props) {

    function handleSubmit(values) {
        props.onSubmit(values)
    }

    return (
        <div className={"border border-gray-200 rounded-md p-5"}>
            <Form
                layout="vertical"
                name="UserForm"
                onFinish={handleSubmit}
                initialValues={props.initialValues}
            >
                <Form.Item
                    label="Tên ngân hàng"
                    name="bank_name"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập thông tin'},
                    ]}
                >
                    <Select
                        showSearch={true}
                        allowClear
                        style={{ width: '100%' }}
                        size="large"
                    >
                        {
                            banks.map(bank =>(
                                <Option key={bank.code} value={bank.name}>{bank.name}</Option>
                            ))
                        }
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Chủ tài khoản"
                    name="bank_owner"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập thông tin'},
                    ]}
                >
                    <Input
                        size="large"
                    />
                </Form.Item>
                <Form.Item
                    label="Số tài khoản"
                    name="bank_account"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập thông tin'},
                    ]}
                >
                    <Input
                        size="large"
                    />
                </Form.Item>
                <Form.Item
                    label="Chi nhánh"
                    name="bank_brand"
                >
                    <Input
                        size="large"
                    />
                </Form.Item>
                <Form.Item
                    label="Số tiền muốn rút"
                    name="amount"
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập số lượng'},
                    ]}
                >
                    <InputNumber
                        formatter={value => `${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ',')}
                        parser={value => value.replace(/\$\s?|(,*)/g, '').substring(0, 15)}
                        className={"w-full"}
                        size="large"
                    />
                </Form.Item>
                <Button
                    type="primary"
                    htmlType="submit"
                    icon={<CheckOutlined />}
                >
                    Rút tiền
                </Button>
            </Form>
        </div>
    );
}

export default WithdrawForm;
