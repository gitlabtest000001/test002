import React from 'react';
import { IoCheckmarkCircle, IoIosContact, IoRemoveCircle, RiBarChart2Fill } from 'react-icons/all';
import Loading from '../../../components/Loading';
import { formatVND } from '../../../utils/helpers';
import { Progress } from 'antd';
import { UserLevel } from '../../../models/member.model';

function LevelInfo(props) {
    const user = props.currentUser;
    const settings = props.settings;

    return (
        !user ? <Loading /> :
        <div className={"rounded shadow px-3 py-2"}>
            <h3 className={"mb-3 font-medium"}><RiBarChart2Fill className={"-mt-1 text-blue-500"} size={18} /> Doanh số &amp; cấp bậc</h3>

            <div className={"mb-5"}>
                <p className={"text-gray-500"}>Doanh số: <span className={"font-bold text-gray-700"}>{formatVND(user.sales)}</span></p>
                <p className={"text-gray-500"}>Cấp bậc: <span className={"font-bold text-gray-700"}>{UserLevel.map((item) => item.code === user.level && item.name)}</span></p>
                {user.officeStatus && user.officeStatus === 'TRUE' &&
                    <p className={"text-gray-500"}>Bạn đang là <span className={"font-bold text-gray-700"}>Văn phòng kinh doanh</span></p>
                }
            </div>

            <div>
                {(Number(user.sales) < Number(settings.upgrade_to_agency_level3_value)) && (Number(user.sales) >= Number(settings.upgrade_to_agency_level2_value))
                    &&
                    <div className={"mb-5"}>
                        Bạn cần <span className={"font-medium text-red-600"}>{formatVND(Number(settings.upgrade_to_agency_level3_value) - Number(user.sales))}</span> nữa để lên cấp <span className={"font-bold text-green-600"}>Đại lý cấp 3</span>
                        <Progress
                            percent={(Number(user.sales) / Number(settings.upgrade_to_agency_level3_value)) *100}
                            status="active"
                            showInfo={false}
                        />
                    </div>
                }
                {(Number(user.sales) < Number(settings.upgrade_to_agency_level2_value)) && (Number(user.sales) >= Number(settings.upgrade_to_agency_level1_value))
                    &&
                    <div className={"mb-5"}>
                        Bạn cần <span className={"font-medium text-red-600"}>{formatVND(Number(settings.upgrade_to_agency_level2_value) - Number(user.sales))}</span> nữa để lên cấp <span className={"font-bold text-green-600"}>Đại lý cấp 2</span>
                        <Progress
                            percent={(Number(user.sales) / Number(settings.upgrade_to_agency_level2_value)) *100}
                            status="active"
                            showInfo={false}
                        />
                    </div>
                }
                {(Number(user.sales) < Number(settings.upgrade_to_agency_level1_value)) && (Number(user.sales) >= Number(settings.upgrade_to_agency_value))
                    &&
                    <div className={"mb-5"}>
                        Bạn cần <span className={"font-medium text-red-600"}>{formatVND(Number(settings.upgrade_to_agency_level1_value) - Number(user.sales))}</span> nữa để lên cấp <span className={"font-bold text-green-600"}>Đại lý cấp 1</span>
                        <Progress
                            percent={(Number(user.sales) / Number(settings.upgrade_to_agency_level1_value)) *100}
                            status="active"
                            showInfo={false}
                        />
                    </div>
                }
                {(Number(user.sales) < Number(settings.upgrade_to_agency_value))
                    &&
                    <div className={"mb-5"}>
                        Bạn cần <span className={"font-medium text-red-600"}>{formatVND(Number(settings.upgrade_to_agency_value) - Number(user.sales))}</span> nữa để lên cấp <span className={"font-bold text-green-600"}>Đại lý</span>
                        <Progress
                            percent={(Number(user.sales) / Number(settings.upgrade_to_agency_value)) *100}
                            status="active"
                            showInfo={false}
                        />
                    </div>
                }
            </div>

            <div>
                <div className={"flex items-center justify-between border-b border-gray-200 pb-2 mb-2"}>
                    <p className={"text-gray-500"}>Doanh số lên cấp</p>
                    <p className={"text-gray-500"}>Cấp bậc</p>
                </div>
                <div className={"flex items-center justify-between border-b border-gray-200 pb-2 mb-2"}>
                    <p>&ge; {formatVND(settings.upgrade_to_agency_value)}</p>
                    <div className={"flex items-center"}>
                        <p className={"mr-2 font-medium"}>Đại lý</p>
                        {(Number(user.sales) >= Number(settings.upgrade_to_agency_value) && (Number(user.sales) < Number(settings.upgrade_to_agency_level1_value))) ? <p><IoCheckmarkCircle className={"text-red-500 -mt-1"}/></p> : <p><IoRemoveCircle className={"text-gray-200 -mt-1"}/></p> }
                    </div>
                </div>
                <div className={"flex items-center justify-between border-b border-gray-200 pb-2 mb-2"}>
                    <p>&ge; {formatVND(settings.upgrade_to_agency_level1_value)}</p>
                    <div className={"flex items-center"}>
                        <p className={"mr-2 font-medium"}>Đại lý cấp 1</p>
                        {(Number(user.sales) >= Number(settings.upgrade_to_agency_level1_value) && (Number(user.sales) < Number(settings.upgrade_to_agency_level2_value))) ? <p><IoCheckmarkCircle className={"text-red-500 -mt-1"}/></p> : <p><IoRemoveCircle className={"text-gray-200 -mt-1"}/></p> }
                    </div>
                </div>
                <div className={"flex items-center justify-between border-b border-gray-200 pb-2 mb-2"}>
                    <p>&ge; {formatVND(settings.upgrade_to_agency_level2_value)}</p>
                    <div className={"flex items-center"}>
                        <p className={"mr-2 font-medium"}>Đại lý cấp 2</p>
                        {(Number(user.sales) >= Number(settings.upgrade_to_agency_level2_value) && (Number(user.sales) < Number(settings.upgrade_to_agency_level3_value))) ? <p><IoCheckmarkCircle className={"text-red-500 -mt-1"}/></p> : <p><IoRemoveCircle className={"text-gray-200 -mt-1"}/></p> }
                    </div>
                </div>
                <div className={"flex items-center justify-between"}>
                    <p>&ge; {formatVND(settings.upgrade_to_agency_level3_value)}</p>
                    <div className={"flex items-center"}>
                        <p className={"mr-2 font-medium"}>Đại lý cấp 3</p>
                        {Number(user.sales) >= Number(settings.upgrade_to_agency_level3_value) ? <p><IoCheckmarkCircle className={"text-red-500 -mt-1"}/></p> : <p><IoRemoveCircle className={"text-gray-200 -mt-1"}/></p> }
                    </div>
                </div>
            </div>

        </div>
    );
}

export default LevelInfo;
