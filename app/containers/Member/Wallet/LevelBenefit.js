import React from 'react';
import { IoCheckmarkCircle, IoRemoveCircle, RiAwardFill, RiBarChart2Fill } from 'react-icons/all';
import { formatVND } from '../../../utils/helpers';
import Loading from '../../../components/Loading';

function LevelBenefit(props) {
    const settings = props.settings;
    return (
        !settings ? <Loading /> :
        <div className={"rounded shadow px-3 py-2"}>
            <h3 className={"mb-3 font-medium"}><RiAwardFill className={"-mt-1 text-yellow-500"} size={18} /> Quyền lợi của các cấp</h3>
            <div>
                <div className={"flex items-center justify-between border-b border-gray-200 pb-2 mb-2"}>
                    <p className={"text-gray-500"}>Cấp bậc</p>
                    <p className={"text-gray-500"}>Quyền lợi</p>
                </div>
                <div className={"flex items-center justify-between border-b border-gray-200 pb-2 mb-2"}>
                    <p className={"font-medium"}>Đại lý</p>
                    <div className={"flex items-center"}>
                        <p>Chiết khấu trực tiếp <span className={"font-bold"}>{settings && settings.agency_order_value_gt_percent}%</span></p>
                    </div>
                </div>
                <div className={"flex items-center justify-between flex-wrap border-b border-gray-200 pb-2 mb-2"}>
                    <p className={"font-medium"}>Đại lý cấp 1</p>
                    <div className={"flex items-center"}>
                        <p>Chiết khấu trực tiếp <span className={"font-bold"}>{settings && settings.agency_discount_level1}%</span> + thưởng <span className={"font-bold"}>{settings && settings.reward_agency_level1}%</span> vào ví mua hàng</p>
                    </div>
                </div>
                <div className={"flex items-center justify-between flex-wrap border-b border-gray-200 pb-2 mb-2"}>
                    <p className={"font-medium"}>Đại lý cấp 2</p>
                    <div className={"flex items-center"}>
                        <p>Chiết khấu trực tiếp <span className={"font-bold"}>{settings && settings.agency_discount_level2}%</span> + thưởng <span className={"font-bold"}>{settings && settings.reward_agency_level2}%</span> vào ví mua hàng</p>
                    </div>
                </div>
                <div className={"flex items-center justify-between flex-wrap border-b border-gray-200 pb-2 mb-2"}>
                    <p className={"font-medium"}>Đại lý cấp 3</p>
                    <div className={"flex items-center"}>
                        <p>Chiết khấu trực tiếp <span className={"font-bold"}>{settings && settings.agency_discount_level3}%</span> + thưởng <span className={"font-bold"}>{settings && settings.reward_agency_level3}%</span> vào ví mua hàng</p>
                    </div>
                </div>
                <div className={"flex items-center justify-between flex-wrap"}>
                    <p className={"font-medium"}>Văn phòng kinh doanh</p>
                    <div className={"flex items-center"}>
                        <p>Các quyền lợi của <span className={"font-bold"}>Đại lý</span> + thưởng <span className={"font-bold"}>{settings && settings.reward_monthly_office_percent}%</span> doanh số hàng tháng</p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default LevelBenefit;
