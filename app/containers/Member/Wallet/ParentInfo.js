import React from 'react';
import Loading from '../../../components/Loading';
import { IoIosContact, MdOutlineContacts } from 'react-icons/all';

function ParentInfo(props) {
    const parent = props.parent;
    return (
        !parent ? <Loading /> :
        <div className={"rounded shadow px-3 py-2"}>
            <h3 className={"mb-3 font-medium"}><IoIosContact className={"-mt-1 text-blue-500"} size={18} /> Người giới thiệu</h3>
            <div className={"text-center"}>
                <img src={parent.avatar} alt={parent.name} className={"h-40 w-40 object-cover rounded mb-5"}/>
            </div>
            <div className={"flex items-center border-b border-gray-100 pb-2 mb-2"}>
                <p className={"text-gray-500 mr-2"}>Họ và tên:</p>
                <p className={"font-medium"}>{parent.name}</p>
            </div>
            {parent.dateofbirth &&
                <div className={"flex items-center border-b border-gray-100 pb-2 mb-2"}>
                    <p className={"text-gray-500 mr-2"}>Ngày sinh:</p>
                    <p className={"font-medium"}>{parent.dateofbirth}</p>
                </div>
            }
            <div className={"flex items-center border-b border-gray-100 pb-2 mb-2"}>
                <p className={"text-gray-500 mr-2"}>Số điện thoại:</p>
                <p className={"font-medium text-blue-500"}><a href={`tel:${parent.phone}`}>{parent.phone}</a></p>
            </div>
            {parent.address &&
                <div className={"flex items-center flex-wrap border-b border-gray-100 pb-2 mb-2"}>
                    <p className={"text-gray-500 mr-2"}>Địa chỉ:</p>
                    <p className={"font-medium"}>{parent.address}</p>
                </div>
            }
            {parent.zalo &&
                <div className={"flex items-center border-b border-gray-100 pb-2 mb-2"}>
                    <p className={"text-gray-500 mr-2"}>Zalo cá nhân:</p>
                    <p className={"font-medium text-blue-500"}><a href={`https://zalo.me/${parent.zalo}`} target="_blank">{parent.zalo}</a></p>
                </div>
            }
            {parent.zaloGroup &&
                <div className={"flex items-center border-b border-gray-100 pb-2 mb-2"}>
                    <p className={"text-gray-500 mr-2"}>Nhóm Zalo:</p>
                    <p className={"font-medium text-blue-500"}><a href={parent.zaloGroup} target="_blank">{parent.zaloGroup}</a></p>
                </div>
            }
            {parent.whatsapp &&
                <div className={"flex items-center border-b border-gray-100 pb-2 mb-2"}>
                    <p className={"text-gray-500 mr-2"}>Whatsapp:</p>
                    <p className={"font-medium text-blue-500"}><a href={`https://wa.me/${parent.whatsapp}`} target="_blank">{parent.whatsapp}</a></p>
                </div>
            }
            {parent.facebook &&
                <div className={"flex items-center flex-wrap mb-2"}>
                    <p className={"text-gray-500 mr-2"}>Facebook:</p>
                    <p className={"font-medium text-blue-500"}><a href={parent.facebook} target="_blank">{parent.facebook}</a></p>
                </div>
            }
            {parent.youtube &&
                <div className={"flex items-center flex-wrap mb-2"}>
                    <p className={"text-gray-500 mr-2"}>Youtube:</p>
                    <p className={"font-medium text-blue-500"}><a href={parent.youtube} target="_blank">{parent.youtube}</a></p>
                </div>
            }
        </div>
    );
}

export default ParentInfo;
