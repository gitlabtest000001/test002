import React from 'react';
import { IoCheckmarkCircle, IoChevronForward, IoRemoveCircle, RiBarChart2Fill } from 'react-icons/all';
import { IoGift } from 'react-icons/io5';
import Table from 'antd/es/table';
import { formatVND } from '../../../utils/helpers';
import Card from 'antd/es/card';
import { Link } from 'react-router-dom';

function MonthlyReward(props) {
    const settings = props.settings;
    const monthlyRevenue = props.monthlyRevenue;
    return (
        <Card
            title={"Thưởng doanh số tháng"}
            size={'small'}
            extra={
                <Link
                    to={`/taikhoan/thuongthang`}
                    className={"text-blue-500"}
                >
                    Lịch sử thưởng <IoChevronForward className={"-mt-1"}/>
                </Link>
            }
        >
            <div className={"mb-5 bg-blue-50 border border-blue-200 p-2 rounded"}>
                <span className={"mr-3"}>Doanh số tháng này:</span><span className={"font-bold text-blue-500"}>{formatVND(monthlyRevenue)}</span>
            </div>
            <div>
                <div className={"flex items-center justify-between border-b border-gray-200 pb-2 mb-2"}>
                    <p className={"font-medium"}>Doanh số cộng dồn</p>
                    <p className={"font-medium"}>Thưởng</p>
                </div>
                <div className={"flex items-center justify-between border-b border-gray-200 pb-2 mb-2"}>
                    <p>&ge; {formatVND(settings.reward_monthly_level1)}</p>
                    <div className={"flex items-center"}>
                        <p className={"mr-2"}>{settings.reward_monthly_level1_percent}%</p>
                        {(Number(monthlyRevenue) >= Number(settings.reward_monthly_level1) && (Number(monthlyRevenue) < Number(settings.reward_monthly_level2))) ? <p><IoCheckmarkCircle className={"text-green-500 -mt-1"}/></p> : <p><IoRemoveCircle className={"text-gray-200 -mt-1"}/></p> }
                    </div>
                </div>
                <div className={"flex items-center justify-between border-b border-gray-200 pb-2 mb-2"}>
                    <p>&ge; {formatVND(settings.reward_monthly_level2)}</p>
                    <div className={"flex items-center"}>
                        <p className={"mr-2"}>{settings.reward_monthly_level2_percent}%</p>
                        {(Number(monthlyRevenue) >= Number(settings.reward_monthly_level2) && (Number(monthlyRevenue) < Number(settings.reward_monthly_level3))) ? <p><IoCheckmarkCircle className={"text-green-500 -mt-1"}/></p> : <p><IoRemoveCircle className={"text-gray-200 -mt-1"}/></p> }
                    </div>
                </div>
                <div className={"flex items-center justify-between border-b border-gray-200 pb-2 mb-2"}>
                    <p>&ge; {formatVND(settings.reward_monthly_level3)}</p>
                    <div className={"flex items-center"}>
                        <p className={"mr-2"}>{settings.reward_monthly_level3_percent}%</p>
                        {(Number(monthlyRevenue) >= Number(settings.reward_monthly_level3) && (Number(monthlyRevenue) < Number(settings.reward_monthly_level4))) ? <p><IoCheckmarkCircle className={"text-green-500 -mt-1"}/></p> : <p><IoRemoveCircle className={"text-gray-200 -mt-1"}/></p> }
                    </div>
                </div>
                <div className={"flex items-center justify-between border-b border-gray-200 pb-2 mb-2"}>
                    <p>&ge; {formatVND(settings.reward_monthly_level4)}</p>
                    <div className={"flex items-center"}>
                        <p className={"mr-2"}>{settings.reward_monthly_level4_percent}%</p>
                        {(Number(monthlyRevenue) >= Number(settings.reward_monthly_level4) && (Number(monthlyRevenue) < Number(settings.reward_monthly_level5))) ? <p><IoCheckmarkCircle className={"text-green-500 -mt-1"}/></p> : <p><IoRemoveCircle className={"text-gray-200 -mt-1"}/></p> }
                    </div>
                </div>
                <div className={"flex items-center justify-between border-b border-gray-200 pb-2 mb-2"}>
                    <p>&ge; {formatVND(settings.reward_monthly_level5)}</p>
                    <div className={"flex items-center"}>
                        <p className={"mr-2"}>{settings.reward_monthly_level5_percent}%</p>
                        {(Number(monthlyRevenue) >= Number(settings.reward_monthly_level5) && (Number(monthlyRevenue) < Number(settings.reward_monthly_level6))) ? <p><IoCheckmarkCircle className={"text-green-500 -mt-1"}/></p> : <p><IoRemoveCircle className={"text-gray-200 -mt-1"}/></p> }
                    </div>
                </div>
                <div className={"flex items-center justify-between"}>
                    <p>&ge; {formatVND(settings.reward_monthly_level6)}</p>
                    <div className={"flex items-center"}>
                        <p className={"mr-2"}>{settings.reward_monthly_level6_percent}%</p>
                        {Number(monthlyRevenue) >= Number(settings.reward_monthly_level6) ? <p><IoCheckmarkCircle className={"text-green-500 -mt-1"}/></p> : <p><IoRemoveCircle className={"text-gray-200 -mt-1"}/></p> }
                    </div>
                </div>
            </div>
        </Card>
    );
}

export default MonthlyReward;
