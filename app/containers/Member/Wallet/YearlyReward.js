import React from 'react';
import { IoCheckmarkCircle, IoChevronForward, IoRemoveCircle, RiBarChart2Fill } from 'react-icons/all';
import { IoGift } from 'react-icons/io5';
import { formatVND } from '../../../utils/helpers';
import Card from 'antd/es/card';
import { Link } from 'react-router-dom';

function YearlyReward(props) {
    const settings = props.settings;
    const yearlyRevenue = props.yearlyRevenue
    return (
        <Card
            title={"Thưởng doanh số năm"}
          size={'small'}
          extra={
              <Link
                  to={`/taikhoan/thuongnam`}
                  className={"text-blue-500"}
              >
                  Lịch sử thưởng <IoChevronForward className={"-mt-1"}/>
              </Link>
          }
        >
            <div className={"mb-5 bg-blue-50 border border-blue-200 p-2 rounded"}>
                <span className={"mr-3"}>Doanh số năm nay:</span><span className={"font-bold text-blue-500"}>{formatVND(yearlyRevenue)}</span>
            </div>

            <div>
                <div className={"flex items-center justify-between border-b border-gray-200 pb-2 mb-2"}>
                    <p className={"font-medium"}>Doanh số cộng dồn</p>
                    <p className={"font-medium"}>Thưởng</p>
                </div>
                <div className={"flex items-center justify-between border-b border-gray-200 pb-2 mb-2"}>
                    <p>&ge; {formatVND(settings.reward_yearly_level1)}</p>
                    <div className={"flex items-center"}>
                        <p className={"mr-2"}>{settings.reward_yearly_level1_percent}%</p>
                        {(Number(yearlyRevenue) >= Number(settings.reward_yearly_level1) && (Number(yearlyRevenue) < Number(settings.reward_yearly_level2)))? <p><IoCheckmarkCircle className={"text-green-500 -mt-1"}/></p> : <p><IoRemoveCircle className={"text-gray-200 -mt-1"}/></p> }
                    </div>
                </div>
                <div className={"flex items-center justify-between border-b border-gray-200 pb-2 mb-2"}>
                    <p>&ge; {formatVND(settings.reward_yearly_level2)}</p>
                    <div className={"flex items-center"}>
                        <p className={"mr-2"}>{settings.reward_yearly_level2_percent}%</p>
                        {(Number(yearlyRevenue) >= Number(settings.reward_yearly_level2) && (Number(yearlyRevenue) < Number(settings.reward_yearly_level3))) ? <p><IoCheckmarkCircle className={"text-green-500 -mt-1"}/></p> : <p><IoRemoveCircle className={"text-gray-200 -mt-1"}/></p> }
                    </div>
                </div>
                <div className={"flex items-center justify-between border-b border-gray-200 pb-2 mb-2"}>
                    <p>&ge; {formatVND(settings.reward_yearly_level3)}</p>
                    <div className={"flex items-center"}>
                        <p className={"mr-2"}>{settings.reward_yearly_level3_percent}%</p>
                        {(Number(yearlyRevenue) >= Number(settings.reward_yearly_level3) && (Number(yearlyRevenue) < Number(settings.reward_yearly_level4))) ? <p><IoCheckmarkCircle className={"text-green-500 -mt-1"}/></p> : <p><IoRemoveCircle className={"text-gray-200 -mt-1"}/></p> }
                    </div>
                </div>
                <div className={"flex items-center justify-between"}>
                    <p>&ge; {formatVND(settings.reward_yearly_level4)}</p>
                    <div className={"flex items-center"}>
                        <p className={"mr-2"}>{settings.reward_yearly_level4_percent}%</p>
                        {Number(yearlyRevenue) >= Number(settings.reward_yearly_level4) ? <p><IoCheckmarkCircle className={"text-green-500 -mt-1"}/></p> : <p><IoRemoveCircle className={"text-gray-200 -mt-1"}/></p> }
                    </div>
                </div>
            </div>
        </Card>
    );
}

export default YearlyReward;
