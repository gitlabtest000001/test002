import React, { useEffect, useState } from 'react';
import { message, Slider } from 'antd';
import Input from 'antd/es/input';
import Button from 'antd/es/button';
import Form from 'antd/es/form';

function RewardWithdrawForm(props) {
    const [amountCash, setAmountCash] = useState(100);
    const [amountProduct, setAmountProduct] = useState(0);
    const [amountStock, setAmountStock] = useState(0);
    const [loginPassword, setLoginPassword] = useState(null);

    function handleSubmit() {
        if (amountStock === 0 && amountProduct === 0 && amountCash === 0 || !loginPassword || typeof loginPassword === 'undefined') {
            message.error('Vui lòng nhập đủ thông tin')
        } else {
            props.onSubmit({
                amountCash: amountCash.toFixed(0),
                amountProduct: amountProduct.toFixed(0),
                amountStock: amountStock.toFixed(0),
                loginPassword
            })
        }
    }

    return (
        <div className={"border border-gray-200 rounded-md p-5"}>
            <div className={"mb-5"}>
                <h4>Rút về ví tiền mặt (<b>{amountCash.toFixed(0)}%</b>)</h4>
                <Slider
                    min={0}
                    max={100}
                    value={amountCash}
                    onChange={(value) => {
                        setAmountCash(value)
                        setAmountProduct(amountProduct > 0 ? amountProduct * (100 - value)/(amountProduct + amountStock) : 0);
                        setAmountStock(amountStock > 0 ? amountStock * (100 - value)/(amountProduct + amountStock) : 0);
                    }}
                />
            </div>
            <div className={"mb-5"}>
                <h4>Rút về ví tiền hàng (<b>{amountProduct.toFixed(0)}%</b>)</h4>
                <Slider
                    min={0}
                    max={100}
                    value={amountProduct}
                    onChange={(value) => {
                        setAmountProduct(value)
                        setAmountCash(amountCash > 0 ? amountCash * (100 - value)/(amountCash + amountStock) : 0);
                        setAmountStock(amountStock > 0 ? amountStock * (100 - value)/(amountCash + amountStock) : 0);
                    }}
                />
            </div>
            <div className={"mb-5"}>
                <h4>Rút về ví cổ phần (<b>{amountStock.toFixed(0)}%</b>)</h4>
                <Slider
                    min={0}
                    max={100}
                    value={amountStock}
                    onChange={(value) => {
                        setAmountStock(value)
                        setAmountCash(amountCash > 0 ? amountCash * (100 - value)/(amountProduct + amountCash) : 0);
                        setAmountProduct(amountProduct > 0 ? amountProduct * (100 - value)/(amountProduct + amountCash) : 0);
                    }}
                />
            </div>
            <div>
                <Form.Item
                    rules={[
                        {
                            required: true,
                            message: 'Vui lòng nhập mật khẩu đăng nhập'},
                    ]}
                >
                    <Input.Password
                        onChange={(value) => setLoginPassword(value.target.value)}
                        placeHolder={"Xác nhận mật khẩu"}
                    />
                </Form.Item>

                <Button
                    type={"primary"}
                    size={"large"}
                    onClick={() => handleSubmit()}
                >
                    Rút tiền
                </Button>
            </div>
        </div>
    );
}

export default RewardWithdrawForm;
