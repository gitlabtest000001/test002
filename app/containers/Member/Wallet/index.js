import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Helmet } from 'react-helmet/es/Helmet';
import Button from 'antd/es/button';
import Row from 'antd/es/grid/row';
import Col from 'antd/es/grid/col';
import { IoGift, IoWallet } from 'react-icons/io5';
import { formatVND } from '../../../utils/helpers';
import { IoBagCheck, IoCash, IoDownload } from 'react-icons/all';
import AccountLayout from '../Account/components/AccountLayout';
import history from '../../../utils/history';
import ParentInfo from './ParentInfo';
import LevelInfo from './LevelInfo';
import NewsUpdate from './NewsUpdate';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import MonthlyReward from './MonthlyReward';
import YearlyReward from './YearlyReward';
import LevelBenefit from './LevelBenefit';

function Dashboard(props) {
    const settings = useSelector(state => state.root.settings.options);
    const currentUser = useSelector(state => state.root.currentUser.user);
    const [post, setPost] = useState();
    const [stats, setStats] = useState();
    const dispatch = useDispatch()

    useEffect(() => {
        dispatch(showLoader());
        async function getCategory() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/post`,
                params: {
                    catId: -1,
                    limit: 3,
                    status: 'ACTIVE',
                    userOnly: 'TRUE'
                }
            }).then(function (response) {
                if (response.status === 200) {
                    setPost(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
                history.push('/404')
                dispatch(hideLoader());
            })
        }
        getCategory();
    },[dispatch])

    useEffect(() => {
        dispatch(showLoader());
        async function getStats() {
            const Token = localStorage.getItem('dvg_user_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/customer/order/revenuestats`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setStats(response.data);
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
            })
        }
        getStats();
    },[dispatch])

    return (
        <AccountLayout path={props.location.pathname} title={"Dashboard"}>
            <Helmet>
                <title>Dashboard</title>
            </Helmet>
            <div className={"p-3 md:pl-5"}>
                <div className={"mb-5 flex items-center justify-between hidden md:flex"}>
                    <h1 className={"text-2xl text-gray-700"}>Dashboard</h1>
                </div>
                <div className={"mb-5"}>
                    <Row gutter={[16, 16]} className={"mb-5"}>
                        <Col xs={24} lg={6} md={12}>
                            <div
                                className={"flex items-center shadow border-t border-gray-50 rounded bg-white px-3 py-2 cursor-pointer group hover:bg-red-500"}
                                onClick={() => history.push('/taikhoan/vi-thuong')}
                            >
                                <IoGift size={30} className={"mr-5 text-red-500 group-hover:text-white"}/>
                                <div>
                                    <h4 className={"text-xs group-hover:text-white"}>Ví tiền thưởng</h4>
                                    <p className={"font-bold text-lg text-gray-800 group-hover:text-white"}>{currentUser && formatVND(currentUser.reward_wallet)}</p>
                                </div>
                            </div>
                        </Col>
                        <Col xs={24} lg={6} md={12}>
                            <div className={"flex items-center shadow border-t border-gray-50 rounded bg-white px-3 py-2 cursor-pointer group hover:bg-green-500"}
                                 onClick={() => history.push('/taikhoan/vi-tienmat')}>
                                <IoCash size={30} className={"mr-5 text-green-500 group-hover:text-white"}/>
                                <div>
                                    <h4 className={"text-xs group-hover:text-white"}>Ví tiền mặt</h4>
                                    <p className={"font-bold text-lg text-gray-800 group-hover:text-white"}>{currentUser && formatVND(currentUser.cash_wallet)}</p>
                                </div>
                            </div>
                        </Col>
                        <Col xs={24} lg={6} md={12}>
                            <div className={"flex items-center shadow border-t border-gray-50 rounded bg-white px-3 py-2 cursor-pointer group hover:bg-blue-500"}
                                 onClick={() => history.push('/taikhoan/vi-tienhang')}>
                                <IoBagCheck size={30} className={"mr-5 text-blue-500 group-hover:text-white"}/>
                                <div>
                                    <h4 className={"text-xs group-hover:text-white"}>Ví tiền hàng</h4>
                                    <p className={"font-bold text-lg text-gray-800 group-hover:text-white"}>{currentUser && formatVND(currentUser.product_wallet)}</p>
                                </div>
                            </div>
                        </Col>
                        <Col xs={24} lg={6} md={12}>
                            <div className={"flex items-center shadow border-t border-gray-50 rounded bg-white px-3 py-2 cursor-pointer group hover:bg-purple-500"}
                                 onClick={() => history.push('/taikhoan/dau-tu')}>
                                <IoWallet size={30} className={"mr-5 text-purple-500 group-hover:text-white"}/>
                                <div>
                                    <h4 className={"text-xs group-hover:text-white"}>Ví cổ phần</h4>
                                    <p className={"font-bold text-lg text-gray-800 group-hover:text-white"}>{currentUser && currentUser.stock ? formatVND(Number(currentUser.stock)*Number(settings.stock_price)) : 0}</p>
                                </div>
                            </div>
                        </Col>
                    </Row>
                    <Row gutter={[32, 32]} className={"mb-5"}>
                        {currentUser && currentUser.parent &&
                            <Col xs={24} lg={12} md={12}>
                                <ParentInfo
                                    parent={currentUser && currentUser.parent}
                                />
                            </Col>
                        }
                        <Col xs={24} lg={12} md={12}>
                            <LevelInfo
                                currentUser={currentUser}
                                settings={settings}
                            />
                        </Col>
                        <Col xs={24} lg={24} md={24}>
                            <LevelBenefit
                                settings={settings}
                            />
                        </Col>
                        <Col xs={24} lg={12} md={12}>
                            <MonthlyReward
                                settings={settings}
                                monthlyRevenue={stats && stats.currentMonthRevenue}
                            />
                        </Col>
                        <Col xs={24} lg={12} md={12}>
                            <YearlyReward
                                settings={settings}
                                yearlyRevenue={stats && stats.currentYearRevenue}
                            />
                        </Col>
                    </Row>
                    {/*<Row gutter={[16, 16]} className={"mb-5"}>
                    </Row>*/}
                    {/*<Row gutter={[16, 16]}>
                        <Col xs={24} lg={24} md={24}>
                            <NewsUpdate
                                posts={post && post.posts}
                            />
                        </Col>
                    </Row>*/}
                </div>
            </div>
        </AccountLayout>
    );
}

export default Dashboard;
