import React, {useEffect, useState} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import AccountLayout from '../Account/components/AccountLayout';
import { Alert } from 'antd';
import Loading from '../../../components/Loading';
import { formatDateTime, formatVND } from '../../../utils/helpers';
import { Link } from 'react-router-dom';
import Card from 'antd/es/card';
import Table from 'antd/es/table';
import Space from 'antd/es/space';
import Button from 'antd/es/button';
import EditTwoTone from '@ant-design/icons/lib/icons/EditTwoTone';
import history from '../../../utils/history';
import Tag from 'antd/es/tag';
import { hideLoader, showLoader } from '../../App/actions';
import { TransactionStatus, TransactionType } from '../../../models/transaction.model';
import { MemberWallet } from '../../../models/member.model';
import { IoChevronBack } from 'react-icons/all';
import { IoArrowBack } from 'react-icons/io5';

function TransactionDetails(props) {
    const dispatch = useDispatch()
    const transactionId = props.match.params.id;
    const [transactionInfo, setTransactionInfo] = useState();
    const settings = useSelector(state => state.root.settings.options);

    useEffect(() => {
        dispatch(showLoader())
        async function getTransactionInfo() {
            const Token = localStorage.getItem('dvg_user_token');
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/customer/transaction/${transactionId}`,
                headers: {Authorization: `Bearer ${Token}`}
            }).then(function (response) {
                if (response.status === 200) {
                    setTransactionInfo(response.data)
                    dispatch(hideLoader())
                }
            }).catch(function(error){
                history.push('/404')
                console.log(error);
                dispatch(hideLoader())
            })
        }
        getTransactionInfo();
    }, [transactionId])

    console.log(transactionInfo);

    const columns = [
        {
            title: 'ID',
            dataIndex: 'id',
            key: 'id',
        },
        {
            title: 'Số tiền',
            dataIndex: 'amount',
            key: 'amount',
            render: (text, row) =>
                <span className={"font-medium"}>{formatVND(row.amount)}</span>
        },
        {
            title: 'Loại',
            dataIndex: 'type',
            key: 'type',
            render: (key, row) => (
                TransactionType.map((item, index) => (item.code === row.type && item.name))
            ),
        },
        {
            title: 'Ngày tháng',
            dataIndex: 'createdAt',
            key: 'createdAt',
            render: (key, row) => formatDateTime(row.createdAt)
        },
        {
            title: '',
            dataIndex: 'action',
            key: 'action',
            align: 'right',
            render: (key, row) => (
                <Space size={'middle'}>
                    <Button
                        type="primary"
                        size="small"
                        ghost
                        icon={<EditTwoTone />}
                        onClick={() => history.push(`/admin/transactions/${row.id}`)}
                    >
                        Chi tiết
                    </Button>
                </Space>
            )
        }
    ]

    return (
        <AccountLayout path={props.location.pathname} title={"Thông tin giao dịch"}>
            {!transactionInfo ? <Loading /> :
                <div className={"mt-5 md:mt-0 pl-5"}>
                    <div className={"md:flex justify-between items-start"}>
                        <h1 className={"text-2xl text-gray-700 hidden md:block"}>Giao dịch #{transactionId}</h1>
                        <Button
                            onClick={() => history.goBack()}
                            icon={<IoArrowBack />}
                        >
                            Quay lại
                        </Button>
                    </div>
                    <div className={"mt-5"}>
                        <div className={"mb-5"}>
                            <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                <span>
                                    Số tiền:
                                </span>
                                <span className={"ml-5 font-medium"}>
                                    {formatVND(transactionInfo.amount)}
                                </span>
                            </div>
                            {transactionInfo.type === 'INVESTMENT' &&
                                <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                    <span>
                                        Số lượng cổ phần:
                                    </span>
                                    <span className={"ml-5 font-medium"}>
                                        {Number(transactionInfo.stockQty)}
                                    </span>
                                </div>
                            }
                            <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                <span>
                                    Loại giao dịch:
                                </span>
                                <span className={"ml-5 font-medium"}>
                                    {TransactionType.map((item, index) => (item.code === transactionInfo.type && item.name))}
                                </span>
                            </div>
                            {transactionInfo.wallet &&
                                <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                    <span>
                                        Ví:
                                    </span>
                                    <span className={"ml-5 font-medium text-red-500"}>
                                        {MemberWallet.map((item) => (item.code === transactionInfo.wallet && item.name))}
                                    </span>
                                </div>
                            }
                            <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                <span>
                                    Trạng thái:
                                </span>
                                <span className={"ml-5 font-medium"}>
                                    {TransactionStatus.map((item, index) => (item.code === transactionInfo.status && item.name))}
                                </span>
                            </div>
                            <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                <span>
                                    Ngày tạo:
                                </span>
                                <span className={"ml-5 font-medium"}>
                                    {formatDateTime(transactionInfo.createdAt)}
                                </span>
                            </div>
                            {transactionInfo.updatedAt &&
                                <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                    <span>
                                        Ngày cập nhật:
                                    </span>
                                        <span className={"ml-5 font-medium"}>
                                        {formatDateTime(transactionInfo.updatedAt)}
                                    </span>
                                </div>
                            }
                            {transactionInfo.note &&
                                <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                    <span>
                                        Ghi chú:
                                    </span>
                                        <span className={"ml-5 font-medium"}>
                                        {transactionInfo.note}
                                    </span>
                                </div>
                            }
                        </div>
                        {transactionInfo && transactionInfo.status === 'PENDING' && transactionInfo && transactionInfo.type === 'INVESTMENT' &&
                            <>
                                <Alert message="Vui lòng thực hiện thanh toán theo thông tin phía bên dưới. Xin cảm ơn!"
                                       type="warning" showIcon />
                                <div style={{ whiteSpace: 'pre-wrap' }} className={"mt-5"}>
                                    {transactionInfo && transactionInfo.type === 'INVESTMENT' && <div dangerouslySetInnerHTML={{__html: settings && settings.payment_info_company}}/>}
                                    <p>Nội dung Chuyển khoản: <b>HD-{transactionInfo.id}-{transactionInfo.createdBy.phone}</b></p>
                                </div>
                            </>
                        }
                        {transactionInfo && transactionInfo.type === 'WITHDRAW' &&
                            <div>
                                <p className={"mb-3 font-bold"}>Thông tin nhận tiền</p>
                                <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                    <span>
                                        Tên ngân hàng:
                                    </span>
                                        <span className={"ml-5 font-medium"}>
                                        {transactionInfo.bank_name}
                                    </span>
                                </div>
                                <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                    <span>
                                        Chủ tài khoản:
                                    </span>
                                        <span className={"ml-5 font-medium"}>
                                        {transactionInfo.bank_owner}
                                    </span>
                                </div>
                                <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                    <span>
                                        Số tài khoản:
                                    </span>
                                        <span className={"ml-5 font-medium"}>
                                        {transactionInfo.bank_account}
                                    </span>
                                </div>
                                {transactionInfo.bank_brand &&
                                    <div className={"mb-2 border-b border-gray-200 pb-2"}>
                                        <span>
                                            Chi nhánh:
                                        </span>
                                            <span className={"ml-5 font-medium"}>
                                            {transactionInfo.bank_brand}
                                        </span>
                                    </div>
                                }
                            </div>
                        }
                        <div>

                        </div>
                    </div>

                </div>
            }
        </AccountLayout>
    );
}

export default TransactionDetails;
