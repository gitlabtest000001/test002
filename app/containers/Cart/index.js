import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { InputNumber, Steps, Table } from 'antd';
import { AppConfig } from '../../appConfig';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { IoBagHandle, IoBasketOutline, IoChevronForward, IoHome, IoRemoveCircle } from 'react-icons/all';
import Button from 'antd/es/button';
import history from '../../utils/history';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import { formatVND } from '../../utils/helpers';
import { checkCart, deleteItemFromCart, updateCart } from '../../utils/cart/cartFunction';
import { hideLoader, showLoader } from '../App/actions';
import Popconfirm from 'antd/es/popconfirm';
import Affix from 'antd/es/affix';
import { IoArrowBack } from 'react-icons/io5';
import { TabletAndBelow } from '../../utils/responsive';
import Loading from '../../components/Loading';
import { ErrorMessage } from '../../components/Message';

const { Step } = Steps;

Cart.propTypes = {};

function Cart(props) {
    const settings = useSelector(state => state.root.settings.options);
    const currentUser = useSelector(state => state.root.currentUser.user);
    const dispatch = useDispatch();
    const [prices, setPrices] = useState([])
    const cart = localStorage.getItem('dvg_shopping_cart');
    if (cart) {
        var listPrice = JSON.parse(cart).items
    }

    const [updateButton, setUpdateButton] = useState(false)

    const getPriceCalc = (productPrice, qty) => {
        return axios({
            method: 'get',
            url: `${AppConfig.apiUrl}/order/calcPrice?productPrice=${productPrice}&quantity=${qty}`,
        }).then(function (response) {
            if (response.status === 200) {
                return response.data
            }
        }).catch(function(error){
            console.log(error);
            localStorage.removeItem("dvg_shopping_cart")
            setTimeout(() => window.location.reload(), 2000);
        })
    }

    useEffect(() => {
        dispatch(showLoader())
        async function calcPrice() {
            const totalListPrice = listPrice && listPrice.map((el) => {
                return getPriceCalc(el.productPrice, el.quantity);
            })
            dispatch(hideLoader())
            setPrices(await Promise.all(totalListPrice));
        }
        calcPrice();

    },[dispatch, updateButton, cart])

    function handleChangeQuantity(id, qty) {
        const Item = {
            productPrice: id,
            quantity: qty
        }
        if (qty > 0 && qty !== null && typeof qty !== 'undefined') {
            updateCart({
                Item
            })
        } else {
            ErrorMessage({message: `Vui lòng chọn số lượng cho sản phẩm`});
        }
    }

    let subTotal = 0;
    prices && prices.map((el) => {
        if (Number(el.quantity) === 0) {
            deleteItemFromCart(el.priceDetail.id)
        }
        subTotal += el.price
    })

    let totalDiscount = 0;
    let discountPercent = 0;
    /*if (currentUser && currentUser.investorStatus === 'TRUE') {
        totalDiscount = subTotal * settings.investor_discount/100;
        discountPercent = settings.investor_discount
    } else {*/
    if (settings) {
        if (currentUser && currentUser.level === 'KhachHang') {
            if (subTotal <= settings.customer_order_value_lte) {
                totalDiscount = subTotal * settings.customer_order_value_lte_percent/100;
                discountPercent = settings.customer_order_value_lte_percent
            }

            if (subTotal > settings.customer_order_value_gt) {
                totalDiscount = subTotal * settings.customer_order_value_gt_percent/100 ;
                discountPercent = settings.customer_order_value_gt_percent
            }
        }

        if (currentUser && currentUser.level === 'DaiLy' && subTotal >= settings.agency_order_value_gt) {
            totalDiscount = subTotal * settings.agency_order_value_gt_percent/100;
            discountPercent = settings.agency_order_value_gt_percent
        }

        if (currentUser && currentUser.level === 'DaiLyCap1') {
            totalDiscount = subTotal * settings.agency_discount_level1/100;
            discountPercent = settings.agency_discount_level1
        }

        if (currentUser && currentUser.level === 'DaiLyCap2') {
            totalDiscount = subTotal * settings.agency_discount_level2/100;
            discountPercent = settings.agency_discount_level2
        }

        if (currentUser && currentUser.level === 'DaiLyCap3') {
            totalDiscount = subTotal * settings.agency_discount_level3/100;
            discountPercent = settings.agency_discount_level3
        }
    }

    /*}*/

    const columns = [
        {
            title: '',
            dataIndex: '',
            key: '',
            render: (text, row) =>
                <Popconfirm
                    title={`Bạn chắc chắn muốn xoá?`}
                    onConfirm={() => deleteItemFromCart(row.priceDetail.id)}
                    okText="Có"
                    cancelText="Không"
                >
                    <Button
                        type={"link"}
                        danger
                    >
                        <IoRemoveCircle size={18}/>
                    </Button>

                </Popconfirm>
        },
        {
            title: 'Hình ảnh',
            dataIndex: 'featureImage',
            key: 'featureImage',
            render: (key, row) =>
                <img src={row.priceDetail && row.priceDetail.product && AppConfig.apiUrl+row.priceDetail.product.featureImage} alt={row.priceDetail.product && row.priceDetail.product.name} className={"w-12 h-12"} />
        },
        {
            title: 'Sản phẩm',
            dataIndex: 'product',
            key: 'product',
            render: (key, row) =>
                <div>
                    <Link to={`/san-pham/${row.priceDetail && row.priceDetail.product && row.priceDetail.product.slug}`} className={"font-medium"}>
                        {row.priceDetail && row.priceDetail.product && row.priceDetail.product.name} - {row.priceDetail && row.priceDetail.name}
                    </Link>
                    <p className={"italic text-xs text-gray-600"}>Kho: {row.priceDetail && row.priceDetail.instock} {row.priceDetail && row.priceDetail.unit}</p>
                </div>

        },
        {
            title: 'Đơn giá',
            dataIndex: 'unitPrice',
            key: 'unitPrice',
            render: (key, row) =>
                <span>
                    {formatVND(row.priceDetail.price)}
                </span>
        },
        {
            title: 'Số lượng',
            dataIndex: 'quantity',
            key: 'quantity',
            render: (key, row) =>
                <InputNumber
                    max={row.priceDetail.instock}
                    min={1}
                    className={"w-16"}
                    defaultValue={row.quantity}
                    onChange={(value) => handleChangeQuantity(row.priceDetail.id, value )}
                />
        },
        {
            title: 'Tạm tính',
            dataIndex: 'unitPrice',
            key: 'unitPrice',
            align: 'right',
            render: (key, row) =>
                <span className={"font-medium"}>
                    {formatVND(row.price)}
                </span>
        },
    ]

    function handleCheckout() {
        history.push('/thanh-toan', {
            subTotal: subTotal,
            totalDiscount: totalDiscount,
            discountPercent: discountPercent,
            totalAmount: subTotal - totalDiscount,
            prices: prices,
            orderParams: listPrice,
        })
    }

    console.log(prices);

    return (
        !settings ? <Loading /> :
        <>
            <TabletAndBelow>
                <Affix offsetTop={0}>
                    <div className={"bg-white border-b border-gray-100"}>
                        <div className={"h-14 flex items-center justify-between mx-3 text-center"}>
                            <IoArrowBack size={24} onClick={() => history.goBack()}/>
                            <span className={"oneLineText w-1/2 font-medium text-lg"}>Giỏ hàng</span>
                            <IoBagHandle  size={20} onClick={() => history.push('/san-pham')}/>
                        </div>
                    </div>
                </Affix>
            </TabletAndBelow>

            <div className={"mx-3 md:mx-auto max-w-6xl pt-5 pb-10"}>
                <div className={"mb-5 hidden md:block"}>
                    <div className={"mb-5"}>
                        <Link to={'/'}><IoHome className={"-mt-1"}/> Trang chủ</Link> <IoChevronForward className={"-mt-1"} /> <span className={"text-green-700"}>Giỏ hàng</span>
                    </div>
                    <h1 className={"font-bold text-xl"}>Giỏ hàng</h1>
                </div>
                {prices && prices.length > 0 ?
                    <div>
                        <Row gutter={[16, 16]}>
                            <Col xs={24} lg={24} md={24}>
                                <div className={"hidden md:block border border-gray-200 bg-white p-5 mb-5 rounded-md"}>
                                    <Table
                                        scroll={{x: '100%'}}
                                        size={'small'}
                                        columns={columns}
                                        dataSource={prices}
                                        pagination={false}
                                    />
                                    {/*<div className={"mt-5 text-right"}>
                                        <Button
                                            type={"primary"}
                                            className={"border-green-500 bg-green-600 hover:bg-green-700 hover:border-green-600 focus:bg-green-600 focus:border-green-500 rounded"}
                                            onClick={() => setUpdateButton(!updateButton)}
                                        >
                                            Cập nhật giỏ hàng
                                        </Button>
                                    </div>*/}
                                </div>
                                <div className={"block md:hidden"}>
                                    {prices && prices.map((item, index) => (
                                        <div className={"mb-3 relative bg-white rounded p-3 shadow"}>
                                            <Row gutter={16}>
                                                <Col xs={6}>
                                                    <img src={item.priceDetail && item.priceDetail.product && AppConfig.apiUrl+item.priceDetail.product.featureImage} alt={item.priceDetail && item.priceDetail.product && item.priceDetail.product.name} className={"w-full h-20 rounded"} />
                                                </Col>
                                                <Col xs={18}>
                                                    <div className={"mb-3"}>
                                                        <Link to={`/san-pham/${item.priceDetail && item.priceDetail.product && item.priceDetail.product.slug}`} className={"font-medium"}>
                                                            {item.priceDetail && item.priceDetail.product && item.priceDetail.product.name} - {item.priceDetail.name}
                                                        </Link>
                                                        <div className={"flex items-center justify-between"}>
                                                            <p className={"text-xs"}>Đơn giá: <span className={"font-bold text-red-600"}>{formatVND(item.priceDetail.price)}</span></p>
                                                            <p className={"italic text-xs text-gray-600"}>Kho: {item.priceDetail.instock} {item.priceDetail.unit}</p>
                                                        </div>
                                                    </div>
                                                    <InputNumber
                                                        max={item.priceDetail.instock}
                                                        min={1}
                                                        className={"w-16"}
                                                        defaultValue={item.quantity}
                                                        onChange={(value) => handleChangeQuantity(item.priceDetail.id, value )}
                                                    />
                                                </Col>
                                            </Row>
                                            <div className={"absolute right-0 top-0"}>
                                                <Popconfirm
                                                    title={`Bạn chắc chắn muốn xoá?`}
                                                    onConfirm={() => deleteItemFromCart(item.priceDetail.id)}
                                                    okText="Có"
                                                    cancelText="Không"
                                                >
                                                    <IoRemoveCircle size={18} className={"text-red-600"}/>
                                                </Popconfirm>
                                            </div>
                                        </div>
                                    ))}
                                </div>

                            </Col>
                            <Col xs={24} lg={16} md={16} />
                            <Col xs={24} lg={8} md={8}>
                                <div className={"border border-gray-200 bg-white p-5 mb-5 rounded-md shadow-lg"}>
                                    <div className={"mb-5"}>
                                        <div className={"mb-3 flex items-center justify-between"}>
                                            <span>Tạm tính: </span>
                                            <span>{formatVND(subTotal)}</span>
                                        </div>
                                        {totalDiscount > 0 &&
                                            <div className={"mb-3 flex items-center justify-between"}>
                                                <span>Chiết khấu ({discountPercent}%): </span>
                                                <span className={"text-red-600"}>-{formatVND(totalDiscount)}</span>
                                            </div>
                                        }
                                        <div className={"mb-3 flex items-center justify-between"}>
                                            <span>Tổng thanh toán: </span>
                                            <span className={"text-lg font-bold text-green-700"}>{formatVND(Number(subTotal - totalDiscount))}</span>
                                        </div>
                                    </div>
                                    <Button
                                        size={"large"}
                                        className={"w-full block bg-yellow-500 rounded-md text-white hover:bg-green-600 hover:boder-green-600 focus:bg-green-600 focus:boder-green-600 hover:text-white focus:text-white"}
                                        onClick={() => handleCheckout()}
                                    >
                                        Tiếp tục thanh toán
                                    </Button>
                                </div>
                            </Col>
                        </Row>
                    </div>
                    :
                    <div className={"text-center"}>
                        <p className={"mb-5"}>
                            <IoBasketOutline size={100} className={"text-gray-100"}/>
                        </p>
                        <p className={"mb-5"}>
                            Chưa có sản phẩm nào trong giỏ hàng
                        </p>
                        <Button
                            type={"primary"}
                            className={"bg-green-600 border-green-500 rounded-md hover:bg-green-700 hover:border-green-700"}
                            onClick={() => history.push('/san-pham')}
                        >
                            Chọn sản phẩm
                        </Button>
                    </div>
                }

            </div>
        </>

    );
}

export default Cart;
