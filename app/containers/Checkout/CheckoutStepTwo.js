import React, { useRef } from 'react';
import { Steps } from 'antd';
import history from '../../utils/history';
import Loading from '../../components/Loading';
import { IoCheckmarkCircleSharp } from 'react-icons/all';
import Button from 'antd/es/button';
import { IoCart, IoInformationCircle, IoPeople, IoPrint, IoReceipt, IoWallet } from 'react-icons/io5';
import { useSelector } from 'react-redux';
import ReactToPrint from 'react-to-print';
import { Link } from 'react-router-dom';
import { formatVND } from '../../utils/helpers';
import { PaymentMethod } from '../../models/order.model';

const { Step } = Steps;

CheckoutStepTwo.propTypes = {

};

function CheckoutStepTwo(props) {
    const componentRef = useRef();
    const settings = useSelector(state => state.root.settings.options);
    const orderId = props.match.params.id;
    const state = props.location.state;
    if (typeof state === 'undefined') {
        history.push('/gio-hang')
    }
    let result = null;
    if (state) {
        result = state.result;
        if (Number(orderId) !== result.order.id) {
            history.push('/gio-hang')
        }
    }
    const currentUser = useSelector(state => state.root.currentUser.user);

    return (
        !result ? <Loading /> :
        <div className={"bg-gray-50 py-5 min-h-screen border-t border-b border-gray-200 mx-3"}>
            <div className={"max-w-2xl mx-auto"}>
                <div className={"mb-5 flex justify-between items-center"}>
                    {currentUser ?
                        <Link
                            className={"bg-purple-50 border border-purple-200 text-purple-500 rounded py-1 px-2 hover:bg-purple-200 hover:text-purple-600"}
                            to={'/taikhoan/don-hang'}
                        >
                            <IoReceipt className={"mr-2"} /> Quản lý đơn hàng
                        </Link>
                        :
                        <Link
                            to={'/dang-nhap'}
                        >
                            Đăng nhập/Đăng ký
                        </Link>
                    }
                    <ReactToPrint
                        trigger={() =>
                            <Button
                                className={"rounded"}
                                icon={<IoPrint className={"mr-2"} />}
                                type={"primary"}
                            >
                                In hoá đơn
                            </Button>
                        }
                        content={() => componentRef.current}
                    />
                </div>
                <div ref={componentRef}>
                    <div className={"bg-white rounded-xl px-6 py-5 shadow-lg border-t-4 border-green-500"}>
                        <div className={"text-center"}>
                            <div className={"mb-5"}>
                                <IoCheckmarkCircleSharp size={60} className={"text-green-500 mb-3"}/>
                                <h1 className={"font-medium text-base mb-5"}>Cảm ơn {result.order.guestName}, đơn hàng của bạn đã được xác nhận!</h1>
                            </div>
                            <div className={"text-gray-600"}>
                                <p>Thông tin đặt hàng và hướng dẫn thanh toán đã được gửi tới email <span className={"font-medium text-gray-800"}>{result.order.guestEmail}</span>.</p>
                                <p>Vui lòng thanh toán theo thông tin bên dưới.</p>
                            </div>
                        </div>
                    </div>
                    <div id={"thong-tin-dich-vu"} className={"bg-white shadow-lg p-5 rounded-xl border-t-2 border-dashed border-gray-300"}>
                        <div className={"flex justify-between items-center mb-3 border-b border-gray-200 pb-3"}>
                            <h2 className={"text-base font-bold text-gray-600"}><IoWallet size={20} className={"-mt-1 mr-2 text-red-600"} /> Thông tin thanh toán</h2>
                        </div>
                        <div>
                            <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                <p className={"text-gray-500"}>Hình thức thanh toán</p>
                                <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>{PaymentMethod.map((item, index) => (item.code === result.order.paymentMethod && item.name))}</p>
                            </div>
                            <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                <p className={"text-gray-500"}>Số tiền thanh toán</p>
                                <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>{result && formatVND(result.order.amount)}</p>
                            </div>
                            {result && result.order.paymentMethod === 'COD' &&
                                <div className={"bg-gray-50 border border-gray-200 px-5 py-3"}>
                                    Thanh toán tiền khi nhận hàng.
                                </div>
                            }
                            {result && result.order.paymentMethod === 'BankTransfer' &&
                                <div className={"bg-gray-50 border border-gray-200 px-5 py-3"}>
                                    <p className={"mb-2 font-bold"}>Quý khách thanh toán theo thông tin bên dưới:</p>
                                    <p>Số tiền thanh toán: <span className={"font-bold"}>{result && formatVND(result.order.amount)}</span></p>
                                    <div dangerouslySetInnerHTML={{__html: settings && settings.payment_info_company}}/>
                                    <p>Nội dung chuyển tiền: <b>HD-{result && result.order.id}-{result && result.order.guestPhone}</b></p>
                                </div>
                            }
                        </div>
                    </div>
                    <div id={"thong-tin-dich-vu"} className={"bg-white shadow-lg p-5 rounded-xl border-t-2 border-dashed border-gray-300"}>
                        <div className={"flex justify-between items-center mb-3 border-b border-gray-200 pb-3"}>
                            <h2 className={"text-base font-bold text-gray-600"}><IoCart size={20} className={"-mt-1 mr-2 text-blue-400"} /> Thông tin đơn hàng</h2>
                        </div>
                        {result && result.orderItems && result.orderItems.length > 0 &&
                            result.orderItems.map((item, index) => (
                                <div>
                                    <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                        <p className={"text-gray-500"}>{item.productName} - {item.productPrice.name} <span className={"font-bold"}>x {item.quantity}</span></p>
                                        <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>{formatVND(item.totalAmount)}</p>
                                    </div>
                                </div>
                            ))
                        }
                        <div className={"flex justify-between py-3 border-b border-gray-100"}>
                            <p className={"text-gray-500"}>Tạm tính</p>
                            <p>{formatVND(result && result.order.revenue)}</p>
                        </div>
                        {Number(result && result.order.discount) > 0 &&
                            <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                <p className={"text-gray-500"}>Chiết khấu</p>
                                <p className={"text-red-400"}>-{formatVND(result.order.discount)}</p>
                            </div>
                        }
                        <div className={"flex flex-row justify-between items-center mt-2"}>
                            <span>Tổng tiền</span>
                            <span className={"font-bold text-lg"}>{formatVND(result && result.order.amount)}</span>
                        </div>

                    </div>
                    <div id={"thong-tin-khach"} className={"bg-white shadow-lg p-5 rounded-xl border-t-2 border-dashed border-gray-300"}>
                        <div className={"flex justify-between items-center mb-3 border-b border-gray-200 pb-3"}>
                            <h2 className={"text-base font-bold text-gray-600"}><IoPeople size={20} className={"mr-2 text-purple-400"} /> Thông tin nhận hàng</h2>
                        </div>
                        <div className={"mb-5"}>
                            <div id={"thong-tin-lien-he"}>
                                <div className={"flex py-2 border-b border-gray-100"}>
                                    <p className={"w-32 mr-2 text-gray-500"}>Họ tên:</p>
                                    <p className={"font-medium text-gray-700"}>{result.order.guestName}</p>
                                </div>
                                <div className={"flex py-2 border-b border-gray-100"}>
                                    <p className={"w-32 mr-2 text-gray-500"}>Số điện thoại:</p>
                                    <p className={"font-medium text-gray-700"}>{result.order.guestPhone}</p>
                                </div>
                                <div className={"flex py-2 border-b border-gray-100"}>
                                    <p className={"w-32 mr-2 text-gray-500"}>Email:</p>
                                    <p className={"font-medium text-gray-700"}>{result.order.guestEmail}</p>
                                </div>
                                <div className={"flex py-2 border-b border-gray-100"}>
                                    <p className={"w-32 mr-2 text-gray-500"}>Địa chỉ:</p>
                                    <p className={"font-medium text-gray-700"}>{result.order.guestAddress ? result.order.guestAddress : <span className={"text-gray-500 font-normal"}>Chưa có</span>}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id={"thong-tin-khach"} className={"bg-white shadow-lg p-5 rounded-xl border-t-2 border-dashed border-gray-300"}>
                        <div className={"flex justify-between items-center mb-3 border-b border-gray-200 pb-3"}>
                            <h2 className={"text-base font-bold text-gray-600"}><IoInformationCircle size={20} className={"mr-1 text-yellow-400"} /> Ghi chú cho đơn hàng</h2>
                        </div>
                        {result.order.note ?
                            <p className={"mb-5"}>{result.order.note}</p>
                            : <p className={"mb-5 text-gray-500"}>Không có yêu cầu khác</p>
                        }
                    </div>
                </div>
                <div className={"py-10 text-center"}>
                    <Button
                        type={"primary"}
                        className={"rounded-md"}
                        size={"large"}
                        onClick={() => {history.push('/')}}
                    >
                        Quay lại trang chủ
                    </Button>
                </div>
            </div>
        </div>
    );
}

export default CheckoutStepTwo;
