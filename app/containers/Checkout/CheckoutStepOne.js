import React, { useState } from 'react';
import history from '../../utils/history';
import { useDispatch, useSelector } from 'react-redux';
import Row from 'antd/es/grid/row';
import Col from 'antd/es/grid/col';
import { Affix, Steps } from 'antd';
import { AppConfig } from '../../appConfig';
import axios from 'axios';
import { FaPiggyBank, IoBagHandle, IoHome, IoPersonCircle, IoRadioButtonOff, IoRadioButtonOn } from 'react-icons/all';
import { ErrorMessage, SuccessMessage } from '../../components/Message';
import Button from 'antd/es/button';
import { Link } from 'react-router-dom';
import { formatVND } from '../../utils/helpers';
import Form from 'antd/es/form';
import Input from 'antd/es/input';
import TextArea from 'antd/es/input/TextArea';
import { hideLoader, showLoader } from '../App/actions';
import { TabletAndBelow } from '../../utils/responsive';
import { IoArrowBack, IoChevronForward } from 'react-icons/io5';
import { useCookies } from 'react-cookie';

const { Step } = Steps;

CheckoutStepOne.propTypes = {

};

function CheckoutStepOne(props) {
    const dispatch = useDispatch();
    const pathname = window.location.pathname;
    const settings = useSelector(state => state.root.settings.options);
    const [paymentMethod, setPaymentMethod] = useState('COD');
    const [cookies, setCookie, removeCookie] = useCookies();

    const state = props.location.state;

    if (typeof state === 'undefined') {
        history.push('/gio-hang')
    }

    console.log(state);

    const invite = cookies.dvg_ref
    const currentUser = useSelector(state => state.root.currentUser.user);

    function handleLogin() {
        history.push(`/dang-nhap?return=${pathname}`)
    }

    if (currentUser) {
        var initialValues = {
            guestName: currentUser && currentUser.name,
            guestEmail: currentUser && currentUser.email,
            guestPhone: currentUser && currentUser.phone,
            guestAddress: currentUser && currentUser.address,
        }
    }

    async function handleCreateOrder(values) {
        dispatch(showLoader());
        const Token = await localStorage.getItem('dvg_user_token');
        const data = {
            orderItems: state && state.orderParams,
            name: values.guestName,
            phone: values.guestPhone,
            email:values.guestEmail,
            address: values.guestAddress,
            note: values.note,
            paymentMethod,
            referral: invite ? invite : null
        }

        console.log('data', data);

        if (Token) {
            axios.post(
                `${AppConfig.apiUrl}/customer/order/create`,
                data,
                {headers: {Authorization: `Bearer ${Token}`}}
            ).then(function (response) {
                dispatch(hideLoader())
                SuccessMessage({message: 'Đơn hàng đã được tạo thành công, vui lòng thanh toán!'})
                localStorage.removeItem('dvg_shopping_cart');
                history.push(`/dat-hang/${response.data.order.id}`, {
                    result: response.data,
                });
            })
                .catch(function (error) {
                    dispatch(hideLoader())
                    ErrorMessage({
                        message: error.response.data.message,
                    });
                });
        } else {
            axios.post(
                `${AppConfig.apiUrl}/order/create`,
                data,
            ).then(function (response) {
                dispatch(hideLoader())
                SuccessMessage({message: 'Đơn hàng đã được tạo thành công, vui lòng thanh toán!'})
                localStorage.removeItem('dvg_shopping_cart');
                history.push(`/dat-hang/${response.data.order.id}`, {
                    result: response.data,
                });
            })
                .catch(function (error) {
                    dispatch(hideLoader())
                    ErrorMessage({
                        message: error.response.data.message,
                    });
                });
        }
    }

    return (
        <Form
            layout={"vertical"}
            name={"create_booking"}
            onFinish={handleCreateOrder}
            initialValues={initialValues}
        >
            <TabletAndBelow>
                <Affix offsetTop={0}>
                    <div className={"bg-white border-b border-gray-100"}>
                        <div className={"h-14 flex items-center justify-between mx-3 text-center"}>
                            <IoArrowBack size={24} onClick={() => history.goBack()}/>
                            <span className={"oneLineText w-1/2 font-medium text-lg"}>Thông tin thanh toán</span>
                            <IoBagHandle  size={20} onClick={() => history.push('/san-pham')}/>
                        </div>
                    </div>
                </Affix>
            </TabletAndBelow>
        <div className={"py-5 bg-gray-50 px-3 md:px-0 min-h-screen md:border-t border-b border-gray-200"}>
            <div>
                <div className={"max-w-6xl mx-auto"}>
                    <div className={"mb-5 hidden md:block"}>
                        <div className={"mb-5"}>
                            <Link to={'/'}><IoHome className={"-mt-1"}/> Trang chủ</Link> <IoChevronForward className={"-mt-1"} /> <Link to={'/gio-hang'}>Giỏ hàng</Link> <IoChevronForward className={"-mt-1"} /> <span className={"text-green-700"}>Thanh toán</span>
                        </div>
                        <h1 className={"font-bold text-xl"}>Thanh toán</h1>
                    </div>
                    <Row gutter={[20, 20]}>
                        <Col xs={24} lg={16} md={16} className={"mb-32 md:mb-0"}>
                            <div id={"thong-tin-lien-he"}
                                 className={"border border-gray-200 bg-white p-5 mb-5 rounded-md"}
                            >
                                <h2 className={"font-bold uppercase mb-3"}>Thông tin khách hàng</h2>
                                {!currentUser &&
                                    <div className={"bg-yellow-100 border-yellow-200 rounded p-2 mb-3"}>
                                        <p><span onClick={handleLogin}
                                                 className={"font-medium text-yellow-600 cursor-pointer"}><IoPersonCircle
                                            size={20} className={"-mt-1"}/> Đăng nhập</span> <span className={"text-gray-600"}>để đặt hàng nhanh hơn và quản lý đơn hàng dễ dàng hơn</span>
                                        </p>
                                    </div>
                                }
                                <Row gutter={20}>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Họ tên"
                                            name="guestName"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'
                                                },
                                            ]}
                                        >
                                            <Input
                                                defaultValue={currentUser && currentUser.name}
                                                size={"large"}
                                                className={"rounded"}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Email"
                                            name="guestEmail"
                                        >
                                            <Input
                                                defaultValue={currentUser && currentUser.email}
                                                size={"large"}
                                                className={"rounded"}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Số điện thoại"
                                            name="guestPhone"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'
                                                },
                                            ]}
                                        >
                                            <Input
                                                defaultValue={currentUser && currentUser.phone}
                                                size={"large"}
                                                className={"rounded"}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={12} md={12}>
                                        <Form.Item
                                            label="Địa chỉ"
                                            name="guestAddress"
                                            rules={[
                                                {
                                                    required: true,
                                                    message: 'Vui lòng nhập thông tin'
                                                },
                                            ]}
                                        >
                                            <Input
                                                defaultValue={currentUser && currentUser.address}
                                                size={"large"}
                                                className={"rounded"}
                                            />
                                        </Form.Item>
                                    </Col>
                                    <Col xs={24} lg={24} md={24}>
                                        <Form.Item
                                            label="Ghi chú cho đơn hàng"
                                            name="note"
                                            className={"mb-0"}
                                        >
                                            <TextArea
                                                rows={3}
                                                className={"rounded"}
                                            />
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </div>
                            <div id={"phuong-thuc-thanh-toan"}
                                 className={"border border-gray-200 bg-white p-5 mb-5 rounded-md"}
                            >
                                <h2 className={"font-bold uppercase mb-3"}>Phương thức thanh toán</h2>
                                <div>

                                    {currentUser && (Number(state.totalAmount) <= Number(currentUser.product_wallet)) &&
                                        <div className={"mb-5"}>
                                            <div
                                                className={"flex items-center"}
                                                onClick={() => setPaymentMethod('ProductWallet')}
                                            >
                                                {paymentMethod !== 'ProductWallet' ?
                                                    <IoRadioButtonOff size={20} className={"mr-2"}/>
                                                    :
                                                    <IoRadioButtonOn size={20} className={"mr-2 text-green-600"}/>
                                                }
                                                Ví tiền hàng ({currentUser && formatVND(currentUser.product_wallet)})
                                            </div>
                                            {paymentMethod === 'ProductWallet' &&
                                                <div className={"mt-2 p-3 bg-gray-50 border border-gray-200"}>
                                                    <p>Sử dụng ví tiền hàng để thanh toán.</p>
                                                </div>
                                            }
                                        </div>
                                    }
                                    {currentUser && (Number(state.totalAmount) <= Number(currentUser.cash_wallet)) &&
                                        <div className={"mb-5"}>
                                            <div
                                                className={"flex items-center"}
                                                onClick={() => setPaymentMethod('CashWallet')}
                                            >
                                                {paymentMethod !== 'CashWallet' ?
                                                    <IoRadioButtonOff size={20} className={"mr-2"}/>
                                                    :
                                                    <IoRadioButtonOn size={20} className={"mr-2 text-green-600"}/>
                                                }
                                                Ví tiền mặt ({currentUser && formatVND(currentUser.cash_wallet)})
                                            </div>
                                            {paymentMethod === 'CashWallet' &&
                                                <div className={"mt-2 p-3 bg-gray-50 border border-gray-200"}>
                                                    <p>Sử dụng ví tiền mặt để thanh toán.</p>
                                                </div>
                                            }
                                        </div>
                                    }
                                    <div className={"mb-5"}>
                                        <div
                                            className={"flex items-center"}
                                            onClick={() => setPaymentMethod('COD')}
                                        >
                                            {paymentMethod !== 'COD' ?
                                                <IoRadioButtonOff size={20} className={"mr-2"}/>
                                                :
                                                <IoRadioButtonOn size={20} className={"mr-2 text-green-600"}/>
                                            }
                                            Thanh toán tiền mặt khi nhận hàng
                                        </div>
                                        {paymentMethod === 'COD' &&
                                            <div className={"mt-2 p-3 bg-gray-50 border border-gray-200"}>
                                                Quý khách sẽ thanh toán cho người vận chuyển sau khi nhận và kiểm tra hàng.
                                            </div>
                                        }
                                    </div>

                                    <div>
                                        <div
                                            className={"flex items-center"}
                                            onClick={() => setPaymentMethod('BankTransfer')}
                                        >
                                            {paymentMethod !== 'BankTransfer' ?
                                                <IoRadioButtonOff size={20} className={"mr-2"}/>
                                                :
                                                <IoRadioButtonOn size={20} className={"mr-2 text-green-600"}/>
                                            }
                                            Chuyển khoản ngân hàng
                                        </div>
                                        {paymentMethod === 'BankTransfer' &&
                                            <div className={"mt-2 p-3 bg-gray-50 border border-gray-200"}>
                                                <p>Thực hiện chuyển khoản vào tài khoản ngân hàng của chúng tôi.</p>
                                            </div>
                                        }
                                    </div>

                                </div>
                            </div>

                            <div id={"tinh-tien"} className={"border border-gray-200 bg-white p-5 mb-5 rounded-md shadow-lg md:hidden"}>
                                <div className={"mb-5"}>
                                    <h2 className={"font-bold uppercase mb-3"}>Chi tiết đơn hàng</h2>
                                    <div>
                                        {state && state.prices && state.prices.length > 0 && state.prices.map((item, index) => (
                                            <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                                <p className={"text-gray-500"}>{item.priceDetail.product.name} - {item.priceDetail.name} <span className={"font-bold"}>x {item.quantity}</span></p>
                                                <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>{formatVND(item.price)}</p>
                                            </div>
                                        ))}

                                        {Number(state && state.totalDiscount) > 0 &&
                                            <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                                <p className={"text-gray-500"}>Chiết khấu</p>
                                                <p className={"text-red-400"}>-{formatVND(state.totalDiscount)}</p>
                                            </div>
                                        }
                                        {Number(state && state.totalDiscount) > 0 &&
                                            <div className={"mt-5 w-full rounded px-2 py-1 bg-red-50 block mt-2"}>
                                                <span className={"text-red-600"}><FaPiggyBank className={"mr-2 -mt-1"}/> Bạn đã tiết kiệm được {formatVND(state.totalDiscount)}</span>
                                            </div>
                                        }
                                    </div>
                                </div>
                            </div>
                        </Col>

                        <Col xs={24} lg={8} md={8} className={"hidden md:block"}>
                            <Affix offsetTop={50}>
                                <div id={"tinh-tien"} className={"border border-gray-200 bg-white p-5 mb-5 rounded-md shadow-lg"}>
                                    <div className={"mb-5"}>
                                        <h2 className={"font-bold uppercase mb-3"}>Chi tiết đơn hàng</h2>
                                        <div>
                                            {state && state.prices && state.prices.length > 0 && state.prices.map((item, index) => (
                                                <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                                    <p className={"text-gray-500"}>{item.priceDetail.product.name} - {item.priceDetail.name} <span className={"font-bold"}>x {item.quantity}</span></p>
                                                    <p className={"whitespace-nowrap ml-3 font-medium text-gray-800"}>{formatVND(item.price)}</p>
                                                </div>
                                            ))}

                                            {Number(state && state.totalDiscount) > 0 &&
                                            <div className={"flex justify-between py-3 border-b border-gray-100"}>
                                                <p className={"text-gray-500"}>Chiết khấu ({state.discountPercent}%)</p>
                                                <p className={"text-red-400"}>-{formatVND(state.totalDiscount)}</p>
                                            </div>
                                            }
                                            <div className={"flex flex-row justify-between items-center"}>
                                                <span>Tổng tiền</span>
                                                <span className={"font-bold text-lg"}>{formatVND(state && state.totalAmount)}</span>
                                            </div>
                                            {Number(state && state.totalDiscount) > 0 &&
                                            <div className={"mt-5 w-full rounded px-2 py-1 bg-red-50 block mt-2"}>
                                                <span className={"text-red-600"}><FaPiggyBank className={"mr-2 -mt-1"}/> Bạn đã tiết kiệm được {formatVND(state.totalDiscount)}</span>
                                            </div>
                                            }
                                        </div>
                                    </div>
                                    {state && state.checkOutType && state.checkOutType === 'buynow' && state.prices[0] &&
                                        <Link
                                            className={"mb-3 block bg-yellow-500 px-3 py-2 text-white text-center rounded hover:text-white hover:bg-yellow-600"}
                                            to={`/san-pham/${state.prices[0] && state.prices[0].priceDetail && state.prices[0].priceDetail.product.slug}`}
                                        >
                                            Thay đổi lựa chọn sản phẩm
                                        </Link>
                                    }
                                    <Button
                                        type={"primary"} size={"large"} block
                                        className={"rounded uppercase h-12 text-white bg-green-600 border-green-700 hover:border-green-600 hover:bg-green-700 hover:text-white focus:border-green-600 focus:bg-green-700 focus:text-white"}
                                        htmlType={"submit"}
                                    >
                                        Đặt hàng
                                    </Button>
                                </div>
                            </Affix>
                        </Col>
                    </Row>
                    <TabletAndBelow>
                        <div className={"fixed bottom-14 left-0 bg-white px-3 py-2 w-full border-t border-gray-200 z-50"}>
                            <Row gutter={16}>
                                <Col xs={14}>
                                    <span>Tổng tiền</span>
                                    <span
                                        className={'font-bold text-lg block'}>{state && formatVND(state.totalAmount)}</span>
                                </Col>
                                <Col xs={10}>
                                    <Button
                                        className={"w-full h-12 rounded bg-red-500 border border-red-600 text-white font-bold"}
                                        htmlType={"submit"}
                                    >
                                        Xác nhận
                                    </Button>
                                </Col>
                            </Row>
                        </div>
                    </TabletAndBelow>
                </div>
            </div>
        </div>
        </Form>
    );
}

export default CheckoutStepOne;
