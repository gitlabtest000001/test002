import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import ProductCategoryWidget from '../../HomePage/Widgets/ProductCategory';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import Loading from '../../../components/Loading';
import ProductItem from '../../../components/ProductItem';
import { Select } from 'antd';
import ProductList from './ProductList';
import ProductHotWidget from '../../HomePage/Widgets/ProductHot';
import queryString from 'query-string';
import { Link } from 'react-router-dom';
import PostCategoryWidget from '../../HomePage/Widgets/PostCategory';
import { IoArrowBackCircle, IoArrowBackCircleOutline, IoBagCheck, IoChevronForward, IoHome } from 'react-icons/all';
import { IoArrowBack } from 'react-icons/io5';
import history from '../../../utils/history';
import { Helmet } from 'react-helmet/es/Helmet';
import { Desktop, TabletAndBelow } from '../../../utils/responsive';
import Affix from 'antd/es/affix';

const {Option} = Select

function Category(props) {
    const slug = props.match.params.slug;
    const dispatch = useDispatch();
    const [category, setCategory] = useState();

    const params = queryString.parse(props.location.search)

    if (params && params.ref) {
        localStorage.setItem('dvg_user_ref', params.ref);
    }

    useEffect(() => {
        dispatch(showLoader());
        async function getCategory() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/category/bySlug/${slug}`,
            }).then(function (response) {
                if (response.status === 200) {
                    setCategory(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
                history.push('/404')
                dispatch(hideLoader());
            })
        }
        getCategory();
    },[slug])

    return (
        !category ? <Loading /> :
            <>
                <TabletAndBelow>
                    <Affix offsetTop={0}>
                        <div className={"bg-white border-b border-gray-100"}>
                            <div className={"h-14 flex items-center justify-between mx-3 text-center"}>
                                <IoArrowBack size={24} onClick={() => history.goBack()}/>
                                <span className={"oneLineText w-1/2 font-medium text-lg"}>{category && category.detail.name}</span>
                                <IoBagCheck  size={24} onClick={() => history.push('/san-pham')}/>
                            </div>
                        </div>
                    </Affix>
                </TabletAndBelow>
                <div className={"max-w-6xl mx-auto py-5 px-3"}>
                    <Helmet>
                        <title>{category && category.detail.name}</title>
                    </Helmet>
                    <Row gutter={32}>
                        <Col xs={24} lg={6} md={6}>
                            <Desktop>
                                <ProductCategoryWidget />
                                <ProductHotWidget />
                            </Desktop>
                        </Col>
                        <Col xs={24} lg={18} md={18}>
                            <div className={"mb-5 hidden md:block"}>
                                <Link to={'/'}><IoHome className={"-mt-1"}/> Trang chủ</Link> <IoChevronForward className={"-mt-1"} />
                                <Link to={'/san-pham'}>Sản phẩm</Link> <IoChevronForward className={"-mt-1"} />
                                {category && category.detail.parent &&
                                    <>
                                        <Link to={`/danh-muc/${category.detail.parent.slug}`}>{category.detail.parent.name}</Link> <IoChevronForward className={"-mt-1"} />
                                    </>
                                }
                                <span className={"text-green-700"}>{category && category.detail.name}</span>
                            </div>
                            <div className={"flex items-center mb-5 hidden md:flex"}>
                                <IoArrowBackCircleOutline
                                    className={"mr-2 text-gray-400 hover:text-green-600"}
                                    size={24}
                                    onClick={() => history.goBack()}
                                />
                                <h1 className={"font-bold text-xl text-gray-700"}>{category && category.detail.name}</h1>
                            </div>
                            {category && category.subCategory && category.subCategory.length > 0 &&
                                <div className={"flex items-center whitespace-nowrap overflow-x-auto pb-3 mt-3 md:pb-0 md:mt-0 mb-8"}>
                                    {category.subCategory.map((item, index) => (
                                        <Link to={`/danh-muc/${item.slug}`} className={"mr-3 border border-green-600 rounded-md px-3 py-1 hover:bg-green-600 hover:text-white"}>{item.name}</Link>
                                    ))}
                                </div>
                            }
                            <ProductList categoryId={category && category.detail.id}/>
                        </Col>
                    </Row>

                </div>
            </>

    );
}

export default Category;
