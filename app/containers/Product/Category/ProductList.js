import React, { useEffect, useState } from 'react';
import Col from 'antd/es/grid/col';
import ProductItem from '../../../components/ProductItem';
import Row from 'antd/es/grid/row';
import { useDispatch } from 'react-redux';
import { hideLoader, showLoader } from '../../App/actions';
import axios from 'axios';
import { AppConfig } from '../../../appConfig';
import { Alert } from 'antd';
import history from '../../../utils/history';

function ProductList(props) {
    const dispatch = useDispatch();
    const [products, setProducts] = useState();

    useEffect(() => {
        dispatch(showLoader());
        async function getCategory() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/product`,
                params: {
                    category: [props.categoryId],
                    status: 'ACTIVE'
                }
            }).then(function (response) {
                if (response.status === 200) {
                    setProducts(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
                history.push('/404')
                dispatch(hideLoader());
            })
        }
        getCategory();
    },[props.categoryId])

    console.log(products);

    return (
        <Row gutter={[16, 16]}>
            {products && products.list.length > 0 ? products.list.map((item, index) => (
                    <Col xs={24} lg={8} md={8}>
                        <ProductItem item={item} key={index} />
                    </Col>
                ))
                :
                <Col xs={24} lg={24} md={24}>
                    <Alert message="Không tìm thấy sản phẩm hợp với tiêu chí tìm kiếm, vui lòng chọn lại" type="warning" showIcon />
                </Col>
            }
        </Row>
    );
}

export default ProductList;
