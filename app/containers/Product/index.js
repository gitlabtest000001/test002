import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import ProductCategoryWidget from '../HomePage/Widgets/ProductCategory';
import { hideLoader, showLoader } from '../App/actions';
import axios from 'axios';
import { AppConfig } from '../../appConfig';
import Loading from '../../components/Loading';
import ProductItem from '../../components/ProductItem';
import { Alert, Select } from 'antd';
import ProductHotWidget from '../HomePage/Widgets/ProductHot';
import queryString from 'query-string';
import { Link } from 'react-router-dom';
import { IoBagCheck, IoChevronForward, IoHome } from 'react-icons/all';
import { Helmet } from 'react-helmet/es/Helmet';
import Affix from 'antd/es/affix';
import { IoArrowBack } from 'react-icons/io5';
import history from '../../utils/history';
import { Desktop, TabletAndBelow } from '../../utils/responsive';
import CategoryBlock from '../HomePage/Widgets/CategoryBlock';

const {Option} = Select

function Shop(props) {
    const dispatch = useDispatch();
    const settings = useSelector(state => state.root.settings.options);
    const [products, setProducts] = useState();

    const params = queryString.parse(props.location.search)

    if (params && params.ref) {
        localStorage.setItem('dvg_user_ref', params.ref);
    }

    useEffect(() => {
        dispatch(showLoader());
        async function getProducts() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/product`,
                params: {
                    status: 'ACTIVE'
                }
            }).then(function (response) {
                if (response.status === 200) {
                    setProducts(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
                //history.push('/404')
                dispatch(hideLoader());
            })
        }
        getProducts();
    },[dispatch])

    return (
        !products ? <Loading /> :
            <>
                <TabletAndBelow>
                    <Affix offsetTop={0}>
                        <div className={"bg-white border-b border-gray-100"}>
                            <div className={"h-14 flex items-center justify-between mx-3 text-center"}>
                                <IoArrowBack size={24} onClick={() => history.goBack()}/>
                                <span className={"oneLineText w-1/2 font-medium text-lg"}>Sản phẩm</span>
                                <IoHome  size={20} onClick={() => history.push('/')}/>
                            </div>
                        </div>
                    </Affix>
                </TabletAndBelow>
                <div className={"max-w-6xl mx-auto py-5 px-3 md:px-0"}>
                    <Helmet>
                        <title>Sản phẩm</title>
                    </Helmet>
                    <Row gutter={[32, 32]}>
                        <Col xs={24} lg={6} md={6}>
                            <ProductCategoryWidget />
                            <Desktop>
                                <ProductHotWidget />
                            </Desktop>
                        </Col>
                        <Col xs={24} lg={18} md={18}>
                            <div className={"mb-5 hidden md:block"}>
                                <Link to={'/'}><IoHome className={"-mt-1"}/> Trang chủ</Link> <IoChevronForward className={"-mt-1"} /> <span className={"text-green-700"}>Sản phẩm</span>
                            </div>
                            <div className={"flex items-center justify-between mb-5 hidden md:flex"}>
                                <h1 className={"font-bold text-xl text-gray-700"}>Tất cả sản phẩm</h1>
                            </div>
                            <Row gutter={[32, 32]}>
                                {products && products.list.length > 0 ? products.list.map((item, index) => (
                                        <Col xs={24} lg={8} md={8}>
                                            <ProductItem item={item} key={index} />
                                        </Col>
                                    ))
                                    :
                                    <Col xs={24} lg={24} md={24}>
                                        <Alert message="Không tìm thấy sản phẩm hợp với tiêu chí tìm kiếm, vui lòng chọn lại" type="warning" showIcon />
                                    </Col>
                                }
                            </Row>
                        </Col>
                    </Row>
                </div>
            </>
    );
}

export default Shop;
