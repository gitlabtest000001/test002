import React, { useEffect, useState } from 'react';
import { changeModalContent, changeModalTitle, hideLoader, setModalWidth, showLoader, showModal } from '../App/actions';
import axios from 'axios';
import { AppConfig } from '../../appConfig';
import { useDispatch, useSelector } from 'react-redux';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import Loading from '../../components/Loading';
import SliderWithThumbnail from '../../components/SliderWithThumbnail';
import ProductCategoryWidget from '../HomePage/Widgets/ProductCategory';
import { formatVND } from '../../utils/helpers';
import { Alert, InputNumber, Space } from 'antd';
import Button from 'antd/es/button';
import { Link } from 'react-router-dom';
import { Tab, Tabs, TabList, TabPanel } from 'react-tabs';
import 'react-tabs/style/react-tabs.css';
import {
    IoAdd,
    IoCaretForward,
    IoChevronForward,
    IoHome, IoImages,
    IoRemove,
    IoRemoveCircleOutline,
    IoTrashBinOutline,
} from 'react-icons/all';
import ProductHotWidget from '../HomePage/Widgets/ProductHot';
import { checkCart, deleteItemFromCart } from '../../utils/cart/cartFunction';
import { ErrorMessage, SuccessMessage } from '../../components/Message';
import { IoArrowBack, IoCart } from 'react-icons/io5';
import queryString from 'query-string';
import Tooltip from 'antd/es/tooltip';
import { Helmet } from 'react-helmet/es/Helmet';
import { TabletAndBelow } from '../../utils/responsive';
import Affix from 'antd/es/affix';
import history from '../../utils/history';
import ProductItem from '../../components/ProductItem';
import { useCookies } from 'react-cookie';
import { UpdateRefInfo } from '../App/actions/RefInfo';
import MetaTags from 'react-meta-tags';

function ProductDetail(props) {
    const params = queryString.parse(props.location.search)
    const [cookies, setCookie, removeCookie] = useCookies();

    useEffect(() => {
        if (params && params.ref) {
            async function getRefInfo() {
                axios({
                    method: 'get',
                    url: `${AppConfig.apiUrl}/user/refinfo/${params.ref}`,
                }).then(function (response) {
                    if (response.status === 200) {
                        dispatch(UpdateRefInfo({ref: params.ref, invitee: response.data}))
                        setCookie('dvg_ref', params.ref, {maxAge: 3600})
                        setCookie('dvg_invitee', response.data, {maxAge: 3600})
                    }
                }).catch(function(error){
                    console.log(error);
                    removeCookie('dvg_ref')
                    removeCookie('dvg_invitee')
                })
            }
            getRefInfo();
        }
    }, [params.ref])

    const dispatch = useDispatch()
    const slug = props.match.params.slug;
    const [result, setResult] = useState();
    const [variableChoosen, setVariableChoosen] = useState(null)
    const currentUser = useSelector(state => state.root.currentUser.user);
    const settings = useSelector(state => state.root.settings.options);

    useEffect(() => {
        dispatch(showLoader());
        async function getProducts() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/product/${slug}`,
            }).then(function (response) {
                if (response.status === 200) {
                    setResult(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
                //history.push('/404')
                dispatch(hideLoader());
            })
        }
        getProducts();
    },[slug])

    let gallery = [];
    let prices = [];
    if (result) {
        gallery = JSON.parse(result.product.gallery)
        prices = result.product.prices.sort((a, b) => {
            return a.price - b.price
        })
    }

    const [quantity, setQuantity] = useState(1);
    useEffect(() => {
        if(quantity === 0 || quantity === null) {
            setQuantity(1)
        }
    }, [quantity])

    async function handleAddToCart(Item) {
        if (result && result.product && result.product.prices && result.product.prices.length > 1 && variableChoosen === null) {
            alert('Vui lòng chọn một loại sản phẩm để cho vào giỏ hàng!')
        } else {
            const cart = await localStorage.getItem('dvg_shopping_cart');
            if (cart) {
                const currentCart = JSON.parse(cart);
                if (currentCart) {
                    const index = currentCart.items.findIndex(item => Number(item.productPrice) === Number(Item.productPrice))
                    if (index !== -1) {
                        let currentItem = currentCart.items[index];
                        if ( Number(currentItem.quantity) + Number(Item.quantity) > Number(Item.inStock)) {
                            ErrorMessage({message: `Giỏ hàng của bạn đã có đủ số lượng sản phẩm trong kho - ${currentItem.quantity}`});
                        } else {
                            await checkCart({
                                Item
                            })
                            SuccessMessage({message: `Sản phẩm ${result.product.name} đã được thêm vào giỏ hàng`})
                        }
                    } else {
                        await checkCart({Item})
                        SuccessMessage({message: `Sản phẩm ${result.product.name} đã được thêm vào giỏ hàng`})
                    }
                }
            } else {
                await checkCart({Item})
                SuccessMessage({message: `Sản phẩm ${result.product.name} đã được thêm vào giỏ hàng`})
            }
        }
    }

    async function handleBuyNow(Item) {
        await axios({
            method: 'get',
            url: `${AppConfig.apiUrl}/order/calcPrice`,
            params: {
                productPrice: Item.productPrice,
                quantity: Item.quantity
            }
        }).then(function (response) {
            if (response.status === 200) {
                let subTotal = response.data.price;
                let totalDiscount = 0;
                let discountPercent = 0;
                /*if (currentUser && currentUser.investorStatus === 'TRUE') {
                    totalDiscount = subTotal * settings.investor_discount/100;
                    discountPercent = settings.investor_discount
                } else {*/
                if (settings) {
                    if (currentUser && currentUser.level === 'KhachHang') {
                        if (subTotal <= settings.customer_order_value_lte) {
                            totalDiscount = subTotal * settings.customer_order_value_lte_percent/100;
                            discountPercent = settings.customer_order_value_lte_percent
                        }

                        if (subTotal > settings.customer_order_value_gt) {
                            totalDiscount = subTotal * settings.customer_order_value_gt_percent/100 ;
                            discountPercent = settings.customer_order_value_gt_percent
                        }
                    }

                    if (currentUser && currentUser.level === 'DaiLy' && subTotal >= settings.agency_order_value_gt) {
                        totalDiscount = subTotal * settings.agency_order_value_gt_percent/100;
                        discountPercent = settings.agency_order_value_gt_percent
                    }

                    if (currentUser && currentUser.level === 'DaiLyCap1') {
                        totalDiscount = subTotal * settings.agency_discount_level1/100;
                        discountPercent = settings.agency_discount_level1
                    }

                    if (currentUser && currentUser.level === 'DaiLyCap2') {
                        totalDiscount = subTotal * settings.agency_discount_level2/100;
                        discountPercent = settings.agency_discount_level2
                    }

                    if (currentUser && currentUser.level === 'DaiLyCap3') {
                        totalDiscount = subTotal * settings.agency_discount_level3/100;
                        discountPercent = settings.agency_discount_level3
                    }
                }

                const checkoutParams = {
                    subTotal: subTotal,
                    totalDiscount: totalDiscount,
                    discountPercent: discountPercent,
                    totalAmount: subTotal - totalDiscount,
                    prices: [response.data],
                    orderParams: [Item],
                    checkOutType: 'buynow'
                }

                console.log(checkoutParams);
                history.push('/thanh-toan', checkoutParams)

            }
        }).catch(function(error){
            console.log(error);
            //localStorage.removeItem("dvg_shopping_cart")
            //setTimeout(() => window.location.reload(), 2000);
        })
    }

    const [scroll, setScroll] = useState(false);

    useEffect(() => {
        window.addEventListener("scroll", () => {
            setScroll(window.scrollY > 100);
        });
    }, []);

    function showModalGallery() {
        dispatch(showModal());
        dispatch(setModalWidth(1000))
        dispatch(changeModalTitle(null));
        dispatch(changeModalContent(
            <div className={"mt-5"}>
                <SliderWithThumbnail gallery={gallery} slideThumbShow={5}/>
            </div>
        ))
    }

    console.log(prices);

    return (
        !result ? <Loading /> :
            <>
                <Helmet>
                    <meta charSet="utf-8" />
                    <title>{result.product && result.product.seoTitle ? result.product.seoTitle : result.product && result.product.name}</title>
                    <meta property="og:type" content="website" />
                    <meta name="description" content={result.product && result.product.seoDescription ? result.product.seoDescription : result.product && result.product.description} />
                    <meta property="og:title" content={result.product && result.product.seoTitle ? result.product.seoTitle : result.product && result.product.name} />
                    <meta property="og:description" content={result.product && result.product.seoDescription ? result.product.seoDescription : result.product && result.product.description} />
                    <meta property="og:keyword" content={result.product && result.product.seoKeyword ? result.product.seoKeyword : result.product && result.product.name} />
                    <meta property="og:image" content={result.product && result.product.seoImage ? AppConfig.apiUrl+result.product.seoImage : result.product && result.product.featureImage && AppConfig.apiUrl+result.product.featureImage} />
                </Helmet>
                <TabletAndBelow>
                    <Affix offsetTop={0}>
                        <div className={"bg-white border-b border-gray-100"}>
                            <div className={"h-14 flex items-center justify-between mx-3 text-center"}>
                                <IoArrowBack size={24} onClick={() => history.goBack()}/>
                                <span className={"oneLineText w-1/2 font-medium text-lg"}>{result.product && result.product.name}</span>
                                <IoImages size={24} onClick={showModalGallery}/>
                            </div>
                        </div>
                    </Affix>
                </TabletAndBelow>
                <div className={"max-w-6xl md:mx-3 md:mx-auto py-5"}>
                    {/*<Helmet>
                        <title>{result.product && result.product.name}</title>
                    </Helmet>*/}
                    <div className={"hidden md:block mb-5"}>
                        <Link to={'/'}><IoHome className={"-mt-1"}/> Trang chủ</Link> <IoChevronForward className={"-mt-1"} /> {result.product && result.product.categories.map((item, index) => <><Link to={`/danh-muc/${item.slug}`}>{item.name}</Link> <IoChevronForward className={"-mt-1"} /> </>)} <span className={"text-green-700"}>{result.product.name}</span>
                    </div>
                    <Row gutter={[32, 32]}>
                        <Col xs={24} lg={18} md={18}>
                            <Row gutter={[32, 32]} className={"mb-5"}>
                                <Col xs={24} lg={12} md={12}>
                                    <div className={"px-3 md:px-0"}>
                                        {gallery && gallery.length > 0 ?
                                            <SliderWithThumbnail gallery={gallery}/>
                                            :
                                            <img src={AppConfig.apiUrl+result.product.featureImage} alt={result.product.name} className={"w-full rounded-md"}/>
                                        }
                                    </div>
                                </Col>
                                <Col xs={24} lg={12} md={12}>
                                    <div className={"bg-white p-3 md:p-0 mb-5 md:mb-0"}>
                                        <h1 className={"font-bold text-xl md:text-2xl mb-2"}>{result.product.name}</h1>
                                        {prices && prices.length > 1 ?
                                            <div>
                                                {variableChoosen ?
                                                    <div>
                                                        {result.product.prices.map((item => (
                                                            item.id === variableChoosen &&
                                                            <div>
                                                                <p className={"font-bold text-red-600 text-2xl"}>{formatVND(item.price)}</p>
                                                                <p className={"text-xs italic"}>Kho: {item.instock} {item.unit}</p>
                                                            </div>
                                                        )))}
                                                    </div>
                                                    :
                                                    <span
                                                        className={"font-bold text-red-600 text-2xl"}>{prices && prices[0] && prices[0].price && formatVND(prices[0].price)} - {formatVND(prices[prices.length - 1].price)}</span>
                                                }
                                                <div className={"mt-5"}>
                                                    <div><span className={"font-medium"}>Chọn {result.product.prices[0].unit}</span></div>
                                                    <Space direction={"horizontal"} size={"small"} className={"mt-2"}>
                                                        {prices.map((item) => (
                                                            <Button
                                                                className={`px-2 py-1 rounded-md ${variableChoosen === item.id ? 'border border-green-500 text-white bg-green-500 focus:bg-green-500 active:text-white focus:text-white' : 'bg-gray-100'}`}
                                                                disabled={Number(item.instock) <= 0}
                                                                onClick={() => setVariableChoosen(item.id)}
                                                            >
                                                                {Number(item.instock) <= 0 && <IoRemoveCircleOutline />} {item.name}
                                                            </Button>
                                                        ))}
                                                    </Space>
                                                </div>

                                            </div>
                                            :
                                            <div>
                                                {
                                                    prices && prices.length === 1 && prices[0] &&
                                                        prices[0].instock <= 0 ?
                                                        <div className={"mb-5"}>
                                                            <div className={"bg-red-50 border border-red-100 rounded p-2 rounded mb-5"}>
                                                                <p className={"text-red-600"}>Sản phẩm đang tạm hết hàng, vui lòng chọn sản phẩm khác hoặc chờ cập nhật.</p>
                                                            </div>
                                                            <div>
                                                                <Link to={`/lien-he`} className={"bg-green-600 rounded px-3 py-2 text-white hover:text-white hover:bg-green-700"}>
                                                                    Gửi liên hệ
                                                                </Link>
                                                            </div>
                                                        </div>
                                                        :
                                                        prices.length === 1 && prices[0] && prices[0].instock > 0 &&
                                                        <div>
                                                            <span className={"font-bold text-red-600 text-2xl"}>{prices && prices[0] && prices[0].price && formatVND(prices[0].price)}</span>
                                                            <p className={"text-xs italic"}>Kho: {prices && prices[0] && prices[0].instock} {prices && prices[0] && prices[0].unit}</p>
                                                            <div className={"mt-5"}>
                                                                <div><span className={"font-medium"}>Chọn Số lượng</span></div>
                                                                <div className={"mt-2"}>
                                                                    <div className={"flex items-center mb-2"}>
                                                                        <div>
                                                                            <Button
                                                                                size={"large"}
                                                                                onClick={() => setQuantity(quantity - 1)}
                                                                                className={"rounded-tl-md rounded-bl-md"}
                                                                                disabled={quantity === 1}
                                                                            >
                                                                                <IoRemove/>
                                                                            </Button>
                                                                        </div>
                                                                        <InputNumber
                                                                            max={prices && prices[0] && prices[0].instock}
                                                                            min={1}
                                                                            value={quantity}
                                                                            size={"large"}
                                                                            className={"w-14"}
                                                                            onChange={(value) => setQuantity(value)}
                                                                        />
                                                                        <div>
                                                                            <Button
                                                                                size={"large"}
                                                                                onClick={() => setQuantity(quantity + 1)}
                                                                                className={"rounded-tr-md rounded-br-md"}
                                                                                disabled={quantity === prices && prices[0] && prices[0].instock}
                                                                            >
                                                                                <IoAdd/>
                                                                            </Button>
                                                                        </div>
                                                                    </div>

                                                                    <div className={"hidden md:block"}>
                                                                        <Row gutter={16}>
                                                                            <Col xs={24} lg={14} md={14}>
                                                                                <Button
                                                                                    type={"primary"}
                                                                                    size={"large"}
                                                                                    className={"w-full bg-yellow-500 border-yellow-500 rounded-md hover:bg-green-600 hover:border-green-600 focus:bg-green-600"}
                                                                                    onClick={() => handleAddToCart({
                                                                                        productPrice: prices && prices[0] &&  prices[0].id,
                                                                                        quantity,
                                                                                        inStock: prices && prices[0] && prices[0].instock
                                                                                    })}
                                                                                    icon={<IoCart className={"mr-2"}/>}
                                                                                    disabled={prices && prices[0] && prices[0].instock <= 0}
                                                                                >
                                                                                    Thêm vào giỏ hàng
                                                                                </Button>
                                                                            </Col>

                                                                            <Col xs={24} lg={10} md={10}>
                                                                                <Button
                                                                                    type={"primary"}
                                                                                    size={"large"}
                                                                                    className={"w-full bg-red-500 border-red-500 rounded-md hover:bg-green-600 hover:border-green-600 focus:bg-green-600"}
                                                                                    onClick={() => handleBuyNow({
                                                                                        productPrice: prices && prices[0] && prices[0].id,
                                                                                        quantity,
                                                                                        inStock: prices && prices[0] && prices[0].instock
                                                                                    })}
                                                                                    disabled={prices && prices[0] && prices[0].instock <= 0}
                                                                                >
                                                                                    Mua ngay
                                                                                </Button>
                                                                            </Col>
                                                                        </Row>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                }
                                            </div>
                                        }
                                        {result.product.prices && result.product.prices.length > 0 &&
                                            result.product.prices.map((item, index) => (
                                                variableChoosen === item.id &&
                                                <div className={"mt-5"}>
                                                    <div><span className={"font-medium"}>Chọn Số lượng</span></div>
                                                    <div className={"mt-2"}>
                                                        <div className={"flex items-center mb-2"}>
                                                            <div>
                                                                <Button
                                                                    size={"large"}
                                                                    onClick={() => setQuantity(quantity - 1)}
                                                                    className={"rounded-tl-md rounded-bl-md"}
                                                                    disabled={quantity === 1}
                                                                >
                                                                    <IoRemove/>
                                                                </Button>
                                                            </div>
                                                            <InputNumber
                                                                max={item.instock}
                                                                min={1}
                                                                value={quantity}
                                                                size={"large"}
                                                                className={"w-14"}
                                                                onChange={(value) => setQuantity(value)}
                                                            />
                                                            <div>
                                                                <Button
                                                                    size={"large"}
                                                                    onClick={() => setQuantity(quantity + 1)}
                                                                    className={"rounded-tr-md rounded-br-md"}
                                                                    disabled={quantity === item.instock}
                                                                >
                                                                    <IoAdd/>
                                                                </Button>
                                                            </div>
                                                        </div>

                                                        <div className={"hidden md:block"}>
                                                            <Row gutter={16}>
                                                                <Col xs={24} lg={14} md={14}>
                                                                    <Button
                                                                        type={"primary"}
                                                                        size={"large"}
                                                                        className={"w-full bg-yellow-500 border-yellow-500 rounded-md hover:bg-green-600 hover:border-green-600 focus:bg-green-600"}
                                                                        onClick={() => handleAddToCart({
                                                                            productPrice: item.id,
                                                                            quantity,
                                                                            inStock: item.instock
                                                                        })}
                                                                        icon={<IoCart className={"mr-2"}/>}
                                                                        disabled={item.instock <= 0}
                                                                    >
                                                                        Thêm vào giỏ hàng
                                                                    </Button>
                                                                </Col>

                                                                <Col xs={24} lg={10} md={10}>
                                                                    <Button
                                                                        type={"primary"}
                                                                        size={"large"}
                                                                        className={"w-full bg-red-500 border-red-500 rounded-md hover:bg-green-600 hover:border-green-600 focus:bg-green-600"}
                                                                        onClick={() => handleBuyNow({
                                                                            productPrice: item.id,
                                                                            quantity,
                                                                            inStock: item.instock
                                                                        })}
                                                                        disabled={item.instock <= 0}
                                                                    >
                                                                        Mua ngay
                                                                    </Button>
                                                                </Col>
                                                            </Row>
                                                        </div>

                                                    </div>
                                                </div>
                                            ))
                                        }
                                    </div>
                                    <div className={"bg-white p-3 md:p-0"}>
                                        {result.product.description &&
                                            <div className={"md:mt-3 mb-3 md:mb-0"}>
                                                <div className={"border border-green-600 p-3 rounded-md bg-green-50"}
                                                     dangerouslySetInnerHTML={{ __html: result.product.description }}/>
                                            </div>
                                        }
                                        <div className={"md:mt-3"}>
                                            <span className={"text-gray-500"}>Danh mục: </span>
                                            {result.product.categories && result.product.categories.map((item, index) => (
                                                <Link
                                                    to={`/danh-muc/${item.slug}`}
                                                    className={"mx-1 text-green-700"}
                                                >
                                                    {item.name}
                                                </Link>
                                            ))}
                                        </div>
                                    </div>
                                </Col>
                            </Row>
                            <Tabs>
                                <TabList>
                                    <Tab><span className={"font-bold text-lg text-gray-600 px-3"}>Thông tin sản phẩm</span></Tab>
                                    <Tab><span className={"font-bold text-lg text-gray-600 px-3"}>Đánh giá</span></Tab>
                                </TabList>

                                <TabPanel className={"bg-white p-3 md:p-0 mt-5"}>
                                    <div className={"main-content"} dangerouslySetInnerHTML={{__html: result.product.content}}/>
                                </TabPanel>
                                <TabPanel>

                                </TabPanel>
                            </Tabs>
                            {result && result.relatedProducts && result.relatedProducts.length > 0 &&
                                <div className={"mt-10"}>
                                    <h2 className={"font-bold text-lg mb-3 text-gray-600"}>Sản phẩm liên quan</h2>
                                    <Row gutter={[32, 32]}>
                                        {result.relatedProducts.map((item, index) => (
                                                <Col xs={24} lg={8} md={8}>
                                                    <ProductItem item={item} key={index} />
                                                </Col>
                                            ))
                                        }
                                    </Row>
                                </div>
                            }

                        </Col>
                        <Col xs={24} lg={6} md={6} className={"hidden md:block"}>
                            <ProductCategoryWidget />
                            <ProductHotWidget />
                        </Col>
                    </Row>
                </div>
                {prices && prices.length === 1 &&
                    prices[0] && prices[0].instock > 0 &&
                    <div
                        className={"fixed bottom-14 left-0 w-full bg-white p-2 border-t border-gray-200 md:hidden z-20"}>
                        <Row gutter={16}>
                            <Col xs={14} lg={14} md={14}>
                                <Button
                                    type={"primary"}
                                    size={"large"}
                                    className={"w-full bg-yellow-500 border-yellow-500 rounded-md hover:bg-green-600 hover:border-green-600 focus:bg-green-600"}
                                    onClick={() => handleAddToCart({
                                        productPrice: prices && prices[0] && prices[0].id,
                                        quantity,
                                        inStock: prices && prices[0] && prices[0].instock
                                    })}
                                    icon={<IoCart className={"mr-2"}/>}
                                    disabled={prices && prices[0] && prices[0].instock <= 0}
                                >
                                    Thêm vào giỏ hàng
                                </Button>
                            </Col>

                            <Col xs={10} lg={10} md={10}>
                                <Button
                                    type={"primary"}
                                    size={"large"}
                                    className={"w-full bg-red-500 border-red-500 rounded-md hover:bg-green-600 hover:border-green-600 focus:bg-green-600"}
                                    onClick={() => handleBuyNow({
                                        productPrice: prices && prices[0] && prices[0].id,
                                        quantity,
                                        inStock: prices && prices[0] && prices[0].instock
                                    })}
                                    disabled={prices && prices[0] && prices[0].instock <= 0}
                                >
                                    Mua ngay
                                </Button>
                            </Col>
                        </Row>
                    </div>
                }

                {result.product.prices && result.product.prices.length > 0 &&
                    result.product.prices.map((item, index) => (
                        variableChoosen === item.id &&
                        <div
                            className={"fixed bottom-14 left-0 w-full bg-white p-2 border-t border-gray-200 md:hidden z-20"}>
                            <Row gutter={16}>
                                <Col xs={14} lg={14} md={14}>
                                    <Button
                                        type={"primary"}
                                        size={"large"}
                                        className={"w-full bg-yellow-500 border-yellow-500 rounded-md hover:bg-green-600 hover:border-green-600 focus:bg-green-600"}
                                        onClick={() => handleAddToCart({
                                            productPrice: item.id,
                                            quantity,
                                            inStock: item.instock
                                        })}
                                        icon={<IoCart className={"mr-2"}/>}
                                        disabled={item.instock <= 0}
                                    >
                                        Thêm vào giỏ hàng
                                    </Button>
                                </Col>

                                <Col xs={10} lg={10} md={10}>
                                    <Button
                                        type={"primary"}
                                        size={"large"}
                                        className={"w-full bg-red-500 border-red-500 rounded-md hover:bg-green-600 hover:border-green-600 focus:bg-green-600"}
                                        onClick={() => handleBuyNow({
                                            productPrice: item.id,
                                            quantity,
                                            inStock: item.instock
                                        })}
                                        disabled={item.instock <= 0}
                                    >
                                        Mua ngay
                                    </Button>
                                </Col>
                            </Row>
                        </div>
                    ))
                }
            </>
    );
}

export default ProductDetail;
