import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import Row from 'antd/es/grid/Row';
import Col from 'antd/es/grid/col';
import ProductCategoryWidget from '../HomePage/Widgets/ProductCategory';
import { hideLoader, showLoader } from '../App/actions';
import axios from 'axios';
import { AppConfig } from '../../appConfig';
import Loading from '../../components/Loading';
import ProductItem from '../../components/ProductItem';
import { Alert, Select } from 'antd';
import ProductHotWidget from '../HomePage/Widgets/ProductHot';
import queryString from 'query-string';
import { Helmet } from 'react-helmet/es/Helmet';
import Affix from 'antd/es/affix';
import { IoArrowBack } from 'react-icons/io5';
import history from '../../utils/history';
import { IoHome, IoImages } from 'react-icons/all';
import { Desktop, TabletAndBelow } from '../../utils/responsive';
import Search from 'antd/es/input/Search';

const {Option} = Select

function ProductSearchResult(props) {
    const dispatch = useDispatch();
    const [products, setProducts] = useState();

    const params = queryString.parse(props.location.search)

    let query;
    if (params) {
        query = params.q
    }

    useEffect(() => {
        dispatch(showLoader());
        async function getProducts() {
            axios({
                method: 'get',
                url: `${AppConfig.apiUrl}/product/search`,
                params: {
                    query
                }
            }).then(function (response) {
                if (response.status === 200) {
                    setProducts(response.data)
                    dispatch(hideLoader());
                }
            }).catch(function(error){
                console.log(error);
                history.push('/404')
                dispatch(hideLoader());
            })
        }
        getProducts();
    },[dispatch, query])

    console.log(products);

    function onSearch(value) {
        if (value && typeof value !== 'undefined') {
            history.push(`/tim-san-pham?q=${value}`)
        }
    }

    return (
        !products ? <Loading /> :
            <>
                <TabletAndBelow>
                    <Affix offsetTop={0}>
                        <div className={"bg-white border-b border-gray-100"}>
                            <div className={"h-14 flex items-center justify-between mx-3 text-center"}>
                                <IoArrowBack size={24} onClick={() => history.goBack()}/>
                                <span className={"oneLineText w-1/2 font-medium text-lg"}>Tìm kiếm sản phẩm</span>
                                <IoHome size={20}/>
                            </div>
                        </div>
                    </Affix>
                    <div className={"mx-3 mt-3"}>
                        <Search
                            placeholder="Nhập tên sản phẩm cần tìm"
                            onSearch={onSearch}
                            className={"w-full"}
                            style={{borderRadius: '100%'}}
                        />
                    </div>
                </TabletAndBelow>

                <div className={"max-w-6xl mx-auto py-5"}>
                    <Helmet>
                        <title>Kết quả tìm kiếm cho "{params && params.q}"</title>
                    </Helmet>
                    <Row gutter={32}>
                        <Desktop>
                            <Col xs={24} lg={6} md={6}>
                                <ProductCategoryWidget />
                                <ProductHotWidget />
                            </Col>
                        </Desktop>

                        <Col xs={24} lg={18} md={18} className={"mx-3 md:mx-0"}>
                            <div className={"flex items-center justify-between mb-5"}>
                                <h1 className={"font-bold text-xl text-gray-700"}>Kết quả tìm kiếm cho "{params && params.q}"</h1>
                            </div>
                            <Row gutter={[32, 32]}>
                                {products && products.list.length > 0 ? products.list.map((item, index) => (
                                        <Col xs={24} lg={8} md={8}>
                                            <ProductItem item={item} key={index} />
                                        </Col>
                                    ))
                                    :
                                    <Col xs={24} lg={24} md={24}>
                                        <Alert message="Không tìm thấy sản phẩm hợp với tiêu chí tìm kiếm, vui lòng chọn lại" type="warning" showIcon />
                                    </Col>
                                }
                            </Row>
                        </Col>
                    </Row>

                </div>
            </>

    );
}

export default ProductSearchResult;
