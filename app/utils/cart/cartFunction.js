import { SuccessMessage } from '../../components/Message';

export async function checkCart({ Item }) {
    const checkCart = await localStorage.getItem('dvg_shopping_cart');
    if (checkCart) {
        const currentCart = JSON.parse(checkCart);
        if (currentCart) {
            const index = currentCart.items.findIndex(item => Number(item.productPrice) === Number(Item.productPrice));
            if (index !== -1) {
                let currentItem = currentCart.items[index];
                let newItem = {
                    productPrice: currentItem.productPrice,
                    quantity: Number(currentItem.quantity) + Number(Item.quantity)
                }
                const newList = [
                    ...currentCart.items.slice(0, index),
                    newItem,
                    ...currentCart.items.slice(index + 1),
                ];
                await localStorage.setItem('dvg_shopping_cart', JSON.stringify({
                    items: newList,
                }));
                setTimeout(() => window.location.reload(), 1500);
            } else {
                await localStorage.setItem('dvg_shopping_cart', JSON.stringify({
                    items: currentCart.items.concat(Item),
                }));
                setTimeout(() => window.location.reload(), 1500);
            }
        } else {
            await localStorage.setItem('dvg_shopping_cart', JSON.stringify({
                items: [Item],
            }));
            setTimeout(() => window.location.reload(), 1500);
        }
    } else {
        await localStorage.setItem('dvg_shopping_cart', JSON.stringify({
            items: [Item],
        }));
        setTimeout(() => window.location.reload(), 1500);
    }
}

export async function updateCart({ Item }) {
    const checkCart = await localStorage.getItem('dvg_shopping_cart');
    if (checkCart) {
        const currentCart = JSON.parse(checkCart);
        if (currentCart) {
            const index = currentCart.items.findIndex(item => Number(item.productPrice) === Number(Item.productPrice));
            if (index !== -1) {
                const newList = [
                    ...currentCart.items.slice(0, index),
                    Item,
                    ...currentCart.items.slice(index + 1),
                ];
                await localStorage.setItem('dvg_shopping_cart', JSON.stringify({
                    items: newList,
                }));
                setTimeout(() => window.location.reload(), 1500);
            } else {
                await localStorage.setItem('dvg_shopping_cart', JSON.stringify({
                    items: currentCart.items.concat(Item),
                }));
                setTimeout(() => window.location.reload(), 1500);
            }
        } else {
            await localStorage.setItem('dvg_shopping_cart', JSON.stringify({
                items: [Item],
            }));
            setTimeout(() => window.location.reload(), 1500);
        }
    } else {
        await localStorage.setItem('dvg_shopping_cart', JSON.stringify({
            items: [Item],
        }));
        setTimeout(() => window.location.reload(), 1500);
    }
}

export function deleteItemFromCart(checkCartId) {
    const checkCart = localStorage.getItem('dvg_shopping_cart');
    if (checkCart) {
        const items = JSON.parse(checkCart).items;
        const newList = items.filter((item) => Number(item.productPrice) !== Number(checkCartId));
        localStorage.setItem('dvg_shopping_cart', JSON.stringify({
            items: newList,
        }));
        SuccessMessage({message: `Sản phẩm đã được xoá khỏi giỏ hàng`})
        setTimeout(() => window.location.reload(), 1500);
    }
}
