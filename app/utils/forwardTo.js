import history from "./history";

export default function forwardTo(location) {
    history.push(location);
}