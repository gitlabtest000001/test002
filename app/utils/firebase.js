import firebase from "firebase/app";
import '@firebase/messaging'

const firebaseConfig = {
    apiKey: "AIzaSyDR60YG4lgMq79QLxyL9xpPRRtmrB4Ea3g",
    authDomain: "bookbook-1f7de.firebaseapp.com",
    projectId: "bookbook-1f7de",
    storageBucket: "bookbook-1f7de.appspot.com",
    messagingSenderId: "366233763270",
    appId: "1:366233763270:web:5b1b810ff1489198e486c7",
    measurementId: "G-35PWG0RNPB"
};
// các tham số này là phần config lấy ra được từ phần 2. setting firebase nhé
firebase.initializeApp(firebaseConfig)
export const message = firebase.messaging()
export default firebase
