// Capitalize
import moment from "moment";
import React from "react";
import { PaymentMethod } from '../models/order.model';

export function capitalize(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

export function lowercase(string) {
    return string.toLowerCase();
}

function toCamelCase(str) {
    return str
        .toLowerCase()
        .replace(/[-_]+/g, ' ')
        .replace(/[^\w\s]/g, '')
        .replace(/ (.)/g, function($1) {
            return $1.toUpperCase();
        })
        .replace(/ /g, '');
}

export function objectToCamelCase(origObj) {
    return Object.keys(origObj).reduce(function(newObj, key) {
        let val = origObj[key];
        let newVal = typeof val === 'object' ? objectToCamelCase(val) : val;
        newObj[toCamelCase(key)] = newVal;
        return newObj;
    }, {});
}

export function formatVND(number) {
    return Number.parseFloat(Math.round(number)).toLocaleString(undefined) + 'đ'
}
export function displayNumber(number) {
    return Number.parseFloat(Math.round(number)).toLocaleString(undefined)
}

export function convertPaymentMethod(method) {
    switch (method) {
        case 'COD':
            return 'Thanh toán khi nhận hàng';
        case 'BankTransfer':
            return 'Chuyển khoản';
        case 'CashWallet':
            return 'Ví tiền mặt';
        case 'ProductWallet':
            return 'Ví tiền hàng';
        case 'VNPay':
            return 'VNPay QR';
    }
}

export function convertStatus(status) {
    switch (status) {
        case 'PENDING':
            return 'Chờ duyệt';
        case 'COMPLETED':
            return 'Hoàn thành';
        case 'CANCELED':
            return 'Huỷ';
        case 'ACTIVE':
            return 'Duyệt';
        case 'DEACTIVE':
            return 'Từ chối';
        case 'PAID':
            return 'Đã thanh toán';
    }
}

export function convertTransactionType(type) {
    switch (type) {
        case 'DIRECT_COMMISSION':
            return 'Thưởng trực tiếp';
        case 'BONUS_COMMISSION':
            return 'Thưởng hoa hồng';
        case 'SYSTEM_COMMISSION':
            return 'Thưởng hệ thống';
        case 'WITHDRAW':
            return 'Rút tiền mặt';
        case 'REWARD_WITHDRAW':
            return 'Rút tiền thưởng';
        case 'INVESTMENT':
            return 'Đầu tư';
        case 'PURCHASE':
            return 'Mua hàng';
        case 'REVERTINVESTMENT':
            return 'Hoàn trả Cổ phần';
        case 'EXCHANGESTOCK':
            return 'Thế chấp cổ phần';
    }
}

export function convertUserLevel(type) {
    switch (type) {
        case 'KhachHang':
            return 'Khách hàng';
        case 'DaiLy':
            return 'Đại lý';
        case 'DaiLyCap1':
            return 'Đại lý cấp 1';
        case 'DaiLyCap2':
            return 'Đại lý cấp 2';
        case 'DaiLyCap3':
            return 'Đại lý cấp 3';
    }
}

export function convertUserAffRoles(type) {
    switch (type) {
        case 'GiamDoc':
            return 'Giám đốc';
        case 'NhanVien':
            return 'Nhân viên';
        case 'CongTacVien':
            return 'Cộng tác viên';
    }
}

export function convertUserStatus(type) {
    switch (type) {
        case 'ACTIVE':
            return 'Hoạt động';
        case 'DEACTIVE':
            return 'Khoá';
    }
}

export function convertUserWallet(type) {
    switch (type) {
        case 'ProductWallet':
            return 'Ví tiền hàng';
        case 'CashWallet':
            return 'Ví tiền mặt';
        case 'RewardWallet':
            return 'Ví tiền thưởng';
        case 'StockWallet':
            return 'Ví cổ phần';
    }
}

export const formatDateTime = x =>
    moment(x)
        .format('DD/MM/YYYY - HH:mm:ss');

export const formatDate = x =>
    moment(x)
        .format('DD/MM/YYYY');

export const formatDateUS = x =>
    moment(x).format("YYYY-MM-DD").toString()

export function checkRole(role, permissions) {
    return !!permissions.includes(role);
}

export function truncate(input, length) {
    if (input.length > length) {
        return input.substring(0, length) + '...';
    }
    return input;
}

export function groupeCheckboxify(obj, labelFrom) {
    for (var i = 0; i < obj.length; i++) {
        if (obj[i][labelFrom]) {
            obj[i]['label'] = obj[i][labelFrom];
            obj[i]['value'] = obj[i][labelFrom];
        }
        if (i == obj.length - 1) {
            return obj;
        }
    }
}
