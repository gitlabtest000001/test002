export const banks = [
  {
    code: 'ABBANK',
    bank_name: 'Ngân hàng An Bình (ABBANK)',
  },
  {
    code: 'ACB',
    bank_name: 'Ngân hàng Á Châu (ACB)',
  },
  {
    code: 'Agribank',
    bank_name: 'Ngân hàng NN&PT Nông thôn Việt Nam (Agribank)',
  },
  {
    code: 'ANZVL',
    bank_name: 'Ngân hàng ANZ Việt Nam (ANZ)',
  },
  {
    code: 'BacABank',
    bank_name: 'Ngân hàng Bắc Á (BacABank)',
  },
  {
    code: 'BAOVIET Bank',
    bank_name: 'Ngân hàng Bảo Việt (BAOVIET)',
  },
  {
    code: 'BIDV',
    bank_name: 'Ngân hàng Đầu tư và Phát triển Việt Nam (BIDV)',
  },
  {
    code: 'CB',
    bank_name: 'Ngân hàng Xây dựng (CB)',
  },
  {
    code: 'CIMB',
    bank_name: 'Ngân hàng CIMB Việt Nam (CIMB)',
  },
  {
    code: 'Co-opBank',
    bank_name: 'Ngân hàng Hợp tác xã Việt Nam (Co-op Bank)',
  },
  {
    code: 'DongABank',
    bank_name: 'Ngân hàng Đông Á (DongABank)',
  },
  {
    code: 'Eximbank',
    bank_name: 'Ngân hàng Xuất Nhập Khẩu (EximBank)',
  },
  {
    code: 'GPBank',
    bank_name: 'Ngân hàng Dầu khí toàn cầu (GPBank)',
  },
  {
    code: 'HDBank',
    bank_name: 'Ngân hàng Phát triển TPHồ Chí Minh (HDBank)',
  },
  {
    code: 'HLBVN',
    bank_name: 'Ngân hàng Hong Leong Việt Nam (HLBVN)',
  },
  {
    code: 'HSBC',
    bank_name: 'Ngân hàng HSBC Việt Nam (HSBC)',
  },
  {
    code: 'IVB',
    bank_name: 'Ngân hàng Indovina (IVB)',
  },
  {
    code: 'Kienlongbank',
    bank_name: 'Ngân hàng Kiên Long (KienLong)',
  },
  {
    code: 'LienVietPostBank',
    bank_name: 'Ngân hàng Bưu điện Liên Việt (LVPBank)',
  },
  {
    code: 'MB',
    bank_name: 'Ngân hàng Quân Đội (MB)',
  },
  {
    code: 'MSB',
    bank_name: 'Ngân hàng Hàng Hải (MSB)',
  },
  {
    code: 'Nam A Bank',
    bank_name: 'Ngân hàng Nam Á (NamABank)',
  },
  {
    code: 'NCB',
    bank_name: 'Ngân hàng Quốc dân (NCB)',
  },
  {
    code: 'OCB',
    bank_name: 'Ngân hàng Phương Đông (OCB)',
  },
  {
    code: 'OceanBank',
    bank_name: 'Ngân hàng Đại Dương (OceanBank)',
  },
  {
    code: 'PBVN',
    bank_name: 'Ngân hàng Public Bank Việt Nam (PBVN)',
  },
  {
    code: 'PG Bank',
    bank_name: 'Ngân hàng Xăng dầu Petrolimex (PGBank)',
  },
  {
    code: 'PVcomBank',
    bank_name: 'Ngân hàng Đại Chúng Việt Nam (PVcomBank)',
  },
  {
    code: 'Sacombank',
    bank_name: 'Ngân hàng Sài Gòn Thương Tín (Sacombank)',
  },
  {
    code: 'SAIGONBANK',
    bank_name: 'Ngân hàng Sài Gòn Công Thương (SAIGONBANK)',
  },
  {
    code: 'SCB',
    bank_name: 'Ngân hàng Sài Gòn (SCB)',
  },
  {
    code: 'SCBVL',
    bank_name: 'Ngân hàng Standard Chartered Việt Nam (SCBVL)',
  },
  {
    code: 'SeABank',
    bank_name: 'Ngân hàng Đông Nam Á (SeaBank)',
  },
  {
    code: 'SHB',
    bank_name: 'Ngân hàng Sài Gòn – Hà Nội (SHB)',
  },
  {
    code: 'SHBVN',
    bank_name: 'Ngân hàng Shinhan Việt Nam (SHBVN)',
  },
  {
    code: 'Techcombank',
    bank_name: 'Ngân hàng Kỹ Thương (Techcombank)',
  },
  {
    code: 'TPBank',
    bank_name: 'Ngân hàng Tiên Phong (TPBank)',
  },
  {
    code: 'UOB',
    bank_name: 'Ngân hàng UOB Việt Nam (UOB)',
  },
  {
    code: 'VBSP',
    bank_name: 'Ngân hàng Chính sách xã hội Việt Nam (VBSP)',
  },
  {
    code: 'VDB',
    bank_name: 'Ngân hàng Phát triển Việt Nam (VDB)',
  },
  {
    code: 'VIB',
    bank_name: 'Ngân hàng Quốc Tế (VIB)',
  },
  {
    code: 'Viet Capital Bank',
    bank_name: 'Ngân hàng Bản Việt (Viet Capital Bank)',
  },
  {
    code: 'VietABank',
    bank_name: 'Ngân hàng Việt Á (VietABank)',
  },
  {
    code: 'Vietbank',
    bank_name: 'Ngân hàng Việt Nam Thương Tín (Vietbank)',
  },
  {
    code: 'Vietcombank',
    bank_name: 'Ngân hàng Ngoại Thương Việt Nam (Vietcombank)',
  },
  {
    code: 'Vietinbank',
    bank_name: 'Ngân hàng Công thương Việt Nam (Vietinbank)',
  },
  {
    code: 'VPBank',
    bank_name: 'Ngân hàng Việt Nam Thịnh Vượng (VPBank)',
  },
  {
    code: 'VRB',
    bank_name: 'Ngân hàng Việt – Nga (VRB)',
  },
  {
    code: 'Woori',
    bank_name: 'Ngân hàng Woori Việt Nam (WooriBank)',
  },
];
