export const provinces = [
    {
        "code": "Hà Nội",
        "name": "Hà Nội"
    },
    {
        "code": "TP Hồ Chí Minh",
        "name": "TP Hồ Chí Minh"
    },
    {
        "code": "Bắc Giang",
        "name": "Bắc Giang"
    },
    {
        "code": "Bắc Kạn",
        "name": "Bắc Kạn"
    },
    {
        "code": "Cao Bằng",
        "name": "Cao Bằng"
    },
    {
        "code": "Hà Giang",
        "name": "Hà Giang"
    },
    {
        "code": "Lạng Sơn",
        "name": "Lạng Sơn"
    },
    {
        "code": "Phú Thọ",
        "name": "Phú Thọ"
    },
    {
        "code": "Quảng Ninh",
        "name": "Quảng Ninh"
    },
    {
        "code": "Thái Nguyên",
        "name": "Thái Nguyên"
    },
    {
        "code": "Tuyên Quang",
        "name": "Tuyên Quang"
    },
    {
        "code": "Lào Cai",
        "name": "Lào Cai"
    },
    {
        "code": "Yên Bái",
        "name": "Yên Bái"
    },
    {
        "code": "Điện Biên",
        "name": "Điện Biên"
    },
    {
        "code": "Hòa Bình",
        "name": "Hòa Bình"
    },
    {
        "code": "Lai Châu",
        "name": "Lai Châu"
    },
    {
        "code": "Sơn La",
        "name": "Sơn La"
    },
    {
        "code": "Bắc Ninh",
        "name": "Bắc Ninh"
    },
    {
        "code": "Hà Nam",
        "name": "Hà Nam"
    },
    {
        "code": "Hải Dương",
        "name": "Hải Dương"
    },
    {
        "code": "Hưng Yên",
        "name": "Hưng Yên"
    },
    {
        "code": "Nam Định",
        "name": "Nam Định"
    },
    {
        "code": "Ninh Bình",
        "name": "Ninh Bình"
    },
    {
        "code": "Thái Bình",
        "name": "Thái Bình"
    },
    {
        "code": "Vĩnh Phúc",
        "name": "Vĩnh Phúc"
    },
    {
        "code": "Hải Phòng",
        "name": "Hải Phòng"
    },
    {
        "code": "Hà Tĩnh",
        "name": "Hà Tĩnh"
    },
    {
        "code": "Nghệ An",
        "name": "Nghệ An"
    },
    {
        "code": "Quảng Bình",
        "name": "Quảng Bình"
    },
    {
        "code": "Quảng Trị",
        "name": "Quảng Trị"
    },
    {
        "code": "Thanh Hóa",
        "name": "Thanh Hóa"
    },
    {
        "code": "Thừa Thiên–Huế",
        "name": "Thừa Thiên–Huế"
    },
    {
        "code": "Đắk Lắk",
        "name": "Đắk Lắk"
    },
    {
        "code": "Đắk Nông",
        "name": "Đắk Nông"
    },
    {
        "code": "Gia Lai",
        "name": "Gia Lai"
    },
    {
        "code": "Kon Tum",
        "name": "Kon Tum"
    },
    {
        "code": "Lâm Đồng",
        "name": "Lâm Đồng"
    },
    {
        "code": "Bình Định",
        "name": "Bình Định"
    },
    {
        "code": "Bình Thuận",
        "name": "Bình Thuận"
    },
    {
        "code": "Khánh Hòa",
        "name": "Khánh Hòa"
    },
    {
        "code": "Ninh Thuận",
        "name": "Ninh Thuận"
    },
    {
        "code": "Phú Yên",
        "name": "Phú Yên"
    },
    {
        "code": "Quảng Nam",
        "name": "Quảng Nam"
    },
    {
        "code": "Quảng Ngãi",
        "name": "Quảng Ngãi"
    },
    {
        "code": "Đà Nẵng",
        "name": "Đà Nẵng"
    },
    {
        "code": "Bà Rịa–Vũng Tàu",
        "name": "Bà Rịa–Vũng Tàu"
    },
    {
        "code": "Bình Dương",
        "name": "Bình Dương"
    },
    {
        "code": "Bình Phước",
        "name": "Bình Phước"
    },
    {
        "code": "Đồng Nai",
        "name": "Đồng Nai"
    },
    {
        "code": "Tây Ninh",
        "name": "Tây Ninh"
    },
    {
        "code": "An Giang",
        "name": "An Giang"
    },
    {
        "code": "Bạc Liêu",
        "name": "Bạc Liêu"
    },
    {
        "code": "Bến Tre",
        "name": "Bến Tre"
    },
    {
        "code": "Cà Mau",
        "name": "Cà Mau"
    },
    {
        "code": "Đồng Tháp",
        "name": "Đồng Tháp"
    },
    {
        "code": "Hậu Giang",
        "name": "Hậu Giang"
    },
    {
        "code": "Kiên Giang",
        "name": "Kiên Giang"
    },
    {
        "code": "Long An",
        "name": "Long An"
    },
    {
        "code": "Sóc Trăng",
        "name": "Sóc Trăng"
    },
    {
        "code": "Tiền Giang",
        "name": "Tiền Giang"
    },
    {
        "code": "Trà Vinh",
        "name": "Trà Vinh"
    },
    {
        "code": "Vĩnh Long",
        "name": "Vĩnh Long"
    },
    {
        "code": "Cần Thơ",
        "name": "Cần Thơ"
    }
]