export const PaymentMethod = [
    {code: 'COD', name: 'Tiền mặt khi nhận hàng'},
    {code: 'BankTransfer', name: 'Chuyển khoản ngân hàng'},
    {code: 'CashWallet', name: 'Ví tiền mặt'},
    {code: 'ProductWallet', name: 'Ví tiền hàng'},
    {code: 'VNPay', name: 'VNPay QR'},
]

export const OrderStatus = [
    {
        code: 'PENDING',
        name: 'Chờ duyệt',
    },
    {
        code: 'COMPLETED',
        name: 'Hoàn thành',
    },
    {
        code: 'CANCELED',
        name: 'Huỷ',
    },
]

export const CanceledReasonList = [
    {name: 'Không gọi được khách', code: 'khonggoiduockhach'},
    {name: 'Đơn trùng', code: 'dontrung'},
    {name: 'Hết hàng', code: 'hethang'},
    {name: 'Sai địa chỉ nhận hàng', code: 'saidiachi'},
    {name: 'Khách không mua nữa', code: 'khachkhongmuanua'},
    {name: 'Đặt nhầm sản phẩm', code: 'datnhamsanpham'},
    {name: 'Lý do khác', code: 'lydokhac'},
]
