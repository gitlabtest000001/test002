export const TransactionType = [
    {code: 'DIRECT_COMMISSION', name: 'Thưởng trực tiếp'},
    {code: 'BONUS_COMMISSION', name: 'Thưởng hoa hồng'},
    {code: 'SYSTEM_COMMISSION', name: 'Thưởng hệ thống'},
    {code: 'WITHDRAW', name: 'Rút tiền mặt'},
    {code: 'REWARD_WITHDRAW', name: 'Rút tiền thưởng'},
    {code: 'INVESTMENT', name: 'Đầu tư'},
    {code: 'PURCHASE', name: 'Mua hàng'},
    {code: 'REVERTINVESTMENT', name: 'Hoàn trả Cổ phần'},
    {code: 'EXCHANGESTOCK', name: 'Thế chấp cổ phần'},
]

export const TransactionStatus = [
    {
        code: 'ACTIVE',
        name: 'Duyệt',
    },
    {
        code: 'DEACTIVE',
        name: 'Từ chối',
    },
    {
        code: 'PENDING',
        name: 'Chờ duyệt',
    },
    {
        code: 'PAID',
        name: 'Đã thanh toán',
    },
]
