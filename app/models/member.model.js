import React from 'react';
import { IoBagHandle } from 'react-icons/all';

export const UserLevel = [
    {
        code: 'KhachHang',
        name: 'Khách hàng',
    },
    {
        code: 'DaiLy',
        name: 'Đại lý',
    },
    {
        code: 'DaiLyCap1',
        name: 'Đại lý Cấp 1',
    },
    {
        code: 'DaiLyCap2',
        name: 'Đại lý Cấp 2',
    },
    {
        code: 'DaiLyCap3',
        name: 'Đại lý Cấp 3',
    },
]

export const UserAffRoles = [
    {
        code: 'GiamDoc',
        name: 'Giám đốc kinh doanh',
    },
    {
        code: 'NhanVien',
        name: 'Nhân viên kinh doanh',
    },
    {
        code: 'CongTacVien',
        name: 'Cộng tác viên',
    },
]

export const UserStatus = [
    {
        code: 'ACTIVE',
        name: 'Hoạt động',
    },
    {
        code: 'DEACTIVE',
        name: 'Khoá',
    },
]

export const MemberWallet = [
    {
        code: 'CashWallet',
        name: 'Ví tiền mặt',
    },
    {
        code: 'ProductWallet',
        name: 'Ví tiền hàng',
    },
    {
        code: 'StockWallet',
        name: 'Ví cổ phần',
    },
    {
        code: 'RewardWallet',
        name: 'Ví tiền thưởng',
    },
]


export const EmployeeRoles = [
    {
        code: 'administrator',
        name: 'Quản trị viên',
    },
    {
        code: 'marketer',
        name: 'Nội dung',
    },
    {
        code: 'accountant',
        name: 'Kế toán',
    },
]
